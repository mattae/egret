import { animation, style, animate, trigger, transition, useAnimation, state } from '@angular/animations';
import { __decorate, __param, __metadata } from 'tslib';
import { Inject, Injectable, ɵɵdefineInjectable, ɵɵinject, HostListener, Component, Pipe, Directive, Input, HostBinding, Host, Self, Optional, Attribute, ElementRef, Renderer2, ChangeDetectorRef, NgModule, EventEmitter, Output, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NavigationEnd, RouteConfigLoadStart, ResolveStart, RouteConfigLoadEnd, ResolveEnd, Router, ActivatedRoute, RouterModule } from '@angular/router';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { filter, takeUntil } from 'rxjs/operators';
import { BehaviorSubject, Subject } from 'rxjs';
import { DOCUMENT, CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { SERVER_API_URL_CONFIG, AccountService, MenuService, LoginService, LamisCoreModule } from '@lamis/web-core';
import { MediaObserver, FlexLayoutModule } from '@angular/flex-layout';
import { MediaObserver as MediaObserver$1 } from '@angular/flex-layout/core';
import { MatSidenav, MatSidenavModule, MatListModule, MatTooltipModule, MatOptionModule, MatSelectModule, MatMenuModule, MatSnackBarModule, MatGridListModule, MatToolbarModule, MatIconModule, MatButtonModule, MatRadioModule, MatCheckboxModule, MatCardModule, MatProgressSpinnerModule, MatRippleModule, MatDialogModule } from '@angular/material';
import { CoreModule } from '@alfresco/adf-core';
import { FormsModule } from '@angular/forms';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

const reusable = animation([
    style({
        opacity: "{{opacity}}",
        transform: "scale({{scale}}) translate3d({{x}}, {{y}}, {{z}})"
    }),
    animate("{{duration}} {{delay}} cubic-bezier(0.0, 0.0, 0.2, 1)", style("*"))
], {
    params: {
        duration: "200ms",
        delay: "0ms",
        opacity: "0",
        scale: "1",
        x: "0",
        y: "0",
        z: "0"
    }
});
const egretAnimations = [
    trigger("animate", [transition("void => *", [useAnimation(reusable)])]),
    trigger("fadeInOut", [
        state("0", style({
            opacity: 0,
            display: "none"
        })),
        state("1", style({
            opacity: 1,
            display: "block"
        })),
        transition("0 => 1", animate("300ms")),
        transition("1 => 0", animate("300ms"))
    ])
];

function getQueryParam(prop) {
    const params = {};
    const search = decodeURIComponent(window.location.href.slice(window.location.href.indexOf('?') + 1));
    const definitions = search.split('&');
    definitions.forEach((val, key) => {
        const parts = val.split('=', 2);
        params[parts[0]] = parts[1];
    });
    return (prop && prop in params) ? params[prop] : params;
}

let ThemeService = class ThemeService {
    constructor(document) {
        this.document = document;
        this.egretThemes = [{
                "name": "egret-dark-purple",
                "baseColor": "#9c27b0",
                "isActive": false
            }, {
                "name": "egret-dark-pink",
                "baseColor": "#e91e63",
                "isActive": false
            }, {
                "name": "egret-blue",
                "baseColor": "#03a9f4",
                "isActive": true
            }, {
                "name": "egret-navy",
                "baseColor": "#10174c",
                "isActive": false
            }];
    }
    // Invoked in AppComponent and apply 'activatedTheme' on startup
    applyMatTheme(r, themeName) {
        this.renderer = r;
        this.activatedTheme = this.egretThemes[2];
        // *********** ONLY FOR DEMO **********
        this.setThemeFromQuery();
        // ************************************
        // this.changeTheme(themeName);
        this.renderer.addClass(this.document.body, themeName);
    }
    changeTheme(prevTheme, themeName) {
        this.renderer.removeClass(this.document.body, prevTheme);
        this.renderer.addClass(this.document.body, themeName);
        this.flipActiveFlag(themeName);
    }
    flipActiveFlag(themeName) {
        this.egretThemes.forEach((t) => {
            t.isActive = false;
            if (t.name === themeName) {
                t.isActive = true;
                this.activatedTheme = t;
            }
        });
    }
    // *********** ONLY FOR DEMO **********
    setThemeFromQuery() {
        let themeStr = getQueryParam('theme');
        try {
            this.activatedTheme = JSON.parse(themeStr);
            this.flipActiveFlag(this.activatedTheme.name);
        }
        catch (e) {
        }
    }
};
ThemeService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] }
];
ThemeService = __decorate([
    Injectable(),
    __param(0, Inject(DOCUMENT)),
    __metadata("design:paramtypes", [Object])
], ThemeService);

let LayoutService = class LayoutService {
    constructor(themeService, http, apiUrlConfig, accountService) {
        this.themeService = themeService;
        this.http = http;
        this.apiUrlConfig = apiUrlConfig;
        this.accountService = accountService;
        this.layoutConfSubject = new BehaviorSubject(this.layoutConf);
        this.layoutConf$ = this.layoutConfSubject.asObservable();
        this.username = 'anonymous';
        this.fullWidthRoutes = ['shop'];
        this.setAppLayout();
    }
    setAppLayout() {
        //******** SET YOUR LAYOUT OPTIONS HERE *********
        this.layoutConf = {
            "navigationPos": "side",
            "sidebarStyle": "full",
            "sidebarColor": "white",
            "sidebarCompactToggle": false,
            "dir": "ltr",
            "useBreadcrumb": true,
            "topbarFixed": false,
            "topbarColor": "white",
            "matTheme": "egret-blue",
            "breadcrumb": "simple",
            "perfectScrollbar": true
        };
        // this.getLayoutConfig();
    }
    publishLayoutChange(lc, opt = {}) {
        if (this.layoutConf.matTheme !== lc.matTheme && lc.matTheme) {
            this.themeService.changeTheme(this.layoutConf.matTheme, lc.matTheme);
        }
        this.layoutConf = Object.assign(this.layoutConf, lc);
        this.layoutConfSubject.next(this.layoutConf);
        //this.saveLayoutConfig();
    }
    applyMatTheme(r) {
        this.themeService.applyMatTheme(r, this.layoutConf.matTheme);
    }
    adjustLayout(options = {}) {
        let sidebarStyle;
        this.isMobile = this.isSm();
        this.currentRoute = options.route || this.currentRoute;
        sidebarStyle = this.isMobile ? 'closed' : this.layoutConf.sidebarStyle;
        if (this.currentRoute) {
            this.fullWidthRoutes.forEach(route => {
                if (this.currentRoute.indexOf(route) !== -1) {
                    sidebarStyle = 'closed';
                }
            });
        }
        this.publishLayoutChange({
            isMobile: this.isMobile,
            sidebarStyle: sidebarStyle
        });
    }
    isSm() {
        return window.matchMedia(`(max-width: 959px)`).matches;
    }
    getLayoutConfig() {
        this.accountService.fetch().subscribe((account) => {
            this.username = account.body.login;
            this.http.get(this.apiUrlConfig.SERVER_API_URL + `api/layout-config/${this.username}`, { observe: 'body' }).subscribe((res) => {
                this.layoutConf = res.config || this.layoutConf;
            });
        });
    }
    saveLayoutConfig() {
        this.http.put(this.apiUrlConfig.SERVER_API_URL + `api/layout-config/${this.username}`, {
            config: this.layoutConf
        }, { observe: 'response' })
            .subscribe((config) => {
            this.layoutConf = config.body;
        });
    }
};
LayoutService.ctorParameters = () => [
    { type: ThemeService },
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] },
    { type: AccountService }
];
LayoutService.ngInjectableDef = ɵɵdefineInjectable({ factory: function LayoutService_Factory() { return new LayoutService(ɵɵinject(ThemeService), ɵɵinject(HttpClient), ɵɵinject(SERVER_API_URL_CONFIG), ɵɵinject(AccountService)); }, token: LayoutService, providedIn: "root" });
LayoutService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(2, Inject(SERVER_API_URL_CONFIG)),
    __metadata("design:paramtypes", [ThemeService,
        HttpClient, Object, AccountService])
], LayoutService);

let AdminLayoutComponent = class AdminLayoutComponent {
    constructor(router, translate, themeService, layout) {
        this.router = router;
        this.translate = translate;
        this.themeService = themeService;
        this.layout = layout;
        this.isModuleLoading = false;
        this.scrollConfig = {};
        this.layoutConf = {};
        // Close sidenav after route change in mobile
        this.routerEventSub = router.events.pipe(filter(event => event instanceof NavigationEnd))
            .subscribe((routeChange) => {
            this.layout.adjustLayout({ route: routeChange.url });
        });
        // Translator init
        const browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    }
    ngOnInit() {
        // this.layoutConf = this.layout.layoutConf;
        this.layoutConfSub = this.layout.layoutConf$.subscribe((layoutConf) => {
            this.layoutConf = layoutConf;
        });
        // FOR MODULE LOADER FLAG
        this.moduleLoaderSub = this.router.events.subscribe(event => {
            if (event instanceof RouteConfigLoadStart || event instanceof ResolveStart) {
                this.isModuleLoading = true;
            }
            if (event instanceof RouteConfigLoadEnd || event instanceof ResolveEnd) {
                this.isModuleLoading = false;
            }
        });
    }
    onResize(event) {
        this.layout.adjustLayout(event);
    }
    ngAfterViewInit() {
    }
    scrollToTop(selector) {
        if (document) {
            let element = document.querySelector(selector);
            element.scrollTop = 0;
        }
    }
    ngOnDestroy() {
        if (this.moduleLoaderSub) {
            this.moduleLoaderSub.unsubscribe();
        }
        if (this.layoutConfSub) {
            this.layoutConfSub.unsubscribe();
        }
        if (this.routerEventSub) {
            this.routerEventSub.unsubscribe();
        }
    }
    closeSidebar() {
        this.layout.publishLayoutChange({
            sidebarStyle: 'closed'
        });
    }
    sidebarMouseenter(e) {
        // console.log(this.layoutConf);
        if (this.layoutConf.sidebarStyle === 'compact') {
            this.layout.publishLayoutChange({ sidebarStyle: 'full' }, { transitionClass: true });
        }
    }
    sidebarMouseleave(e) {
        // console.log(this.layoutConf);
        if (this.layoutConf.sidebarStyle === 'full' &&
            this.layoutConf.sidebarCompactToggle) {
            this.layout.publishLayoutChange({ sidebarStyle: 'compact' }, { transitionClass: true });
        }
    }
};
AdminLayoutComponent.ctorParameters = () => [
    { type: Router },
    { type: TranslateService },
    { type: ThemeService },
    { type: LayoutService }
];
__decorate([
    HostListener('window:resize', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], AdminLayoutComponent.prototype, "onResize", null);
AdminLayoutComponent = __decorate([
    Component({
        selector: 'egret-admin-layout',
        template: "<div class=\"app-admin-wrap\" [dir]='layoutConf?.dir'>\r\n    <!-- Header for top navigation layout -->\r\n    <!-- ONLY REQUIRED FOR **TOP** NAVIGATION LAYOUT -->\r\n    <egret-header-top\r\n            *ngIf=\"layoutConf.navigationPos === 'top'\"\r\n            [notificPanel]=\"notificationPanel\">\r\n    </egret-header-top>\r\n    <!-- Main Container -->\r\n    <mat-sidenav-container\r\n            [dir]='layoutConf.dir'\r\n            class=\"app-admin-container app-side-nav-container mat-drawer-transition sidebar-{{layoutConf?.sidebarColor}} topbar-{{layoutConf?.topbarColor}}\"\r\n            [ngClass]=\"{\r\n    'navigation-top': layoutConf.navigationPos === 'top',\r\n    'sidebar-full': layoutConf.sidebarStyle === 'full',\r\n    'sidebar-compact': layoutConf.sidebarStyle === 'compact' && layoutConf.navigationPos === 'side',\r\n    'compact-toggle-active': layoutConf.sidebarCompactToggle,\r\n    'sidebar-compact-big': layoutConf.sidebarStyle === 'compact-big' && layoutConf.navigationPos === 'side',\r\n    'sidebar-opened': layoutConf.sidebarStyle !== 'closed' && layoutConf.navigationPos === 'side',\r\n    'sidebar-closed': layoutConf.sidebarStyle === 'closed',\r\n    'fixed-topbar': layoutConf.topbarFixed && layoutConf.navigationPos === 'side'\r\n  }\">\r\n        <!-- SIDEBAR -->\r\n        <!-- ONLY REQUIRED FOR **SIDE** NAVIGATION LAYOUT -->\r\n        <egret-sidebar-side\r\n                *ngIf=\"layoutConf.navigationPos === 'side'\"\r\n                (mouseenter)=\"sidebarMouseenter($event)\"\r\n                (mouseleave)=\"sidebarMouseleave($event)\">\r\n        </egret-sidebar-side>\r\n\r\n        <!-- Top navigation layout (navigation for mobile screen) -->\r\n        <!-- ONLY REQUIRED FOR **TOP** NAVIGATION MOBILE LAYOUT -->\r\n        <egret-sidebar-top *ngIf=\"layoutConf.navigationPos === 'top' && layoutConf.isMobile\"></egret-sidebar-top>\r\n\r\n        <!-- App content -->\r\n        <div class=\"main-content-wrap\" id=\"main-content-wrap\" [perfectScrollbar]=\"\"\r\n             [disabled]=\"layoutConf.topbarFixed || !layoutConf.perfectScrollbar\" style=\"display: block;\">\r\n            <!-- Header for side navigation layout -->\r\n            <!-- ONLY REQUIRED FOR **SIDE** NAVIGATION LAYOUT -->\r\n            <egret-header-side\r\n                    *ngIf=\"layoutConf.navigationPos === 'side'\"\r\n                    [notificPanel]=\"notificationPanel\">\r\n            </egret-header-side>\r\n\r\n            <div class=\"rightside-content-hold\" id=\"rightside-content-hold\" [perfectScrollbar]=\"scrollConfig\"\r\n                 [disabled]=\"!layoutConf.topbarFixed || !layoutConf.perfectScrollbar\">\r\n                <!-- View Loader -->\r\n                <div class=\"view-loader\" *ngIf=\"isModuleLoading\">\r\n                    <div class=\"spinner\">\r\n                        <div class=\"double-bounce1 mat-bg-accent\"></div>\r\n                        <div class=\"double-bounce2 mat-bg-primary\"></div>\r\n                    </div>\r\n                </div>\r\n                <!-- Breadcrumb -->\r\n                <egret-breadcrumb></egret-breadcrumb>\r\n                <!-- View outlet -->\r\n                <router-outlet></router-outlet>\r\n            </div>\r\n        </div>\r\n        <!-- View overlay for mobile navigation -->\r\n        <div class=\"sidebar-backdrop\"\r\n             [ngClass]=\"{'visible': layoutConf.sidebarStyle !== 'closed' && layoutConf.isMobile}\"\r\n             (click)=\"closeSidebar()\">\r\n        </div>\r\n\r\n        <!-- Notificaation bar -->\r\n        <mat-sidenav #notificationPanel mode=\"over\" class=\"\" position=\"end\">\r\n            <div class=\"nofication-panel\" fxLayout=\"column\">\r\n                <egret-notifications [notificPanel]=\"notificationPanel\"></egret-notifications>\r\n            </div>\r\n        </mat-sidenav>\r\n    </mat-sidenav-container>\r\n</div>\r\n\r\n\r\n<!-- Only for demo purpose -->\r\n<!-- Remove this from your production version -->\r\n"
    }),
    __metadata("design:paramtypes", [Router,
        TranslateService,
        ThemeService,
        LayoutService])
], AdminLayoutComponent);

let AuthLayoutComponent = class AuthLayoutComponent {
    constructor() {
    }
    ngOnInit() {
    }
};
AuthLayoutComponent = __decorate([
    Component({
        selector: 'egret-auth-layout',
        template: "<router-outlet></router-outlet>"
    }),
    __metadata("design:paramtypes", [])
], AuthLayoutComponent);

let GetValueByKeyPipe = class GetValueByKeyPipe {
    transform(value, id, property) {
        const filteredObj = value.find(item => {
            if (item.id !== undefined) {
                return item.id === id;
            }
            return false;
        });
        if (filteredObj) {
            return filteredObj[property];
        }
    }
};
GetValueByKeyPipe = __decorate([
    Pipe({
        name: "getValueByKey",
        pure: false
    })
], GetValueByKeyPipe);

let ExcerptPipe = class ExcerptPipe {
    transform(text, limit = 5) {
        if (text.length <= limit)
            return text;
        return text.substring(0, limit) + '...';
    }
};
ExcerptPipe = __decorate([
    Pipe({ name: 'excerpt' })
], ExcerptPipe);

let RelativeTimePipe = class RelativeTimePipe {
    transform(value) {
        if (!(value instanceof Date))
            value = new Date(value);
        let seconds = Math.floor(((new Date()).getTime() - value.getTime()) / 1000);
        let interval = Math.floor(seconds / 31536000);
        if (interval > 1) {
            return interval + " years ago";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return interval + " months ago";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return interval + " days ago";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return interval + " hours ago";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return interval + " minutes ago";
        }
        return Math.floor(seconds) + " seconds ago";
    }
};
RelativeTimePipe = __decorate([
    Pipe({ name: 'relativeTime' })
], RelativeTimePipe);

let RoutePartsService = class RoutePartsService {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    generateRouteParts(snapshot) {
        let routeParts = [];
        if (snapshot) {
            if (snapshot.firstChild) {
                routeParts = routeParts.concat(this.generateRouteParts(snapshot.firstChild));
            }
            if (snapshot.data['title'] && snapshot.url.length) {
                // console.log(snapshot.data['title'], snapshot.url)
                routeParts.push({
                    title: snapshot.data['title'],
                    breadcrumb: snapshot.data['breadcrumb'],
                    url: snapshot.url[0].path,
                    urlSegments: snapshot.url,
                    params: snapshot.params
                });
            }
        }
        return routeParts;
    }
};
RoutePartsService.ctorParameters = () => [
    { type: Router }
];
RoutePartsService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Router])
], RoutePartsService);

let NavigationService = class NavigationService {
    constructor(menuService) {
        this.menuService = menuService;
        // This title will appear if any icon type item is present in menu.
        this.iconTypeMenuTitle = 'Frequently Accessed';
        this.menuItems$ = menuService.getMenus();
    }
    publishNavigationChange(menuType) {
        /*switch (menuType) {
            case "separator-menu":
                this.menuItems.next(this.separatorMenu);
                break;
            case "icon-menu":
                this.menuItems.next(this.iconMenu);
                break;
            default:
                this.menuItems.next(this.plainMenu);
        }*/
    }
};
NavigationService.ctorParameters = () => [
    { type: MenuService }
];
NavigationService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [MenuService])
], NavigationService);

let MatchMediaService = class MatchMediaService {
    constructor(media) {
        this.media = media;
        this.onMediaChange = new BehaviorSubject('');
        this.activeMediaQuery = '';
        this.init();
    }
    init() {
        this.media
            .asObservable()
            .subscribe((change) => {
            if (this.activeMediaQuery !== change[0].mqAlias) {
                this.activeMediaQuery = change[0].mqAlias;
                this.onMediaChange.next(change[0].mqAlias);
            }
        });
    }
};
MatchMediaService.ctorParameters = () => [
    { type: MediaObserver }
];
MatchMediaService.ngInjectableDef = ɵɵdefineInjectable({ factory: function MatchMediaService_Factory() { return new MatchMediaService(ɵɵinject(MediaObserver$1)); }, token: MatchMediaService, providedIn: "root" });
MatchMediaService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __metadata("design:paramtypes", [MediaObserver])
], MatchMediaService);

let AppDropdownDirective = class AppDropdownDirective {
    constructor(router) {
        this.router = router;
        this.navlinks = [];
    }
    closeOtherLinks(openLink) {
        this.navlinks.forEach((link) => {
            if (link !== openLink) {
                link.open = false;
            }
        });
    }
    addLink(link) {
        this.navlinks.push(link);
    }
    removeGroup(link) {
        const index = this.navlinks.indexOf(link);
        if (index !== -1) {
            this.navlinks.splice(index, 1);
        }
    }
    getUrl() {
        return this.router.url;
    }
    ngOnInit() {
        this._router = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((event) => {
            this.navlinks.forEach((link) => {
                if (link.group) {
                    const routeUrl = this.getUrl();
                    const currentUrl = routeUrl.split('/');
                    if (currentUrl.indexOf(link.group) > 0) {
                        link.open = true;
                        this.closeOtherLinks(link);
                    }
                }
            });
        });
    }
};
AppDropdownDirective.ctorParameters = () => [
    { type: Router }
];
AppDropdownDirective = __decorate([
    Directive({
        selector: '[appDropdown]'
    }),
    __metadata("design:paramtypes", [Router])
], AppDropdownDirective);

let DropdownLinkDirective = class DropdownLinkDirective {
    constructor(nav) {
        this.nav = nav;
    }
    get open() {
        return this._open;
    }
    set open(value) {
        this._open = value;
        if (value) {
            this.nav.closeOtherLinks(this);
        }
    }
    ngOnInit() {
        this.nav.addLink(this);
    }
    ngOnDestroy() {
        this.nav.removeGroup(this);
    }
    toggle() {
        this.open = !this.open;
    }
};
DropdownLinkDirective.ctorParameters = () => [
    { type: AppDropdownDirective, decorators: [{ type: Inject, args: [AppDropdownDirective,] }] }
];
__decorate([
    Input(),
    __metadata("design:type", Object)
], DropdownLinkDirective.prototype, "group", void 0);
__decorate([
    HostBinding('class.open'),
    Input(),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [Boolean])
], DropdownLinkDirective.prototype, "open", null);
DropdownLinkDirective = __decorate([
    Directive({
        selector: '[appDropdownLink]'
    }),
    __param(0, Inject(AppDropdownDirective)),
    __metadata("design:paramtypes", [AppDropdownDirective])
], DropdownLinkDirective);

let DropdownAnchorDirective = class DropdownAnchorDirective {
    constructor(navlink) {
        this.navlink = navlink;
    }
    onClick(e) {
        this.navlink.toggle();
    }
};
DropdownAnchorDirective.ctorParameters = () => [
    { type: DropdownLinkDirective, decorators: [{ type: Inject, args: [DropdownLinkDirective,] }] }
];
__decorate([
    HostListener('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], DropdownAnchorDirective.prototype, "onClick", null);
DropdownAnchorDirective = __decorate([
    Directive({
        selector: '[appDropdownToggle]'
    }),
    __param(0, Inject(DropdownLinkDirective)),
    __metadata("design:paramtypes", [DropdownLinkDirective])
], DropdownAnchorDirective);

let EgretSideNavToggleDirective = class EgretSideNavToggleDirective {
    constructor(media, sideNav) {
        this.media = media;
        this.sideNav = sideNav;
    }
    ngOnInit() {
        this.initSideNav();
    }
    ngOnDestroy() {
        if (this.screenSizeWatcher) {
            this.screenSizeWatcher.unsubscribe();
        }
    }
    updateSidenav() {
        var self = this;
        setTimeout(() => {
            self.sideNav.opened = !self.isMobile;
            self.sideNav.mode = self.isMobile ? 'over' : 'side';
        });
    }
    initSideNav() {
        this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
        // console.log(this.isMobile)
        this.updateSidenav();
        this.screenSizeWatcher = this.media.asObservable().subscribe((change) => {
            this.isMobile = (change[0].mqAlias == 'xs') || (change[0].mqAlias == 'sm');
            this.updateSidenav();
        });
    }
};
EgretSideNavToggleDirective.ctorParameters = () => [
    { type: MediaObserver },
    { type: MatSidenav, decorators: [{ type: Host }, { type: Self }, { type: Optional }] }
];
EgretSideNavToggleDirective = __decorate([
    Directive({
        selector: '[EgretSideNavToggle]'
    }),
    __param(1, Host()), __param(1, Self()), __param(1, Optional()),
    __metadata("design:paramtypes", [MediaObserver,
        MatSidenav])
], EgretSideNavToggleDirective);

let EgretSidenavHelperService = class EgretSidenavHelperService {
    constructor() {
        this.sidenavList = [];
    }
    setSidenav(id, sidenav) {
        this.sidenavList[id] = sidenav;
    }
    getSidenav(id) {
        return this.sidenavList[id];
    }
};
EgretSidenavHelperService.ngInjectableDef = ɵɵdefineInjectable({ factory: function EgretSidenavHelperService_Factory() { return new EgretSidenavHelperService(); }, token: EgretSidenavHelperService, providedIn: "root" });
EgretSidenavHelperService = __decorate([
    Injectable({
        providedIn: "root"
    }),
    __metadata("design:paramtypes", [])
], EgretSidenavHelperService);

let EgretSidenavHelperDirective = class EgretSidenavHelperDirective {
    constructor(matchMediaService, egretSidenavHelperService, matSidenav, media) {
        this.matchMediaService = matchMediaService;
        this.egretSidenavHelperService = egretSidenavHelperService;
        this.matSidenav = matSidenav;
        this.media = media;
        // Set the default value
        this.isOpen = true;
        this.unsubscribeAll = new Subject();
    }
    ngOnInit() {
        this.egretSidenavHelperService.setSidenav(this.id, this.matSidenav);
        if (this.media.isActive(this.isOpenBreakpoint)) {
            this.isOpen = true;
            this.matSidenav.mode = 'side';
            this.matSidenav.toggle(true);
        }
        else {
            this.isOpen = false;
            this.matSidenav.mode = 'over';
            this.matSidenav.toggle(false);
        }
        this.matchMediaService.onMediaChange
            .pipe(takeUntil(this.unsubscribeAll))
            .subscribe(() => {
            if (this.media.isActive(this.isOpenBreakpoint)) {
                this.isOpen = true;
                this.matSidenav.mode = 'side';
                this.matSidenav.toggle(true);
            }
            else {
                this.isOpen = false;
                this.matSidenav.mode = 'over';
                this.matSidenav.toggle(false);
            }
        });
    }
    ngOnDestroy() {
        this.unsubscribeAll.next();
        this.unsubscribeAll.complete();
    }
};
EgretSidenavHelperDirective.ctorParameters = () => [
    { type: MatchMediaService },
    { type: EgretSidenavHelperService },
    { type: MatSidenav },
    { type: MediaObserver }
];
__decorate([
    HostBinding('class.is-open'),
    __metadata("design:type", Boolean)
], EgretSidenavHelperDirective.prototype, "isOpen", void 0);
__decorate([
    Input('egretSidenavHelper'),
    __metadata("design:type", String)
], EgretSidenavHelperDirective.prototype, "id", void 0);
__decorate([
    Input('isOpen'),
    __metadata("design:type", String)
], EgretSidenavHelperDirective.prototype, "isOpenBreakpoint", void 0);
EgretSidenavHelperDirective = __decorate([
    Directive({
        selector: '[egretSidenavHelper]'
    }),
    __metadata("design:paramtypes", [MatchMediaService,
        EgretSidenavHelperService,
        MatSidenav,
        MediaObserver])
], EgretSidenavHelperDirective);
let EgretSidenavTogglerDirective = class EgretSidenavTogglerDirective {
    constructor(egretSidenavHelperService) {
        this.egretSidenavHelperService = egretSidenavHelperService;
    }
    onClick() {
        // console.log(this.egretSidenavHelperService.getSidenav(this.id))
        this.egretSidenavHelperService.getSidenav(this.id).toggle();
    }
};
EgretSidenavTogglerDirective.ctorParameters = () => [
    { type: EgretSidenavHelperService }
];
__decorate([
    Input('egretSidenavToggler'),
    __metadata("design:type", Object)
], EgretSidenavTogglerDirective.prototype, "id", void 0);
__decorate([
    HostListener('click'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], EgretSidenavTogglerDirective.prototype, "onClick", null);
EgretSidenavTogglerDirective = __decorate([
    Directive({
        selector: '[egretSidenavToggler]'
    }),
    __metadata("design:paramtypes", [EgretSidenavHelperService])
], EgretSidenavTogglerDirective);

let FontSizeDirective = class FontSizeDirective {
    constructor(fontSize, el) {
        this.fontSize = fontSize;
        this.el = el;
    }
    ngOnInit() {
        this.el.nativeElement.fontSize = this.fontSize;
    }
};
FontSizeDirective.ctorParameters = () => [
    { type: String, decorators: [{ type: Attribute, args: ['fontSize',] }] },
    { type: ElementRef }
];
FontSizeDirective = __decorate([
    Directive({ selector: '[fontSize]' }),
    __param(0, Attribute('fontSize')),
    __metadata("design:paramtypes", [String, ElementRef])
], FontSizeDirective);

var ScrollToDirective_1;
let ScrollToDirective = ScrollToDirective_1 = class ScrollToDirective {
    constructor(elmID, el) {
        this.elmID = elmID;
        this.el = el;
    }
    ngOnInit() {
    }
    static currentYPosition() {
        // Firefox, Chrome, Opera, Safari
        if (self.pageYOffset)
            return self.pageYOffset;
        // Internet Explorer 6 - standards mode
        if (document.documentElement && document.documentElement.scrollTop)
            return document.documentElement.scrollTop;
        // Internet Explorer 6, 7 and 8
        if (document.body.scrollTop)
            return document.body.scrollTop;
        return 0;
    }
    ;
    static elmYPosition(eID) {
        const elm = document.getElementById(eID);
        let y = elm.offsetTop;
        let node = elm;
        while (node.offsetParent && node.offsetParent != document.body) {
            node = node.offsetParent;
            y += node.offsetTop;
        }
        return y;
    }
    ;
    smoothScroll() {
        let i;
        if (!this.elmID)
            return;
        const startY = ScrollToDirective_1.currentYPosition();
        const stopY = ScrollToDirective_1.elmYPosition(this.elmID);
        const distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY);
            return;
        }
        let speed = Math.round(distance / 50);
        if (speed >= 20)
            speed = 20;
        let step = Math.round(distance / 25);
        let leapY = stopY > startY ? startY + step : startY - step;
        let timer = 0;
        if (stopY > startY) {
            for (i = startY; i < stopY; i += step) {
                setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                leapY += step;
                if (leapY > stopY)
                    leapY = stopY;
                timer++;
            }
            return;
        }
        for (i = startY; i > stopY; i -= step) {
            setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
            leapY -= step;
            if (leapY < stopY)
                leapY = stopY;
            timer++;
        }
        return false;
    }
    ;
};
ScrollToDirective.ctorParameters = () => [
    { type: String, decorators: [{ type: Attribute, args: ['scrollTo',] }] },
    { type: ElementRef }
];
__decorate([
    HostListener('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], ScrollToDirective.prototype, "smoothScroll", null);
ScrollToDirective = ScrollToDirective_1 = __decorate([
    Directive({ selector: '[scrollTo]' }),
    __param(0, Attribute('scrollTo')),
    __metadata("design:paramtypes", [String, ElementRef])
], ScrollToDirective);

let EgretSidebarHelperService = class EgretSidebarHelperService {
    constructor() {
        this.sidebarList = [];
    }
    setSidebar(name, sidebar) {
        this.sidebarList[name] = sidebar;
    }
    getSidebar(name) {
        return this.sidebarList[name];
    }
    removeSidebar(name) {
        if (!this.sidebarList[name]) {
            console.warn(`The sidebar with name '${name}' doesn't exist.`);
        }
        // remove sidebar
        delete this.sidebarList[name];
    }
};
EgretSidebarHelperService.ngInjectableDef = ɵɵdefineInjectable({ factory: function EgretSidebarHelperService_Factory() { return new EgretSidebarHelperService(); }, token: EgretSidebarHelperService, providedIn: "root" });
EgretSidebarHelperService = __decorate([
    Injectable({
        providedIn: "root"
    }),
    __metadata("design:paramtypes", [])
], EgretSidebarHelperService);

let EgretSidebarComponent = class EgretSidebarComponent {
    constructor(matchMediaService, mediaObserver, sidebarHelperService, _renderer, _elementRef, cdr) {
        this.matchMediaService = matchMediaService;
        this.mediaObserver = mediaObserver;
        this.sidebarHelperService = sidebarHelperService;
        this._renderer = _renderer;
        this._elementRef = _elementRef;
        this.cdr = cdr;
        this.backdrop = null;
        this.lockedBreakpoint = "gt-sm";
        this.unsubscribeAll = new Subject();
    }
    ngOnInit() {
        this.sidebarHelperService.setSidebar(this.name, this);
        if (this.mediaObserver.isActive(this.lockedBreakpoint)) {
            this.sidebarLockedOpen = true;
            this.opened = true;
        }
        else {
            this.sidebarLockedOpen = false;
            this.opened = false;
        }
        this.matchMediaService.onMediaChange
            .pipe(takeUntil(this.unsubscribeAll))
            .subscribe(() => {
            // console.log("medua sub");
            if (this.mediaObserver.isActive(this.lockedBreakpoint)) {
                this.sidebarLockedOpen = true;
                this.opened = true;
            }
            else {
                this.sidebarLockedOpen = false;
                this.opened = false;
            }
        });
    }
    open() {
        this.opened = true;
        if (!this.sidebarLockedOpen && !this.backdrop) {
            this.showBackdrop();
        }
    }
    close() {
        this.opened = false;
        this.hideBackdrop();
    }
    toggle() {
        if (this.opened) {
            this.close();
        }
        else {
            this.open();
        }
    }
    showBackdrop() {
        this.backdrop = this._renderer.createElement("div");
        this.backdrop.classList.add("egret-sidebar-overlay");
        this._renderer.appendChild(this._elementRef.nativeElement.parentElement, this.backdrop);
        // Close sidebar onclick
        this.backdrop.addEventListener("click", () => {
            this.close();
        });
        this.cdr.markForCheck();
    }
    hideBackdrop() {
        if (this.backdrop) {
            this.backdrop.parentNode.removeChild(this.backdrop);
            this.backdrop = null;
        }
        this.cdr.markForCheck();
    }
    ngOnDestroy() {
        this.unsubscribeAll.next();
        this.unsubscribeAll.complete();
        this.sidebarHelperService.removeSidebar(this.name);
    }
};
EgretSidebarComponent.ctorParameters = () => [
    { type: MatchMediaService },
    { type: MediaObserver },
    { type: EgretSidebarHelperService },
    { type: Renderer2 },
    { type: ElementRef },
    { type: ChangeDetectorRef }
];
__decorate([
    Input(),
    __metadata("design:type", String)
], EgretSidebarComponent.prototype, "name", void 0);
__decorate([
    Input(),
    HostBinding("class.position-right"),
    __metadata("design:type", Boolean)
], EgretSidebarComponent.prototype, "right", void 0);
__decorate([
    HostBinding("class.open"),
    __metadata("design:type", Boolean)
], EgretSidebarComponent.prototype, "opened", void 0);
__decorate([
    HostBinding("class.sidebar-locked-open"),
    __metadata("design:type", Boolean)
], EgretSidebarComponent.prototype, "sidebarLockedOpen", void 0);
__decorate([
    HostBinding("class.is-over"),
    __metadata("design:type", Boolean)
], EgretSidebarComponent.prototype, "isOver", void 0);
EgretSidebarComponent = __decorate([
    Component({
        selector: 'egret-sidebar',
        template: "<div>\r\n  <ng-content></ng-content>\r\n</div>",
        styles: [""]
    }),
    __metadata("design:paramtypes", [MatchMediaService,
        MediaObserver,
        EgretSidebarHelperService,
        Renderer2,
        ElementRef,
        ChangeDetectorRef])
], EgretSidebarComponent);
let EgretSidebarTogglerDirective = class EgretSidebarTogglerDirective {
    constructor(egretSidebarHelperService) {
        this.egretSidebarHelperService = egretSidebarHelperService;
    }
    onClick() {
        this.egretSidebarHelperService.getSidebar(this.id).toggle();
    }
};
EgretSidebarTogglerDirective.ctorParameters = () => [
    { type: EgretSidebarHelperService }
];
__decorate([
    Input("egretSidebarToggler"),
    __metadata("design:type", Object)
], EgretSidebarTogglerDirective.prototype, "id", void 0);
__decorate([
    HostListener("click"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], EgretSidebarTogglerDirective.prototype, "onClick", null);
EgretSidebarTogglerDirective = __decorate([
    Directive({
        selector: "[egretSidebarToggler]"
    }),
    __metadata("design:paramtypes", [EgretSidebarHelperService])
], EgretSidebarTogglerDirective);

const directives = [
    FontSizeDirective,
    ScrollToDirective,
    AppDropdownDirective,
    DropdownAnchorDirective,
    DropdownLinkDirective,
    EgretSideNavToggleDirective,
    EgretSidenavHelperDirective,
    EgretSidenavTogglerDirective,
    EgretSidebarTogglerDirective
];
let SharedDirectivesModule = class SharedDirectivesModule {
};
SharedDirectivesModule = __decorate([
    NgModule({
        imports: [
            CommonModule
        ],
        declarations: directives,
        exports: directives
    })
], SharedDirectivesModule);

let BreadcrumbComponent = class BreadcrumbComponent {
    // public isEnabled: boolean = true;
    constructor(router, routePartsService, activeRoute, layout) {
        this.router = router;
        this.routePartsService = routePartsService;
        this.activeRoute = activeRoute;
        this.layout = layout;
        this.routerEventSub = this.router.events
            .pipe(filter(event => event instanceof NavigationEnd))
            .subscribe((routeChange) => {
            this.routeParts = this.routePartsService.generateRouteParts(this.activeRoute.snapshot);
            // generate url from parts
            this.routeParts.reverse().map((item, i) => {
                item.breadcrumb = this.parseText(item);
                item.urlSegments.forEach((urlSegment, j) => {
                    if (j === 0)
                        return item.url = `${urlSegment.path}`;
                    item.url += `/${urlSegment.path}`;
                });
                if (i === 0) {
                    return item;
                }
                // prepend previous part to current part
                item.url = `${this.routeParts[i - 1].url}/${item.url}`;
                return item;
            });
        });
    }
    ngOnInit() {
    }
    ngOnDestroy() {
        if (this.routerEventSub) {
            this.routerEventSub.unsubscribe();
        }
    }
    parseText(part) {
        if (!part.breadcrumb) {
            return '';
        }
        part.breadcrumb = part.breadcrumb.replace(/{{([^{}]*)}}/g, function (a, b) {
            var r = part.params[b];
            return typeof r === 'string' ? r : a;
        });
        return part.breadcrumb;
    }
};
BreadcrumbComponent.ctorParameters = () => [
    { type: Router },
    { type: RoutePartsService },
    { type: ActivatedRoute },
    { type: LayoutService }
];
BreadcrumbComponent = __decorate([
    Component({
        selector: 'egret-breadcrumb',
        template: "<ng-container *ngIf=\"routeParts && routeParts.length > 0\">\r\n    <div class=\"breadcrumb-bar\" *ngIf=\"layout.layoutConf.useBreadcrumb && layout.layoutConf.breadcrumb === 'simple'\">\r\n        <ul class=\"breadcrumb\">\r\n            <li *ngFor=\"let part of routeParts\">\r\n                <a routerLink=\"/{{part.url}}\">{{part.breadcrumb | translate}}</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n\r\n    <div class=\"breadcrumb-title\" *ngIf=\"layout.layoutConf.useBreadcrumb && layout.layoutConf.breadcrumb === 'title'\">\r\n        <h1 class=\"bc-title\">{{routeParts[routeParts.length - 1]?.breadcrumb | translate}}</h1>\r\n        <ul class=\"breadcrumb\" *ngIf=\"routeParts.length > 1\">\r\n            <li *ngFor=\"let part of routeParts\">\r\n                <a routerLink=\"/{{part.url}}\" class=\"text-muted\">{{part.breadcrumb | translate}}</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</ng-container>\r\n",
        styles: [""]
    }),
    __metadata("design:paramtypes", [Router,
        RoutePartsService,
        ActivatedRoute,
        LayoutService])
], BreadcrumbComponent);

let HeaderSideComponent = class HeaderSideComponent {
    constructor(themeService, layout, translate, renderer) {
        this.themeService = themeService;
        this.layout = layout;
        this.translate = translate;
        this.renderer = renderer;
        this.availableLangs = [{
                name: 'EN',
                code: 'en',
                flag: 'flag-icon-us'
            }];
        this.currentLang = this.availableLangs[0];
        this.openCustomizer = false;
    }
    ngOnInit() {
        this.egretThemes = this.themeService.egretThemes;
        this.layoutConf = this.layout.layoutConf;
        this.translate.use(this.currentLang.code);
    }
    setLang(lng) {
        this.currentLang = lng;
        this.translate.use(lng.code);
    }
    changeTheme(theme) {
        // this.themeService.changeTheme(theme);
    }
    toggleNotific() {
        this.notificPanel.toggle();
    }
    toggleSidenav() {
        if (this.layoutConf.sidebarStyle === 'closed') {
            return this.layout.publishLayoutChange({
                sidebarStyle: 'full'
            });
        }
        this.layout.publishLayoutChange({
            sidebarStyle: 'closed'
        });
    }
    toggleCollapse() {
        // compact --> full
        if (this.layoutConf.sidebarStyle === 'compact') {
            return this.layout.publishLayoutChange({
                sidebarStyle: 'full',
                sidebarCompactToggle: false
            }, { transitionClass: true });
        }
        // * --> compact
        this.layout.publishLayoutChange({
            sidebarStyle: 'compact',
            sidebarCompactToggle: true
        }, { transitionClass: true });
    }
};
HeaderSideComponent.ctorParameters = () => [
    { type: ThemeService },
    { type: LayoutService },
    { type: TranslateService },
    { type: Renderer2 }
];
__decorate([
    Input(),
    __metadata("design:type", Object)
], HeaderSideComponent.prototype, "notificPanel", void 0);
HeaderSideComponent = __decorate([
    Component({
        selector: 'egret-header-side',
        template: "<mat-toolbar class=\"topbar\">\r\n    <!-- Sidenav toggle button -->\r\n    <button\r\n            *ngIf=\"layoutConf.sidebarStyle !== 'compact'\"\r\n            mat-icon-button\r\n            id=\"sidenavToggle\"\r\n            (click)=\"toggleSidenav()\"\r\n            matTooltip=\"Toggle Hide/Open\"\r\n    >\r\n        <mat-icon>menu</mat-icon>\r\n    </button>\r\n\r\n    <!-- Search form -->\r\n    <!-- <div fxFlex fxHide.lt-sm=\"true\" class=\"search-bar\">\r\n      <form class=\"top-search-form\">\r\n        <mat-icon role=\"img\">search</mat-icon>\r\n        <input autofocus=\"true\" placeholder=\"Search\" type=\"text\" />\r\n      </form>\r\n    </div> -->\r\n\r\n    <span fxFlex></span>\r\n    <!-- Language Switcher -->\r\n   <!-- <button mat-button [matMenuTriggerFor]=\"menu\">\r\n        <span class=\"flag-icon {{currentLang.flag}} mr-05\"></span>\r\n        <span>{{currentLang.name}}</span>\r\n    </button>\r\n    <mat-menu #menu=\"matMenu\">\r\n        <button mat-menu-item *ngFor=\"let lang of availableLangs\" (click)=\"setLang(lang)\">\r\n            <span class=\"flag-icon mr-05 {{lang.flag}}\"></span>\r\n            <span>{{lang.name}}</span>\r\n        </button>\r\n    </mat-menu>\r\n    \\-->\r\n    <!-- Open \"views/search-view/result-page.component\" to understand how to subscribe to input field value -->\r\n    <egret-customizer [isCustomizerOpen]=\"openCustomizer\"\r\n                      (customizerClosed)=\"openCustomizer = false\">\r\n    </egret-customizer>\r\n    <!-- Notification toggle button -->\r\n    <button\r\n            mat-icon-button\r\n            matTooltip=\"Notifications\"\r\n            (click)=\"toggleNotific()\"\r\n            [style.overflow]=\"'visible'\"\r\n            class=\"topbar-button-right\"\r\n    >\r\n        <mat-icon>notifications</mat-icon>\r\n        <span class=\"notification-number mat-bg-warn\">3</span>\r\n    </button>\r\n    <!-- Top left user menu -->\r\n    <button\r\n            mat-icon-button\r\n            [matMenuTriggerFor]=\"accountMenu\"\r\n            class=\"topbar-button-right img-button\">\r\n        <img src=\"assets/images/face-7.jpg\" alt=\"\"/>\r\n    </button>\r\n\r\n    <mat-menu #accountMenu=\"matMenu\">\r\n        <button mat-menu-item [routerLink]=\"['/profile/overview']\">\r\n            <mat-icon>account_box</mat-icon>\r\n            <span>Profile</span>\r\n        </button>\r\n        <button mat-menu-item (click)=\"openCustomizer = true\">\r\n            <mat-icon>settings</mat-icon>\r\n            <span>Layout Settings</span>\r\n        </button>\r\n        <button mat-menu-item>\r\n            <mat-icon>notifications_off</mat-icon>\r\n            <span>Disable alerts</span>\r\n        </button>\r\n        <button mat-menu-item [routerLink]=\"['/sessions/signin']\">\r\n            <mat-icon>exit_to_app</mat-icon>\r\n            <span>Sign out</span>\r\n        </button>\r\n    </mat-menu>\r\n</mat-toolbar>\r\n"
    }),
    __metadata("design:paramtypes", [ThemeService,
        LayoutService,
        TranslateService,
        Renderer2])
], HeaderSideComponent);

let HeaderTopComponent = class HeaderTopComponent {
    constructor(layout, navService, themeService, translate, renderer, loginService, accountService) {
        this.layout = layout;
        this.navService = navService;
        this.themeService = themeService;
        this.translate = translate;
        this.renderer = renderer;
        this.loginService = loginService;
        this.accountService = accountService;
        this.egretThemes = [];
        this.openCustomizer = false;
        this.currentLang = 'en';
        this.availableLangs = [{
                name: 'English',
                code: 'en',
            }, {
                name: 'Spanish',
                code: 'es',
            }];
    }
    ngOnInit() {
        this.layoutConf = this.layout.layoutConf;
        this.egretThemes = this.themeService.egretThemes;
        this.menuItemSub = this.navService.menuItems$
            .subscribe(res => {
            res = res.filter(item => item.type !== 'icon' && item.type !== 'separator');
            let limit = 4;
            let mainItems = res.slice(0, limit);
            if (res.length <= limit) {
                return this.menuItems = mainItems;
            }
            let subItems = res.slice(limit, res.length - 1);
            mainItems.push({
                name: 'More',
                type: 'dropDown',
                tooltip: 'More',
                icon: 'more_horiz',
                sub: subItems
            });
            this.menuItems = mainItems;
        });
    }
    ngOnDestroy() {
        this.menuItemSub.unsubscribe();
    }
    setLang() {
        this.translate.use(this.currentLang);
    }
    changeTheme(theme) {
        this.layout.publishLayoutChange({ matTheme: theme.name });
    }
    toggleNotific() {
        this.notificPanel.toggle();
    }
    toggleSidenav() {
        if (this.layoutConf.sidebarStyle === 'closed') {
            return this.layout.publishLayoutChange({
                sidebarStyle: 'full'
            });
        }
        this.layout.publishLayoutChange({
            sidebarStyle: 'closed'
        });
    }
    logout() {
        this.loginService.logout();
    }
};
HeaderTopComponent.ctorParameters = () => [
    { type: LayoutService },
    { type: NavigationService },
    { type: ThemeService },
    { type: TranslateService },
    { type: Renderer2 },
    { type: LoginService },
    { type: AccountService }
];
__decorate([
    Input(),
    __metadata("design:type", Object)
], HeaderTopComponent.prototype, "notificPanel", void 0);
HeaderTopComponent = __decorate([
    Component({
        selector: 'egret-header-top',
        template: "<div class=\"header-topnav mat-elevation-z2\">\r\n    <div class=\"container\">\r\n        <div class=\"topnav\">\r\n            <!-- App Logo -->\r\n            <div class=\"topbar-branding\">\r\n                <img src=\"assets/images/logo.png\" alt=\"\" class=\"app-logo\">\r\n            </div>\r\n\r\n            <ul class=\"menu\" *ngIf=\"!layoutConf.isMobile\">\r\n                <li *ngFor=\"let item of menuItems; let i = index;\">\r\n                    <ng-container *jhiHasAnyAuthority=\"item.authorities\">\r\n                        <div *ngIf=\"item.type !== 'separator'\" routerLinkActive=\"open\">\r\n                            <a matRipple routerLink=\"/{{item.state}}\" *ngIf=\"item.type === 'link'\">\r\n                                <mat-icon>{{item.icon}}</mat-icon>\r\n                                {{item.name | translate}}\r\n                            </a>\r\n                            <div *ngIf=\"item.type === 'dropDown'\">\r\n                                <label matRipple for=\"drop-{{i}}\" class=\"toggle\">\r\n                                    <mat-icon>{{item.icon}}</mat-icon>\r\n                                    {{item.name | translate}}</label>\r\n                                <a matRipple>\r\n                                    <mat-icon>{{item.icon}}</mat-icon>\r\n                                    {{item.name | translate}}</a>\r\n                                <input type=\"checkbox\" id=\"drop-{{i}}\"/>\r\n                                <ul>\r\n                                    <li *ngFor=\"let itemLvL2 of item.subs; let j = index;\" routerLinkActive=\"open\">\r\n                                        <a matRipple\r\n                                           routerLink=\"{{item.state ? '/'+item.state : ''}}/{{itemLvL2.state}}\"\r\n                                           *ngIf=\"itemLvL2.type !== 'dropDown'\">\r\n                                            <mat-icon *ngIf=\"itemLvL2.icon\">{{itemLvL2.icon}}</mat-icon>\r\n                                            {{itemLvL2.name | translate}}\r\n                                        </a>\r\n\r\n                                        <div *ngIf=\"itemLvL2.type === 'dropDown'\">\r\n                                            <label matRipple for=\"drop-{{i}}{{j}}\"\r\n                                                   class=\"toggle\">{{itemLvL2.name | translate}}</label>\r\n                                            <a matRipple>\r\n                                                <mat-icon *ngIf=\"itemLvL2.icon\">{{itemLvL2.icon}}</mat-icon>\r\n                                                {{itemLvL2.name | translate}}</a>\r\n                                            <input type=\"checkbox\" id=\"drop-{{i}}{{j}}\"/>\r\n                                            <!-- Level 3 -->\r\n                                            <ul>\r\n                                                <li *ngFor=\"let itemLvL3 of itemLvL2.subs\" routerLinkActive=\"open\">\r\n                                                    <a matRipple\r\n                                                       routerLink=\"{{item.state ? '/'+item.state : ''}}{{itemLvL2.state ? '/'+itemLvL2.state : ''}}/{{itemLvL3.state}}\">\r\n                                                        <mat-icon *ngIf=\"itemLvL3.icon\">{{itemLvL3.icon}}</mat-icon>\r\n                                                        {{itemLvL3.name | translate}}\r\n                                                    </a>\r\n                                                </li>\r\n                                            </ul>\r\n                                        </div>\r\n                                    </li>\r\n                                </ul>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </ng-container>\r\n                </li>\r\n            </ul>\r\n            <egret-customizer [isCustomizerOpen]=\"openCustomizer\"\r\n                              (customizerClosed)=\"openCustomizer = false\"></egret-customizer>\r\n            <span fxFlex></span>\r\n            <!-- End Navigation -->\r\n\r\n            <!-- Language Switcher -->\r\n            <mat-select\r\n                    *ngIf=\"!layoutConf.isMobile\"\r\n                    placeholder=\"\"\r\n                    id=\"langToggle\"\r\n                    [style.width]=\"'auto'\"\r\n                    name=\"currentLang\"\r\n                    [(ngModel)]=\"currentLang\"\r\n                    (selectionChange)=\"setLang()\"\r\n                    class=\"topbar-button-right\">\r\n                <mat-option\r\n                        *ngFor=\"let lang of availableLangs\"\r\n                        [value]=\"lang.code\" ngDefaultControl>{{ lang.name }}</mat-option>\r\n            </mat-select>\r\n            <!-- Theme Switcher -->\r\n            <button\r\n                    mat-icon-button\r\n                    id=\"schemeToggle\"\r\n                    [style.overflow]=\"'visible'\"\r\n                    matTooltip=\"Color Schemes\"\r\n                    [matMenuTriggerFor]=\"themeMenu\"\r\n                    class=\"topbar-button-right\">\r\n                <mat-icon>format_color_fill</mat-icon>\r\n            </button>\r\n            <mat-menu #themeMenu=\"matMenu\">\r\n                <mat-grid-list\r\n                        class=\"theme-list\"\r\n                        cols=\"2\"\r\n                        rowHeight=\"48px\">\r\n                    <mat-grid-tile\r\n                            *ngFor=\"let theme of egretThemes\"\r\n                            (click)=\"changeTheme(theme)\">\r\n                        <div mat-menu-item [title]=\"theme.name\">\r\n                            <div [style.background]=\"theme.baseColor\" class=\"egret-swatch\"></div>\r\n                            <mat-icon class=\"active-icon\" *ngIf=\"theme.isActive\">check</mat-icon>\r\n                        </div>\r\n                    </mat-grid-tile>\r\n                </mat-grid-list>\r\n            </mat-menu>\r\n            <!-- Notification toggle button -->\r\n            <button\r\n                    mat-icon-button\r\n                    matTooltip=\"Notifications\"\r\n                    (click)=\"toggleNotific()\"\r\n                    [style.overflow]=\"'visible'\"\r\n                    class=\"topbar-button-right\">\r\n                <mat-icon>notifications</mat-icon>\r\n                <span class=\"notification-number mat-bg-warn\">3</span>\r\n            </button>\r\n            <!-- Top left user menu -->\r\n            <button mat-icon-button [matMenuTriggerFor]=\"accountMenu\" class=\"topbar-button-right mr-1 img-button\">\r\n                <img src=\"assets/images/face-7.jpg\" alt=\"\">\r\n            </button>\r\n            <mat-menu #accountMenu=\"matMenu\">\r\n                <button mat-menu-item [routerLink]=\"['/profile/overview']\">\r\n                    <mat-icon>account_box</mat-icon>\r\n                    <span>Profile</span>\r\n                </button>\r\n                <button mat-menu-item (click)=\"openCustomizer = true\">\r\n                    <mat-icon>settings</mat-icon>\r\n                    <span>Layout Settings</span>\r\n                </button>\r\n                <button mat-menu-item>\r\n                    <mat-icon>notifications_off</mat-icon>\r\n                    <span>Disable alerts</span>\r\n                </button>\r\n                <button mat-menu-item\r\n                        (click)=\"logout()\">\r\n                    <mat-icon>exit_to_app</mat-icon>\r\n                    <span>Sign out</span>\r\n                </button>\r\n            </mat-menu>\r\n            <!-- Mobile screen menu toggle -->\r\n            <button\r\n                    mat-icon-button\r\n                    class=\"mr-1\"\r\n                    (click)=\"toggleSidenav()\"\r\n                    *ngIf=\"layoutConf.isMobile\">\r\n                <mat-icon>menu</mat-icon>\r\n            </button>\r\n        </div>\r\n    </div>\r\n</div>\r\n"
    }),
    __metadata("design:paramtypes", [LayoutService,
        NavigationService,
        ThemeService,
        TranslateService,
        Renderer2,
        LoginService,
        AccountService])
], HeaderTopComponent);

let NotificationsComponent = class NotificationsComponent {
    constructor(router) {
        this.router = router;
        // Dummy notifications
        this.notifications = [{
                message: 'New contact added',
                icon: 'assignment_ind',
                time: '1 min ago',
                route: '/inbox',
                color: 'primary'
            }, {
                message: 'New message',
                icon: 'chat',
                time: '4 min ago',
                route: '/chat',
                color: 'accent'
            }, {
                message: 'Server rebooted',
                icon: 'settings_backup_restore',
                time: '12 min ago',
                route: '/charts',
                color: 'warn'
            }];
    }
    ngOnInit() {
        this.router.events.subscribe((routeChange) => {
            if (routeChange instanceof NavigationEnd) {
                this.notificPanel.close();
            }
        });
    }
    clearAll(e) {
        e.preventDefault();
        this.notifications = [];
    }
};
NotificationsComponent.ctorParameters = () => [
    { type: Router }
];
__decorate([
    Input(),
    __metadata("design:type", Object)
], NotificationsComponent.prototype, "notificPanel", void 0);
NotificationsComponent = __decorate([
    Component({
        selector: 'egret-notifications',
        template: "<div class=\"text-center mat-bg-primary pt-1 pb-1\">\r\n    <h6 class=\"m-0\">Notifications</h6>\r\n</div>\r\n<mat-nav-list class=\"notification-list\" role=\"list\">\r\n    <!-- Notification item -->\r\n    <mat-list-item *ngFor=\"let n of notifications\" class=\"notific-item\" role=\"listitem\" routerLinkActive=\"open\">\r\n        <mat-icon [color]=\"n.color\" class=\"notific-icon mr-1\">{{n.icon}}</mat-icon>\r\n        <a [routerLink]=\"[n.route || '/dashboard']\">\r\n            <div class=\"mat-list-text\">\r\n                <h4 class=\"message\">{{n.message}}</h4>\r\n                <small class=\"time text-muted\">{{n.time}}</small>\r\n            </div>\r\n        </a>\r\n    </mat-list-item>\r\n</mat-nav-list>\r\n<div class=\"text-center mt-1\" *ngIf=\"notifications.length\">\r\n    <small><a href=\"#\" (click)=\"clearAll($event)\">Clear all notifications</a></small>\r\n</div>\r\n"
    }),
    __metadata("design:paramtypes", [Router])
], NotificationsComponent);

let SidebarSideComponent = class SidebarSideComponent {
    constructor(navService, themeService, layout, loginService, accountService) {
        this.navService = navService;
        this.themeService = themeService;
        this.layout = layout;
        this.loginService = loginService;
        this.accountService = accountService;
    }
    ngOnInit() {
        this.iconTypeMenuTitle = this.navService.iconTypeMenuTitle;
        this.menuItemsSub = this.navService.menuItems$.subscribe(menuItem => {
            this.menuItems = menuItem;
            //Checks item list has any icon type.
            this.hasIconTypeMenuItem = !!this.menuItems.filter(item => item.type === "icon").length;
        });
        this.layoutConf = this.layout.layoutConf;
        this.accountService.fetch().subscribe((res) => this.account = res.body);
    }
    ngAfterViewInit() {
    }
    ngOnDestroy() {
        if (this.menuItemsSub) {
            this.menuItemsSub.unsubscribe();
        }
    }
    toggleCollapse() {
        if (this.layoutConf.sidebarCompactToggle) {
            this.layout.publishLayoutChange({
                sidebarCompactToggle: false
            });
        }
        else {
            this.layout.publishLayoutChange({
                // sidebarStyle: "compact",
                sidebarCompactToggle: true
            });
        }
    }
    logout() {
        this.loginService.logout();
    }
};
SidebarSideComponent.ctorParameters = () => [
    { type: NavigationService },
    { type: ThemeService },
    { type: LayoutService },
    { type: LoginService },
    { type: AccountService }
];
SidebarSideComponent = __decorate([
    Component({
        selector: 'egret-sidebar-side',
        template: "<div class=\"sidebar-panel\">\r\n    <div id=\"scroll-area\" [perfectScrollbar] class=\"navigation-hold\" fxLayout=\"column\">\r\n        <div class=\"sidebar-hold\">\r\n\r\n            <!-- App Logo -->\r\n            <div class=\"branding\">\r\n                <img src=\"assets/images/logo.png\" alt=\"\" class=\"app-logo\">\r\n                <span class=\"app-logo-text\">LAMIS</span>\r\n\r\n                <span style=\"margin: auto\" *ngIf=\"layoutConf.sidebarStyle !== 'compact'\"></span>\r\n                <div\r\n                        class=\"sidebar-compact-switch\"\r\n                        [ngClass]=\"{active: layoutConf.sidebarCompactToggle}\"\r\n                        (click)=\"toggleCollapse()\"\r\n                        *ngIf=\"layoutConf.sidebarStyle !== 'compact'\"><span></span></div>\r\n            </div>\r\n\r\n            <!-- Sidebar user -->\r\n            <div class=\"app-user\">\r\n                <div class=\"app-user-photo\">\r\n                    <img src=\"assets/images/face-7.jpg\" class=\"mat-elevation-z1\" alt=\"\">\r\n                </div>\r\n                <span class=\"app-user-name mb-05\">\r\n                <mat-icon class=\"icon-xs text-muted\">lock</mat-icon>\r\n                {{account?.firstName}} {{account?.lastName}}\r\n                </span>\r\n                <!-- Small buttons -->\r\n                <div class=\"app-user-controls\">\r\n                    <button\r\n                            class=\"text-muted\"\r\n                            mat-icon-button\r\n                            mat-xs-button\r\n                            [matMenuTriggerFor]=\"appUserMenu\">\r\n                        <mat-icon>settings</mat-icon>\r\n                    </button>\r\n                    <button\r\n                            class=\"text-muted\"\r\n                            mat-icon-button\r\n                            mat-xs-button\r\n                            matTooltip=\"Inbox\"\r\n                            routerLink=\"/inbox\">\r\n                        <mat-icon>email</mat-icon>\r\n                    </button>\r\n                    <button\r\n                            class=\"text-muted\"\r\n                            mat-icon-button\r\n                            mat-xs-button\r\n                            (click)=\"logout()\"\r\n                            matTooltip=\"Sign Out\">\r\n                        <mat-icon>exit_to_app</mat-icon>\r\n                    </button>\r\n                    <mat-menu #appUserMenu=\"matMenu\">\r\n                        <button mat-menu-item routerLink=\"/profile/overview\">\r\n                            <mat-icon>account_box</mat-icon>\r\n                            <span>Profile</span>\r\n                        </button>\r\n                        <button mat-menu-item routerLink=\"/profile/settings\">\r\n                            <mat-icon>settings</mat-icon>\r\n                            <span>Account Settings</span>\r\n                        </button>\r\n                        <button mat-menu-item\r\n                                (click)=\"logout()\">\r\n                            <mat-icon>exit_to_app</mat-icon>\r\n                            <span>Sign out</span>\r\n                        </button>\r\n                    </mat-menu>\r\n                </div>\r\n            </div>\r\n            <!-- Navigation -->\r\n            <egret-sidenav [items]=\"menuItems\" [hasIconMenu]=\"hasIconTypeMenuItem\"\r\n                           [iconMenuTitle]=\"iconTypeMenuTitle\"></egret-sidenav>\r\n        </div>\r\n    </div>\r\n</div>\r\n"
    }),
    __metadata("design:paramtypes", [NavigationService,
        ThemeService,
        LayoutService,
        LoginService,
        AccountService])
], SidebarSideComponent);

let SidebarTopComponent = class SidebarTopComponent {
    constructor(navService) {
        this.navService = navService;
    }
    ngOnInit() {
        this.menuItemsSub = this.navService.menuItems$.subscribe(menuItem => {
            this.menuItems = menuItem.filter(item => item.type !== 'icon' && item.type !== 'separator');
        });
    }
    ngAfterViewInit() {
        // setTimeout(() => {
        //   this.sidebarPS = new PerfectScrollbar('#sidebar-top-scroll-area', {
        //     suppressScrollX: true
        //   })
        // })
    }
    ngOnDestroy() {
        // if(this.sidebarPS) {
        //   this.sidebarPS.destroy();
        // }
        if (this.menuItemsSub) {
            this.menuItemsSub.unsubscribe();
        }
    }
};
SidebarTopComponent.ctorParameters = () => [
    { type: NavigationService }
];
SidebarTopComponent = __decorate([
    Component({
        selector: 'egret-sidebar-top',
        template: "<div class=\"sidebar-panel\">\r\n    <div id=\"sidebar-top-scroll-area\" [perfectScrollbar] class=\"navigation-hold\" fxLayout=\"column\">\r\n        <egret-sidenav [items]=\"menuItems\"></egret-sidenav>\r\n    </div>\r\n</div>\r\n"
    }),
    __metadata("design:paramtypes", [NavigationService])
], SidebarTopComponent);

let SidenavComponent = class SidenavComponent {
    constructor() {
        this.menuItems = [];
    }
    ngOnInit() {
    }
};
__decorate([
    Input('items'),
    __metadata("design:type", Array)
], SidenavComponent.prototype, "menuItems", void 0);
__decorate([
    Input('hasIconMenu'),
    __metadata("design:type", Boolean)
], SidenavComponent.prototype, "hasIconTypeMenuItem", void 0);
__decorate([
    Input('iconMenuTitle'),
    __metadata("design:type", String)
], SidenavComponent.prototype, "iconTypeMenuTitle", void 0);
SidenavComponent = __decorate([
    Component({
        selector: 'egret-sidenav',
        template: "<div class=\"sidenav-hold\">\r\n    <div class=\"icon-menu mb-1\" *ngIf=\"hasIconTypeMenuItem\">\r\n        <!-- Icon menu separator -->\r\n        <div class=\"mb-1 nav-item-sep\">\r\n            <mat-divider [ngStyle]=\"{margin: '0 -24px'}\"></mat-divider>\r\n            <span class=\"text-muted icon-menu-title\">{{iconTypeMenuTitle}}</span>\r\n        </div>\r\n        <!-- Icon menu items -->\r\n        <div class=\"icon-menu-item\" *ngFor=\"let item of menuItems\">\r\n            <ng-container *jhiHasAnyAuthority=\"item.authorities\">\r\n                <button *ngIf=\"!item.disabled && item.type === 'icon'\" mat-raised-button [matTooltip]=\"item.tooltip\"\r\n                        routerLink=\"/{{item.state}}\"\r\n                        routerLinkActive=\"mat-bg-primary\">\r\n                    <mat-icon>{{item.icon}}</mat-icon>\r\n                </button>\r\n            </ng-container>\r\n        </div>\r\n    </div>\r\n\r\n    <ul appDropdown class=\"sidenav\">\r\n        <li *ngFor=\"let item of menuItems\" appDropdownLink routerLinkActive=\"open\">\r\n            <ng-container *jhiHasAnyAuthority=\"item.authorities\">\r\n                <div class=\"nav-item-sep\" *ngIf=\"item.type === 'separator'\">\r\n                    <mat-divider></mat-divider>\r\n                    <span class=\"text-muted\">{{item.name | translate}}</span>\r\n                </div>\r\n                <div *ngIf=\"!item.disabled && item.type !== 'separator' && item.type !== 'icon'\" class=\"lvl1\">\r\n                    <a routerLink=\"/{{item.state}}\" appDropdownToggle matRipple *ngIf=\"item.type === 'link'\">\r\n                        <mat-icon>{{item.icon}}</mat-icon>\r\n                        <span class=\"item-name lvl1\">{{item.name | translate}}</span>\r\n                        <span fxFlex></span>\r\n                        <span class=\"menuitem-badge mat-bg-{{ badge.color }}\" [ngStyle]=\"{background: badge.color}\"\r\n                              *ngFor=\"let badge of item.badges\">{{ badge.value }}</span>\r\n                    </a>\r\n                    <a [href]=\"item.state\" appDropdownToggle matRipple *ngIf=\"item.type === 'extLink'\" target=\"_blank\">\r\n                        <mat-icon>{{item.icon}}</mat-icon>\r\n                        <span class=\"item-name lvl1\">{{item.name | translate}}</span>\r\n                        <span fxFlex></span>\r\n                        <span class=\"menuitem-badge mat-bg-{{ badge.color }}\" [ngStyle]=\"{background: badge.color}\"\r\n                              *ngFor=\"let badge of item.badges\">{{ badge.value }}</span>\r\n                    </a>\r\n\r\n                    <!-- DropDown -->\r\n                    <a *ngIf=\"item.type === 'dropDown'\" appDropdownToggle matRipple>\r\n                        <mat-icon>{{item.icon}}</mat-icon>\r\n                        <span class=\"item-name lvl1\">{{item.name | translate}}</span>\r\n                        <span fxFlex></span>\r\n                        <span class=\"menuitem-badge mat-bg-{{ badge.color }}\" [ngStyle]=\"{background: badge.color}\"\r\n                              *ngFor=\"let badge of item.badges\">{{ badge.value }}</span>\r\n                        <mat-icon class=\"menu-caret\">keyboard_arrow_right</mat-icon>\r\n                    </a>\r\n                    <!-- LEVEL 2 -->\r\n                    <ul class=\"submenu lvl2\" appDropdown *ngIf=\"item.type === 'dropDown'\">\r\n                        <li *ngFor=\"let itemLvL2 of item.subs\" appDropdownLink routerLinkActive=\"open\">\r\n                            <ng-container *jhiHasAnyAuthority=\"itemLvL2.authorities\">\r\n                            <a routerLink=\"{{item.state ? '/'+item.state : ''}}/{{itemLvL2.state}}\" appDropdownToggle\r\n                               *ngIf=\"itemLvL2.type !== 'dropDown'\"\r\n                               matRipple>\r\n                                <span class=\"item-name lvl2\">{{itemLvL2.name | translate}}</span>\r\n                                <span fxFlex></span>\r\n                            </a>\r\n\r\n                            <a *ngIf=\"itemLvL2.type === 'dropDown'\" appDropdownToggle matRipple>\r\n                                <span class=\"item-name lvl2\">{{itemLvL2.name | translate}}</span>\r\n                                <span fxFlex></span>\r\n                                <mat-icon class=\"menu-caret\">keyboard_arrow_right</mat-icon>\r\n                            </a>\r\n\r\n                            <!-- LEVEL 3 -->\r\n                            <ul class=\"submenu lvl3\" appDropdown *ngIf=\"itemLvL2.type === 'dropDown'\">\r\n                                <li *ngFor=\"let itemLvL3 of itemLvL2.subs\" appDropdownLink routerLinkActive=\"open\">\r\n                                    <ng-container *jhiHasAnyAuthority=\"itemLvL3.authorities\">\r\n                                    <a routerLink=\"{{item.state ? '/'+item.state : ''}}{{itemLvL2.state ? '/'+itemLvL2.state : ''}}/{{itemLvL3.state}}\"\r\n                                       appDropdownToggle\r\n                                       matRipple>\r\n                                        <span class=\"item-name lvl3\">{{itemLvL3.name | translate}}</span>\r\n                                    </a>\r\n                                    </ng-container>\r\n                                </li>\r\n                            </ul>\r\n                            </ng-container>\r\n                        </li>\r\n                    </ul>\r\n                </div>\r\n            </ng-container>\r\n        </li>\r\n    </ul>\r\n</div>\r\n"
    }),
    __metadata("design:paramtypes", [])
], SidenavComponent);

let CustomizerService = class CustomizerService {
    constructor(router, layout) {
        this.router = router;
        this.layout = layout;
        this.colors = [
            {
                class: "black",
                active: false
            },
            {
                class: "white",
                active: false
            },
            {
                class: "dark-blue",
                active: false
            },
            {
                class: "grey",
                active: false
            },
            {
                class: "brown",
                active: false
            },
            {
                class: "gray",
                active: false
            },
            {
                class: "purple",
                active: false
            },
            {
                class: "blue",
                active: false
            },
            {
                class: "indigo",
                active: false
            },
            {
                class: "yellow",
                active: false
            },
            {
                class: "green",
                active: false
            },
            {
                class: "pink",
                active: false
            },
            {
                class: "red",
                active: false
            }
        ];
        this.topbarColors = this.getTopbarColors();
        this.sidebarColors = this.getSidebarColors();
    }
    getSidebarColors() {
        let sidebarColors = ['black', 'white', 'grey', 'brown', 'purple', 'dark-blue',];
        return this.colors.filter(color => {
            return sidebarColors.includes(color.class);
        })
            .map(c => {
            c.active = c.class === this.layout.layoutConf.sidebarColor;
            return Object.assign({}, c);
        });
    }
    getTopbarColors() {
        let topbarColors = ['black', 'white', 'dark-gray', 'purple', 'dark-blue', 'indigo', 'pink', 'red', 'yellow', 'green'];
        return this.colors.filter(color => {
            return topbarColors.includes(color.class);
        })
            .map(c => {
            c.active = c.class === this.layout.layoutConf.topbarColor;
            return Object.assign({}, c);
        });
    }
    changeSidebarColor(color) {
        this.layout.publishLayoutChange({ sidebarColor: color.class });
        this.sidebarColors = this.getSidebarColors();
    }
    changeTopbarColor(color) {
        this.layout.publishLayoutChange({ topbarColor: color.class });
        this.topbarColors = this.getTopbarColors();
    }
    removeClass(el, className) {
        if (!el || el.length === 0)
            return;
        if (!el.length) {
            el.classList.remove(className);
        }
        else {
            for (var i = 0; i < el.length; i++) {
                el[i].classList.remove(className);
            }
        }
    }
    addClass(el, className) {
        if (!el)
            return;
        if (!el.length) {
            el.classList.add(className);
        }
        else {
            for (var i = 0; i < el.length; i++) {
                el[i].classList.add(className);
            }
        }
    }
    findClosest(el, className) {
        if (!el)
            return;
        while (el) {
            var parent = el.parentElement;
            if (parent && this.hasClass(parent, className)) {
                return parent;
            }
            el = parent;
        }
    }
    hasClass(el, className) {
        if (!el)
            return;
        return (` ${el.className} `.replace(/[\n\t]/g, " ").indexOf(` ${className} `) > -1);
    }
    toggleClass(el, className) {
        if (!el)
            return;
        if (this.hasClass(el, className)) {
            this.removeClass(el, className);
        }
        else {
            this.addClass(el, className);
        }
    }
};
CustomizerService.ctorParameters = () => [
    { type: Router },
    { type: LayoutService }
];
CustomizerService.ngInjectableDef = ɵɵdefineInjectable({ factory: function CustomizerService_Factory() { return new CustomizerService(ɵɵinject(Router), ɵɵinject(LayoutService)); }, token: CustomizerService, providedIn: "root" });
CustomizerService = __decorate([
    Injectable({
        providedIn: "root"
    }),
    __metadata("design:paramtypes", [Router,
        LayoutService])
], CustomizerService);

let CustomizerComponent = class CustomizerComponent {
    constructor(navService, layout, themeService, customizer, renderer) {
        this.navService = navService;
        this.layout = layout;
        this.themeService = themeService;
        this.customizer = customizer;
        this.renderer = renderer;
        this.isCustomizerOpen = false;
        this.customizerClosed = new EventEmitter();
        this.sidenavTypes = [
            {
                name: 'Default Menu',
                value: 'default-menu'
            },
            {
                name: 'Separator Menu',
                value: 'separator-menu'
            },
            {
                name: 'Icon Menu',
                value: 'icon-menu'
            }
        ];
        this.selectedMenu = 'icon-menu';
        this.isTopbarFixed = false;
        this.isRTL = false;
        this.perfectScrollbarEnabled = true;
    }
    ngOnInit() {
        this.layoutConf = this.layout.layoutConf;
        this.selectedLayout = this.layoutConf.navigationPos;
        this.isTopbarFixed = this.layoutConf.topbarFixed;
        this.isRTL = this.layoutConf.dir === 'rtl';
        this.egretThemes = this.themeService.egretThemes;
    }
    changeTheme(theme) {
        // this.themeService.changeTheme(theme);
        this.layout.publishLayoutChange({ matTheme: theme.name });
    }
    changeLayoutStyle(data) {
        this.layout.publishLayoutChange({ navigationPos: this.selectedLayout });
    }
    changeSidenav(data) {
        this.navService.publishNavigationChange(data.value);
    }
    toggleBreadcrumb(data) {
        this.layout.publishLayoutChange({ useBreadcrumb: data.checked });
    }
    sidebarCompactToggle(data) {
        this.layout.publishLayoutChange({ sidebarCompactToggle: data.checked });
    }
    toggleTopbarFixed(data) {
        this.layout.publishLayoutChange({ topbarFixed: data.checked });
    }
    toggleDir(data) {
        let dir = data.checked ? 'rtl' : 'ltr';
        this.layout.publishLayoutChange({ dir: dir });
    }
    tooglePerfectScrollbar(data) {
        this.layout.publishLayoutChange({ perfectScrollbar: this.perfectScrollbarEnabled });
    }
    close() {
        this.isCustomizerOpen = false;
        this.customizerClosed.emit(null);
    }
};
CustomizerComponent.ctorParameters = () => [
    { type: NavigationService },
    { type: LayoutService },
    { type: ThemeService },
    { type: CustomizerService },
    { type: Renderer2 }
];
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], CustomizerComponent.prototype, "isCustomizerOpen", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], CustomizerComponent.prototype, "customizerClosed", void 0);
CustomizerComponent = __decorate([
    Component({
        selector: 'egret-customizer',
        template: "<div id=\"app-customizer\" *ngIf=\"isCustomizerOpen\">\r\n    <mat-card class=\"p-0\">\r\n        <mat-card-title class=\"m-0 light-gray\">\r\n            <div class=\"card-title-text\" fxLayout=\"row wrap\" fxLayoutAlign=\"center center\">\r\n                <span fxFlex></span>\r\n                <button\r\n                        class=\"card-control\"\r\n                        mat-icon-button\r\n                        (click)=\"close()\">\r\n                    <mat-icon>close</mat-icon>\r\n                </button>\r\n            </div>\r\n        </mat-card-title>\r\n\r\n        <mat-card-content [perfectScrollbar]>\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Layouts</h6>\r\n                <mat-radio-group fxLayout=\"column\" [(ngModel)]=\"selectedLayout\" (change)=\"changeLayoutStyle($event)\">\r\n                    <mat-radio-button [value]=\"'top'\"> Top Navigation</mat-radio-button>\r\n                    <mat-radio-button [value]=\"'side'\"> Side Navigation</mat-radio-button>\r\n                </mat-radio-group>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Header Colors</h6>\r\n                <div class=\"mb-1\">\r\n                    <mat-checkbox [(ngModel)]=\"isTopbarFixed\" (change)=\"toggleTopbarFixed($event)\"\r\n                                  [disabled]=\"selectedLayout === 'top'\" [value]=\"selectedLayout !== 'top'\">Fixed Header\r\n                    </mat-checkbox>\r\n                </div>\r\n                <div class=\"colors\">\r\n                    <div\r\n                            class=\"color {{c.class}}\"\r\n                            *ngFor=\"let c of customizer.topbarColors\"\r\n                            (click)=\"customizer.changeTopbarColor(c)\">\r\n                        <mat-icon class=\"active-icon\" *ngIf=\"c.active\">check</mat-icon>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Sidebar colors</h6>\r\n                <div class=\"colors\">\r\n                    <div\r\n                            class=\"color {{c.class}}\"\r\n                            *ngFor=\"let c of customizer.sidebarColors\"\r\n                            (click)=\"customizer.changeSidebarColor(c)\">\r\n                        <mat-icon class=\"active-icon\" *ngIf=\"c.active\">check</mat-icon>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Material Themes</h6>\r\n                <div class=\"colors\">\r\n                    <div class=\"color\" *ngFor=\"let theme of egretThemes\"\r\n                         (click)=\"changeTheme(theme)\" [style.background]=\"theme.baseColor\">\r\n                        <mat-icon class=\"active-icon\" *ngIf=\"theme.isActive\">check</mat-icon>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Sidebar Toggle</h6>\r\n                <div class=\"mb-1\">\r\n                    <mat-checkbox [(ngModel)]=\"layoutConf.sidebarCompactToggle\" (change)=\"sidebarCompactToggle($event)\">\r\n                        Toggle Sidebar\r\n                    </mat-checkbox>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Breadcrumb</h6>\r\n                <div class=\"mb-1\">\r\n                    <mat-checkbox [(ngModel)]=\"layoutConf.useBreadcrumb\" (change)=\"toggleBreadcrumb($event)\">Use\r\n                        breadcrumb\r\n                    </mat-checkbox>\r\n                </div>\r\n                <small class=\"text-muted\">Breadcrumb types</small>\r\n                <mat-radio-group fxLayout=\"column\" [(ngModel)]=\"layoutConf.breadcrumb\"\r\n                                 [disabled]=\"!layoutConf.useBreadcrumb\">\r\n                    <mat-radio-button [value]=\"'simple'\"> Simple</mat-radio-button>\r\n                    <mat-radio-button [value]=\"'title'\"> Simple with title</mat-radio-button>\r\n                </mat-radio-group>\r\n            </div>\r\n            <div class=\"pb-1 pos-rel mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Navigation</h6>\r\n                <mat-radio-group\r\n                        fxLayout=\"column\"\r\n                        [(ngModel)]=\"selectedMenu\"\r\n                        (change)=\"changeSidenav($event)\"\r\n                        [disabled]=\"selectedLayout === 'top'\">\r\n                    <mat-radio-button\r\n                            *ngFor=\"let type of sidenavTypes\"\r\n                            [value]=\"type.value\">\r\n                        {{type.name}}\r\n                    </mat-radio-button>\r\n                </mat-radio-group>\r\n            </div>\r\n\r\n            <div class=\"pb-1 \">\r\n                <mat-checkbox [(ngModel)]=\"isRTL\" (change)=\"toggleDir($event)\">RTL</mat-checkbox>\r\n            </div>\r\n        </mat-card-content>\r\n    </mat-card>\r\n</div>\r\n",
        styles: [".handle{position:fixed;bottom:30px;right:30px;z-index:99}#app-customizer{position:fixed;bottom:0;top:0;right:0;min-width:180px;max-width:280px;z-index:999}#app-customizer .title{text-transform:uppercase;font-size:12px;font-weight:700;margin:0 0 1rem}#app-customizer .mat-card{margin:0;border-radius:0}#app-customizer .mat-card-content{padding:1rem 1.5rem 2rem;max-height:calc(100vh - 80px)}.pos-rel{position:relative;z-index:99}.pos-rel .olay{position:absolute;width:100%;height:100%;background:rgba(0,0,0,.5);z-index:100}.colors{display:flex;flex-wrap:wrap}.colors .color{position:relative;width:36px;height:36px;display:inline-block;border-radius:50%;margin:8px;text-align:center;box-shadow:0 4px 20px 1px rgba(0,0,0,.06),0 1px 4px rgba(0,0,0,.03);cursor:pointer}.colors .color .active-icon{position:absolute;left:0;right:0;margin:auto;top:6px}"]
    }),
    __metadata("design:paramtypes", [NavigationService,
        LayoutService,
        ThemeService,
        CustomizerService,
        Renderer2])
], CustomizerComponent);

const classesToInclude = [
    HeaderTopComponent,
    SidebarTopComponent,
    SidenavComponent,
    NotificationsComponent,
    SidebarSideComponent,
    EgretSidebarComponent,
    HeaderSideComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    BreadcrumbComponent,
    RelativeTimePipe,
    ExcerptPipe,
    GetValueByKeyPipe,
    CustomizerComponent
];
let EgretModule = class EgretModule {
};
EgretModule = __decorate([
    NgModule({
        imports: [
            CommonModule,
            FormsModule,
            RouterModule,
            LamisCoreModule,
            FlexLayoutModule,
            CoreModule,
            MatSidenavModule,
            MatListModule,
            MatTooltipModule,
            MatOptionModule,
            MatSelectModule,
            MatMenuModule,
            MatSnackBarModule,
            MatGridListModule,
            MatToolbarModule,
            MatIconModule,
            MatButtonModule,
            MatRadioModule,
            MatCheckboxModule,
            MatCardModule,
            MatProgressSpinnerModule,
            MatRippleModule,
            MatDialogModule,
            SharedDirectivesModule,
            PerfectScrollbarModule,
            TranslateModule
        ],
        entryComponents: [],
        providers: [
            ThemeService,
            LayoutService,
            NavigationService,
            RoutePartsService
        ],
        declarations: classesToInclude,
        exports: [...classesToInclude, SharedDirectivesModule],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
], EgretModule);

/*
 * Public API Surface of fingerprint
 */

/**
 * Generated bundle index. Do not edit.
 */

export { AdminLayoutComponent, AppDropdownDirective, AuthLayoutComponent, DropdownAnchorDirective, DropdownLinkDirective, EgretModule, EgretSideNavToggleDirective, EgretSidenavHelperDirective, EgretSidenavHelperService, EgretSidenavTogglerDirective, ExcerptPipe, FontSizeDirective, GetValueByKeyPipe, LayoutService, MatchMediaService, NavigationService, RelativeTimePipe, RoutePartsService, ScrollToDirective, SharedDirectivesModule, ThemeService, egretAnimations, EgretSidebarComponent as ɵa, EgretSidebarTogglerDirective as ɵb, EgretSidebarHelperService as ɵc, HeaderTopComponent as ɵd, SidebarTopComponent as ɵe, SidenavComponent as ɵf, NotificationsComponent as ɵg, SidebarSideComponent as ɵh, HeaderSideComponent as ɵi, BreadcrumbComponent as ɵj, CustomizerComponent as ɵk, CustomizerService as ɵl };
//# sourceMappingURL=lamis-egret.js.map
