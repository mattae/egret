import { PipeTransform } from "@angular/core";
export declare class GetValueByKeyPipe implements PipeTransform {
    transform(value: any[], id: number, property: string): any;
}
