import { AppDropdownDirective } from './dropdown.directive';
export declare class DropdownLinkDirective {
    group: any;
    open: boolean;
    protected _open: boolean;
    protected nav: AppDropdownDirective;
    constructor(nav: AppDropdownDirective);
    ngOnInit(): any;
    ngOnDestroy(): any;
    toggle(): any;
}
