import { ElementRef, OnInit } from '@angular/core';
export declare class FontSizeDirective implements OnInit {
    fontSize: string;
    private el;
    constructor(fontSize: string, el: ElementRef);
    ngOnInit(): void;
}
