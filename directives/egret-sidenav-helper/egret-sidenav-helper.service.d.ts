import { MatSidenav } from "@angular/material";
export declare class EgretSidenavHelperService {
    sidenavList: MatSidenav[];
    constructor();
    setSidenav(id: any, sidenav: any): void;
    getSidenav(id: any): any;
}
