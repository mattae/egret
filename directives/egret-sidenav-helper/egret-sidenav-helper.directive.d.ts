import { OnDestroy, OnInit } from '@angular/core';
import { EgretSidenavHelperService } from './egret-sidenav-helper.service';
import { MatSidenav } from '@angular/material';
import { MediaObserver } from '@angular/flex-layout';
import { MatchMediaService } from '../../services/match-media.service';
export declare class EgretSidenavHelperDirective implements OnInit, OnDestroy {
    private matchMediaService;
    private egretSidenavHelperService;
    private matSidenav;
    private media;
    isOpen: boolean;
    id: string;
    isOpenBreakpoint: string;
    private unsubscribeAll;
    constructor(matchMediaService: MatchMediaService, egretSidenavHelperService: EgretSidenavHelperService, matSidenav: MatSidenav, media: MediaObserver);
    ngOnInit(): void;
    ngOnDestroy(): void;
}
export declare class EgretSidenavTogglerDirective {
    private egretSidenavHelperService;
    id: any;
    constructor(egretSidenavHelperService: EgretSidenavHelperService);
    onClick(): void;
}
