import { ElementRef, OnInit } from '@angular/core';
export declare class ScrollToDirective implements OnInit {
    elmID: string;
    private el;
    constructor(elmID: string, el: ElementRef);
    ngOnInit(): void;
    static currentYPosition(): number;
    static elmYPosition(eID: any): number;
    smoothScroll(): boolean;
}
