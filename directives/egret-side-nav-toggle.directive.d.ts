import { OnDestroy, OnInit } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { Subscription } from 'rxjs';
import { MatSidenav } from '@angular/material';
export declare class EgretSideNavToggleDirective implements OnInit, OnDestroy {
    private media;
    sideNav: MatSidenav;
    isMobile: any;
    screenSizeWatcher: Subscription;
    constructor(media: MediaObserver, sideNav: MatSidenav);
    ngOnInit(): void;
    ngOnDestroy(): void;
    updateSidenav(): void;
    initSideNav(): void;
}
