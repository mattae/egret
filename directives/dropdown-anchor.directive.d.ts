import { DropdownLinkDirective } from './dropdown-link.directive';
export declare class DropdownAnchorDirective {
    protected navlink: DropdownLinkDirective;
    constructor(navlink: DropdownLinkDirective);
    onClick(e: any): void;
}
