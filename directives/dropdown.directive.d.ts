import { Router } from '@angular/router';
import { DropdownLinkDirective } from './dropdown-link.directive';
export declare class AppDropdownDirective {
    private router;
    protected navlinks: Array<DropdownLinkDirective>;
    private _router;
    closeOtherLinks(openLink: DropdownLinkDirective): void;
    addLink(link: DropdownLinkDirective): void;
    removeGroup(link: DropdownLinkDirective): void;
    getUrl(): string;
    ngOnInit(): any;
    constructor(router: Router);
}
