import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';
var AppLoaderComponent = /** @class */ (function () {
    function AppLoaderComponent(dialogRef) {
        this.dialogRef = dialogRef;
    }
    AppLoaderComponent.prototype.ngOnInit = function () {
    };
    AppLoaderComponent.ctorParameters = function () { return [
        { type: MatDialogRef }
    ]; };
    AppLoaderComponent = tslib_1.__decorate([
        Component({
            selector: 'egret-app-loader',
            template: "<div class=\"text-center\">\r\n    <h6 class=\"m-0 pb-1\">{{ title }}</h6>\r\n    <div mat-dialog-content>\r\n        <mat-spinner [style.margin]=\"'auto'\"></mat-spinner>\r\n    </div>\r\n</div>\r\n",
            styles: [".mat-dialog-content{min-height:122px}"]
        }),
        tslib_1.__metadata("design:paramtypes", [MatDialogRef])
    ], AppLoaderComponent);
    return AppLoaderComponent;
}());
export { AppLoaderComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLWxvYWRlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvZWdyZXQvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9hcHAtbG9hZGVyL2FwcC1sb2FkZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ2xELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQU9qRDtJQUlJLDRCQUFtQixTQUEyQztRQUEzQyxjQUFTLEdBQVQsU0FBUyxDQUFrQztJQUM5RCxDQUFDO0lBRUQscUNBQVEsR0FBUjtJQUNBLENBQUM7O2dCQUo2QixZQUFZOztJQUpqQyxrQkFBa0I7UUFMOUIsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLGtCQUFrQjtZQUM1QixtTkFBMEM7O1NBRTdDLENBQUM7aURBS2dDLFlBQVk7T0FKakMsa0JBQWtCLENBVTlCO0lBQUQseUJBQUM7Q0FBQSxBQVZELElBVUM7U0FWWSxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2dSZWYgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnZWdyZXQtYXBwLWxvYWRlcicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vYXBwLWxvYWRlci5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9hcHAtbG9hZGVyLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQXBwTG9hZGVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIHRpdGxlO1xyXG4gICAgbWVzc2FnZTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8QXBwTG9hZGVyQ29tcG9uZW50Pikge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=