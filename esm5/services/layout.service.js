import * as tslib_1 from "tslib";
import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ThemeService } from './theme.service';
import { HttpClient } from '@angular/common/http';
import { AccountService, SERVER_API_URL_CONFIG, ServerApiUrlConfig } from '@lamis/web-core';
import * as i0 from "@angular/core";
import * as i1 from "./theme.service";
import * as i2 from "@angular/common/http";
import * as i3 from "@lamis/web-core";
var LayoutService = /** @class */ (function () {
    function LayoutService(themeService, http, apiUrlConfig, accountService) {
        this.themeService = themeService;
        this.http = http;
        this.apiUrlConfig = apiUrlConfig;
        this.accountService = accountService;
        this.layoutConfSubject = new BehaviorSubject(this.layoutConf);
        this.layoutConf$ = this.layoutConfSubject.asObservable();
        this.username = 'anonymous';
        this.fullWidthRoutes = ['shop'];
        this.setAppLayout();
    }
    LayoutService.prototype.setAppLayout = function () {
        //******** SET YOUR LAYOUT OPTIONS HERE *********
        this.layoutConf = {
            "navigationPos": "side",
            "sidebarStyle": "full",
            "sidebarColor": "white",
            "sidebarCompactToggle": false,
            "dir": "ltr",
            "useBreadcrumb": true,
            "topbarFixed": false,
            "topbarColor": "white",
            "matTheme": "egret-blue",
            "breadcrumb": "simple",
            "perfectScrollbar": true
        };
        // this.getLayoutConfig();
    };
    LayoutService.prototype.publishLayoutChange = function (lc, opt) {
        if (opt === void 0) { opt = {}; }
        if (this.layoutConf.matTheme !== lc.matTheme && lc.matTheme) {
            this.themeService.changeTheme(this.layoutConf.matTheme, lc.matTheme);
        }
        this.layoutConf = Object.assign(this.layoutConf, lc);
        this.layoutConfSubject.next(this.layoutConf);
        //this.saveLayoutConfig();
    };
    LayoutService.prototype.applyMatTheme = function (r) {
        this.themeService.applyMatTheme(r, this.layoutConf.matTheme);
    };
    LayoutService.prototype.adjustLayout = function (options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        var sidebarStyle;
        this.isMobile = this.isSm();
        this.currentRoute = options.route || this.currentRoute;
        sidebarStyle = this.isMobile ? 'closed' : this.layoutConf.sidebarStyle;
        if (this.currentRoute) {
            this.fullWidthRoutes.forEach(function (route) {
                if (_this.currentRoute.indexOf(route) !== -1) {
                    sidebarStyle = 'closed';
                }
            });
        }
        this.publishLayoutChange({
            isMobile: this.isMobile,
            sidebarStyle: sidebarStyle
        });
    };
    LayoutService.prototype.isSm = function () {
        return window.matchMedia("(max-width: 959px)").matches;
    };
    LayoutService.prototype.getLayoutConfig = function () {
        var _this = this;
        this.accountService.fetch().subscribe(function (account) {
            _this.username = account.body.login;
            _this.http.get(_this.apiUrlConfig.SERVER_API_URL + ("api/layout-config/" + _this.username), { observe: 'body' }).subscribe(function (res) {
                _this.layoutConf = res.config || _this.layoutConf;
            });
        });
    };
    LayoutService.prototype.saveLayoutConfig = function () {
        var _this = this;
        this.http.put(this.apiUrlConfig.SERVER_API_URL + ("api/layout-config/" + this.username), {
            config: this.layoutConf
        }, { observe: 'response' })
            .subscribe(function (config) {
            _this.layoutConf = config.body;
        });
    };
    LayoutService.ctorParameters = function () { return [
        { type: ThemeService },
        { type: HttpClient },
        { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] },
        { type: AccountService }
    ]; };
    LayoutService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function LayoutService_Factory() { return new LayoutService(i0.ɵɵinject(i1.ThemeService), i0.ɵɵinject(i2.HttpClient), i0.ɵɵinject(i3.SERVER_API_URL_CONFIG), i0.ɵɵinject(i3.AccountService)); }, token: LayoutService, providedIn: "root" });
    LayoutService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__param(2, Inject(SERVER_API_URL_CONFIG)),
        tslib_1.__metadata("design:paramtypes", [ThemeService,
            HttpClient, Object, AccountService])
    ], LayoutService);
    return LayoutService;
}());
export { LayoutService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGF5b3V0LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvZWdyZXQvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9sYXlvdXQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQWEsTUFBTSxlQUFlLENBQUM7QUFDOUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN2QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxjQUFjLEVBQUUscUJBQXFCLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQzs7Ozs7QUErQjVGO0lBU0ksdUJBQ1ksWUFBMEIsRUFDMUIsSUFBZ0IsRUFDZSxZQUFnQyxFQUMvRCxjQUE4QjtRQUg5QixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2UsaUJBQVksR0FBWixZQUFZLENBQW9CO1FBQy9ELG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQVgxQyxzQkFBaUIsR0FBRyxJQUFJLGVBQWUsQ0FBYyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDdEUsZ0JBQVcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFHNUMsYUFBUSxHQUFHLFdBQVcsQ0FBQztRQUN4QixvQkFBZSxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7UUFROUIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFRCxvQ0FBWSxHQUFaO1FBQ0ksaURBQWlEO1FBQ2pELElBQUksQ0FBQyxVQUFVLEdBQUc7WUFDZCxlQUFlLEVBQUUsTUFBTTtZQUN2QixjQUFjLEVBQUUsTUFBTTtZQUN0QixjQUFjLEVBQUUsT0FBTztZQUN2QixzQkFBc0IsRUFBRSxLQUFLO1lBQzdCLEtBQUssRUFBRSxLQUFLO1lBQ1osZUFBZSxFQUFFLElBQUk7WUFDckIsYUFBYSxFQUFFLEtBQUs7WUFDcEIsYUFBYSxFQUFFLE9BQU87WUFDdEIsVUFBVSxFQUFFLFlBQVk7WUFDeEIsWUFBWSxFQUFFLFFBQVE7WUFDdEIsa0JBQWtCLEVBQUUsSUFBSTtTQUMzQixDQUFDO1FBQ0YsMEJBQTBCO0lBQzlCLENBQUM7SUFFRCwyQ0FBbUIsR0FBbkIsVUFBb0IsRUFBZSxFQUFFLEdBQThCO1FBQTlCLG9CQUFBLEVBQUEsUUFBOEI7UUFDL0QsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsS0FBSyxFQUFFLENBQUMsUUFBUSxJQUFJLEVBQUUsQ0FBQyxRQUFRLEVBQUU7WUFDekQsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ3hFO1FBRUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDN0MsMEJBQTBCO0lBQzlCLENBQUM7SUFFRCxxQ0FBYSxHQUFiLFVBQWMsQ0FBWTtRQUN0QixJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBRUQsb0NBQVksR0FBWixVQUFhLE9BQWtDO1FBQS9DLGlCQWtCQztRQWxCWSx3QkFBQSxFQUFBLFlBQWtDO1FBQzNDLElBQUksWUFBb0IsQ0FBQztRQUN6QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUM1QixJQUFJLENBQUMsWUFBWSxHQUFHLE9BQU8sQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQztRQUN2RCxZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQztRQUV2RSxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDbkIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsVUFBQSxLQUFLO2dCQUM5QixJQUFJLEtBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO29CQUN6QyxZQUFZLEdBQUcsUUFBUSxDQUFDO2lCQUMzQjtZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFFRCxJQUFJLENBQUMsbUJBQW1CLENBQUM7WUFDckIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3ZCLFlBQVksRUFBRSxZQUFZO1NBQzdCLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw0QkFBSSxHQUFKO1FBQ0ksT0FBTyxNQUFNLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLENBQUMsT0FBTyxDQUFDO0lBQzNELENBQUM7SUFFTyx1Q0FBZSxHQUF2QjtRQUFBLGlCQVFDO1FBUEcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQyxPQUFPO1lBQzFDLEtBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7WUFDbkMsS0FBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLElBQUcsdUJBQXFCLEtBQUksQ0FBQyxRQUFVLENBQUEsRUFDakYsRUFBQyxPQUFPLEVBQUUsTUFBTSxFQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQyxHQUFRO2dCQUNsQyxLQUFJLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxNQUFNLElBQUksS0FBSSxDQUFDLFVBQVUsQ0FBQTtZQUNuRCxDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVPLHdDQUFnQixHQUF4QjtRQUFBLGlCQVFDO1FBUEcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLElBQUcsdUJBQXFCLElBQUksQ0FBQyxRQUFVLENBQUEsRUFDakY7WUFDSSxNQUFNLEVBQUUsSUFBSSxDQUFDLFVBQVU7U0FDMUIsRUFBRSxFQUFDLE9BQU8sRUFBRSxVQUFVLEVBQUMsQ0FBQzthQUN4QixTQUFTLENBQUMsVUFBQyxNQUFNO1lBQ2QsS0FBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFBO1FBQ2pDLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQzs7Z0JBbEZ5QixZQUFZO2dCQUNwQixVQUFVO2dEQUN2QixNQUFNLFNBQUMscUJBQXFCO2dCQUNMLGNBQWM7OztJQWJqQyxhQUFhO1FBSHpCLFVBQVUsQ0FBQztZQUNSLFVBQVUsRUFBRSxNQUFNO1NBQ3JCLENBQUM7UUFhTyxtQkFBQSxNQUFNLENBQUMscUJBQXFCLENBQUMsQ0FBQTtpREFGUixZQUFZO1lBQ3BCLFVBQVUsVUFFQSxjQUFjO09BYmpDLGFBQWEsQ0E2RnpCO3dCQWhJRDtDQWdJQyxBQTdGRCxJQTZGQztTQTdGWSxhQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlLCBSZW5kZXJlcjIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFRoZW1lU2VydmljZSB9IGZyb20gJy4vdGhlbWUuc2VydmljZSc7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEFjY291bnRTZXJ2aWNlLCBTRVJWRVJfQVBJX1VSTF9DT05GSUcsIFNlcnZlckFwaVVybENvbmZpZyB9IGZyb20gJ0BsYW1pcy93ZWItY29yZSc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIElMYXlvdXRDb25mIHtcclxuICAgIG5hdmlnYXRpb25Qb3M/OiBzdHJpbmc7ICAgICAgICAgLy8gc2lkZSwgdG9wXHJcbiAgICBzaWRlYmFyU3R5bGU/OiBzdHJpbmc7ICAgICAgICAgIC8vIGZ1bGwsIGNvbXBhY3QsIGNsb3NlZFxyXG4gICAgc2lkZWJhckNvbXBhY3RUb2dnbGU/OiBib29sZWFuOyAvLyBzaWRlYmFyIGV4cGFuZGFibGUgb24gaG92ZXJcclxuICAgIHNpZGViYXJDb2xvcj86IHN0cmluZzsgICAgICAgICAgLy8gU2lkZWJhciBiYWNrZ3JvdW5kIGNvbG9yIGh0dHA6Ly9kZW1vcy51aS1saWIuY29tL2VncmV0LWRvYy8jZWdyZXQtY29sb3JzXHJcbiAgICBkaXI/OiBzdHJpbmc7ICAgICAgICAgICAgICAgICAgIC8vIGx0ciwgcnRsXHJcbiAgICBpc01vYmlsZT86IGJvb2xlYW47ICAgICAgICAgICAgIC8vIHVwZGF0ZWQgYXV0b21hdGljYWxseVxyXG4gICAgdXNlQnJlYWRjcnVtYj86IGJvb2xlYW47ICAgICAgICAvLyBCcmVhZGNydW1iIGVuYWJsZWQvZGlzYWJsZWRcclxuICAgIGJyZWFkY3J1bWI/OiBzdHJpbmc7ICAgICAgICAgICAgLy8gc2ltcGxlLCB0aXRsZVxyXG4gICAgdG9wYmFyRml4ZWQ/OiBib29sZWFuOyAgICAgICAgICAvLyBGaXhlZCBoZWFkZXJcclxuICAgIHRvcGJhckNvbG9yPzogc3RyaW5nOyAgICAgICAgICAgLy8gSGVhZGVyIGJhY2tncm91bmQgY29sb3IgaHR0cDovL2RlbW9zLnVpLWxpYi5jb20vZWdyZXQtZG9jLyNlZ3JldC1jb2xvcnNcclxuICAgIG1hdFRoZW1lPzogc3RyaW5nICAgICAgICAgICAgICAgLy8gbWF0ZXJpYWwgdGhlbWUuIGVncmV0LWJsdWUsIGVncmV0LW5hdnksIGVncmV0LWRhcmstcHVycGxlLCBlZ3JldC1kYXJrLXBpbmtcclxuICAgIHBlcmZlY3RTY3JvbGxiYXI/OiBib29sZWFuO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIElMYXlvdXRDaGFuZ2VPcHRpb25zIHtcclxuICAgIGR1cmF0aW9uPzogbnVtYmVyLFxyXG4gICAgdHJhbnNpdGlvbkNsYXNzPzogYm9vbGVhblxyXG59XHJcblxyXG5pbnRlcmZhY2UgSUFkanVzdFNjcmVlbk9wdGlvbnMge1xyXG4gICAgYnJvd3NlckV2ZW50PzogYW55LFxyXG4gICAgcm91dGU/OiBzdHJpbmdcclxufVxyXG5cclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTGF5b3V0U2VydmljZSB7XHJcbiAgICBwdWJsaWMgbGF5b3V0Q29uZjogSUxheW91dENvbmY7XHJcbiAgICBsYXlvdXRDb25mU3ViamVjdCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8SUxheW91dENvbmY+KHRoaXMubGF5b3V0Q29uZik7XHJcbiAgICBsYXlvdXRDb25mJCA9IHRoaXMubGF5b3V0Q29uZlN1YmplY3QuYXNPYnNlcnZhYmxlKCk7XHJcbiAgICBwdWJsaWMgaXNNb2JpbGU6IGJvb2xlYW47XHJcbiAgICBwdWJsaWMgY3VycmVudFJvdXRlOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIHVzZXJuYW1lID0gJ2Fub255bW91cyc7XHJcbiAgICBwdWJsaWMgZnVsbFdpZHRoUm91dGVzID0gWydzaG9wJ107XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSB0aGVtZVNlcnZpY2U6IFRoZW1lU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQsXHJcbiAgICAgICAgQEluamVjdChTRVJWRVJfQVBJX1VSTF9DT05GSUcpIHByaXZhdGUgYXBpVXJsQ29uZmlnOiBTZXJ2ZXJBcGlVcmxDb25maWcsXHJcbiAgICAgICAgcHJpdmF0ZSBhY2NvdW50U2VydmljZTogQWNjb3VudFNlcnZpY2VcclxuICAgICkge1xyXG4gICAgICAgIHRoaXMuc2V0QXBwTGF5b3V0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0QXBwTGF5b3V0KCkge1xyXG4gICAgICAgIC8vKioqKioqKiogU0VUIFlPVVIgTEFZT1VUIE9QVElPTlMgSEVSRSAqKioqKioqKipcclxuICAgICAgICB0aGlzLmxheW91dENvbmYgPSB7XHJcbiAgICAgICAgICAgIFwibmF2aWdhdGlvblBvc1wiOiBcInNpZGVcIiwgICAgICAvLyBzaWRlLCB0b3BcclxuICAgICAgICAgICAgXCJzaWRlYmFyU3R5bGVcIjogXCJmdWxsXCIsICAgICAgIC8vIGZ1bGwsIGNvbXBhY3QsIGNsb3NlZFxyXG4gICAgICAgICAgICBcInNpZGViYXJDb2xvclwiOiBcIndoaXRlXCIsICAgICAgLy8gaHR0cDovL2RlbW9zLnVpLWxpYi5jb20vZWdyZXQtZG9jLyNlZ3JldC1jb2xvcnNcclxuICAgICAgICAgICAgXCJzaWRlYmFyQ29tcGFjdFRvZ2dsZVwiOiBmYWxzZSwgLy8gYXBwbGllZCB3aGVuIFwic2lkZWJhclN0eWxlXCIgaXMgXCJjb21wYWN0XCJcclxuICAgICAgICAgICAgXCJkaXJcIjogXCJsdHJcIiwgICAgICAgICAgICAgICAgIC8vIGx0ciwgcnRsXHJcbiAgICAgICAgICAgIFwidXNlQnJlYWRjcnVtYlwiOiB0cnVlLFxyXG4gICAgICAgICAgICBcInRvcGJhckZpeGVkXCI6IGZhbHNlLFxyXG4gICAgICAgICAgICBcInRvcGJhckNvbG9yXCI6IFwid2hpdGVcIiwgICAgICAgLy8gaHR0cDovL2RlbW9zLnVpLWxpYi5jb20vZWdyZXQtZG9jLyNlZ3JldC1jb2xvcnNcclxuICAgICAgICAgICAgXCJtYXRUaGVtZVwiOiBcImVncmV0LWJsdWVcIiwgICAgIC8vIGVncmV0LWJsdWUsIGVncmV0LW5hdnksIGVncmV0LWRhcmstcHVycGxlLCBlZ3JldC1kYXJrLXBpbmtcclxuICAgICAgICAgICAgXCJicmVhZGNydW1iXCI6IFwic2ltcGxlXCIsICAgICAgIC8vIHNpbXBsZSwgdGl0bGVcclxuICAgICAgICAgICAgXCJwZXJmZWN0U2Nyb2xsYmFyXCI6IHRydWVcclxuICAgICAgICB9O1xyXG4gICAgICAgIC8vIHRoaXMuZ2V0TGF5b3V0Q29uZmlnKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGlzaExheW91dENoYW5nZShsYzogSUxheW91dENvbmYsIG9wdDogSUxheW91dENoYW5nZU9wdGlvbnMgPSB7fSkge1xyXG4gICAgICAgIGlmICh0aGlzLmxheW91dENvbmYubWF0VGhlbWUgIT09IGxjLm1hdFRoZW1lICYmIGxjLm1hdFRoZW1lKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGhlbWVTZXJ2aWNlLmNoYW5nZVRoZW1lKHRoaXMubGF5b3V0Q29uZi5tYXRUaGVtZSwgbGMubWF0VGhlbWUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5sYXlvdXRDb25mID0gT2JqZWN0LmFzc2lnbih0aGlzLmxheW91dENvbmYsIGxjKTtcclxuICAgICAgICB0aGlzLmxheW91dENvbmZTdWJqZWN0Lm5leHQodGhpcy5sYXlvdXRDb25mKTtcclxuICAgICAgICAvL3RoaXMuc2F2ZUxheW91dENvbmZpZygpO1xyXG4gICAgfVxyXG5cclxuICAgIGFwcGx5TWF0VGhlbWUocjogUmVuZGVyZXIyKSB7XHJcbiAgICAgICAgdGhpcy50aGVtZVNlcnZpY2UuYXBwbHlNYXRUaGVtZShyLCB0aGlzLmxheW91dENvbmYubWF0VGhlbWUpO1xyXG4gICAgfVxyXG5cclxuICAgIGFkanVzdExheW91dChvcHRpb25zOiBJQWRqdXN0U2NyZWVuT3B0aW9ucyA9IHt9KSB7XHJcbiAgICAgICAgbGV0IHNpZGViYXJTdHlsZTogc3RyaW5nO1xyXG4gICAgICAgIHRoaXMuaXNNb2JpbGUgPSB0aGlzLmlzU20oKTtcclxuICAgICAgICB0aGlzLmN1cnJlbnRSb3V0ZSA9IG9wdGlvbnMucm91dGUgfHwgdGhpcy5jdXJyZW50Um91dGU7XHJcbiAgICAgICAgc2lkZWJhclN0eWxlID0gdGhpcy5pc01vYmlsZSA/ICdjbG9zZWQnIDogdGhpcy5sYXlvdXRDb25mLnNpZGViYXJTdHlsZTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuY3VycmVudFJvdXRlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZnVsbFdpZHRoUm91dGVzLmZvckVhY2gocm91dGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuY3VycmVudFJvdXRlLmluZGV4T2Yocm91dGUpICE9PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHNpZGViYXJTdHlsZSA9ICdjbG9zZWQnO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMucHVibGlzaExheW91dENoYW5nZSh7XHJcbiAgICAgICAgICAgIGlzTW9iaWxlOiB0aGlzLmlzTW9iaWxlLFxyXG4gICAgICAgICAgICBzaWRlYmFyU3R5bGU6IHNpZGViYXJTdHlsZVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGlzU20oKSB7XHJcbiAgICAgICAgcmV0dXJuIHdpbmRvdy5tYXRjaE1lZGlhKGAobWF4LXdpZHRoOiA5NTlweClgKS5tYXRjaGVzO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0TGF5b3V0Q29uZmlnKCkge1xyXG4gICAgICAgIHRoaXMuYWNjb3VudFNlcnZpY2UuZmV0Y2goKS5zdWJzY3JpYmUoKGFjY291bnQpID0+IHtcclxuICAgICAgICAgICAgdGhpcy51c2VybmFtZSA9IGFjY291bnQuYm9keS5sb2dpbjtcclxuICAgICAgICAgICAgdGhpcy5odHRwLmdldCh0aGlzLmFwaVVybENvbmZpZy5TRVJWRVJfQVBJX1VSTCArIGBhcGkvbGF5b3V0LWNvbmZpZy8ke3RoaXMudXNlcm5hbWV9YCxcclxuICAgICAgICAgICAgICAgIHtvYnNlcnZlOiAnYm9keSd9KS5zdWJzY3JpYmUoKHJlczogYW55KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sYXlvdXRDb25mID0gcmVzLmNvbmZpZyB8fCB0aGlzLmxheW91dENvbmZcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBzYXZlTGF5b3V0Q29uZmlnKCkge1xyXG4gICAgICAgIHRoaXMuaHR0cC5wdXQodGhpcy5hcGlVcmxDb25maWcuU0VSVkVSX0FQSV9VUkwgKyBgYXBpL2xheW91dC1jb25maWcvJHt0aGlzLnVzZXJuYW1lfWAsXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGNvbmZpZzogdGhpcy5sYXlvdXRDb25mXHJcbiAgICAgICAgICAgIH0sIHtvYnNlcnZlOiAncmVzcG9uc2UnfSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgoY29uZmlnKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxheW91dENvbmYgPSBjb25maWcuYm9keVxyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=