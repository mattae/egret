import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { AppConfirmComponent } from './app-confirm.component';
var AppConfirmService = /** @class */ (function () {
    function AppConfirmService(dialog) {
        this.dialog = dialog;
    }
    AppConfirmService.prototype.confirm = function (data) {
        if (data === void 0) { data = {}; }
        data.title = data.title || 'Confirm';
        data.message = data.message || 'Are you sure?';
        var dialogRef;
        dialogRef = this.dialog.open(AppConfirmComponent, {
            width: '380px',
            disableClose: true,
            data: { title: data.title, message: data.message }
        });
        return dialogRef.afterClosed();
    };
    AppConfirmService.ctorParameters = function () { return [
        { type: MatDialog }
    ]; };
    AppConfirmService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [MatDialog])
    ], AppConfirmService);
    return AppConfirmService;
}());
export { AppConfirmService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLWNvbmZpcm0uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2FwcC1jb25maXJtL2FwcC1jb25maXJtLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUc1RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQVE5RDtJQUVJLDJCQUFvQixNQUFpQjtRQUFqQixXQUFNLEdBQU4sTUFBTSxDQUFXO0lBQ3JDLENBQUM7SUFFTSxtQ0FBTyxHQUFkLFVBQWUsSUFBc0I7UUFBdEIscUJBQUEsRUFBQSxTQUFzQjtRQUNqQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLElBQUksU0FBUyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sSUFBSSxlQUFlLENBQUM7UUFDL0MsSUFBSSxTQUE0QyxDQUFDO1FBQ2pELFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtZQUM5QyxLQUFLLEVBQUUsT0FBTztZQUNkLFlBQVksRUFBRSxJQUFJO1lBQ2xCLElBQUksRUFBRSxFQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFDO1NBQ25ELENBQUMsQ0FBQztRQUNILE9BQU8sU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ25DLENBQUM7O2dCQWIyQixTQUFTOztJQUY1QixpQkFBaUI7UUFEN0IsVUFBVSxFQUFFO2lEQUdtQixTQUFTO09BRjVCLGlCQUFpQixDQWdCN0I7SUFBRCx3QkFBQztDQUFBLEFBaEJELElBZ0JDO1NBaEJZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0RGlhbG9nLCBNYXREaWFsb2dSZWYgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IEFwcENvbmZpcm1Db21wb25lbnQgfSBmcm9tICcuL2FwcC1jb25maXJtLmNvbXBvbmVudCc7XHJcblxyXG5pbnRlcmZhY2UgY29uZmlybURhdGEge1xyXG4gICAgdGl0bGU/OiBzdHJpbmcsXHJcbiAgICBtZXNzYWdlPzogc3RyaW5nXHJcbn1cclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEFwcENvbmZpcm1TZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGRpYWxvZzogTWF0RGlhbG9nKSB7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGNvbmZpcm0oZGF0YTogY29uZmlybURhdGEgPSB7fSk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xyXG4gICAgICAgIGRhdGEudGl0bGUgPSBkYXRhLnRpdGxlIHx8ICdDb25maXJtJztcclxuICAgICAgICBkYXRhLm1lc3NhZ2UgPSBkYXRhLm1lc3NhZ2UgfHwgJ0FyZSB5b3Ugc3VyZT8nO1xyXG4gICAgICAgIGxldCBkaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxBcHBDb25maXJtQ29tcG9uZW50PjtcclxuICAgICAgICBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKEFwcENvbmZpcm1Db21wb25lbnQsIHtcclxuICAgICAgICAgICAgd2lkdGg6ICczODBweCcsXHJcbiAgICAgICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICAgICAgZGF0YToge3RpdGxlOiBkYXRhLnRpdGxlLCBtZXNzYWdlOiBkYXRhLm1lc3NhZ2V9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIGRpYWxvZ1JlZi5hZnRlckNsb3NlZCgpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==