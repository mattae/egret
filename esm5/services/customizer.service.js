import * as tslib_1 from "tslib";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { LayoutService } from "./layout.service";
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "./layout.service";
var CustomizerService = /** @class */ (function () {
    function CustomizerService(router, layout) {
        this.router = router;
        this.layout = layout;
        this.colors = [
            {
                class: "black",
                active: false
            },
            {
                class: "white",
                active: false
            },
            {
                class: "dark-blue",
                active: false
            },
            {
                class: "grey",
                active: false
            },
            {
                class: "brown",
                active: false
            },
            {
                class: "gray",
                active: false
            },
            {
                class: "purple",
                active: false
            },
            {
                class: "blue",
                active: false
            },
            {
                class: "indigo",
                active: false
            },
            {
                class: "yellow",
                active: false
            },
            {
                class: "green",
                active: false
            },
            {
                class: "pink",
                active: false
            },
            {
                class: "red",
                active: false
            }
        ];
        this.topbarColors = this.getTopbarColors();
        this.sidebarColors = this.getSidebarColors();
    }
    CustomizerService.prototype.getSidebarColors = function () {
        var _this = this;
        var sidebarColors = ['black', 'white', 'grey', 'brown', 'purple', 'dark-blue',];
        return this.colors.filter(function (color) {
            return sidebarColors.includes(color.class);
        })
            .map(function (c) {
            c.active = c.class === _this.layout.layoutConf.sidebarColor;
            return tslib_1.__assign({}, c);
        });
    };
    CustomizerService.prototype.getTopbarColors = function () {
        var _this = this;
        var topbarColors = ['black', 'white', 'dark-gray', 'purple', 'dark-blue', 'indigo', 'pink', 'red', 'yellow', 'green'];
        return this.colors.filter(function (color) {
            return topbarColors.includes(color.class);
        })
            .map(function (c) {
            c.active = c.class === _this.layout.layoutConf.topbarColor;
            return tslib_1.__assign({}, c);
        });
    };
    CustomizerService.prototype.changeSidebarColor = function (color) {
        this.layout.publishLayoutChange({ sidebarColor: color.class });
        this.sidebarColors = this.getSidebarColors();
    };
    CustomizerService.prototype.changeTopbarColor = function (color) {
        this.layout.publishLayoutChange({ topbarColor: color.class });
        this.topbarColors = this.getTopbarColors();
    };
    CustomizerService.prototype.removeClass = function (el, className) {
        if (!el || el.length === 0)
            return;
        if (!el.length) {
            el.classList.remove(className);
        }
        else {
            for (var i = 0; i < el.length; i++) {
                el[i].classList.remove(className);
            }
        }
    };
    CustomizerService.prototype.addClass = function (el, className) {
        if (!el)
            return;
        if (!el.length) {
            el.classList.add(className);
        }
        else {
            for (var i = 0; i < el.length; i++) {
                el[i].classList.add(className);
            }
        }
    };
    CustomizerService.prototype.findClosest = function (el, className) {
        if (!el)
            return;
        while (el) {
            var parent = el.parentElement;
            if (parent && this.hasClass(parent, className)) {
                return parent;
            }
            el = parent;
        }
    };
    CustomizerService.prototype.hasClass = function (el, className) {
        if (!el)
            return;
        return ((" " + el.className + " ").replace(/[\n\t]/g, " ").indexOf(" " + className + " ") > -1);
    };
    CustomizerService.prototype.toggleClass = function (el, className) {
        if (!el)
            return;
        if (this.hasClass(el, className)) {
            this.removeClass(el, className);
        }
        else {
            this.addClass(el, className);
        }
    };
    CustomizerService.ctorParameters = function () { return [
        { type: Router },
        { type: LayoutService }
    ]; };
    CustomizerService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function CustomizerService_Factory() { return new CustomizerService(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.LayoutService)); }, token: CustomizerService, providedIn: "root" });
    CustomizerService = tslib_1.__decorate([
        Injectable({
            providedIn: "root"
        }),
        tslib_1.__metadata("design:paramtypes", [Router,
            LayoutService])
    ], CustomizerService);
    return CustomizerService;
}());
export { CustomizerService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9taXplci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsic2VydmljZXMvY3VzdG9taXplci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7Ozs7QUFLakQ7SUE0REMsMkJBQ1MsTUFBYyxFQUNkLE1BQXFCO1FBRHJCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxXQUFNLEdBQU4sTUFBTSxDQUFlO1FBN0Q5QixXQUFNLEdBQUc7WUFDUjtnQkFDQyxLQUFLLEVBQUUsT0FBTztnQkFDZCxNQUFNLEVBQUUsS0FBSzthQUNiO1lBQ0Q7Z0JBQ0MsS0FBSyxFQUFFLE9BQU87Z0JBQ2QsTUFBTSxFQUFFLEtBQUs7YUFDYjtZQUNEO2dCQUNDLEtBQUssRUFBRSxXQUFXO2dCQUNsQixNQUFNLEVBQUUsS0FBSzthQUNiO1lBQ0Q7Z0JBQ0MsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsTUFBTSxFQUFFLEtBQUs7YUFDYjtZQUNEO2dCQUNDLEtBQUssRUFBRSxPQUFPO2dCQUNkLE1BQU0sRUFBRSxLQUFLO2FBQ2I7WUFDRDtnQkFDQyxLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsS0FBSzthQUNiO1lBQ0Q7Z0JBQ0MsS0FBSyxFQUFFLFFBQVE7Z0JBQ2YsTUFBTSxFQUFFLEtBQUs7YUFDYjtZQUNEO2dCQUNDLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxLQUFLO2FBQ2I7WUFFRDtnQkFDQyxLQUFLLEVBQUUsUUFBUTtnQkFDZixNQUFNLEVBQUUsS0FBSzthQUNiO1lBQ0Q7Z0JBQ0MsS0FBSyxFQUFFLFFBQVE7Z0JBQ2YsTUFBTSxFQUFFLEtBQUs7YUFDYjtZQUNEO2dCQUNDLEtBQUssRUFBRSxPQUFPO2dCQUNkLE1BQU0sRUFBRSxLQUFLO2FBQ2I7WUFDRDtnQkFDQyxLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsS0FBSzthQUNiO1lBQ0Q7Z0JBQ0MsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osTUFBTSxFQUFFLEtBQUs7YUFDYjtTQUNELENBQUM7UUFTRCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUMzQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzlDLENBQUM7SUFFRCw0Q0FBZ0IsR0FBaEI7UUFBQSxpQkFTQztRQVJBLElBQUksYUFBYSxHQUFHLENBQUMsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsQ0FBQztRQUNoRixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFVBQUEsS0FBSztZQUM5QixPQUFPLGFBQWEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzVDLENBQUMsQ0FBQzthQUNBLEdBQUcsQ0FBQyxVQUFBLENBQUM7WUFDTCxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxLQUFLLEtBQUssS0FBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDO1lBQzNELDRCQUFXLENBQUMsRUFBRTtRQUNmLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDJDQUFlLEdBQWY7UUFBQSxpQkFTQztRQVJBLElBQUksWUFBWSxHQUFHLENBQUMsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDdEgsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxVQUFBLEtBQUs7WUFDOUIsT0FBTyxZQUFZLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMzQyxDQUFDLENBQUM7YUFDQSxHQUFHLENBQUMsVUFBQSxDQUFDO1lBQ0wsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxLQUFLLEtBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQztZQUMxRCw0QkFBVyxDQUFDLEVBQUU7UUFDZixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCw4Q0FBa0IsR0FBbEIsVUFBbUIsS0FBSztRQUN2QixJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEVBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxLQUFLLEVBQUMsQ0FBQyxDQUFDO1FBQzdELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDOUMsQ0FBQztJQUVELDZDQUFpQixHQUFqQixVQUFrQixLQUFLO1FBQ3RCLElBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUMsRUFBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLEtBQUssRUFBQyxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDNUMsQ0FBQztJQUVELHVDQUFXLEdBQVgsVUFBWSxFQUFFLEVBQUUsU0FBUztRQUN4QixJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxNQUFNLEtBQUssQ0FBQztZQUFFLE9BQU87UUFDbkMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUU7WUFDZixFQUFFLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUMvQjthQUFNO1lBQ04sS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ25DLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ2xDO1NBQ0Q7SUFDRixDQUFDO0lBRUQsb0NBQVEsR0FBUixVQUFTLEVBQUUsRUFBRSxTQUFTO1FBQ3JCLElBQUksQ0FBQyxFQUFFO1lBQUUsT0FBTztRQUNoQixJQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRTtZQUNmLEVBQUUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQzVCO2FBQU07WUFDTixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDbkMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDL0I7U0FDRDtJQUNGLENBQUM7SUFFRCx1Q0FBVyxHQUFYLFVBQVksRUFBRSxFQUFFLFNBQVM7UUFDeEIsSUFBSSxDQUFDLEVBQUU7WUFBRSxPQUFPO1FBQ2hCLE9BQU8sRUFBRSxFQUFFO1lBQ1YsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDLGFBQWEsQ0FBQztZQUM5QixJQUFJLE1BQU0sSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsRUFBRTtnQkFDL0MsT0FBTyxNQUFNLENBQUM7YUFDZDtZQUNELEVBQUUsR0FBRyxNQUFNLENBQUM7U0FDWjtJQUNGLENBQUM7SUFFRCxvQ0FBUSxHQUFSLFVBQVMsRUFBRSxFQUFFLFNBQVM7UUFDckIsSUFBSSxDQUFDLEVBQUU7WUFBRSxPQUFPO1FBQ2hCLE9BQU8sQ0FDTixDQUFBLE1BQUksRUFBRSxDQUFDLFNBQVMsTUFBRyxDQUFBLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBSSxTQUFTLE1BQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUMxRSxDQUFDO0lBQ0gsQ0FBQztJQUVELHVDQUFXLEdBQVgsVUFBWSxFQUFFLEVBQUUsU0FBUztRQUN4QixJQUFJLENBQUMsRUFBRTtZQUFFLE9BQU87UUFDaEIsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsRUFBRSxTQUFTLENBQUMsRUFBRTtZQUNqQyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsRUFBRSxTQUFTLENBQUMsQ0FBQztTQUNoQzthQUFNO1lBQ04sSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUUsU0FBUyxDQUFDLENBQUM7U0FDN0I7SUFDRixDQUFDOztnQkF0RmdCLE1BQU07Z0JBQ04sYUFBYTs7O0lBOURsQixpQkFBaUI7UUFIN0IsVUFBVSxDQUFDO1lBQ1gsVUFBVSxFQUFFLE1BQU07U0FDbEIsQ0FBQztpREE4RGdCLE1BQU07WUFDTixhQUFhO09BOURsQixpQkFBaUIsQ0FxSjdCOzRCQTVKRDtDQTRKQyxBQXJKRCxJQXFKQztTQXJKWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBMYXlvdXRTZXJ2aWNlIH0gZnJvbSBcIi4vbGF5b3V0LnNlcnZpY2VcIjtcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuXHRwcm92aWRlZEluOiBcInJvb3RcIlxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ3VzdG9taXplclNlcnZpY2Uge1xyXG5cdGNvbG9ycyA9IFtcclxuXHRcdHtcclxuXHRcdFx0Y2xhc3M6IFwiYmxhY2tcIixcclxuXHRcdFx0YWN0aXZlOiBmYWxzZVxyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0Y2xhc3M6IFwid2hpdGVcIixcclxuXHRcdFx0YWN0aXZlOiBmYWxzZVxyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0Y2xhc3M6IFwiZGFyay1ibHVlXCIsXHJcblx0XHRcdGFjdGl2ZTogZmFsc2VcclxuXHRcdH0sXHJcblx0XHR7XHJcblx0XHRcdGNsYXNzOiBcImdyZXlcIixcclxuXHRcdFx0YWN0aXZlOiBmYWxzZVxyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0Y2xhc3M6IFwiYnJvd25cIixcclxuXHRcdFx0YWN0aXZlOiBmYWxzZVxyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0Y2xhc3M6IFwiZ3JheVwiLFxyXG5cdFx0XHRhY3RpdmU6IGZhbHNlXHJcblx0XHR9LFxyXG5cdFx0e1xyXG5cdFx0XHRjbGFzczogXCJwdXJwbGVcIixcclxuXHRcdFx0YWN0aXZlOiBmYWxzZVxyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0Y2xhc3M6IFwiYmx1ZVwiLFxyXG5cdFx0XHRhY3RpdmU6IGZhbHNlXHJcblx0XHR9LFxyXG5cclxuXHRcdHtcclxuXHRcdFx0Y2xhc3M6IFwiaW5kaWdvXCIsXHJcblx0XHRcdGFjdGl2ZTogZmFsc2VcclxuXHRcdH0sXHJcblx0XHR7XHJcblx0XHRcdGNsYXNzOiBcInllbGxvd1wiLFxyXG5cdFx0XHRhY3RpdmU6IGZhbHNlXHJcblx0XHR9LFxyXG5cdFx0e1xyXG5cdFx0XHRjbGFzczogXCJncmVlblwiLFxyXG5cdFx0XHRhY3RpdmU6IGZhbHNlXHJcblx0XHR9LFxyXG5cdFx0e1xyXG5cdFx0XHRjbGFzczogXCJwaW5rXCIsXHJcblx0XHRcdGFjdGl2ZTogZmFsc2VcclxuXHRcdH0sXHJcblx0XHR7XHJcblx0XHRcdGNsYXNzOiBcInJlZFwiLFxyXG5cdFx0XHRhY3RpdmU6IGZhbHNlXHJcblx0XHR9XHJcblx0XTtcclxuXHRzZWxlY3RlZFNpZGViYXJDb2xvcjtcclxuXHR0b3BiYXJDb2xvcnM6IGFueVtdO1xyXG5cdHNpZGViYXJDb2xvcnM6IGFueVtdO1xyXG5cclxuXHRjb25zdHJ1Y3RvcihcclxuXHRcdHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcblx0XHRwcml2YXRlIGxheW91dDogTGF5b3V0U2VydmljZSxcclxuXHQpIHtcclxuXHRcdHRoaXMudG9wYmFyQ29sb3JzID0gdGhpcy5nZXRUb3BiYXJDb2xvcnMoKTtcclxuXHRcdHRoaXMuc2lkZWJhckNvbG9ycyA9IHRoaXMuZ2V0U2lkZWJhckNvbG9ycygpO1xyXG5cdH1cclxuXHJcblx0Z2V0U2lkZWJhckNvbG9ycygpIHtcclxuXHRcdGxldCBzaWRlYmFyQ29sb3JzID0gWydibGFjaycsICd3aGl0ZScsICdncmV5JywgJ2Jyb3duJywgJ3B1cnBsZScsICdkYXJrLWJsdWUnLF07XHJcblx0XHRyZXR1cm4gdGhpcy5jb2xvcnMuZmlsdGVyKGNvbG9yID0+IHtcclxuXHRcdFx0cmV0dXJuIHNpZGViYXJDb2xvcnMuaW5jbHVkZXMoY29sb3IuY2xhc3MpO1xyXG5cdFx0fSlcclxuXHRcdFx0Lm1hcChjID0+IHtcclxuXHRcdFx0XHRjLmFjdGl2ZSA9IGMuY2xhc3MgPT09IHRoaXMubGF5b3V0LmxheW91dENvbmYuc2lkZWJhckNvbG9yO1xyXG5cdFx0XHRcdHJldHVybiB7Li4uY307XHJcblx0XHRcdH0pO1xyXG5cdH1cclxuXHJcblx0Z2V0VG9wYmFyQ29sb3JzKCkge1xyXG5cdFx0bGV0IHRvcGJhckNvbG9ycyA9IFsnYmxhY2snLCAnd2hpdGUnLCAnZGFyay1ncmF5JywgJ3B1cnBsZScsICdkYXJrLWJsdWUnLCAnaW5kaWdvJywgJ3BpbmsnLCAncmVkJywgJ3llbGxvdycsICdncmVlbiddO1xyXG5cdFx0cmV0dXJuIHRoaXMuY29sb3JzLmZpbHRlcihjb2xvciA9PiB7XHJcblx0XHRcdHJldHVybiB0b3BiYXJDb2xvcnMuaW5jbHVkZXMoY29sb3IuY2xhc3MpO1xyXG5cdFx0fSlcclxuXHRcdFx0Lm1hcChjID0+IHtcclxuXHRcdFx0XHRjLmFjdGl2ZSA9IGMuY2xhc3MgPT09IHRoaXMubGF5b3V0LmxheW91dENvbmYudG9wYmFyQ29sb3I7XHJcblx0XHRcdFx0cmV0dXJuIHsuLi5jfTtcclxuXHRcdFx0fSk7XHJcblx0fVxyXG5cclxuXHRjaGFuZ2VTaWRlYmFyQ29sb3IoY29sb3IpIHtcclxuXHRcdHRoaXMubGF5b3V0LnB1Ymxpc2hMYXlvdXRDaGFuZ2Uoe3NpZGViYXJDb2xvcjogY29sb3IuY2xhc3N9KTtcclxuXHRcdHRoaXMuc2lkZWJhckNvbG9ycyA9IHRoaXMuZ2V0U2lkZWJhckNvbG9ycygpO1xyXG5cdH1cclxuXHJcblx0Y2hhbmdlVG9wYmFyQ29sb3IoY29sb3IpIHtcclxuXHRcdHRoaXMubGF5b3V0LnB1Ymxpc2hMYXlvdXRDaGFuZ2Uoe3RvcGJhckNvbG9yOiBjb2xvci5jbGFzc30pO1xyXG5cdFx0dGhpcy50b3BiYXJDb2xvcnMgPSB0aGlzLmdldFRvcGJhckNvbG9ycygpO1xyXG5cdH1cclxuXHJcblx0cmVtb3ZlQ2xhc3MoZWwsIGNsYXNzTmFtZSkge1xyXG5cdFx0aWYgKCFlbCB8fCBlbC5sZW5ndGggPT09IDApIHJldHVybjtcclxuXHRcdGlmICghZWwubGVuZ3RoKSB7XHJcblx0XHRcdGVsLmNsYXNzTGlzdC5yZW1vdmUoY2xhc3NOYW1lKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdGZvciAodmFyIGkgPSAwOyBpIDwgZWwubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0XHRlbFtpXS5jbGFzc0xpc3QucmVtb3ZlKGNsYXNzTmFtZSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdGFkZENsYXNzKGVsLCBjbGFzc05hbWUpIHtcclxuXHRcdGlmICghZWwpIHJldHVybjtcclxuXHRcdGlmICghZWwubGVuZ3RoKSB7XHJcblx0XHRcdGVsLmNsYXNzTGlzdC5hZGQoY2xhc3NOYW1lKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdGZvciAodmFyIGkgPSAwOyBpIDwgZWwubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0XHRlbFtpXS5jbGFzc0xpc3QuYWRkKGNsYXNzTmFtZSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdGZpbmRDbG9zZXN0KGVsLCBjbGFzc05hbWUpIHtcclxuXHRcdGlmICghZWwpIHJldHVybjtcclxuXHRcdHdoaWxlIChlbCkge1xyXG5cdFx0XHR2YXIgcGFyZW50ID0gZWwucGFyZW50RWxlbWVudDtcclxuXHRcdFx0aWYgKHBhcmVudCAmJiB0aGlzLmhhc0NsYXNzKHBhcmVudCwgY2xhc3NOYW1lKSkge1xyXG5cdFx0XHRcdHJldHVybiBwYXJlbnQ7XHJcblx0XHRcdH1cclxuXHRcdFx0ZWwgPSBwYXJlbnQ7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRoYXNDbGFzcyhlbCwgY2xhc3NOYW1lKSB7XHJcblx0XHRpZiAoIWVsKSByZXR1cm47XHJcblx0XHRyZXR1cm4gKFxyXG5cdFx0XHRgICR7ZWwuY2xhc3NOYW1lfSBgLnJlcGxhY2UoL1tcXG5cXHRdL2csIFwiIFwiKS5pbmRleE9mKGAgJHtjbGFzc05hbWV9IGApID4gLTFcclxuXHRcdCk7XHJcblx0fVxyXG5cclxuXHR0b2dnbGVDbGFzcyhlbCwgY2xhc3NOYW1lKSB7XHJcblx0XHRpZiAoIWVsKSByZXR1cm47XHJcblx0XHRpZiAodGhpcy5oYXNDbGFzcyhlbCwgY2xhc3NOYW1lKSkge1xyXG5cdFx0XHR0aGlzLnJlbW92ZUNsYXNzKGVsLCBjbGFzc05hbWUpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0dGhpcy5hZGRDbGFzcyhlbCwgY2xhc3NOYW1lKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG59XHJcbiJdfQ==