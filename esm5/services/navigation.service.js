import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { MenuService } from '@lamis/web-core';
var NavigationService = /** @class */ (function () {
    function NavigationService(menuService) {
        this.menuService = menuService;
        // This title will appear if any icon type item is present in menu.
        this.iconTypeMenuTitle = 'Frequently Accessed';
        this.menuItems$ = menuService.getMenus();
    }
    NavigationService.prototype.publishNavigationChange = function (menuType) {
        /*switch (menuType) {
            case "separator-menu":
                this.menuItems.next(this.separatorMenu);
                break;
            case "icon-menu":
                this.menuItems.next(this.iconMenu);
                break;
            default:
                this.menuItems.next(this.plainMenu);
        }*/
    };
    NavigationService.ctorParameters = function () { return [
        { type: MenuService }
    ]; };
    NavigationService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [MenuService])
    ], NavigationService);
    return NavigationService;
}());
export { NavigationService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsic2VydmljZXMvbmF2aWdhdGlvbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUc5QztJQU9JLDJCQUFvQixXQUF3QjtRQUF4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUw1QyxtRUFBbUU7UUFDbkUsc0JBQWlCLEdBQVcscUJBQXFCLENBQUM7UUFLOUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDN0MsQ0FBQztJQUVELG1EQUF1QixHQUF2QixVQUF3QixRQUFnQjtRQUNwQzs7Ozs7Ozs7O1dBU0c7SUFDUCxDQUFDOztnQkFmZ0MsV0FBVzs7SUFQbkMsaUJBQWlCO1FBRDdCLFVBQVUsRUFBRTtpREFRd0IsV0FBVztPQVBuQyxpQkFBaUIsQ0F1QjdCO0lBQUQsd0JBQUM7Q0FBQSxBQXZCRCxJQXVCQztTQXZCWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1lbnVTZXJ2aWNlIH0gZnJvbSAnQGxhbWlzL3dlYi1jb3JlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIE5hdmlnYXRpb25TZXJ2aWNlIHtcclxuXHJcbiAgICAvLyBUaGlzIHRpdGxlIHdpbGwgYXBwZWFyIGlmIGFueSBpY29uIHR5cGUgaXRlbSBpcyBwcmVzZW50IGluIG1lbnUuXHJcbiAgICBpY29uVHlwZU1lbnVUaXRsZTogc3RyaW5nID0gJ0ZyZXF1ZW50bHkgQWNjZXNzZWQnO1xyXG4gICAgLy8gbmF2aWdhdGlvbiBjb21wb25lbnQgaGFzIHN1YnNjcmliZWQgdG8gdGhpcyBPYnNlcnZhYmxlXHJcbiAgICBtZW51SXRlbXMkO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgbWVudVNlcnZpY2U6IE1lbnVTZXJ2aWNlKSB7XHJcbiAgICAgICAgdGhpcy5tZW51SXRlbXMkID0gbWVudVNlcnZpY2UuZ2V0TWVudXMoKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaXNoTmF2aWdhdGlvbkNoYW5nZShtZW51VHlwZTogc3RyaW5nKSB7XHJcbiAgICAgICAgLypzd2l0Y2ggKG1lbnVUeXBlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgXCJzZXBhcmF0b3ItbWVudVwiOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5tZW51SXRlbXMubmV4dCh0aGlzLnNlcGFyYXRvck1lbnUpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgXCJpY29uLW1lbnVcIjpcclxuICAgICAgICAgICAgICAgIHRoaXMubWVudUl0ZW1zLm5leHQodGhpcy5pY29uTWVudSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgIHRoaXMubWVudUl0ZW1zLm5leHQodGhpcy5wbGFpbk1lbnUpO1xyXG4gICAgICAgIH0qL1xyXG4gICAgfVxyXG59XHJcbiJdfQ==