import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { MediaObserver, MediaChange } from '@angular/flex-layout';
import { BehaviorSubject } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/core";
var MatchMediaService = /** @class */ (function () {
    function MatchMediaService(media) {
        this.media = media;
        this.onMediaChange = new BehaviorSubject('');
        this.activeMediaQuery = '';
        this.init();
    }
    MatchMediaService.prototype.init = function () {
        var _this = this;
        this.media
            .asObservable()
            .subscribe(function (change) {
            if (_this.activeMediaQuery !== change[0].mqAlias) {
                _this.activeMediaQuery = change[0].mqAlias;
                _this.onMediaChange.next(change[0].mqAlias);
            }
        });
    };
    MatchMediaService.ctorParameters = function () { return [
        { type: MediaObserver }
    ]; };
    MatchMediaService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function MatchMediaService_Factory() { return new MatchMediaService(i0.ɵɵinject(i1.MediaObserver)); }, token: MatchMediaService, providedIn: "root" });
    MatchMediaService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [MediaObserver])
    ], MatchMediaService);
    return MatchMediaService;
}());
export { MatchMediaService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0Y2gtbWVkaWEuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL21hdGNoLW1lZGlhLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGFBQWEsRUFBRSxXQUFXLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNsRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sTUFBTSxDQUFDOzs7QUFLdkM7SUFJRSwyQkFDVSxLQUFvQjtRQUFwQixVQUFLLEdBQUwsS0FBSyxDQUFlO1FBSDlCLGtCQUFhLEdBQTRCLElBQUksZUFBZSxDQUFTLEVBQUUsQ0FBQyxDQUFDO1FBS3ZFLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2QsQ0FBQztJQUdPLGdDQUFJLEdBQVo7UUFBQSxpQkFXQztRQVRHLElBQUksQ0FBQyxLQUFLO2FBQ0wsWUFBWSxFQUFFO2FBQ2QsU0FBUyxDQUFDLFVBQUMsTUFBcUI7WUFDN0IsSUFBSyxLQUFJLENBQUMsZ0JBQWdCLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFDaEQ7Z0JBQ0ksS0FBSSxDQUFDLGdCQUFnQixHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7Z0JBQzFDLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUM5QztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQzs7Z0JBbEJnQixhQUFhOzs7SUFMbkIsaUJBQWlCO1FBSDdCLFVBQVUsQ0FBQztZQUNWLFVBQVUsRUFBRSxNQUFNO1NBQ25CLENBQUM7aURBTWlCLGFBQWE7T0FMbkIsaUJBQWlCLENBd0I3Qjs0QkEvQkQ7Q0ErQkMsQUF4QkQsSUF3QkM7U0F4QlksaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNZWRpYU9ic2VydmVyLCBNZWRpYUNoYW5nZSB9IGZyb20gJ0Bhbmd1bGFyL2ZsZXgtbGF5b3V0JztcclxuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNYXRjaE1lZGlhU2VydmljZSB7XHJcbiAgYWN0aXZlTWVkaWFRdWVyeTogc3RyaW5nO1xyXG4gIG9uTWVkaWFDaGFuZ2U6IEJlaGF2aW9yU3ViamVjdDxzdHJpbmc+ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxzdHJpbmc+KCcnKTtcclxuICBcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgbWVkaWE6IE1lZGlhT2JzZXJ2ZXJcclxuICApIHtcclxuICAgIHRoaXMuYWN0aXZlTWVkaWFRdWVyeSA9ICcnO1xyXG4gICAgdGhpcy5pbml0KCk7XHJcbiAgfVxyXG5cclxuXHJcbiAgcHJpdmF0ZSBpbml0KCk6IHZvaWRcclxuICB7XHJcbiAgICAgIHRoaXMubWVkaWFcclxuICAgICAgICAgIC5hc09ic2VydmFibGUoKVxyXG4gICAgICAgICAgLnN1YnNjcmliZSgoY2hhbmdlOiBNZWRpYUNoYW5nZVtdKSA9PiB7XHJcbiAgICAgICAgICAgICAgaWYgKCB0aGlzLmFjdGl2ZU1lZGlhUXVlcnkgIT09IGNoYW5nZVswXS5tcUFsaWFzIClcclxuICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgIHRoaXMuYWN0aXZlTWVkaWFRdWVyeSA9IGNoYW5nZVswXS5tcUFsaWFzO1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLm9uTWVkaWFDaGFuZ2UubmV4dChjaGFuZ2VbMF0ubXFBbGlhcyk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==