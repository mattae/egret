import * as tslib_1 from "tslib";
import { Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { getQueryParam } from '../helpers/url.helper';
var ThemeService = /** @class */ (function () {
    function ThemeService(document) {
        this.document = document;
        this.egretThemes = [{
                "name": "egret-dark-purple",
                "baseColor": "#9c27b0",
                "isActive": false
            }, {
                "name": "egret-dark-pink",
                "baseColor": "#e91e63",
                "isActive": false
            }, {
                "name": "egret-blue",
                "baseColor": "#03a9f4",
                "isActive": true
            }, {
                "name": "egret-navy",
                "baseColor": "#10174c",
                "isActive": false
            }];
    }
    // Invoked in AppComponent and apply 'activatedTheme' on startup
    ThemeService.prototype.applyMatTheme = function (r, themeName) {
        this.renderer = r;
        this.activatedTheme = this.egretThemes[2];
        // *********** ONLY FOR DEMO **********
        this.setThemeFromQuery();
        // ************************************
        // this.changeTheme(themeName);
        this.renderer.addClass(this.document.body, themeName);
    };
    ThemeService.prototype.changeTheme = function (prevTheme, themeName) {
        this.renderer.removeClass(this.document.body, prevTheme);
        this.renderer.addClass(this.document.body, themeName);
        this.flipActiveFlag(themeName);
    };
    ThemeService.prototype.flipActiveFlag = function (themeName) {
        var _this = this;
        this.egretThemes.forEach(function (t) {
            t.isActive = false;
            if (t.name === themeName) {
                t.isActive = true;
                _this.activatedTheme = t;
            }
        });
    };
    // *********** ONLY FOR DEMO **********
    ThemeService.prototype.setThemeFromQuery = function () {
        var themeStr = getQueryParam('theme');
        try {
            this.activatedTheme = JSON.parse(themeStr);
            this.flipActiveFlag(this.activatedTheme.name);
        }
        catch (e) {
        }
    };
    ThemeService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] }
    ]; };
    ThemeService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__param(0, Inject(DOCUMENT)),
        tslib_1.__metadata("design:paramtypes", [Object])
    ], ThemeService);
    return ThemeService;
}());
export { ThemeService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGhlbWUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL3RoZW1lLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFhLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMzQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFTdEQ7SUFxQkMsc0JBQXNDLFFBQWE7UUFBYixhQUFRLEdBQVIsUUFBUSxDQUFLO1FBcEI1QyxnQkFBVyxHQUFhLENBQUM7Z0JBQy9CLE1BQU0sRUFBRSxtQkFBbUI7Z0JBQzNCLFdBQVcsRUFBRSxTQUFTO2dCQUN0QixVQUFVLEVBQUUsS0FBSzthQUNqQixFQUFFO2dCQUNGLE1BQU0sRUFBRSxpQkFBaUI7Z0JBQ3pCLFdBQVcsRUFBRSxTQUFTO2dCQUN0QixVQUFVLEVBQUUsS0FBSzthQUNqQixFQUFFO2dCQUNGLE1BQU0sRUFBRSxZQUFZO2dCQUNwQixXQUFXLEVBQUUsU0FBUztnQkFDdEIsVUFBVSxFQUFFLElBQUk7YUFDaEIsRUFBRTtnQkFDRixNQUFNLEVBQUUsWUFBWTtnQkFDcEIsV0FBVyxFQUFFLFNBQVM7Z0JBQ3RCLFVBQVUsRUFBRSxLQUFLO2FBQ2pCLENBQUMsQ0FBQztJQUtILENBQUM7SUFFRCxnRUFBZ0U7SUFDaEUsb0NBQWEsR0FBYixVQUFjLENBQVksRUFBRSxTQUFpQjtRQUM1QyxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQztRQUVsQixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFMUMsdUNBQXVDO1FBQ3ZDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQ3pCLHVDQUF1QztRQUV2QywrQkFBK0I7UUFDL0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFFdkQsQ0FBQztJQUVELGtDQUFXLEdBQVgsVUFBWSxTQUFTLEVBQUUsU0FBaUI7UUFDdkMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDekQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRUQscUNBQWMsR0FBZCxVQUFlLFNBQWlCO1FBQWhDLGlCQVFDO1FBUEEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1lBQ25CLElBQUksQ0FBQyxDQUFDLElBQUksS0FBSyxTQUFTLEVBQUU7Z0JBQ3pCLENBQUMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2dCQUNsQixLQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQzthQUN4QjtRQUNGLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUVELHVDQUF1QztJQUN2Qyx3Q0FBaUIsR0FBakI7UUFDQyxJQUFJLFFBQVEsR0FBRyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdEMsSUFBSTtZQUNILElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMzQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDOUM7UUFBQyxPQUFPLENBQUMsRUFBRTtTQUNYO0lBQ0YsQ0FBQzs7Z0RBMUNZLE1BQU0sU0FBQyxRQUFROztJQXJCaEIsWUFBWTtRQUR4QixVQUFVLEVBQUU7UUFzQkMsbUJBQUEsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFBOztPQXJCakIsWUFBWSxDQWdFeEI7SUFBRCxtQkFBQztDQUFBLEFBaEVELElBZ0VDO1NBaEVZLFlBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUsIFJlbmRlcmVyMiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBET0NVTUVOVCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IGdldFF1ZXJ5UGFyYW0gfSBmcm9tICcuLi9oZWxwZXJzL3VybC5oZWxwZXInO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBJVGhlbWUge1xyXG5cdG5hbWU6IHN0cmluZyxcclxuXHRiYXNlQ29sb3I/OiBzdHJpbmcsXHJcblx0aXNBY3RpdmU/OiBib29sZWFuXHJcbn1cclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFRoZW1lU2VydmljZSB7XHJcblx0cHVibGljIGVncmV0VGhlbWVzOiBJVGhlbWVbXSA9IFt7XHJcblx0XHRcIm5hbWVcIjogXCJlZ3JldC1kYXJrLXB1cnBsZVwiLFxyXG5cdFx0XCJiYXNlQ29sb3JcIjogXCIjOWMyN2IwXCIsXHJcblx0XHRcImlzQWN0aXZlXCI6IGZhbHNlXHJcblx0fSwge1xyXG5cdFx0XCJuYW1lXCI6IFwiZWdyZXQtZGFyay1waW5rXCIsXHJcblx0XHRcImJhc2VDb2xvclwiOiBcIiNlOTFlNjNcIixcclxuXHRcdFwiaXNBY3RpdmVcIjogZmFsc2VcclxuXHR9LCB7XHJcblx0XHRcIm5hbWVcIjogXCJlZ3JldC1ibHVlXCIsXHJcblx0XHRcImJhc2VDb2xvclwiOiBcIiMwM2E5ZjRcIixcclxuXHRcdFwiaXNBY3RpdmVcIjogdHJ1ZVxyXG5cdH0sIHtcclxuXHRcdFwibmFtZVwiOiBcImVncmV0LW5hdnlcIixcclxuXHRcdFwiYmFzZUNvbG9yXCI6IFwiIzEwMTc0Y1wiLFxyXG5cdFx0XCJpc0FjdGl2ZVwiOiBmYWxzZVxyXG5cdH1dO1xyXG5cdHB1YmxpYyBhY3RpdmF0ZWRUaGVtZTogSVRoZW1lO1xyXG5cdHByaXZhdGUgcmVuZGVyZXI6IFJlbmRlcmVyMjtcclxuXHJcblx0Y29uc3RydWN0b3IoQEluamVjdChET0NVTUVOVCkgcHJpdmF0ZSBkb2N1bWVudDogYW55KSB7XHJcblx0fVxyXG5cclxuXHQvLyBJbnZva2VkIGluIEFwcENvbXBvbmVudCBhbmQgYXBwbHkgJ2FjdGl2YXRlZFRoZW1lJyBvbiBzdGFydHVwXHJcblx0YXBwbHlNYXRUaGVtZShyOiBSZW5kZXJlcjIsIHRoZW1lTmFtZTogc3RyaW5nKSB7XHJcblx0XHR0aGlzLnJlbmRlcmVyID0gcjtcclxuXHJcblx0XHR0aGlzLmFjdGl2YXRlZFRoZW1lID0gdGhpcy5lZ3JldFRoZW1lc1syXTtcclxuXHJcblx0XHQvLyAqKioqKioqKioqKiBPTkxZIEZPUiBERU1PICoqKioqKioqKipcclxuXHRcdHRoaXMuc2V0VGhlbWVGcm9tUXVlcnkoKTtcclxuXHRcdC8vICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG5cclxuXHRcdC8vIHRoaXMuY2hhbmdlVGhlbWUodGhlbWVOYW1lKTtcclxuXHRcdHRoaXMucmVuZGVyZXIuYWRkQ2xhc3ModGhpcy5kb2N1bWVudC5ib2R5LCB0aGVtZU5hbWUpO1xyXG5cclxuXHR9XHJcblxyXG5cdGNoYW5nZVRoZW1lKHByZXZUaGVtZSwgdGhlbWVOYW1lOiBzdHJpbmcpIHtcclxuXHRcdHRoaXMucmVuZGVyZXIucmVtb3ZlQ2xhc3ModGhpcy5kb2N1bWVudC5ib2R5LCBwcmV2VGhlbWUpO1xyXG5cdFx0dGhpcy5yZW5kZXJlci5hZGRDbGFzcyh0aGlzLmRvY3VtZW50LmJvZHksIHRoZW1lTmFtZSk7XHJcblx0XHR0aGlzLmZsaXBBY3RpdmVGbGFnKHRoZW1lTmFtZSk7XHJcblx0fVxyXG5cclxuXHRmbGlwQWN0aXZlRmxhZyh0aGVtZU5hbWU6IHN0cmluZykge1xyXG5cdFx0dGhpcy5lZ3JldFRoZW1lcy5mb3JFYWNoKCh0KSA9PiB7XHJcblx0XHRcdHQuaXNBY3RpdmUgPSBmYWxzZTtcclxuXHRcdFx0aWYgKHQubmFtZSA9PT0gdGhlbWVOYW1lKSB7XHJcblx0XHRcdFx0dC5pc0FjdGl2ZSA9IHRydWU7XHJcblx0XHRcdFx0dGhpcy5hY3RpdmF0ZWRUaGVtZSA9IHQ7XHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cdH1cclxuXHJcblx0Ly8gKioqKioqKioqKiogT05MWSBGT1IgREVNTyAqKioqKioqKioqXHJcblx0c2V0VGhlbWVGcm9tUXVlcnkoKSB7XHJcblx0XHRsZXQgdGhlbWVTdHIgPSBnZXRRdWVyeVBhcmFtKCd0aGVtZScpO1xyXG5cdFx0dHJ5IHtcclxuXHRcdFx0dGhpcy5hY3RpdmF0ZWRUaGVtZSA9IEpTT04ucGFyc2UodGhlbWVTdHIpO1xyXG5cdFx0XHR0aGlzLmZsaXBBY3RpdmVGbGFnKHRoaXMuYWN0aXZhdGVkVGhlbWUubmFtZSk7XHJcblx0XHR9IGNhdGNoIChlKSB7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiJdfQ==