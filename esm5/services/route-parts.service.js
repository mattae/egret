import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Params, Router } from "@angular/router";
var RoutePartsService = /** @class */ (function () {
    function RoutePartsService(router) {
        this.router = router;
    }
    RoutePartsService.prototype.ngOnInit = function () {
    };
    RoutePartsService.prototype.generateRouteParts = function (snapshot) {
        var routeParts = [];
        if (snapshot) {
            if (snapshot.firstChild) {
                routeParts = routeParts.concat(this.generateRouteParts(snapshot.firstChild));
            }
            if (snapshot.data['title'] && snapshot.url.length) {
                // console.log(snapshot.data['title'], snapshot.url)
                routeParts.push({
                    title: snapshot.data['title'],
                    breadcrumb: snapshot.data['breadcrumb'],
                    url: snapshot.url[0].path,
                    urlSegments: snapshot.url,
                    params: snapshot.params
                });
            }
        }
        return routeParts;
    };
    RoutePartsService.ctorParameters = function () { return [
        { type: Router }
    ]; };
    RoutePartsService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [Router])
    ], RoutePartsService);
    return RoutePartsService;
}());
export { RoutePartsService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm91dGUtcGFydHMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL3JvdXRlLXBhcnRzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQVd6RTtJQUdDLDJCQUFvQixNQUFjO1FBQWQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtJQUNsQyxDQUFDO0lBRUQsb0NBQVEsR0FBUjtJQUNBLENBQUM7SUFFRCw4Q0FBa0IsR0FBbEIsVUFBbUIsUUFBZ0M7UUFDbEQsSUFBSSxVQUFVLEdBQWlCLEVBQUUsQ0FBQztRQUNsQyxJQUFJLFFBQVEsRUFBRTtZQUNiLElBQUksUUFBUSxDQUFDLFVBQVUsRUFBRTtnQkFDeEIsVUFBVSxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO2FBQzdFO1lBQ0QsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFO2dCQUNsRCxvREFBb0Q7Z0JBQ3BELFVBQVUsQ0FBQyxJQUFJLENBQUM7b0JBQ2YsS0FBSyxFQUFFLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO29CQUM3QixVQUFVLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7b0JBQ3ZDLEdBQUcsRUFBRSxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7b0JBQ3pCLFdBQVcsRUFBRSxRQUFRLENBQUMsR0FBRztvQkFDekIsTUFBTSxFQUFFLFFBQVEsQ0FBQyxNQUFNO2lCQUN2QixDQUFDLENBQUM7YUFDSDtTQUNEO1FBQ0QsT0FBTyxVQUFVLENBQUM7SUFDbkIsQ0FBQzs7Z0JBeEIyQixNQUFNOztJQUh0QixpQkFBaUI7UUFEN0IsVUFBVSxFQUFFO2lEQUlnQixNQUFNO09BSHRCLGlCQUFpQixDQTRCN0I7SUFBRCx3QkFBQztDQUFBLEFBNUJELElBNEJDO1NBNUJZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgUGFyYW1zLCBSb3V0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5pbnRlcmZhY2UgSVJvdXRlUGFydCB7XHJcblx0dGl0bGU6IHN0cmluZyxcclxuXHRicmVhZGNydW1iOiBzdHJpbmcsXHJcblx0cGFyYW1zPzogUGFyYW1zLFxyXG5cdHVybDogc3RyaW5nLFxyXG5cdHVybFNlZ21lbnRzOiBhbnlbXVxyXG59XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBSb3V0ZVBhcnRzU2VydmljZSB7XHJcblx0cHVibGljIHJvdXRlUGFydHM6IElSb3V0ZVBhcnRbXTtcclxuXHJcblx0Y29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikge1xyXG5cdH1cclxuXHJcblx0bmdPbkluaXQoKSB7XHJcblx0fVxyXG5cclxuXHRnZW5lcmF0ZVJvdXRlUGFydHMoc25hcHNob3Q6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QpOiBJUm91dGVQYXJ0W10ge1xyXG5cdFx0bGV0IHJvdXRlUGFydHMgPSA8SVJvdXRlUGFydFtdPltdO1xyXG5cdFx0aWYgKHNuYXBzaG90KSB7XHJcblx0XHRcdGlmIChzbmFwc2hvdC5maXJzdENoaWxkKSB7XHJcblx0XHRcdFx0cm91dGVQYXJ0cyA9IHJvdXRlUGFydHMuY29uY2F0KHRoaXMuZ2VuZXJhdGVSb3V0ZVBhcnRzKHNuYXBzaG90LmZpcnN0Q2hpbGQpKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRpZiAoc25hcHNob3QuZGF0YVsndGl0bGUnXSAmJiBzbmFwc2hvdC51cmwubGVuZ3RoKSB7XHJcblx0XHRcdFx0Ly8gY29uc29sZS5sb2coc25hcHNob3QuZGF0YVsndGl0bGUnXSwgc25hcHNob3QudXJsKVxyXG5cdFx0XHRcdHJvdXRlUGFydHMucHVzaCh7XHJcblx0XHRcdFx0XHR0aXRsZTogc25hcHNob3QuZGF0YVsndGl0bGUnXSxcclxuXHRcdFx0XHRcdGJyZWFkY3J1bWI6IHNuYXBzaG90LmRhdGFbJ2JyZWFkY3J1bWInXSxcclxuXHRcdFx0XHRcdHVybDogc25hcHNob3QudXJsWzBdLnBhdGgsXHJcblx0XHRcdFx0XHR1cmxTZWdtZW50czogc25hcHNob3QudXJsLFxyXG5cdFx0XHRcdFx0cGFyYW1zOiBzbmFwc2hvdC5wYXJhbXNcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIHJvdXRlUGFydHM7XHJcblx0fVxyXG59XHJcbiJdfQ==