import * as tslib_1 from "tslib";
import { Pipe } from "@angular/core";
var ExcerptPipe = /** @class */ (function () {
    function ExcerptPipe() {
    }
    ExcerptPipe.prototype.transform = function (text, limit) {
        if (limit === void 0) { limit = 5; }
        if (text.length <= limit)
            return text;
        return text.substring(0, limit) + '...';
    };
    ExcerptPipe = tslib_1.__decorate([
        Pipe({ name: 'excerpt' })
    ], ExcerptPipe);
    return ExcerptPipe;
}());
export { ExcerptPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhjZXJwdC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsicGlwZXMvZXhjZXJwdC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUdwRDtJQUFBO0lBTUEsQ0FBQztJQUxHLCtCQUFTLEdBQVQsVUFBVSxJQUFZLEVBQUUsS0FBaUI7UUFBakIsc0JBQUEsRUFBQSxTQUFpQjtRQUNyQyxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksS0FBSztZQUNwQixPQUFPLElBQUksQ0FBQztRQUNoQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxHQUFHLEtBQUssQ0FBQztJQUM1QyxDQUFDO0lBTFEsV0FBVztRQUR2QixJQUFJLENBQUMsRUFBQyxJQUFJLEVBQUUsU0FBUyxFQUFDLENBQUM7T0FDWCxXQUFXLENBTXZCO0lBQUQsa0JBQUM7Q0FBQSxBQU5ELElBTUM7U0FOWSxXQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcblxyXG5AUGlwZSh7bmFtZTogJ2V4Y2VycHQnfSlcclxuZXhwb3J0IGNsYXNzIEV4Y2VycHRQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XHJcbiAgICB0cmFuc2Zvcm0odGV4dDogc3RyaW5nLCBsaW1pdDogbnVtYmVyID0gNSkge1xyXG4gICAgICAgIGlmICh0ZXh0Lmxlbmd0aCA8PSBsaW1pdClcclxuICAgICAgICAgICAgcmV0dXJuIHRleHQ7XHJcbiAgICAgICAgcmV0dXJuIHRleHQuc3Vic3RyaW5nKDAsIGxpbWl0KSArICcuLi4nO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==