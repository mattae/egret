import * as tslib_1 from "tslib";
import { Pipe } from "@angular/core";
var GetValueByKeyPipe = /** @class */ (function () {
    function GetValueByKeyPipe() {
    }
    GetValueByKeyPipe.prototype.transform = function (value, id, property) {
        var filteredObj = value.find(function (item) {
            if (item.id !== undefined) {
                return item.id === id;
            }
            return false;
        });
        if (filteredObj) {
            return filteredObj[property];
        }
    };
    GetValueByKeyPipe = tslib_1.__decorate([
        Pipe({
            name: "getValueByKey",
            pure: false
        })
    ], GetValueByKeyPipe);
    return GetValueByKeyPipe;
}());
export { GetValueByKeyPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2V0LXZhbHVlLWJ5LWtleS5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsicGlwZXMvZ2V0LXZhbHVlLWJ5LWtleS5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQU1wRDtJQUFBO0lBY0EsQ0FBQztJQWJDLHFDQUFTLEdBQVQsVUFBVSxLQUFZLEVBQUUsRUFBVSxFQUFFLFFBQWdCO1FBQ2xELElBQU0sV0FBVyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBQSxJQUFJO1lBQ2pDLElBQUksSUFBSSxDQUFDLEVBQUUsS0FBSyxTQUFTLEVBQUU7Z0JBQ3pCLE9BQU8sSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLENBQUM7YUFDdkI7WUFFRCxPQUFPLEtBQUssQ0FBQztRQUNmLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxXQUFXLEVBQUU7WUFDZixPQUFPLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUM5QjtJQUNILENBQUM7SUFiVSxpQkFBaUI7UUFKN0IsSUFBSSxDQUFDO1lBQ0osSUFBSSxFQUFFLGVBQWU7WUFDckIsSUFBSSxFQUFFLEtBQUs7U0FDWixDQUFDO09BQ1csaUJBQWlCLENBYzdCO0lBQUQsd0JBQUM7Q0FBQSxBQWRELElBY0M7U0FkWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuXHJcbkBQaXBlKHtcclxuICBuYW1lOiBcImdldFZhbHVlQnlLZXlcIixcclxuICBwdXJlOiBmYWxzZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgR2V0VmFsdWVCeUtleVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuICB0cmFuc2Zvcm0odmFsdWU6IGFueVtdLCBpZDogbnVtYmVyLCBwcm9wZXJ0eTogc3RyaW5nKTogYW55IHtcclxuICAgIGNvbnN0IGZpbHRlcmVkT2JqID0gdmFsdWUuZmluZChpdGVtID0+IHtcclxuICAgICAgaWYgKGl0ZW0uaWQgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgIHJldHVybiBpdGVtLmlkID09PSBpZDtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfSk7XHJcblxyXG4gICAgaWYgKGZpbHRlcmVkT2JqKSB7XHJcbiAgICAgIHJldHVybiBmaWx0ZXJlZE9ialtwcm9wZXJ0eV07XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==