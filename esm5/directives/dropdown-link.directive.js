import * as tslib_1 from "tslib";
import { Directive, HostBinding, Inject, Input } from '@angular/core';
import { AppDropdownDirective } from './dropdown.directive';
var DropdownLinkDirective = /** @class */ (function () {
    function DropdownLinkDirective(nav) {
        this.nav = nav;
    }
    Object.defineProperty(DropdownLinkDirective.prototype, "open", {
        get: function () {
            return this._open;
        },
        set: function (value) {
            this._open = value;
            if (value) {
                this.nav.closeOtherLinks(this);
            }
        },
        enumerable: true,
        configurable: true
    });
    DropdownLinkDirective.prototype.ngOnInit = function () {
        this.nav.addLink(this);
    };
    DropdownLinkDirective.prototype.ngOnDestroy = function () {
        this.nav.removeGroup(this);
    };
    DropdownLinkDirective.prototype.toggle = function () {
        this.open = !this.open;
    };
    DropdownLinkDirective.ctorParameters = function () { return [
        { type: AppDropdownDirective, decorators: [{ type: Inject, args: [AppDropdownDirective,] }] }
    ]; };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], DropdownLinkDirective.prototype, "group", void 0);
    tslib_1.__decorate([
        HostBinding('class.open'),
        Input(),
        tslib_1.__metadata("design:type", Boolean),
        tslib_1.__metadata("design:paramtypes", [Boolean])
    ], DropdownLinkDirective.prototype, "open", null);
    DropdownLinkDirective = tslib_1.__decorate([
        Directive({
            selector: '[appDropdownLink]'
        }),
        tslib_1.__param(0, Inject(AppDropdownDirective)),
        tslib_1.__metadata("design:paramtypes", [AppDropdownDirective])
    ], DropdownLinkDirective);
    return DropdownLinkDirective;
}());
export { DropdownLinkDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24tbGluay5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvZWdyZXQvIiwic291cmNlcyI6WyJkaXJlY3RpdmVzL2Ryb3Bkb3duLWxpbmsuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXRFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBSzVEO0lBb0JDLCtCQUFpRCxHQUF5QjtRQUN6RSxJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztJQUNoQixDQUFDO0lBaEJELHNCQUFJLHVDQUFJO2FBQVI7WUFDQyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDbkIsQ0FBQzthQUVELFVBQVMsS0FBYztZQUN0QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztZQUNuQixJQUFJLEtBQUssRUFBRTtnQkFDVixJQUFJLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUMvQjtRQUNGLENBQUM7OztPQVBBO0lBZ0JNLHdDQUFRLEdBQWY7UUFDQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN4QixDQUFDO0lBRU0sMkNBQVcsR0FBbEI7UUFDQyxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRU0sc0NBQU0sR0FBYjtRQUNDLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ3hCLENBQUM7O2dCQWRxRCxvQkFBb0IsdUJBQXRELE1BQU0sU0FBQyxvQkFBb0I7O0lBbEJ0QztRQUFSLEtBQUssRUFBRTs7d0RBQW1CO0lBSTNCO1FBRkMsV0FBVyxDQUFDLFlBQVksQ0FBQztRQUN6QixLQUFLLEVBQUU7OztxREFHUDtJQVJXLHFCQUFxQjtRQUhqQyxTQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsbUJBQW1CO1NBQzdCLENBQUM7UUFxQm1CLG1CQUFBLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFBO2lEQUFNLG9CQUFvQjtPQXBCOUQscUJBQXFCLENBb0NqQztJQUFELDRCQUFDO0NBQUEsQUFwQ0QsSUFvQ0M7U0FwQ1kscUJBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBIb3N0QmluZGluZywgSW5qZWN0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgQXBwRHJvcGRvd25EaXJlY3RpdmUgfSBmcm9tICcuL2Ryb3Bkb3duLmRpcmVjdGl2ZSc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuXHRzZWxlY3RvcjogJ1thcHBEcm9wZG93bkxpbmtdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRHJvcGRvd25MaW5rRGlyZWN0aXZlIHtcclxuXHJcblx0QElucHV0KCkgcHVibGljIGdyb3VwOiBhbnk7XHJcblxyXG5cdEBIb3N0QmluZGluZygnY2xhc3Mub3BlbicpXHJcblx0QElucHV0KClcclxuXHRnZXQgb3BlbigpOiBib29sZWFuIHtcclxuXHRcdHJldHVybiB0aGlzLl9vcGVuO1xyXG5cdH1cclxuXHJcblx0c2V0IG9wZW4odmFsdWU6IGJvb2xlYW4pIHtcclxuXHRcdHRoaXMuX29wZW4gPSB2YWx1ZTtcclxuXHRcdGlmICh2YWx1ZSkge1xyXG5cdFx0XHR0aGlzLm5hdi5jbG9zZU90aGVyTGlua3ModGhpcyk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRwcm90ZWN0ZWQgX29wZW46IGJvb2xlYW47XHJcblx0cHJvdGVjdGVkIG5hdjogQXBwRHJvcGRvd25EaXJlY3RpdmU7XHJcblxyXG5cdHB1YmxpYyBjb25zdHJ1Y3RvcihASW5qZWN0KEFwcERyb3Bkb3duRGlyZWN0aXZlKSBuYXY6IEFwcERyb3Bkb3duRGlyZWN0aXZlKSB7XHJcblx0XHR0aGlzLm5hdiA9IG5hdjtcclxuXHR9XHJcblxyXG5cdHB1YmxpYyBuZ09uSW5pdCgpOiBhbnkge1xyXG5cdFx0dGhpcy5uYXYuYWRkTGluayh0aGlzKTtcclxuXHR9XHJcblxyXG5cdHB1YmxpYyBuZ09uRGVzdHJveSgpOiBhbnkge1xyXG5cdFx0dGhpcy5uYXYucmVtb3ZlR3JvdXAodGhpcyk7XHJcblx0fVxyXG5cclxuXHRwdWJsaWMgdG9nZ2xlKCk6IGFueSB7XHJcblx0XHR0aGlzLm9wZW4gPSAhdGhpcy5vcGVuO1xyXG5cdH1cclxuXHJcbn1cclxuIl19