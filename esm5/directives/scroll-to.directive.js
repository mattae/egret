import * as tslib_1 from "tslib";
import { Attribute, Directive, ElementRef, HostListener, OnInit } from '@angular/core';
var ScrollToDirective = /** @class */ (function () {
    function ScrollToDirective(elmID, el) {
        this.elmID = elmID;
        this.el = el;
    }
    ScrollToDirective_1 = ScrollToDirective;
    ScrollToDirective.prototype.ngOnInit = function () {
    };
    ScrollToDirective.currentYPosition = function () {
        // Firefox, Chrome, Opera, Safari
        if (self.pageYOffset)
            return self.pageYOffset;
        // Internet Explorer 6 - standards mode
        if (document.documentElement && document.documentElement.scrollTop)
            return document.documentElement.scrollTop;
        // Internet Explorer 6, 7 and 8
        if (document.body.scrollTop)
            return document.body.scrollTop;
        return 0;
    };
    ;
    ScrollToDirective.elmYPosition = function (eID) {
        var elm = document.getElementById(eID);
        var y = elm.offsetTop;
        var node = elm;
        while (node.offsetParent && node.offsetParent != document.body) {
            node = node.offsetParent;
            y += node.offsetTop;
        }
        return y;
    };
    ;
    ScrollToDirective.prototype.smoothScroll = function () {
        var i;
        if (!this.elmID)
            return;
        var startY = ScrollToDirective_1.currentYPosition();
        var stopY = ScrollToDirective_1.elmYPosition(this.elmID);
        var distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY);
            return;
        }
        var speed = Math.round(distance / 50);
        if (speed >= 20)
            speed = 20;
        var step = Math.round(distance / 25);
        var leapY = stopY > startY ? startY + step : startY - step;
        var timer = 0;
        if (stopY > startY) {
            for (i = startY; i < stopY; i += step) {
                setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                leapY += step;
                if (leapY > stopY)
                    leapY = stopY;
                timer++;
            }
            return;
        }
        for (i = startY; i > stopY; i -= step) {
            setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
            leapY -= step;
            if (leapY < stopY)
                leapY = stopY;
            timer++;
        }
        return false;
    };
    ;
    var ScrollToDirective_1;
    ScrollToDirective.ctorParameters = function () { return [
        { type: String, decorators: [{ type: Attribute, args: ['scrollTo',] }] },
        { type: ElementRef }
    ]; };
    tslib_1.__decorate([
        HostListener('click', ['$event']),
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", []),
        tslib_1.__metadata("design:returntype", void 0)
    ], ScrollToDirective.prototype, "smoothScroll", null);
    ScrollToDirective = ScrollToDirective_1 = tslib_1.__decorate([
        Directive({ selector: '[scrollTo]' }),
        tslib_1.__param(0, Attribute('scrollTo')),
        tslib_1.__metadata("design:paramtypes", [String, ElementRef])
    ], ScrollToDirective);
    return ScrollToDirective;
}());
export { ScrollToDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Nyb2xsLXRvLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvc2Nyb2xsLXRvLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHdkY7SUFDQywyQkFBMEMsS0FBYSxFQUFVLEVBQWM7UUFBckMsVUFBSyxHQUFMLEtBQUssQ0FBUTtRQUFVLE9BQUUsR0FBRixFQUFFLENBQVk7SUFDL0UsQ0FBQzswQkFGVyxpQkFBaUI7SUFJN0Isb0NBQVEsR0FBUjtJQUNBLENBQUM7SUFFTSxrQ0FBZ0IsR0FBdkI7UUFDQyxpQ0FBaUM7UUFDakMsSUFBSSxJQUFJLENBQUMsV0FBVztZQUFFLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUM5Qyx1Q0FBdUM7UUFDdkMsSUFBSSxRQUFRLENBQUMsZUFBZSxJQUFJLFFBQVEsQ0FBQyxlQUFlLENBQUMsU0FBUztZQUNqRSxPQUFPLFFBQVEsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDO1FBQzNDLCtCQUErQjtRQUMvQixJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUztZQUFFLE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDNUQsT0FBTyxDQUFDLENBQUM7SUFDVixDQUFDO0lBQUEsQ0FBQztJQUVLLDhCQUFZLEdBQW5CLFVBQW9CLEdBQUc7UUFDdEIsSUFBTSxHQUFHLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsU0FBUyxDQUFDO1FBQ3RCLElBQUksSUFBSSxHQUFRLEdBQUcsQ0FBQztRQUNwQixPQUFPLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFlBQVksSUFBSSxRQUFRLENBQUMsSUFBSSxFQUFFO1lBQy9ELElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ3pCLENBQUMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDO1NBQ3BCO1FBQ0QsT0FBTyxDQUFDLENBQUM7SUFDVixDQUFDO0lBQUEsQ0FBQztJQUdGLHdDQUFZLEdBQVo7UUFDQyxJQUFJLENBQUMsQ0FBQztRQUNOLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSztZQUNkLE9BQU87UUFDUixJQUFNLE1BQU0sR0FBRyxtQkFBaUIsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQ3BELElBQU0sS0FBSyxHQUFHLG1CQUFpQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekQsSUFBTSxRQUFRLEdBQUcsS0FBSyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNsRSxJQUFJLFFBQVEsR0FBRyxHQUFHLEVBQUU7WUFDbkIsUUFBUSxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNuQixPQUFPO1NBQ1A7UUFDRCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUMsQ0FBQztRQUN0QyxJQUFJLEtBQUssSUFBSSxFQUFFO1lBQUUsS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUM1QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUMsQ0FBQztRQUNyQyxJQUFJLEtBQUssR0FBRyxLQUFLLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQzNELElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNkLElBQUksS0FBSyxHQUFHLE1BQU0sRUFBRTtZQUNuQixLQUFLLENBQUMsR0FBRyxNQUFNLEVBQUUsQ0FBQyxHQUFHLEtBQUssRUFBRSxDQUFDLElBQUksSUFBSSxFQUFFO2dCQUN0QyxVQUFVLENBQUMscUJBQXFCLEdBQUcsS0FBSyxHQUFHLEdBQUcsRUFBRSxLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUM7Z0JBQy9ELEtBQUssSUFBSSxJQUFJLENBQUM7Z0JBQ2QsSUFBSSxLQUFLLEdBQUcsS0FBSztvQkFBRSxLQUFLLEdBQUcsS0FBSyxDQUFDO2dCQUNqQyxLQUFLLEVBQUUsQ0FBQzthQUNSO1lBQ0QsT0FBTztTQUNQO1FBQ0QsS0FBSyxDQUFDLEdBQUcsTUFBTSxFQUFFLENBQUMsR0FBRyxLQUFLLEVBQUUsQ0FBQyxJQUFJLElBQUksRUFBRTtZQUN0QyxVQUFVLENBQUMscUJBQXFCLEdBQUcsS0FBSyxHQUFHLEdBQUcsRUFBRSxLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUM7WUFDL0QsS0FBSyxJQUFJLElBQUksQ0FBQztZQUNkLElBQUksS0FBSyxHQUFHLEtBQUs7Z0JBQUUsS0FBSyxHQUFHLEtBQUssQ0FBQztZQUNqQyxLQUFLLEVBQUUsQ0FBQztTQUNSO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDZCxDQUFDO0lBQUEsQ0FBQzs7OzZDQTdEVyxTQUFTLFNBQUMsVUFBVTtnQkFBb0MsVUFBVTs7SUE2Qi9FO1FBREMsWUFBWSxDQUFDLE9BQU8sRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDOzs7O3lEQWlDakM7SUE5RFcsaUJBQWlCO1FBRDdCLFNBQVMsQ0FBQyxFQUFDLFFBQVEsRUFBRSxZQUFZLEVBQUMsQ0FBQztRQUV0QixtQkFBQSxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUE7eURBQW1DLFVBQVU7T0FEbkUsaUJBQWlCLENBK0Q3QjtJQUFELHdCQUFDO0NBQUEsQUEvREQsSUErREM7U0EvRFksaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQXR0cmlidXRlLCBEaXJlY3RpdmUsIEVsZW1lbnRSZWYsIEhvc3RMaXN0ZW5lciwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ARGlyZWN0aXZlKHtzZWxlY3RvcjogJ1tzY3JvbGxUb10nfSlcclxuZXhwb3J0IGNsYXNzIFNjcm9sbFRvRGlyZWN0aXZlIGltcGxlbWVudHMgT25Jbml0IHtcclxuXHRjb25zdHJ1Y3RvcihAQXR0cmlidXRlKCdzY3JvbGxUbycpIHB1YmxpYyBlbG1JRDogc3RyaW5nLCBwcml2YXRlIGVsOiBFbGVtZW50UmVmKSB7XHJcblx0fVxyXG5cclxuXHRuZ09uSW5pdCgpIHtcclxuXHR9XHJcblxyXG5cdHN0YXRpYyBjdXJyZW50WVBvc2l0aW9uKCkge1xyXG5cdFx0Ly8gRmlyZWZveCwgQ2hyb21lLCBPcGVyYSwgU2FmYXJpXHJcblx0XHRpZiAoc2VsZi5wYWdlWU9mZnNldCkgcmV0dXJuIHNlbGYucGFnZVlPZmZzZXQ7XHJcblx0XHQvLyBJbnRlcm5ldCBFeHBsb3JlciA2IC0gc3RhbmRhcmRzIG1vZGVcclxuXHRcdGlmIChkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQgJiYgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcClcclxuXHRcdFx0cmV0dXJuIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxUb3A7XHJcblx0XHQvLyBJbnRlcm5ldCBFeHBsb3JlciA2LCA3IGFuZCA4XHJcblx0XHRpZiAoZG9jdW1lbnQuYm9keS5zY3JvbGxUb3ApIHJldHVybiBkb2N1bWVudC5ib2R5LnNjcm9sbFRvcDtcclxuXHRcdHJldHVybiAwO1xyXG5cdH07XHJcblxyXG5cdHN0YXRpYyBlbG1ZUG9zaXRpb24oZUlEKSB7XHJcblx0XHRjb25zdCBlbG0gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChlSUQpO1xyXG5cdFx0bGV0IHkgPSBlbG0ub2Zmc2V0VG9wO1xyXG5cdFx0bGV0IG5vZGU6IGFueSA9IGVsbTtcclxuXHRcdHdoaWxlIChub2RlLm9mZnNldFBhcmVudCAmJiBub2RlLm9mZnNldFBhcmVudCAhPSBkb2N1bWVudC5ib2R5KSB7XHJcblx0XHRcdG5vZGUgPSBub2RlLm9mZnNldFBhcmVudDtcclxuXHRcdFx0eSArPSBub2RlLm9mZnNldFRvcDtcclxuXHRcdH1cclxuXHRcdHJldHVybiB5O1xyXG5cdH07XHJcblxyXG5cdEBIb3N0TGlzdGVuZXIoJ2NsaWNrJywgWyckZXZlbnQnXSlcclxuXHRzbW9vdGhTY3JvbGwoKSB7XHJcblx0XHRsZXQgaTtcclxuXHRcdGlmICghdGhpcy5lbG1JRClcclxuXHRcdFx0cmV0dXJuO1xyXG5cdFx0Y29uc3Qgc3RhcnRZID0gU2Nyb2xsVG9EaXJlY3RpdmUuY3VycmVudFlQb3NpdGlvbigpO1xyXG5cdFx0Y29uc3Qgc3RvcFkgPSBTY3JvbGxUb0RpcmVjdGl2ZS5lbG1ZUG9zaXRpb24odGhpcy5lbG1JRCk7XHJcblx0XHRjb25zdCBkaXN0YW5jZSA9IHN0b3BZID4gc3RhcnRZID8gc3RvcFkgLSBzdGFydFkgOiBzdGFydFkgLSBzdG9wWTtcclxuXHRcdGlmIChkaXN0YW5jZSA8IDEwMCkge1xyXG5cdFx0XHRzY3JvbGxUbygwLCBzdG9wWSk7XHJcblx0XHRcdHJldHVybjtcclxuXHRcdH1cclxuXHRcdGxldCBzcGVlZCA9IE1hdGgucm91bmQoZGlzdGFuY2UgLyA1MCk7XHJcblx0XHRpZiAoc3BlZWQgPj0gMjApIHNwZWVkID0gMjA7XHJcblx0XHRsZXQgc3RlcCA9IE1hdGgucm91bmQoZGlzdGFuY2UgLyAyNSk7XHJcblx0XHRsZXQgbGVhcFkgPSBzdG9wWSA+IHN0YXJ0WSA/IHN0YXJ0WSArIHN0ZXAgOiBzdGFydFkgLSBzdGVwO1xyXG5cdFx0bGV0IHRpbWVyID0gMDtcclxuXHRcdGlmIChzdG9wWSA+IHN0YXJ0WSkge1xyXG5cdFx0XHRmb3IgKGkgPSBzdGFydFk7IGkgPCBzdG9wWTsgaSArPSBzdGVwKSB7XHJcblx0XHRcdFx0c2V0VGltZW91dChcIndpbmRvdy5zY3JvbGxUbygwLCBcIiArIGxlYXBZICsgXCIpXCIsIHRpbWVyICogc3BlZWQpO1xyXG5cdFx0XHRcdGxlYXBZICs9IHN0ZXA7XHJcblx0XHRcdFx0aWYgKGxlYXBZID4gc3RvcFkpIGxlYXBZID0gc3RvcFk7XHJcblx0XHRcdFx0dGltZXIrKztcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm47XHJcblx0XHR9XHJcblx0XHRmb3IgKGkgPSBzdGFydFk7IGkgPiBzdG9wWTsgaSAtPSBzdGVwKSB7XHJcblx0XHRcdHNldFRpbWVvdXQoXCJ3aW5kb3cuc2Nyb2xsVG8oMCwgXCIgKyBsZWFwWSArIFwiKVwiLCB0aW1lciAqIHNwZWVkKTtcclxuXHRcdFx0bGVhcFkgLT0gc3RlcDtcclxuXHRcdFx0aWYgKGxlYXBZIDwgc3RvcFkpIGxlYXBZID0gc3RvcFk7XHJcblx0XHRcdHRpbWVyKys7XHJcblx0XHR9XHJcblx0XHRyZXR1cm4gZmFsc2U7XHJcblx0fTtcclxufVxyXG4iXX0=