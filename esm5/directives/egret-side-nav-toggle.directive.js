import * as tslib_1 from "tslib";
import { Directive, Host, Optional, Self } from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { MatSidenav } from '@angular/material';
var EgretSideNavToggleDirective = /** @class */ (function () {
    function EgretSideNavToggleDirective(media, sideNav) {
        this.media = media;
        this.sideNav = sideNav;
    }
    EgretSideNavToggleDirective.prototype.ngOnInit = function () {
        this.initSideNav();
    };
    EgretSideNavToggleDirective.prototype.ngOnDestroy = function () {
        if (this.screenSizeWatcher) {
            this.screenSizeWatcher.unsubscribe();
        }
    };
    EgretSideNavToggleDirective.prototype.updateSidenav = function () {
        var self = this;
        setTimeout(function () {
            self.sideNav.opened = !self.isMobile;
            self.sideNav.mode = self.isMobile ? 'over' : 'side';
        });
    };
    EgretSideNavToggleDirective.prototype.initSideNav = function () {
        var _this = this;
        this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
        // console.log(this.isMobile)
        this.updateSidenav();
        this.screenSizeWatcher = this.media.asObservable().subscribe(function (change) {
            _this.isMobile = (change[0].mqAlias == 'xs') || (change[0].mqAlias == 'sm');
            _this.updateSidenav();
        });
    };
    EgretSideNavToggleDirective.ctorParameters = function () { return [
        { type: MediaObserver },
        { type: MatSidenav, decorators: [{ type: Host }, { type: Self }, { type: Optional }] }
    ]; };
    EgretSideNavToggleDirective = tslib_1.__decorate([
        Directive({
            selector: '[EgretSideNavToggle]'
        }),
        tslib_1.__param(1, Host()), tslib_1.__param(1, Self()), tslib_1.__param(1, Optional()),
        tslib_1.__metadata("design:paramtypes", [MediaObserver,
            MatSidenav])
    ], EgretSideNavToggleDirective);
    return EgretSideNavToggleDirective;
}());
export { EgretSideNavToggleDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWdyZXQtc2lkZS1uYXYtdG9nZ2xlLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvZWdyZXQtc2lkZS1uYXYtdG9nZ2xlLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQXFCLFFBQVEsRUFBRSxJQUFJLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkYsT0FBTyxFQUFFLFdBQVcsRUFBRSxhQUFhLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUVsRSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFNL0M7SUFJQyxxQ0FDUyxLQUFvQixFQUNPLE9BQW1CO1FBRDlDLFVBQUssR0FBTCxLQUFLLENBQWU7UUFDTyxZQUFPLEdBQVAsT0FBTyxDQUFZO0lBRXZELENBQUM7SUFFRCw4Q0FBUSxHQUFSO1FBQ0MsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ3BCLENBQUM7SUFFRCxpREFBVyxHQUFYO1FBQ0MsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDM0IsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsRUFBRSxDQUFBO1NBQ3BDO0lBQ0YsQ0FBQztJQUVELG1EQUFhLEdBQWI7UUFDQyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsVUFBVSxDQUFDO1lBQ1YsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ3JDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQ3JELENBQUMsQ0FBQyxDQUFBO0lBQ0gsQ0FBQztJQUVELGlEQUFXLEdBQVg7UUFBQSxpQkFRQztRQVBBLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdkUsNkJBQTZCO1FBQzdCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQyxNQUFxQjtZQUNsRixLQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLENBQUM7WUFDM0UsS0FBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3RCLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQzs7Z0JBL0JlLGFBQWE7Z0JBQ2dCLFVBQVUsdUJBQXJELElBQUksWUFBSSxJQUFJLFlBQUksUUFBUTs7SUFOZCwyQkFBMkI7UUFIdkMsU0FBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLHNCQUFzQjtTQUNoQyxDQUFDO1FBT0MsbUJBQUEsSUFBSSxFQUFFLENBQUEsRUFBRSxtQkFBQSxJQUFJLEVBQUUsQ0FBQSxFQUFFLG1CQUFBLFFBQVEsRUFBRSxDQUFBO2lEQURaLGFBQWE7WUFDZ0IsVUFBVTtPQU4zQywyQkFBMkIsQ0FzQ3ZDO0lBQUQsa0NBQUM7Q0FBQSxBQXRDRCxJQXNDQztTQXRDWSwyQkFBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEhvc3QsIE9uRGVzdHJveSwgT25Jbml0LCBPcHRpb25hbCwgU2VsZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNZWRpYUNoYW5nZSwgTWVkaWFPYnNlcnZlciB9IGZyb20gJ0Bhbmd1bGFyL2ZsZXgtbGF5b3V0JztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IE1hdFNpZGVuYXYgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcblxyXG5cclxuQERpcmVjdGl2ZSh7XHJcblx0c2VsZWN0b3I6ICdbRWdyZXRTaWRlTmF2VG9nZ2xlXSdcclxufSlcclxuZXhwb3J0IGNsYXNzIEVncmV0U2lkZU5hdlRvZ2dsZURpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuXHRpc01vYmlsZTtcclxuXHRzY3JlZW5TaXplV2F0Y2hlcjogU3Vic2NyaXB0aW9uO1xyXG5cclxuXHRjb25zdHJ1Y3RvcihcclxuXHRcdHByaXZhdGUgbWVkaWE6IE1lZGlhT2JzZXJ2ZXIsXHJcblx0XHRASG9zdCgpIEBTZWxmKCkgQE9wdGlvbmFsKCkgcHVibGljIHNpZGVOYXY6IE1hdFNpZGVuYXZcclxuXHQpIHtcclxuXHR9XHJcblxyXG5cdG5nT25Jbml0KCkge1xyXG5cdFx0dGhpcy5pbml0U2lkZU5hdigpO1xyXG5cdH1cclxuXHJcblx0bmdPbkRlc3Ryb3koKSB7XHJcblx0XHRpZiAodGhpcy5zY3JlZW5TaXplV2F0Y2hlcikge1xyXG5cdFx0XHR0aGlzLnNjcmVlblNpemVXYXRjaGVyLnVuc3Vic2NyaWJlKClcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHVwZGF0ZVNpZGVuYXYoKSB7XHJcblx0XHR2YXIgc2VsZiA9IHRoaXM7XHJcblx0XHRzZXRUaW1lb3V0KCgpID0+IHtcclxuXHRcdFx0c2VsZi5zaWRlTmF2Lm9wZW5lZCA9ICFzZWxmLmlzTW9iaWxlO1xyXG5cdFx0XHRzZWxmLnNpZGVOYXYubW9kZSA9IHNlbGYuaXNNb2JpbGUgPyAnb3ZlcicgOiAnc2lkZSc7XHJcblx0XHR9KVxyXG5cdH1cclxuXHJcblx0aW5pdFNpZGVOYXYoKSB7XHJcblx0XHR0aGlzLmlzTW9iaWxlID0gdGhpcy5tZWRpYS5pc0FjdGl2ZSgneHMnKSB8fCB0aGlzLm1lZGlhLmlzQWN0aXZlKCdzbScpO1xyXG5cdFx0Ly8gY29uc29sZS5sb2codGhpcy5pc01vYmlsZSlcclxuXHRcdHRoaXMudXBkYXRlU2lkZW5hdigpO1xyXG5cdFx0dGhpcy5zY3JlZW5TaXplV2F0Y2hlciA9IHRoaXMubWVkaWEuYXNPYnNlcnZhYmxlKCkuc3Vic2NyaWJlKChjaGFuZ2U6IE1lZGlhQ2hhbmdlW10pID0+IHtcclxuXHRcdFx0dGhpcy5pc01vYmlsZSA9IChjaGFuZ2VbMF0ubXFBbGlhcyA9PSAneHMnKSB8fCAoY2hhbmdlWzBdLm1xQWxpYXMgPT0gJ3NtJyk7XHJcblx0XHRcdHRoaXMudXBkYXRlU2lkZW5hdigpO1xyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxufVxyXG4iXX0=