import * as tslib_1 from "tslib";
import { Directive, HostListener, Inject } from '@angular/core';
import { DropdownLinkDirective } from './dropdown-link.directive';
var DropdownAnchorDirective = /** @class */ (function () {
    function DropdownAnchorDirective(navlink) {
        this.navlink = navlink;
    }
    DropdownAnchorDirective.prototype.onClick = function (e) {
        this.navlink.toggle();
    };
    DropdownAnchorDirective.ctorParameters = function () { return [
        { type: DropdownLinkDirective, decorators: [{ type: Inject, args: [DropdownLinkDirective,] }] }
    ]; };
    tslib_1.__decorate([
        HostListener('click', ['$event']),
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", [Object]),
        tslib_1.__metadata("design:returntype", void 0)
    ], DropdownAnchorDirective.prototype, "onClick", null);
    DropdownAnchorDirective = tslib_1.__decorate([
        Directive({
            selector: '[appDropdownToggle]'
        }),
        tslib_1.__param(0, Inject(DropdownLinkDirective)),
        tslib_1.__metadata("design:paramtypes", [DropdownLinkDirective])
    ], DropdownAnchorDirective);
    return DropdownAnchorDirective;
}());
export { DropdownAnchorDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24tYW5jaG9yLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvZHJvcGRvd24tYW5jaG9yLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBS2xFO0lBSUMsaUNBQTJDLE9BQThCO1FBQ3hFLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO0lBQ3hCLENBQUM7SUFHRCx5Q0FBTyxHQUFQLFVBQVEsQ0FBTTtRQUNiLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDdkIsQ0FBQzs7Z0JBUG1ELHFCQUFxQix1QkFBNUQsTUFBTSxTQUFDLHFCQUFxQjs7SUFLekM7UUFEQyxZQUFZLENBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUM7Ozs7MERBR2pDO0lBWFcsdUJBQXVCO1FBSG5DLFNBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxxQkFBcUI7U0FDL0IsQ0FBQztRQUtZLG1CQUFBLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxDQUFBO2lEQUFVLHFCQUFxQjtPQUo3RCx1QkFBdUIsQ0FZbkM7SUFBRCw4QkFBQztDQUFBLEFBWkQsSUFZQztTQVpZLHVCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgSG9zdExpc3RlbmVyLCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRHJvcGRvd25MaW5rRGlyZWN0aXZlIH0gZnJvbSAnLi9kcm9wZG93bi1saW5rLmRpcmVjdGl2ZSc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuXHRzZWxlY3RvcjogJ1thcHBEcm9wZG93blRvZ2dsZV0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEcm9wZG93bkFuY2hvckRpcmVjdGl2ZSB7XHJcblxyXG5cdHByb3RlY3RlZCBuYXZsaW5rOiBEcm9wZG93bkxpbmtEaXJlY3RpdmU7XHJcblxyXG5cdGNvbnN0cnVjdG9yKEBJbmplY3QoRHJvcGRvd25MaW5rRGlyZWN0aXZlKSBuYXZsaW5rOiBEcm9wZG93bkxpbmtEaXJlY3RpdmUpIHtcclxuXHRcdHRoaXMubmF2bGluayA9IG5hdmxpbms7XHJcblx0fVxyXG5cclxuXHRASG9zdExpc3RlbmVyKCdjbGljaycsIFsnJGV2ZW50J10pXHJcblx0b25DbGljayhlOiBhbnkpIHtcclxuXHRcdHRoaXMubmF2bGluay50b2dnbGUoKTtcclxuXHR9XHJcbn1cclxuIl19