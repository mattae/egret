import * as tslib_1 from "tslib";
import { Injectable } from "@angular/core";
import * as i0 from "@angular/core";
var EgretSidenavHelperService = /** @class */ (function () {
    function EgretSidenavHelperService() {
        this.sidenavList = [];
    }
    EgretSidenavHelperService.prototype.setSidenav = function (id, sidenav) {
        this.sidenavList[id] = sidenav;
    };
    EgretSidenavHelperService.prototype.getSidenav = function (id) {
        return this.sidenavList[id];
    };
    EgretSidenavHelperService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function EgretSidenavHelperService_Factory() { return new EgretSidenavHelperService(); }, token: EgretSidenavHelperService, providedIn: "root" });
    EgretSidenavHelperService = tslib_1.__decorate([
        Injectable({
            providedIn: "root"
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], EgretSidenavHelperService);
    return EgretSidenavHelperService;
}());
export { EgretSidenavHelperService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWdyZXQtc2lkZW5hdi1oZWxwZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvZWdyZXQtc2lkZW5hdi1oZWxwZXIvZWdyZXQtc2lkZW5hdi1oZWxwZXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFNM0M7SUFHQztRQUNDLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCw4Q0FBVSxHQUFWLFVBQVcsRUFBRSxFQUFFLE9BQU87UUFDckIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUM7SUFDaEMsQ0FBQztJQUVELDhDQUFVLEdBQVYsVUFBVyxFQUFFO1FBQ1osT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzdCLENBQUM7O0lBYlcseUJBQXlCO1FBSHJDLFVBQVUsQ0FBQztZQUNYLFVBQVUsRUFBRSxNQUFNO1NBQ2xCLENBQUM7O09BQ1cseUJBQXlCLENBY3JDO29DQXBCRDtDQW9CQyxBQWRELElBY0M7U0FkWSx5QkFBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgTWF0U2lkZW5hdiB9IGZyb20gXCJAYW5ndWxhci9tYXRlcmlhbFwiO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG5cdHByb3ZpZGVkSW46IFwicm9vdFwiXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBFZ3JldFNpZGVuYXZIZWxwZXJTZXJ2aWNlIHtcclxuXHRzaWRlbmF2TGlzdDogTWF0U2lkZW5hdltdO1xyXG5cclxuXHRjb25zdHJ1Y3RvcigpIHtcclxuXHRcdHRoaXMuc2lkZW5hdkxpc3QgPSBbXTtcclxuXHR9XHJcblxyXG5cdHNldFNpZGVuYXYoaWQsIHNpZGVuYXYpOiB2b2lkIHtcclxuXHRcdHRoaXMuc2lkZW5hdkxpc3RbaWRdID0gc2lkZW5hdjtcclxuXHR9XHJcblxyXG5cdGdldFNpZGVuYXYoaWQpOiBhbnkge1xyXG5cdFx0cmV0dXJuIHRoaXMuc2lkZW5hdkxpc3RbaWRdO1xyXG5cdH1cclxufVxyXG4iXX0=