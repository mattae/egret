import * as tslib_1 from "tslib";
import { Directive, HostBinding, HostListener, Input } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { EgretSidenavHelperService } from './egret-sidenav-helper.service';
import { MatSidenav } from '@angular/material';
import { MediaObserver } from '@angular/flex-layout';
import { MatchMediaService } from '../../services/match-media.service';
var EgretSidenavHelperDirective = /** @class */ (function () {
    function EgretSidenavHelperDirective(matchMediaService, egretSidenavHelperService, matSidenav, media) {
        this.matchMediaService = matchMediaService;
        this.egretSidenavHelperService = egretSidenavHelperService;
        this.matSidenav = matSidenav;
        this.media = media;
        // Set the default value
        this.isOpen = true;
        this.unsubscribeAll = new Subject();
    }
    EgretSidenavHelperDirective.prototype.ngOnInit = function () {
        var _this = this;
        this.egretSidenavHelperService.setSidenav(this.id, this.matSidenav);
        if (this.media.isActive(this.isOpenBreakpoint)) {
            this.isOpen = true;
            this.matSidenav.mode = 'side';
            this.matSidenav.toggle(true);
        }
        else {
            this.isOpen = false;
            this.matSidenav.mode = 'over';
            this.matSidenav.toggle(false);
        }
        this.matchMediaService.onMediaChange
            .pipe(takeUntil(this.unsubscribeAll))
            .subscribe(function () {
            if (_this.media.isActive(_this.isOpenBreakpoint)) {
                _this.isOpen = true;
                _this.matSidenav.mode = 'side';
                _this.matSidenav.toggle(true);
            }
            else {
                _this.isOpen = false;
                _this.matSidenav.mode = 'over';
                _this.matSidenav.toggle(false);
            }
        });
    };
    EgretSidenavHelperDirective.prototype.ngOnDestroy = function () {
        this.unsubscribeAll.next();
        this.unsubscribeAll.complete();
    };
    EgretSidenavHelperDirective.ctorParameters = function () { return [
        { type: MatchMediaService },
        { type: EgretSidenavHelperService },
        { type: MatSidenav },
        { type: MediaObserver }
    ]; };
    tslib_1.__decorate([
        HostBinding('class.is-open'),
        tslib_1.__metadata("design:type", Boolean)
    ], EgretSidenavHelperDirective.prototype, "isOpen", void 0);
    tslib_1.__decorate([
        Input('egretSidenavHelper'),
        tslib_1.__metadata("design:type", String)
    ], EgretSidenavHelperDirective.prototype, "id", void 0);
    tslib_1.__decorate([
        Input('isOpen'),
        tslib_1.__metadata("design:type", String)
    ], EgretSidenavHelperDirective.prototype, "isOpenBreakpoint", void 0);
    EgretSidenavHelperDirective = tslib_1.__decorate([
        Directive({
            selector: '[egretSidenavHelper]'
        }),
        tslib_1.__metadata("design:paramtypes", [MatchMediaService,
            EgretSidenavHelperService,
            MatSidenav,
            MediaObserver])
    ], EgretSidenavHelperDirective);
    return EgretSidenavHelperDirective;
}());
export { EgretSidenavHelperDirective };
var EgretSidenavTogglerDirective = /** @class */ (function () {
    function EgretSidenavTogglerDirective(egretSidenavHelperService) {
        this.egretSidenavHelperService = egretSidenavHelperService;
    }
    EgretSidenavTogglerDirective.prototype.onClick = function () {
        // console.log(this.egretSidenavHelperService.getSidenav(this.id))
        this.egretSidenavHelperService.getSidenav(this.id).toggle();
    };
    EgretSidenavTogglerDirective.ctorParameters = function () { return [
        { type: EgretSidenavHelperService }
    ]; };
    tslib_1.__decorate([
        Input('egretSidenavToggler'),
        tslib_1.__metadata("design:type", Object)
    ], EgretSidenavTogglerDirective.prototype, "id", void 0);
    tslib_1.__decorate([
        HostListener('click'),
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", []),
        tslib_1.__metadata("design:returntype", void 0)
    ], EgretSidenavTogglerDirective.prototype, "onClick", null);
    EgretSidenavTogglerDirective = tslib_1.__decorate([
        Directive({
            selector: '[egretSidenavToggler]'
        }),
        tslib_1.__metadata("design:paramtypes", [EgretSidenavHelperService])
    ], EgretSidenavTogglerDirective);
    return EgretSidenavTogglerDirective;
}());
export { EgretSidenavTogglerDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWdyZXQtc2lkZW5hdi1oZWxwZXIuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsiZGlyZWN0aXZlcy9lZ3JldC1zaWRlbmF2LWhlbHBlci9lZ3JldC1zaWRlbmF2LWhlbHBlci5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQXFCLE1BQU0sZUFBZSxDQUFDO0FBQy9GLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQy9CLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDckQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFLdkU7SUFZQyxxQ0FDUyxpQkFBb0MsRUFDcEMseUJBQW9ELEVBQ3BELFVBQXNCLEVBQ3RCLEtBQW9CO1FBSHBCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsOEJBQXlCLEdBQXpCLHlCQUF5QixDQUEyQjtRQUNwRCxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLFVBQUssR0FBTCxLQUFLLENBQWU7UUFFNUIsd0JBQXdCO1FBQ3hCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBRW5CLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztJQUNyQyxDQUFDO0lBRUQsOENBQVEsR0FBUjtRQUFBLGlCQTBCQztRQXpCQSxJQUFJLENBQUMseUJBQXlCLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRXBFLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEVBQUU7WUFDL0MsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDO1lBQzlCLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzdCO2FBQU07WUFDTixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUNwQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7WUFDOUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDOUI7UUFFRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYTthQUNsQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQzthQUNwQyxTQUFTLENBQUM7WUFDVixJQUFJLEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFO2dCQUMvQyxLQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztnQkFDbkIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDO2dCQUM5QixLQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUM3QjtpQkFBTTtnQkFDTixLQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztnQkFDcEIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDO2dCQUM5QixLQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUM5QjtRQUNGLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGlEQUFXLEdBQVg7UUFDQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDaEMsQ0FBQzs7Z0JBMUMyQixpQkFBaUI7Z0JBQ1QseUJBQXlCO2dCQUN4QyxVQUFVO2dCQUNmLGFBQWE7O0lBZDdCO1FBREMsV0FBVyxDQUFDLGVBQWUsQ0FBQzs7K0RBQ2I7SUFHaEI7UUFEQyxLQUFLLENBQUMsb0JBQW9CLENBQUM7OzJEQUNqQjtJQUdYO1FBREMsS0FBSyxDQUFDLFFBQVEsQ0FBQzs7eUVBQ1M7SUFSYiwyQkFBMkI7UUFIdkMsU0FBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLHNCQUFzQjtTQUNoQyxDQUFDO2lEQWMyQixpQkFBaUI7WUFDVCx5QkFBeUI7WUFDeEMsVUFBVTtZQUNmLGFBQWE7T0FoQmpCLDJCQUEyQixDQXdEdkM7SUFBRCxrQ0FBQztDQUFBLEFBeERELElBd0RDO1NBeERZLDJCQUEyQjtBQTZEeEM7SUFJQyxzQ0FBb0IseUJBQW9EO1FBQXBELDhCQUF5QixHQUF6Qix5QkFBeUIsQ0FBMkI7SUFDeEUsQ0FBQztJQUdELDhDQUFPLEdBQVA7UUFDQyxrRUFBa0U7UUFDbEUsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDN0QsQ0FBQzs7Z0JBUDhDLHlCQUF5Qjs7SUFGeEU7UUFEQyxLQUFLLENBQUMscUJBQXFCLENBQUM7OzREQUNkO0lBTWY7UUFEQyxZQUFZLENBQUMsT0FBTyxDQUFDOzs7OytEQUlyQjtJQVhXLDRCQUE0QjtRQUh4QyxTQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsdUJBQXVCO1NBQ2pDLENBQUM7aURBSzhDLHlCQUF5QjtPQUo1RCw0QkFBNEIsQ0FZeEM7SUFBRCxtQ0FBQztDQUFBLEFBWkQsSUFZQztTQVpZLDRCQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgSG9zdEJpbmRpbmcsIEhvc3RMaXN0ZW5lciwgSW5wdXQsIE9uRGVzdHJveSwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBFZ3JldFNpZGVuYXZIZWxwZXJTZXJ2aWNlIH0gZnJvbSAnLi9lZ3JldC1zaWRlbmF2LWhlbHBlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTWF0U2lkZW5hdiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgTWVkaWFPYnNlcnZlciB9IGZyb20gJ0Bhbmd1bGFyL2ZsZXgtbGF5b3V0JztcclxuaW1wb3J0IHsgTWF0Y2hNZWRpYVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9tYXRjaC1tZWRpYS5zZXJ2aWNlJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG5cdHNlbGVjdG9yOiAnW2VncmV0U2lkZW5hdkhlbHBlcl0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBFZ3JldFNpZGVuYXZIZWxwZXJEaXJlY3RpdmUgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcblx0QEhvc3RCaW5kaW5nKCdjbGFzcy5pcy1vcGVuJylcclxuXHRpc09wZW46IGJvb2xlYW47XHJcblxyXG5cdEBJbnB1dCgnZWdyZXRTaWRlbmF2SGVscGVyJylcclxuXHRpZDogc3RyaW5nO1xyXG5cclxuXHRASW5wdXQoJ2lzT3BlbicpXHJcblx0aXNPcGVuQnJlYWtwb2ludDogc3RyaW5nO1xyXG5cclxuXHRwcml2YXRlIHVuc3Vic2NyaWJlQWxsOiBTdWJqZWN0PGFueT47XHJcblxyXG5cdGNvbnN0cnVjdG9yKFxyXG5cdFx0cHJpdmF0ZSBtYXRjaE1lZGlhU2VydmljZTogTWF0Y2hNZWRpYVNlcnZpY2UsXHJcblx0XHRwcml2YXRlIGVncmV0U2lkZW5hdkhlbHBlclNlcnZpY2U6IEVncmV0U2lkZW5hdkhlbHBlclNlcnZpY2UsXHJcblx0XHRwcml2YXRlIG1hdFNpZGVuYXY6IE1hdFNpZGVuYXYsXHJcblx0XHRwcml2YXRlIG1lZGlhOiBNZWRpYU9ic2VydmVyXHJcblx0KSB7XHJcblx0XHQvLyBTZXQgdGhlIGRlZmF1bHQgdmFsdWVcclxuXHRcdHRoaXMuaXNPcGVuID0gdHJ1ZTtcclxuXHJcblx0XHR0aGlzLnVuc3Vic2NyaWJlQWxsID0gbmV3IFN1YmplY3QoKTtcclxuXHR9XHJcblxyXG5cdG5nT25Jbml0KCk6IHZvaWQge1xyXG5cdFx0dGhpcy5lZ3JldFNpZGVuYXZIZWxwZXJTZXJ2aWNlLnNldFNpZGVuYXYodGhpcy5pZCwgdGhpcy5tYXRTaWRlbmF2KTtcclxuXHJcblx0XHRpZiAodGhpcy5tZWRpYS5pc0FjdGl2ZSh0aGlzLmlzT3BlbkJyZWFrcG9pbnQpKSB7XHJcblx0XHRcdHRoaXMuaXNPcGVuID0gdHJ1ZTtcclxuXHRcdFx0dGhpcy5tYXRTaWRlbmF2Lm1vZGUgPSAnc2lkZSc7XHJcblx0XHRcdHRoaXMubWF0U2lkZW5hdi50b2dnbGUodHJ1ZSk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHR0aGlzLmlzT3BlbiA9IGZhbHNlO1xyXG5cdFx0XHR0aGlzLm1hdFNpZGVuYXYubW9kZSA9ICdvdmVyJztcclxuXHRcdFx0dGhpcy5tYXRTaWRlbmF2LnRvZ2dsZShmYWxzZSk7XHJcblx0XHR9XHJcblxyXG5cdFx0dGhpcy5tYXRjaE1lZGlhU2VydmljZS5vbk1lZGlhQ2hhbmdlXHJcblx0XHRcdC5waXBlKHRha2VVbnRpbCh0aGlzLnVuc3Vic2NyaWJlQWxsKSlcclxuXHRcdFx0LnN1YnNjcmliZSgoKSA9PiB7XHJcblx0XHRcdFx0aWYgKHRoaXMubWVkaWEuaXNBY3RpdmUodGhpcy5pc09wZW5CcmVha3BvaW50KSkge1xyXG5cdFx0XHRcdFx0dGhpcy5pc09wZW4gPSB0cnVlO1xyXG5cdFx0XHRcdFx0dGhpcy5tYXRTaWRlbmF2Lm1vZGUgPSAnc2lkZSc7XHJcblx0XHRcdFx0XHR0aGlzLm1hdFNpZGVuYXYudG9nZ2xlKHRydWUpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHR0aGlzLmlzT3BlbiA9IGZhbHNlO1xyXG5cdFx0XHRcdFx0dGhpcy5tYXRTaWRlbmF2Lm1vZGUgPSAnb3Zlcic7XHJcblx0XHRcdFx0XHR0aGlzLm1hdFNpZGVuYXYudG9nZ2xlKGZhbHNlKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cdH1cclxuXHJcblx0bmdPbkRlc3Ryb3koKTogdm9pZCB7XHJcblx0XHR0aGlzLnVuc3Vic2NyaWJlQWxsLm5leHQoKTtcclxuXHRcdHRoaXMudW5zdWJzY3JpYmVBbGwuY29tcGxldGUoKTtcclxuXHR9XHJcbn1cclxuXHJcbkBEaXJlY3RpdmUoe1xyXG5cdHNlbGVjdG9yOiAnW2VncmV0U2lkZW5hdlRvZ2dsZXJdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRWdyZXRTaWRlbmF2VG9nZ2xlckRpcmVjdGl2ZSB7XHJcblx0QElucHV0KCdlZ3JldFNpZGVuYXZUb2dnbGVyJylcclxuXHRwdWJsaWMgaWQ6IGFueTtcclxuXHJcblx0Y29uc3RydWN0b3IocHJpdmF0ZSBlZ3JldFNpZGVuYXZIZWxwZXJTZXJ2aWNlOiBFZ3JldFNpZGVuYXZIZWxwZXJTZXJ2aWNlKSB7XHJcblx0fVxyXG5cclxuXHRASG9zdExpc3RlbmVyKCdjbGljaycpXHJcblx0b25DbGljaygpIHtcclxuXHRcdC8vIGNvbnNvbGUubG9nKHRoaXMuZWdyZXRTaWRlbmF2SGVscGVyU2VydmljZS5nZXRTaWRlbmF2KHRoaXMuaWQpKVxyXG5cdFx0dGhpcy5lZ3JldFNpZGVuYXZIZWxwZXJTZXJ2aWNlLmdldFNpZGVuYXYodGhpcy5pZCkudG9nZ2xlKCk7XHJcblx0fVxyXG59XHJcbiJdfQ==