import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontSizeDirective } from './font-size.directive';
import { ScrollToDirective } from './scroll-to.directive';
import { AppDropdownDirective } from './dropdown.directive';
import { DropdownAnchorDirective } from './dropdown-anchor.directive';
import { DropdownLinkDirective } from './dropdown-link.directive';
import { EgretSideNavToggleDirective } from './egret-side-nav-toggle.directive';
import { EgretSidenavHelperDirective, EgretSidenavTogglerDirective } from './egret-sidenav-helper/egret-sidenav-helper.directive';
import { EgretSidebarTogglerDirective } from '../components/egret-sidebar/egret-sidebar.component';
var directives = [
    FontSizeDirective,
    ScrollToDirective,
    AppDropdownDirective,
    DropdownAnchorDirective,
    DropdownLinkDirective,
    EgretSideNavToggleDirective,
    EgretSidenavHelperDirective,
    EgretSidenavTogglerDirective,
    EgretSidebarTogglerDirective
];
var SharedDirectivesModule = /** @class */ (function () {
    function SharedDirectivesModule() {
    }
    SharedDirectivesModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule
            ],
            declarations: directives,
            exports: directives
        })
    ], SharedDirectivesModule);
    return SharedDirectivesModule;
}());
export { SharedDirectivesModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLWRpcmVjdGl2ZXMubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsiZGlyZWN0aXZlcy9zaGFyZWQtZGlyZWN0aXZlcy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzFELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzFELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQzVELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQ3RFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ2xFLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ2hGLE9BQU8sRUFDTiwyQkFBMkIsRUFDM0IsNEJBQTRCLEVBQzVCLE1BQU0sdURBQXVELENBQUM7QUFDL0QsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0scURBQXFELENBQUM7QUFHbkcsSUFBTSxVQUFVLEdBQUc7SUFDbEIsaUJBQWlCO0lBQ2pCLGlCQUFpQjtJQUNqQixvQkFBb0I7SUFDcEIsdUJBQXVCO0lBQ3ZCLHFCQUFxQjtJQUNyQiwyQkFBMkI7SUFDM0IsMkJBQTJCO0lBQzNCLDRCQUE0QjtJQUM1Qiw0QkFBNEI7Q0FDNUIsQ0FBQztBQVNGO0lBQUE7SUFDQSxDQUFDO0lBRFksc0JBQXNCO1FBUGxDLFFBQVEsQ0FBQztZQUNULE9BQU8sRUFBRTtnQkFDUixZQUFZO2FBQ1o7WUFDRCxZQUFZLEVBQUUsVUFBVTtZQUN4QixPQUFPLEVBQUUsVUFBVTtTQUNuQixDQUFDO09BQ1csc0JBQXNCLENBQ2xDO0lBQUQsNkJBQUM7Q0FBQSxBQURELElBQ0M7U0FEWSxzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5cclxuaW1wb3J0IHsgRm9udFNpemVEaXJlY3RpdmUgfSBmcm9tICcuL2ZvbnQtc2l6ZS5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBTY3JvbGxUb0RpcmVjdGl2ZSB9IGZyb20gJy4vc2Nyb2xsLXRvLmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IEFwcERyb3Bkb3duRGlyZWN0aXZlIH0gZnJvbSAnLi9kcm9wZG93bi5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBEcm9wZG93bkFuY2hvckRpcmVjdGl2ZSB9IGZyb20gJy4vZHJvcGRvd24tYW5jaG9yLmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IERyb3Bkb3duTGlua0RpcmVjdGl2ZSB9IGZyb20gJy4vZHJvcGRvd24tbGluay5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBFZ3JldFNpZGVOYXZUb2dnbGVEaXJlY3RpdmUgfSBmcm9tICcuL2VncmV0LXNpZGUtbmF2LXRvZ2dsZS5kaXJlY3RpdmUnO1xyXG5pbXBvcnQge1xyXG5cdEVncmV0U2lkZW5hdkhlbHBlckRpcmVjdGl2ZSxcclxuXHRFZ3JldFNpZGVuYXZUb2dnbGVyRGlyZWN0aXZlXHJcbn0gZnJvbSAnLi9lZ3JldC1zaWRlbmF2LWhlbHBlci9lZ3JldC1zaWRlbmF2LWhlbHBlci5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBFZ3JldFNpZGViYXJUb2dnbGVyRGlyZWN0aXZlIH0gZnJvbSAnLi4vY29tcG9uZW50cy9lZ3JldC1zaWRlYmFyL2VncmV0LXNpZGViYXIuY29tcG9uZW50JztcclxuXHJcblxyXG5jb25zdCBkaXJlY3RpdmVzID0gW1xyXG5cdEZvbnRTaXplRGlyZWN0aXZlLFxyXG5cdFNjcm9sbFRvRGlyZWN0aXZlLFxyXG5cdEFwcERyb3Bkb3duRGlyZWN0aXZlLFxyXG5cdERyb3Bkb3duQW5jaG9yRGlyZWN0aXZlLFxyXG5cdERyb3Bkb3duTGlua0RpcmVjdGl2ZSxcclxuXHRFZ3JldFNpZGVOYXZUb2dnbGVEaXJlY3RpdmUsXHJcblx0RWdyZXRTaWRlbmF2SGVscGVyRGlyZWN0aXZlLFxyXG5cdEVncmV0U2lkZW5hdlRvZ2dsZXJEaXJlY3RpdmUsXHJcblx0RWdyZXRTaWRlYmFyVG9nZ2xlckRpcmVjdGl2ZVxyXG5dO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuXHRpbXBvcnRzOiBbXHJcblx0XHRDb21tb25Nb2R1bGVcclxuXHRdLFxyXG5cdGRlY2xhcmF0aW9uczogZGlyZWN0aXZlcyxcclxuXHRleHBvcnRzOiBkaXJlY3RpdmVzXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTaGFyZWREaXJlY3RpdmVzTW9kdWxlIHtcclxufVxyXG4iXX0=