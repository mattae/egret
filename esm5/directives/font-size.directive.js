import * as tslib_1 from "tslib";
import { Attribute, Directive, ElementRef, OnInit } from '@angular/core';
var FontSizeDirective = /** @class */ (function () {
    function FontSizeDirective(fontSize, el) {
        this.fontSize = fontSize;
        this.el = el;
    }
    FontSizeDirective.prototype.ngOnInit = function () {
        this.el.nativeElement.fontSize = this.fontSize;
    };
    FontSizeDirective.ctorParameters = function () { return [
        { type: String, decorators: [{ type: Attribute, args: ['fontSize',] }] },
        { type: ElementRef }
    ]; };
    FontSizeDirective = tslib_1.__decorate([
        Directive({ selector: '[fontSize]' }),
        tslib_1.__param(0, Attribute('fontSize')),
        tslib_1.__metadata("design:paramtypes", [String, ElementRef])
    ], FontSizeDirective);
    return FontSizeDirective;
}());
export { FontSizeDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9udC1zaXplLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvZm9udC1zaXplLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUd6RTtJQUNFLDJCQUEyQyxRQUFnQixFQUFVLEVBQWM7UUFBeEMsYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUFVLE9BQUUsR0FBRixFQUFFLENBQVk7SUFBSSxDQUFDO0lBQ3hGLG9DQUFRLEdBQVI7UUFDRSxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUNqRCxDQUFDOzs2Q0FIYSxTQUFTLFNBQUMsVUFBVTtnQkFBdUMsVUFBVTs7SUFEeEUsaUJBQWlCO1FBRDdCLFNBQVMsQ0FBQyxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztRQUV0QixtQkFBQSxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUE7eURBQXNDLFVBQVU7T0FEeEUsaUJBQWlCLENBSzdCO0lBQUQsd0JBQUM7Q0FBQSxBQUxELElBS0M7U0FMWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBdHRyaWJ1dGUsIERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ARGlyZWN0aXZlKHsgc2VsZWN0b3I6ICdbZm9udFNpemVdJyB9KVxyXG5leHBvcnQgY2xhc3MgRm9udFNpemVEaXJlY3RpdmUgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIGNvbnN0cnVjdG9yKCBAQXR0cmlidXRlKCdmb250U2l6ZScpIHB1YmxpYyBmb250U2l6ZTogc3RyaW5nLCBwcml2YXRlIGVsOiBFbGVtZW50UmVmKSB7IH1cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC5mb250U2l6ZSA9IHRoaXMuZm9udFNpemU7XHJcbiAgfVxyXG59XHJcbiJdfQ==