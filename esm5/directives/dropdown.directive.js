import * as tslib_1 from "tslib";
import { Directive } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
var AppDropdownDirective = /** @class */ (function () {
    function AppDropdownDirective(router) {
        this.router = router;
        this.navlinks = [];
    }
    AppDropdownDirective.prototype.closeOtherLinks = function (openLink) {
        this.navlinks.forEach(function (link) {
            if (link !== openLink) {
                link.open = false;
            }
        });
    };
    AppDropdownDirective.prototype.addLink = function (link) {
        this.navlinks.push(link);
    };
    AppDropdownDirective.prototype.removeGroup = function (link) {
        var index = this.navlinks.indexOf(link);
        if (index !== -1) {
            this.navlinks.splice(index, 1);
        }
    };
    AppDropdownDirective.prototype.getUrl = function () {
        return this.router.url;
    };
    AppDropdownDirective.prototype.ngOnInit = function () {
        var _this = this;
        this._router = this.router.events.pipe(filter(function (event) { return event instanceof NavigationEnd; })).subscribe(function (event) {
            _this.navlinks.forEach(function (link) {
                if (link.group) {
                    var routeUrl = _this.getUrl();
                    var currentUrl = routeUrl.split('/');
                    if (currentUrl.indexOf(link.group) > 0) {
                        link.open = true;
                        _this.closeOtherLinks(link);
                    }
                }
            });
        });
    };
    AppDropdownDirective.ctorParameters = function () { return [
        { type: Router }
    ]; };
    AppDropdownDirective = tslib_1.__decorate([
        Directive({
            selector: '[appDropdown]'
        }),
        tslib_1.__metadata("design:paramtypes", [Router])
    ], AppDropdownDirective);
    return AppDropdownDirective;
}());
export { AppDropdownDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24uZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsiZGlyZWN0aXZlcy9kcm9wZG93bi5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDMUMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUd4RCxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFLeEM7SUEyQ0MsOEJBQW9CLE1BQWM7UUFBZCxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBMUN4QixhQUFRLEdBQWlDLEVBQUUsQ0FBQztJQTJDdEQsQ0FBQztJQXZDTSw4Q0FBZSxHQUF0QixVQUF1QixRQUErQjtRQUNyRCxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQTJCO1lBQ2pELElBQUksSUFBSSxLQUFLLFFBQVEsRUFBRTtnQkFDdEIsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7YUFDbEI7UUFDRixDQUFDLENBQUMsQ0FBQztJQUNKLENBQUM7SUFFTSxzQ0FBTyxHQUFkLFVBQWUsSUFBMkI7UUFDekMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDMUIsQ0FBQztJQUVNLDBDQUFXLEdBQWxCLFVBQW1CLElBQTJCO1FBQzdDLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFDLElBQUksS0FBSyxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQ2pCLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztTQUMvQjtJQUNGLENBQUM7SUFFTSxxQ0FBTSxHQUFiO1FBQ0MsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQztJQUN4QixDQUFDO0lBRU0sdUNBQVEsR0FBZjtRQUFBLGlCQWFDO1FBWkEsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSyxZQUFZLGFBQWEsRUFBOUIsQ0FBOEIsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUMsS0FBb0I7WUFDdEgsS0FBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUEyQjtnQkFDakQsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO29CQUNmLElBQU0sUUFBUSxHQUFHLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztvQkFDL0IsSUFBTSxVQUFVLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDdkMsSUFBSSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQ3ZDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO3dCQUNqQixLQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUMzQjtpQkFDRDtZQUNGLENBQUMsQ0FBQyxDQUFDO1FBQ0osQ0FBQyxDQUFDLENBQUM7SUFDSixDQUFDOztnQkFFMkIsTUFBTTs7SUEzQ3RCLG9CQUFvQjtRQUhoQyxTQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsZUFBZTtTQUN6QixDQUFDO2lEQTRDMkIsTUFBTTtPQTNDdEIsb0JBQW9CLENBOENoQztJQUFELDJCQUFDO0NBQUEsQUE5Q0QsSUE4Q0M7U0E5Q1ksb0JBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5hdmlnYXRpb25FbmQsIFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IERyb3Bkb3duTGlua0RpcmVjdGl2ZSB9IGZyb20gJy4vZHJvcGRvd24tbGluay5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgZmlsdGVyIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcblx0c2VsZWN0b3I6ICdbYXBwRHJvcGRvd25dJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgQXBwRHJvcGRvd25EaXJlY3RpdmUge1xyXG5cdHByb3RlY3RlZCBuYXZsaW5rczogQXJyYXk8RHJvcGRvd25MaW5rRGlyZWN0aXZlPiA9IFtdO1xyXG5cclxuXHRwcml2YXRlIF9yb3V0ZXI6IFN1YnNjcmlwdGlvbjtcclxuXHJcblx0cHVibGljIGNsb3NlT3RoZXJMaW5rcyhvcGVuTGluazogRHJvcGRvd25MaW5rRGlyZWN0aXZlKTogdm9pZCB7XHJcblx0XHR0aGlzLm5hdmxpbmtzLmZvckVhY2goKGxpbms6IERyb3Bkb3duTGlua0RpcmVjdGl2ZSkgPT4ge1xyXG5cdFx0XHRpZiAobGluayAhPT0gb3BlbkxpbmspIHtcclxuXHRcdFx0XHRsaW5rLm9wZW4gPSBmYWxzZTtcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHRwdWJsaWMgYWRkTGluayhsaW5rOiBEcm9wZG93bkxpbmtEaXJlY3RpdmUpOiB2b2lkIHtcclxuXHRcdHRoaXMubmF2bGlua3MucHVzaChsaW5rKTtcclxuXHR9XHJcblxyXG5cdHB1YmxpYyByZW1vdmVHcm91cChsaW5rOiBEcm9wZG93bkxpbmtEaXJlY3RpdmUpOiB2b2lkIHtcclxuXHRcdGNvbnN0IGluZGV4ID0gdGhpcy5uYXZsaW5rcy5pbmRleE9mKGxpbmspO1xyXG5cdFx0aWYgKGluZGV4ICE9PSAtMSkge1xyXG5cdFx0XHR0aGlzLm5hdmxpbmtzLnNwbGljZShpbmRleCwgMSk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRwdWJsaWMgZ2V0VXJsKCkge1xyXG5cdFx0cmV0dXJuIHRoaXMucm91dGVyLnVybDtcclxuXHR9XHJcblxyXG5cdHB1YmxpYyBuZ09uSW5pdCgpOiBhbnkge1xyXG5cdFx0dGhpcy5fcm91dGVyID0gdGhpcy5yb3V0ZXIuZXZlbnRzLnBpcGUoZmlsdGVyKGV2ZW50ID0+IGV2ZW50IGluc3RhbmNlb2YgTmF2aWdhdGlvbkVuZCkpLnN1YnNjcmliZSgoZXZlbnQ6IE5hdmlnYXRpb25FbmQpID0+IHtcclxuXHRcdFx0dGhpcy5uYXZsaW5rcy5mb3JFYWNoKChsaW5rOiBEcm9wZG93bkxpbmtEaXJlY3RpdmUpID0+IHtcclxuXHRcdFx0XHRpZiAobGluay5ncm91cCkge1xyXG5cdFx0XHRcdFx0Y29uc3Qgcm91dGVVcmwgPSB0aGlzLmdldFVybCgpO1xyXG5cdFx0XHRcdFx0Y29uc3QgY3VycmVudFVybCA9IHJvdXRlVXJsLnNwbGl0KCcvJyk7XHJcblx0XHRcdFx0XHRpZiAoY3VycmVudFVybC5pbmRleE9mKGxpbmsuZ3JvdXApID4gMCkge1xyXG5cdFx0XHRcdFx0XHRsaW5rLm9wZW4gPSB0cnVlO1xyXG5cdFx0XHRcdFx0XHR0aGlzLmNsb3NlT3RoZXJMaW5rcyhsaW5rKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjogUm91dGVyKSB7XHJcblx0fVxyXG5cclxufVxyXG4iXX0=