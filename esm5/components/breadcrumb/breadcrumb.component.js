import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { RoutePartsService } from '../../services/route-parts.service';
import { LayoutService } from '../../services/layout.service';
var BreadcrumbComponent = /** @class */ (function () {
    // public isEnabled: boolean = true;
    function BreadcrumbComponent(router, routePartsService, activeRoute, layout) {
        var _this = this;
        this.router = router;
        this.routePartsService = routePartsService;
        this.activeRoute = activeRoute;
        this.layout = layout;
        this.routerEventSub = this.router.events
            .pipe(filter(function (event) { return event instanceof NavigationEnd; }))
            .subscribe(function (routeChange) {
            _this.routeParts = _this.routePartsService.generateRouteParts(_this.activeRoute.snapshot);
            // generate url from parts
            _this.routeParts.reverse().map(function (item, i) {
                item.breadcrumb = _this.parseText(item);
                item.urlSegments.forEach(function (urlSegment, j) {
                    if (j === 0)
                        return item.url = "" + urlSegment.path;
                    item.url += "/" + urlSegment.path;
                });
                if (i === 0) {
                    return item;
                }
                // prepend previous part to current part
                item.url = _this.routeParts[i - 1].url + "/" + item.url;
                return item;
            });
        });
    }
    BreadcrumbComponent.prototype.ngOnInit = function () {
    };
    BreadcrumbComponent.prototype.ngOnDestroy = function () {
        if (this.routerEventSub) {
            this.routerEventSub.unsubscribe();
        }
    };
    BreadcrumbComponent.prototype.parseText = function (part) {
        if (!part.breadcrumb) {
            return '';
        }
        part.breadcrumb = part.breadcrumb.replace(/{{([^{}]*)}}/g, function (a, b) {
            var r = part.params[b];
            return typeof r === 'string' ? r : a;
        });
        return part.breadcrumb;
    };
    BreadcrumbComponent.ctorParameters = function () { return [
        { type: Router },
        { type: RoutePartsService },
        { type: ActivatedRoute },
        { type: LayoutService }
    ]; };
    BreadcrumbComponent = tslib_1.__decorate([
        Component({
            selector: 'egret-breadcrumb',
            template: "<ng-container *ngIf=\"routeParts && routeParts.length > 0\">\r\n    <div class=\"breadcrumb-bar\" *ngIf=\"layout.layoutConf.useBreadcrumb && layout.layoutConf.breadcrumb === 'simple'\">\r\n        <ul class=\"breadcrumb\">\r\n            <li *ngFor=\"let part of routeParts\">\r\n                <a routerLink=\"/{{part.url}}\">{{part.breadcrumb | translate}}</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n\r\n    <div class=\"breadcrumb-title\" *ngIf=\"layout.layoutConf.useBreadcrumb && layout.layoutConf.breadcrumb === 'title'\">\r\n        <h1 class=\"bc-title\">{{routeParts[routeParts.length - 1]?.breadcrumb | translate}}</h1>\r\n        <ul class=\"breadcrumb\" *ngIf=\"routeParts.length > 1\">\r\n            <li *ngFor=\"let part of routeParts\">\r\n                <a routerLink=\"/{{part.url}}\" class=\"text-muted\">{{part.breadcrumb | translate}}</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</ng-container>\r\n",
            styles: [""]
        }),
        tslib_1.__metadata("design:paramtypes", [Router,
            RoutePartsService,
            ActivatedRoute,
            LayoutService])
    ], BreadcrumbComponent);
    return BreadcrumbComponent;
}());
export { BreadcrumbComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnJlYWRjcnVtYi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvZWdyZXQvIiwic291cmNlcyI6WyJjb21wb25lbnRzL2JyZWFkY3J1bWIvYnJlYWRjcnVtYi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQXFCLE1BQU0sZUFBZSxDQUFDO0FBQzdELE9BQU8sRUFBRSxjQUFjLEVBQUUsYUFBYSxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRXhFLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN4QyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUN2RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFPOUQ7SUFJSSxvQ0FBb0M7SUFDcEMsNkJBQ1ksTUFBYyxFQUNkLGlCQUFvQyxFQUNwQyxXQUEyQixFQUM1QixNQUFxQjtRQUpoQyxpQkEwQkM7UUF6QlcsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsZ0JBQVcsR0FBWCxXQUFXLENBQWdCO1FBQzVCLFdBQU0sR0FBTixNQUFNLENBQWU7UUFFNUIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU07YUFDbkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssWUFBWSxhQUFhLEVBQTlCLENBQThCLENBQUMsQ0FBQzthQUNyRCxTQUFTLENBQUMsVUFBQyxXQUFXO1lBQ25CLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDdkYsMEJBQTBCO1lBQzFCLEtBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ2xDLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDdkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBQyxVQUFVLEVBQUUsQ0FBQztvQkFDbkMsSUFBSSxDQUFDLEtBQUssQ0FBQzt3QkFDUCxPQUFPLElBQUksQ0FBQyxHQUFHLEdBQUcsS0FBRyxVQUFVLENBQUMsSUFBTSxDQUFDO29CQUMzQyxJQUFJLENBQUMsR0FBRyxJQUFJLE1BQUksVUFBVSxDQUFDLElBQU0sQ0FBQTtnQkFDckMsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUNULE9BQU8sSUFBSSxDQUFDO2lCQUNmO2dCQUNELHdDQUF3QztnQkFDeEMsSUFBSSxDQUFDLEdBQUcsR0FBTSxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLFNBQUksSUFBSSxDQUFDLEdBQUssQ0FBQztnQkFDdkQsT0FBTyxJQUFJLENBQUM7WUFDaEIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCxzQ0FBUSxHQUFSO0lBQ0EsQ0FBQztJQUVELHlDQUFXLEdBQVg7UUFDSSxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDckIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEVBQUUsQ0FBQTtTQUNwQztJQUNMLENBQUM7SUFFRCx1Q0FBUyxHQUFULFVBQVUsSUFBSTtRQUNWLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2xCLE9BQU8sRUFBRSxDQUFBO1NBQ1o7UUFDRCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRSxVQUFVLENBQUMsRUFBRSxDQUFDO1lBQ3JFLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkIsT0FBTyxPQUFPLENBQUMsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3pDLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQzNCLENBQUM7O2dCQTdDbUIsTUFBTTtnQkFDSyxpQkFBaUI7Z0JBQ3ZCLGNBQWM7Z0JBQ3BCLGFBQWE7O0lBVHZCLG1CQUFtQjtRQUwvQixTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsa0JBQWtCO1lBQzVCLG84QkFBMEM7O1NBRTdDLENBQUM7aURBT3NCLE1BQU07WUFDSyxpQkFBaUI7WUFDdkIsY0FBYztZQUNwQixhQUFhO09BVHZCLG1CQUFtQixDQW9EL0I7SUFBRCwwQkFBQztDQUFBLEFBcERELElBb0RDO1NBcERZLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25EZXN0cm95LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUsIE5hdmlnYXRpb25FbmQsIFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gXCJyeGpzXCI7XHJcbmltcG9ydCB7IGZpbHRlciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgUm91dGVQYXJ0c1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9yb3V0ZS1wYXJ0cy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTGF5b3V0U2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2xheW91dC5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdlZ3JldC1icmVhZGNydW1iJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9icmVhZGNydW1iLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2JyZWFkY3J1bWIuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQnJlYWRjcnVtYkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICAgIHJvdXRlUGFydHM6IGFueVtdO1xyXG4gICAgcm91dGVyRXZlbnRTdWI6IFN1YnNjcmlwdGlvbjtcclxuXHJcbiAgICAvLyBwdWJsaWMgaXNFbmFibGVkOiBib29sZWFuID0gdHJ1ZTtcclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgcHJpdmF0ZSByb3V0ZVBhcnRzU2VydmljZTogUm91dGVQYXJ0c1NlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBhY3RpdmVSb3V0ZTogQWN0aXZhdGVkUm91dGUsXHJcbiAgICAgICAgcHVibGljIGxheW91dDogTGF5b3V0U2VydmljZVxyXG4gICAgKSB7XHJcbiAgICAgICAgdGhpcy5yb3V0ZXJFdmVudFN1YiA9IHRoaXMucm91dGVyLmV2ZW50c1xyXG4gICAgICAgICAgICAucGlwZShmaWx0ZXIoZXZlbnQgPT4gZXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uRW5kKSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgocm91dGVDaGFuZ2UpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMucm91dGVQYXJ0cyA9IHRoaXMucm91dGVQYXJ0c1NlcnZpY2UuZ2VuZXJhdGVSb3V0ZVBhcnRzKHRoaXMuYWN0aXZlUm91dGUuc25hcHNob3QpO1xyXG4gICAgICAgICAgICAgICAgLy8gZ2VuZXJhdGUgdXJsIGZyb20gcGFydHNcclxuICAgICAgICAgICAgICAgIHRoaXMucm91dGVQYXJ0cy5yZXZlcnNlKCkubWFwKChpdGVtLCBpKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbS5icmVhZGNydW1iID0gdGhpcy5wYXJzZVRleHQoaXRlbSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbS51cmxTZWdtZW50cy5mb3JFYWNoKCh1cmxTZWdtZW50LCBqKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChqID09PSAwKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGl0ZW0udXJsID0gYCR7dXJsU2VnbWVudC5wYXRofWA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW0udXJsICs9IGAvJHt1cmxTZWdtZW50LnBhdGh9YFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChpID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBpdGVtO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAvLyBwcmVwZW5kIHByZXZpb3VzIHBhcnQgdG8gY3VycmVudCBwYXJ0XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbS51cmwgPSBgJHt0aGlzLnJvdXRlUGFydHNbaSAtIDFdLnVybH0vJHtpdGVtLnVybH1gO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBpdGVtO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIGlmICh0aGlzLnJvdXRlckV2ZW50U3ViKSB7XHJcbiAgICAgICAgICAgIHRoaXMucm91dGVyRXZlbnRTdWIudW5zdWJzY3JpYmUoKVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwYXJzZVRleHQocGFydCkge1xyXG4gICAgICAgIGlmICghcGFydC5icmVhZGNydW1iKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAnJ1xyXG4gICAgICAgIH1cclxuICAgICAgICBwYXJ0LmJyZWFkY3J1bWIgPSBwYXJ0LmJyZWFkY3J1bWIucmVwbGFjZSgve3soW157fV0qKX19L2csIGZ1bmN0aW9uIChhLCBiKSB7XHJcbiAgICAgICAgICAgIHZhciByID0gcGFydC5wYXJhbXNbYl07XHJcbiAgICAgICAgICAgIHJldHVybiB0eXBlb2YgciA9PT0gJ3N0cmluZycgPyByIDogYTtcclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gcGFydC5icmVhZGNydW1iO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==