import * as tslib_1 from "tslib";
import { Component, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { NavigationService } from '../../services/navigation.service';
import { LayoutService } from '../../services/layout.service';
import { CustomizerService } from '../../services/customizer.service';
import { ITheme, ThemeService } from '../../services/theme.service';
var CustomizerComponent = /** @class */ (function () {
    function CustomizerComponent(navService, layout, themeService, customizer, renderer) {
        this.navService = navService;
        this.layout = layout;
        this.themeService = themeService;
        this.customizer = customizer;
        this.renderer = renderer;
        this.isCustomizerOpen = false;
        this.customizerClosed = new EventEmitter();
        this.sidenavTypes = [
            {
                name: 'Default Menu',
                value: 'default-menu'
            },
            {
                name: 'Separator Menu',
                value: 'separator-menu'
            },
            {
                name: 'Icon Menu',
                value: 'icon-menu'
            }
        ];
        this.selectedMenu = 'icon-menu';
        this.isTopbarFixed = false;
        this.isRTL = false;
        this.perfectScrollbarEnabled = true;
    }
    CustomizerComponent.prototype.ngOnInit = function () {
        this.layoutConf = this.layout.layoutConf;
        this.selectedLayout = this.layoutConf.navigationPos;
        this.isTopbarFixed = this.layoutConf.topbarFixed;
        this.isRTL = this.layoutConf.dir === 'rtl';
        this.egretThemes = this.themeService.egretThemes;
    };
    CustomizerComponent.prototype.changeTheme = function (theme) {
        // this.themeService.changeTheme(theme);
        this.layout.publishLayoutChange({ matTheme: theme.name });
    };
    CustomizerComponent.prototype.changeLayoutStyle = function (data) {
        this.layout.publishLayoutChange({ navigationPos: this.selectedLayout });
    };
    CustomizerComponent.prototype.changeSidenav = function (data) {
        this.navService.publishNavigationChange(data.value);
    };
    CustomizerComponent.prototype.toggleBreadcrumb = function (data) {
        this.layout.publishLayoutChange({ useBreadcrumb: data.checked });
    };
    CustomizerComponent.prototype.sidebarCompactToggle = function (data) {
        this.layout.publishLayoutChange({ sidebarCompactToggle: data.checked });
    };
    CustomizerComponent.prototype.toggleTopbarFixed = function (data) {
        this.layout.publishLayoutChange({ topbarFixed: data.checked });
    };
    CustomizerComponent.prototype.toggleDir = function (data) {
        var dir = data.checked ? 'rtl' : 'ltr';
        this.layout.publishLayoutChange({ dir: dir });
    };
    CustomizerComponent.prototype.tooglePerfectScrollbar = function (data) {
        this.layout.publishLayoutChange({ perfectScrollbar: this.perfectScrollbarEnabled });
    };
    CustomizerComponent.prototype.close = function () {
        this.isCustomizerOpen = false;
        this.customizerClosed.emit(null);
    };
    CustomizerComponent.ctorParameters = function () { return [
        { type: NavigationService },
        { type: LayoutService },
        { type: ThemeService },
        { type: CustomizerService },
        { type: Renderer2 }
    ]; };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], CustomizerComponent.prototype, "isCustomizerOpen", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], CustomizerComponent.prototype, "customizerClosed", void 0);
    CustomizerComponent = tslib_1.__decorate([
        Component({
            selector: 'egret-customizer',
            template: "<div id=\"app-customizer\" *ngIf=\"isCustomizerOpen\">\r\n    <mat-card class=\"p-0\">\r\n        <mat-card-title class=\"m-0 light-gray\">\r\n            <div class=\"card-title-text\" fxLayout=\"row wrap\" fxLayoutAlign=\"center center\">\r\n                <span fxFlex></span>\r\n                <button\r\n                        class=\"card-control\"\r\n                        mat-icon-button\r\n                        (click)=\"close()\">\r\n                    <mat-icon>close</mat-icon>\r\n                </button>\r\n            </div>\r\n        </mat-card-title>\r\n\r\n        <mat-card-content [perfectScrollbar]>\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Layouts</h6>\r\n                <mat-radio-group fxLayout=\"column\" [(ngModel)]=\"selectedLayout\" (change)=\"changeLayoutStyle($event)\">\r\n                    <mat-radio-button [value]=\"'top'\"> Top Navigation</mat-radio-button>\r\n                    <mat-radio-button [value]=\"'side'\"> Side Navigation</mat-radio-button>\r\n                </mat-radio-group>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Header Colors</h6>\r\n                <div class=\"mb-1\">\r\n                    <mat-checkbox [(ngModel)]=\"isTopbarFixed\" (change)=\"toggleTopbarFixed($event)\"\r\n                                  [disabled]=\"selectedLayout === 'top'\" [value]=\"selectedLayout !== 'top'\">Fixed Header\r\n                    </mat-checkbox>\r\n                </div>\r\n                <div class=\"colors\">\r\n                    <div\r\n                            class=\"color {{c.class}}\"\r\n                            *ngFor=\"let c of customizer.topbarColors\"\r\n                            (click)=\"customizer.changeTopbarColor(c)\">\r\n                        <mat-icon class=\"active-icon\" *ngIf=\"c.active\">check</mat-icon>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Sidebar colors</h6>\r\n                <div class=\"colors\">\r\n                    <div\r\n                            class=\"color {{c.class}}\"\r\n                            *ngFor=\"let c of customizer.sidebarColors\"\r\n                            (click)=\"customizer.changeSidebarColor(c)\">\r\n                        <mat-icon class=\"active-icon\" *ngIf=\"c.active\">check</mat-icon>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Material Themes</h6>\r\n                <div class=\"colors\">\r\n                    <div class=\"color\" *ngFor=\"let theme of egretThemes\"\r\n                         (click)=\"changeTheme(theme)\" [style.background]=\"theme.baseColor\">\r\n                        <mat-icon class=\"active-icon\" *ngIf=\"theme.isActive\">check</mat-icon>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Sidebar Toggle</h6>\r\n                <div class=\"mb-1\">\r\n                    <mat-checkbox [(ngModel)]=\"layoutConf.sidebarCompactToggle\" (change)=\"sidebarCompactToggle($event)\">\r\n                        Toggle Sidebar\r\n                    </mat-checkbox>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Breadcrumb</h6>\r\n                <div class=\"mb-1\">\r\n                    <mat-checkbox [(ngModel)]=\"layoutConf.useBreadcrumb\" (change)=\"toggleBreadcrumb($event)\">Use\r\n                        breadcrumb\r\n                    </mat-checkbox>\r\n                </div>\r\n                <small class=\"text-muted\">Breadcrumb types</small>\r\n                <mat-radio-group fxLayout=\"column\" [(ngModel)]=\"layoutConf.breadcrumb\"\r\n                                 [disabled]=\"!layoutConf.useBreadcrumb\">\r\n                    <mat-radio-button [value]=\"'simple'\"> Simple</mat-radio-button>\r\n                    <mat-radio-button [value]=\"'title'\"> Simple with title</mat-radio-button>\r\n                </mat-radio-group>\r\n            </div>\r\n            <div class=\"pb-1 pos-rel mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Navigation</h6>\r\n                <mat-radio-group\r\n                        fxLayout=\"column\"\r\n                        [(ngModel)]=\"selectedMenu\"\r\n                        (change)=\"changeSidenav($event)\"\r\n                        [disabled]=\"selectedLayout === 'top'\">\r\n                    <mat-radio-button\r\n                            *ngFor=\"let type of sidenavTypes\"\r\n                            [value]=\"type.value\">\r\n                        {{type.name}}\r\n                    </mat-radio-button>\r\n                </mat-radio-group>\r\n            </div>\r\n\r\n            <div class=\"pb-1 \">\r\n                <mat-checkbox [(ngModel)]=\"isRTL\" (change)=\"toggleDir($event)\">RTL</mat-checkbox>\r\n            </div>\r\n        </mat-card-content>\r\n    </mat-card>\r\n</div>\r\n",
            styles: [".handle{position:fixed;bottom:30px;right:30px;z-index:99}#app-customizer{position:fixed;bottom:0;top:0;right:0;min-width:180px;max-width:280px;z-index:999}#app-customizer .title{text-transform:uppercase;font-size:12px;font-weight:700;margin:0 0 1rem}#app-customizer .mat-card{margin:0;border-radius:0}#app-customizer .mat-card-content{padding:1rem 1.5rem 2rem;max-height:calc(100vh - 80px)}.pos-rel{position:relative;z-index:99}.pos-rel .olay{position:absolute;width:100%;height:100%;background:rgba(0,0,0,.5);z-index:100}.colors{display:flex;flex-wrap:wrap}.colors .color{position:relative;width:36px;height:36px;display:inline-block;border-radius:50%;margin:8px;text-align:center;box-shadow:0 4px 20px 1px rgba(0,0,0,.06),0 1px 4px rgba(0,0,0,.03);cursor:pointer}.colors .color .active-icon{position:absolute;left:0;right:0;margin:auto;top:6px}"]
        }),
        tslib_1.__metadata("design:paramtypes", [NavigationService,
            LayoutService,
            ThemeService,
            CustomizerService,
            Renderer2])
    ], CustomizerComponent);
    return CustomizerComponent;
}());
export { CustomizerComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9taXplci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvZWdyZXQvIiwic291cmNlcyI6WyJjb21wb25lbnRzL2N1c3RvbWl6ZXIvY3VzdG9taXplci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxRixPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUN0RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDOUQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDdEUsT0FBTyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQU9wRTtJQThCQyw2QkFDUyxVQUE2QixFQUM3QixNQUFxQixFQUNyQixZQUEwQixFQUMzQixVQUE2QixFQUM1QixRQUFtQjtRQUpuQixlQUFVLEdBQVYsVUFBVSxDQUFtQjtRQUM3QixXQUFNLEdBQU4sTUFBTSxDQUFlO1FBQ3JCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzNCLGVBQVUsR0FBVixVQUFVLENBQW1CO1FBQzVCLGFBQVEsR0FBUixRQUFRLENBQVc7UUFqQzVCLHFCQUFnQixHQUFZLEtBQUssQ0FBQztRQUVsQyxxQkFBZ0IsR0FBc0IsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN6RCxpQkFBWSxHQUFHO1lBQ2Q7Z0JBQ0MsSUFBSSxFQUFFLGNBQWM7Z0JBQ3BCLEtBQUssRUFBRSxjQUFjO2FBQ3JCO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLGdCQUFnQjtnQkFDdEIsS0FBSyxFQUFFLGdCQUFnQjthQUN2QjtZQUNEO2dCQUNDLElBQUksRUFBRSxXQUFXO2dCQUNqQixLQUFLLEVBQUUsV0FBVzthQUNsQjtTQUNELENBQUM7UUFLRixpQkFBWSxHQUFXLFdBQVcsQ0FBQztRQUVuQyxrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QixVQUFLLEdBQUcsS0FBSyxDQUFDO1FBRWQsNEJBQXVCLEdBQVksSUFBSSxDQUFDO0lBU3hDLENBQUM7SUFFRCxzQ0FBUSxHQUFSO1FBQ0MsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQztRQUN6QyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDO1FBQ3BELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUM7UUFDakQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsS0FBSyxLQUFLLENBQUM7UUFDM0MsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQztJQUNsRCxDQUFDO0lBRUQseUNBQVcsR0FBWCxVQUFZLEtBQUs7UUFDaEIsd0NBQXdDO1FBQ3hDLElBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUMsRUFBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLElBQUksRUFBQyxDQUFDLENBQUE7SUFDeEQsQ0FBQztJQUVELCtDQUFpQixHQUFqQixVQUFrQixJQUFJO1FBQ3JCLElBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUMsRUFBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBQyxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVELDJDQUFhLEdBQWIsVUFBYyxJQUFJO1FBQ2pCLElBQUksQ0FBQyxVQUFVLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JELENBQUM7SUFFRCw4Q0FBZ0IsR0FBaEIsVUFBaUIsSUFBSTtRQUNwQixJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEVBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUMsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7SUFFRCxrREFBb0IsR0FBcEIsVUFBcUIsSUFBSTtRQUN4QixJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEVBQUMsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBQyxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVELCtDQUFpQixHQUFqQixVQUFrQixJQUFJO1FBQ3JCLElBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUMsRUFBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBQyxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVELHVDQUFTLEdBQVQsVUFBVSxJQUFJO1FBQ2IsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDdkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxFQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUMsQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFFRCxvREFBc0IsR0FBdEIsVUFBdUIsSUFBSTtRQUMxQixJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEVBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixFQUFDLENBQUMsQ0FBQTtJQUNsRixDQUFDO0lBRUQsbUNBQUssR0FBTDtRQUNDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDOUIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNsQyxDQUFDOztnQkFyRG9CLGlCQUFpQjtnQkFDckIsYUFBYTtnQkFDUCxZQUFZO2dCQUNmLGlCQUFpQjtnQkFDbEIsU0FBUzs7SUFqQzVCO1FBREMsS0FBSyxFQUFFOztpRUFDMEI7SUFFbEM7UUFEQyxNQUFNLEVBQUU7MENBQ1MsWUFBWTtpRUFBMkI7SUFKN0MsbUJBQW1CO1FBTC9CLFNBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsc3lLQUEwQzs7U0FFMUMsQ0FBQztpREFnQ29CLGlCQUFpQjtZQUNyQixhQUFhO1lBQ1AsWUFBWTtZQUNmLGlCQUFpQjtZQUNsQixTQUFTO09BbkNoQixtQkFBbUIsQ0FxRi9CO0lBQUQsMEJBQUM7Q0FBQSxBQXJGRCxJQXFGQztTQXJGWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uSW5pdCwgT3V0cHV0LCBSZW5kZXJlcjIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTmF2aWdhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9uYXZpZ2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBMYXlvdXRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvbGF5b3V0LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDdXN0b21pemVyU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2N1c3RvbWl6ZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IElUaGVtZSwgVGhlbWVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvdGhlbWUuc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuXHRzZWxlY3RvcjogJ2VncmV0LWN1c3RvbWl6ZXInLFxyXG5cdHRlbXBsYXRlVXJsOiAnLi9jdXN0b21pemVyLmNvbXBvbmVudC5odG1sJyxcclxuXHRzdHlsZVVybHM6IFsnLi9jdXN0b21pemVyLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEN1c3RvbWl6ZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cdEBJbnB1dCgpXHJcblx0aXNDdXN0b21pemVyT3BlbjogYm9vbGVhbiA9IGZhbHNlO1xyXG5cdEBPdXRwdXQoKVxyXG5cdGN1c3RvbWl6ZXJDbG9zZWQ6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cdHNpZGVuYXZUeXBlcyA9IFtcclxuXHRcdHtcclxuXHRcdFx0bmFtZTogJ0RlZmF1bHQgTWVudScsXHJcblx0XHRcdHZhbHVlOiAnZGVmYXVsdC1tZW51J1xyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0bmFtZTogJ1NlcGFyYXRvciBNZW51JyxcclxuXHRcdFx0dmFsdWU6ICdzZXBhcmF0b3ItbWVudSdcclxuXHRcdH0sXHJcblx0XHR7XHJcblx0XHRcdG5hbWU6ICdJY29uIE1lbnUnLFxyXG5cdFx0XHR2YWx1ZTogJ2ljb24tbWVudSdcclxuXHRcdH1cclxuXHRdO1xyXG5cdHNpZGViYXJDb2xvcnM6IGFueVtdO1xyXG5cdHRvcGJhckNvbG9yczogYW55W107XHJcblxyXG5cdGxheW91dENvbmY7XHJcblx0c2VsZWN0ZWRNZW51OiBzdHJpbmcgPSAnaWNvbi1tZW51JztcclxuXHRzZWxlY3RlZExheW91dDogc3RyaW5nO1xyXG5cdGlzVG9wYmFyRml4ZWQgPSBmYWxzZTtcclxuXHRpc1JUTCA9IGZhbHNlO1xyXG5cdGVncmV0VGhlbWVzOiBJVGhlbWVbXTtcclxuXHRwZXJmZWN0U2Nyb2xsYmFyRW5hYmxlZDogYm9vbGVhbiA9IHRydWU7XHJcblxyXG5cdGNvbnN0cnVjdG9yKFxyXG5cdFx0cHJpdmF0ZSBuYXZTZXJ2aWNlOiBOYXZpZ2F0aW9uU2VydmljZSxcclxuXHRcdHByaXZhdGUgbGF5b3V0OiBMYXlvdXRTZXJ2aWNlLFxyXG5cdFx0cHJpdmF0ZSB0aGVtZVNlcnZpY2U6IFRoZW1lU2VydmljZSxcclxuXHRcdHB1YmxpYyBjdXN0b21pemVyOiBDdXN0b21pemVyU2VydmljZSxcclxuXHRcdHByaXZhdGUgcmVuZGVyZXI6IFJlbmRlcmVyMlxyXG5cdCkge1xyXG5cdH1cclxuXHJcblx0bmdPbkluaXQoKSB7XHJcblx0XHR0aGlzLmxheW91dENvbmYgPSB0aGlzLmxheW91dC5sYXlvdXRDb25mO1xyXG5cdFx0dGhpcy5zZWxlY3RlZExheW91dCA9IHRoaXMubGF5b3V0Q29uZi5uYXZpZ2F0aW9uUG9zO1xyXG5cdFx0dGhpcy5pc1RvcGJhckZpeGVkID0gdGhpcy5sYXlvdXRDb25mLnRvcGJhckZpeGVkO1xyXG5cdFx0dGhpcy5pc1JUTCA9IHRoaXMubGF5b3V0Q29uZi5kaXIgPT09ICdydGwnO1xyXG5cdFx0dGhpcy5lZ3JldFRoZW1lcyA9IHRoaXMudGhlbWVTZXJ2aWNlLmVncmV0VGhlbWVzO1xyXG5cdH1cclxuXHJcblx0Y2hhbmdlVGhlbWUodGhlbWUpIHtcclxuXHRcdC8vIHRoaXMudGhlbWVTZXJ2aWNlLmNoYW5nZVRoZW1lKHRoZW1lKTtcclxuXHRcdHRoaXMubGF5b3V0LnB1Ymxpc2hMYXlvdXRDaGFuZ2Uoe21hdFRoZW1lOiB0aGVtZS5uYW1lfSlcclxuXHR9XHJcblxyXG5cdGNoYW5nZUxheW91dFN0eWxlKGRhdGEpIHtcclxuXHRcdHRoaXMubGF5b3V0LnB1Ymxpc2hMYXlvdXRDaGFuZ2Uoe25hdmlnYXRpb25Qb3M6IHRoaXMuc2VsZWN0ZWRMYXlvdXR9KTtcclxuXHR9XHJcblxyXG5cdGNoYW5nZVNpZGVuYXYoZGF0YSkge1xyXG5cdFx0dGhpcy5uYXZTZXJ2aWNlLnB1Ymxpc2hOYXZpZ2F0aW9uQ2hhbmdlKGRhdGEudmFsdWUpO1xyXG5cdH1cclxuXHJcblx0dG9nZ2xlQnJlYWRjcnVtYihkYXRhKSB7XHJcblx0XHR0aGlzLmxheW91dC5wdWJsaXNoTGF5b3V0Q2hhbmdlKHt1c2VCcmVhZGNydW1iOiBkYXRhLmNoZWNrZWR9KTtcclxuXHR9XHJcblxyXG5cdHNpZGViYXJDb21wYWN0VG9nZ2xlKGRhdGEpIHtcclxuXHRcdHRoaXMubGF5b3V0LnB1Ymxpc2hMYXlvdXRDaGFuZ2Uoe3NpZGViYXJDb21wYWN0VG9nZ2xlOiBkYXRhLmNoZWNrZWR9KTtcclxuXHR9XHJcblxyXG5cdHRvZ2dsZVRvcGJhckZpeGVkKGRhdGEpIHtcclxuXHRcdHRoaXMubGF5b3V0LnB1Ymxpc2hMYXlvdXRDaGFuZ2Uoe3RvcGJhckZpeGVkOiBkYXRhLmNoZWNrZWR9KTtcclxuXHR9XHJcblxyXG5cdHRvZ2dsZURpcihkYXRhKSB7XHJcblx0XHRsZXQgZGlyID0gZGF0YS5jaGVja2VkID8gJ3J0bCcgOiAnbHRyJztcclxuXHRcdHRoaXMubGF5b3V0LnB1Ymxpc2hMYXlvdXRDaGFuZ2Uoe2RpcjogZGlyfSk7XHJcblx0fVxyXG5cclxuXHR0b29nbGVQZXJmZWN0U2Nyb2xsYmFyKGRhdGEpIHtcclxuXHRcdHRoaXMubGF5b3V0LnB1Ymxpc2hMYXlvdXRDaGFuZ2Uoe3BlcmZlY3RTY3JvbGxiYXI6IHRoaXMucGVyZmVjdFNjcm9sbGJhckVuYWJsZWR9KVxyXG5cdH1cclxuXHJcblx0Y2xvc2UoKSB7XHJcblx0XHR0aGlzLmlzQ3VzdG9taXplck9wZW4gPSBmYWxzZTtcclxuXHRcdHRoaXMuY3VzdG9taXplckNsb3NlZC5lbWl0KG51bGwpO1xyXG5cdH1cclxufVxyXG4iXX0=