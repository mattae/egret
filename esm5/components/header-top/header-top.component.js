import * as tslib_1 from "tslib";
import { Component, Input, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LayoutService } from '../../services/layout.service';
import { NavigationService } from '../../services/navigation.service';
import { ThemeService } from '../../services/theme.service';
import { AccountService, IMenuItem, LoginService } from '@lamis/web-core';
var HeaderTopComponent = /** @class */ (function () {
    function HeaderTopComponent(layout, navService, themeService, translate, renderer, loginService, accountService) {
        this.layout = layout;
        this.navService = navService;
        this.themeService = themeService;
        this.translate = translate;
        this.renderer = renderer;
        this.loginService = loginService;
        this.accountService = accountService;
        this.egretThemes = [];
        this.openCustomizer = false;
        this.currentLang = 'en';
        this.availableLangs = [{
                name: 'English',
                code: 'en',
            }, {
                name: 'Spanish',
                code: 'es',
            }];
    }
    HeaderTopComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.layoutConf = this.layout.layoutConf;
        this.egretThemes = this.themeService.egretThemes;
        this.menuItemSub = this.navService.menuItems$
            .subscribe(function (res) {
            res = res.filter(function (item) { return item.type !== 'icon' && item.type !== 'separator'; });
            var limit = 4;
            var mainItems = res.slice(0, limit);
            if (res.length <= limit) {
                return _this.menuItems = mainItems;
            }
            var subItems = res.slice(limit, res.length - 1);
            mainItems.push({
                name: 'More',
                type: 'dropDown',
                tooltip: 'More',
                icon: 'more_horiz',
                sub: subItems
            });
            _this.menuItems = mainItems;
        });
    };
    HeaderTopComponent.prototype.ngOnDestroy = function () {
        this.menuItemSub.unsubscribe();
    };
    HeaderTopComponent.prototype.setLang = function () {
        this.translate.use(this.currentLang);
    };
    HeaderTopComponent.prototype.changeTheme = function (theme) {
        this.layout.publishLayoutChange({ matTheme: theme.name });
    };
    HeaderTopComponent.prototype.toggleNotific = function () {
        this.notificPanel.toggle();
    };
    HeaderTopComponent.prototype.toggleSidenav = function () {
        if (this.layoutConf.sidebarStyle === 'closed') {
            return this.layout.publishLayoutChange({
                sidebarStyle: 'full'
            });
        }
        this.layout.publishLayoutChange({
            sidebarStyle: 'closed'
        });
    };
    HeaderTopComponent.prototype.logout = function () {
        this.loginService.logout();
    };
    HeaderTopComponent.ctorParameters = function () { return [
        { type: LayoutService },
        { type: NavigationService },
        { type: ThemeService },
        { type: TranslateService },
        { type: Renderer2 },
        { type: LoginService },
        { type: AccountService }
    ]; };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], HeaderTopComponent.prototype, "notificPanel", void 0);
    HeaderTopComponent = tslib_1.__decorate([
        Component({
            selector: 'egret-header-top',
            template: "<div class=\"header-topnav mat-elevation-z2\">\r\n    <div class=\"container\">\r\n        <div class=\"topnav\">\r\n            <!-- App Logo -->\r\n            <div class=\"topbar-branding\">\r\n                <img src=\"assets/images/logo.png\" alt=\"\" class=\"app-logo\">\r\n            </div>\r\n\r\n            <ul class=\"menu\" *ngIf=\"!layoutConf.isMobile\">\r\n                <li *ngFor=\"let item of menuItems; let i = index;\">\r\n                    <ng-container *jhiHasAnyAuthority=\"item.authorities\">\r\n                        <div *ngIf=\"item.type !== 'separator'\" routerLinkActive=\"open\">\r\n                            <a matRipple routerLink=\"/{{item.state}}\" *ngIf=\"item.type === 'link'\">\r\n                                <mat-icon>{{item.icon}}</mat-icon>\r\n                                {{item.name | translate}}\r\n                            </a>\r\n                            <div *ngIf=\"item.type === 'dropDown'\">\r\n                                <label matRipple for=\"drop-{{i}}\" class=\"toggle\">\r\n                                    <mat-icon>{{item.icon}}</mat-icon>\r\n                                    {{item.name | translate}}</label>\r\n                                <a matRipple>\r\n                                    <mat-icon>{{item.icon}}</mat-icon>\r\n                                    {{item.name | translate}}</a>\r\n                                <input type=\"checkbox\" id=\"drop-{{i}}\"/>\r\n                                <ul>\r\n                                    <li *ngFor=\"let itemLvL2 of item.subs; let j = index;\" routerLinkActive=\"open\">\r\n                                        <a matRipple\r\n                                           routerLink=\"{{item.state ? '/'+item.state : ''}}/{{itemLvL2.state}}\"\r\n                                           *ngIf=\"itemLvL2.type !== 'dropDown'\">\r\n                                            <mat-icon *ngIf=\"itemLvL2.icon\">{{itemLvL2.icon}}</mat-icon>\r\n                                            {{itemLvL2.name | translate}}\r\n                                        </a>\r\n\r\n                                        <div *ngIf=\"itemLvL2.type === 'dropDown'\">\r\n                                            <label matRipple for=\"drop-{{i}}{{j}}\"\r\n                                                   class=\"toggle\">{{itemLvL2.name | translate}}</label>\r\n                                            <a matRipple>\r\n                                                <mat-icon *ngIf=\"itemLvL2.icon\">{{itemLvL2.icon}}</mat-icon>\r\n                                                {{itemLvL2.name | translate}}</a>\r\n                                            <input type=\"checkbox\" id=\"drop-{{i}}{{j}}\"/>\r\n                                            <!-- Level 3 -->\r\n                                            <ul>\r\n                                                <li *ngFor=\"let itemLvL3 of itemLvL2.subs\" routerLinkActive=\"open\">\r\n                                                    <a matRipple\r\n                                                       routerLink=\"{{item.state ? '/'+item.state : ''}}{{itemLvL2.state ? '/'+itemLvL2.state : ''}}/{{itemLvL3.state}}\">\r\n                                                        <mat-icon *ngIf=\"itemLvL3.icon\">{{itemLvL3.icon}}</mat-icon>\r\n                                                        {{itemLvL3.name | translate}}\r\n                                                    </a>\r\n                                                </li>\r\n                                            </ul>\r\n                                        </div>\r\n                                    </li>\r\n                                </ul>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </ng-container>\r\n                </li>\r\n            </ul>\r\n            <egret-customizer [isCustomizerOpen]=\"openCustomizer\"\r\n                              (customizerClosed)=\"openCustomizer = false\"></egret-customizer>\r\n            <span fxFlex></span>\r\n            <!-- End Navigation -->\r\n\r\n            <!-- Language Switcher -->\r\n            <mat-select\r\n                    *ngIf=\"!layoutConf.isMobile\"\r\n                    placeholder=\"\"\r\n                    id=\"langToggle\"\r\n                    [style.width]=\"'auto'\"\r\n                    name=\"currentLang\"\r\n                    [(ngModel)]=\"currentLang\"\r\n                    (selectionChange)=\"setLang()\"\r\n                    class=\"topbar-button-right\">\r\n                <mat-option\r\n                        *ngFor=\"let lang of availableLangs\"\r\n                        [value]=\"lang.code\" ngDefaultControl>{{ lang.name }}</mat-option>\r\n            </mat-select>\r\n            <!-- Theme Switcher -->\r\n            <button\r\n                    mat-icon-button\r\n                    id=\"schemeToggle\"\r\n                    [style.overflow]=\"'visible'\"\r\n                    matTooltip=\"Color Schemes\"\r\n                    [matMenuTriggerFor]=\"themeMenu\"\r\n                    class=\"topbar-button-right\">\r\n                <mat-icon>format_color_fill</mat-icon>\r\n            </button>\r\n            <mat-menu #themeMenu=\"matMenu\">\r\n                <mat-grid-list\r\n                        class=\"theme-list\"\r\n                        cols=\"2\"\r\n                        rowHeight=\"48px\">\r\n                    <mat-grid-tile\r\n                            *ngFor=\"let theme of egretThemes\"\r\n                            (click)=\"changeTheme(theme)\">\r\n                        <div mat-menu-item [title]=\"theme.name\">\r\n                            <div [style.background]=\"theme.baseColor\" class=\"egret-swatch\"></div>\r\n                            <mat-icon class=\"active-icon\" *ngIf=\"theme.isActive\">check</mat-icon>\r\n                        </div>\r\n                    </mat-grid-tile>\r\n                </mat-grid-list>\r\n            </mat-menu>\r\n            <!-- Notification toggle button -->\r\n            <button\r\n                    mat-icon-button\r\n                    matTooltip=\"Notifications\"\r\n                    (click)=\"toggleNotific()\"\r\n                    [style.overflow]=\"'visible'\"\r\n                    class=\"topbar-button-right\">\r\n                <mat-icon>notifications</mat-icon>\r\n                <span class=\"notification-number mat-bg-warn\">3</span>\r\n            </button>\r\n            <!-- Top left user menu -->\r\n            <button mat-icon-button [matMenuTriggerFor]=\"accountMenu\" class=\"topbar-button-right mr-1 img-button\">\r\n                <img src=\"assets/images/face-7.jpg\" alt=\"\">\r\n            </button>\r\n            <mat-menu #accountMenu=\"matMenu\">\r\n                <button mat-menu-item [routerLink]=\"['/profile/overview']\">\r\n                    <mat-icon>account_box</mat-icon>\r\n                    <span>Profile</span>\r\n                </button>\r\n                <button mat-menu-item (click)=\"openCustomizer = true\">\r\n                    <mat-icon>settings</mat-icon>\r\n                    <span>Layout Settings</span>\r\n                </button>\r\n                <button mat-menu-item>\r\n                    <mat-icon>notifications_off</mat-icon>\r\n                    <span>Disable alerts</span>\r\n                </button>\r\n                <button mat-menu-item\r\n                        (click)=\"logout()\">\r\n                    <mat-icon>exit_to_app</mat-icon>\r\n                    <span>Sign out</span>\r\n                </button>\r\n            </mat-menu>\r\n            <!-- Mobile screen menu toggle -->\r\n            <button\r\n                    mat-icon-button\r\n                    class=\"mr-1\"\r\n                    (click)=\"toggleSidenav()\"\r\n                    *ngIf=\"layoutConf.isMobile\">\r\n                <mat-icon>menu</mat-icon>\r\n            </button>\r\n        </div>\r\n    </div>\r\n</div>\r\n"
        }),
        tslib_1.__metadata("design:paramtypes", [LayoutService,
            NavigationService,
            ThemeService,
            TranslateService,
            Renderer2,
            LoginService,
            AccountService])
    ], HeaderTopComponent);
    return HeaderTopComponent;
}());
export { HeaderTopComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLXRvcC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvZWdyZXQvIiwic291cmNlcyI6WyJjb21wb25lbnRzL2hlYWRlci10b3AvaGVhZGVyLXRvcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRS9FLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUM5RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUN0RSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDNUQsT0FBTyxFQUFDLGNBQWMsRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFNeEU7SUFnQkMsNEJBQ1MsTUFBcUIsRUFDckIsVUFBNkIsRUFDOUIsWUFBMEIsRUFDMUIsU0FBMkIsRUFDMUIsUUFBbUIsRUFDbkIsWUFBMEIsRUFDMUIsY0FBOEI7UUFOOUIsV0FBTSxHQUFOLE1BQU0sQ0FBZTtRQUNyQixlQUFVLEdBQVYsVUFBVSxDQUFtQjtRQUM5QixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQUMxQixhQUFRLEdBQVIsUUFBUSxDQUFXO1FBQ25CLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzFCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQW5CdkMsZ0JBQVcsR0FBVSxFQUFFLENBQUM7UUFDeEIsbUJBQWMsR0FBRyxLQUFLLENBQUM7UUFDdkIsZ0JBQVcsR0FBRyxJQUFJLENBQUM7UUFDbkIsbUJBQWMsR0FBRyxDQUFDO2dCQUNqQixJQUFJLEVBQUUsU0FBUztnQkFDZixJQUFJLEVBQUUsSUFBSTthQUNWLEVBQUU7Z0JBQ0YsSUFBSSxFQUFFLFNBQVM7Z0JBQ2YsSUFBSSxFQUFFLElBQUk7YUFDVixDQUFDLENBQUE7SUFZRixDQUFDO0lBRUQscUNBQVEsR0FBUjtRQUFBLGlCQXFCQztRQXBCQSxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUM7UUFDakQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVU7YUFDM0MsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUNiLEdBQUcsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLElBQUksS0FBSyxNQUFNLElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxXQUFXLEVBQWpELENBQWlELENBQUMsQ0FBQztZQUM1RSxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7WUFDZCxJQUFJLFNBQVMsR0FBVSxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUMzQyxJQUFJLEdBQUcsQ0FBQyxNQUFNLElBQUksS0FBSyxFQUFFO2dCQUN4QixPQUFPLEtBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFBO2FBQ2pDO1lBQ0QsSUFBSSxRQUFRLEdBQVUsR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN2RCxTQUFTLENBQUMsSUFBSSxDQUFDO2dCQUNkLElBQUksRUFBRSxNQUFNO2dCQUNaLElBQUksRUFBRSxVQUFVO2dCQUNoQixPQUFPLEVBQUUsTUFBTTtnQkFDZixJQUFJLEVBQUUsWUFBWTtnQkFDbEIsR0FBRyxFQUFFLFFBQVE7YUFDYixDQUFDLENBQUM7WUFDSCxLQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztRQUM1QixDQUFDLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFFRCx3Q0FBVyxHQUFYO1FBQ0MsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQTtJQUMvQixDQUFDO0lBRUQsb0NBQU8sR0FBUDtRQUNDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQTtJQUNyQyxDQUFDO0lBRUQsd0NBQVcsR0FBWCxVQUFZLEtBQUs7UUFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxFQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsSUFBSSxFQUFDLENBQUMsQ0FBQTtJQUN4RCxDQUFDO0lBRUQsMENBQWEsR0FBYjtRQUNDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUVELDBDQUFhLEdBQWI7UUFDQyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxLQUFLLFFBQVEsRUFBRTtZQUM5QyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUM7Z0JBQ3RDLFlBQVksRUFBRSxNQUFNO2FBQ3BCLENBQUMsQ0FBQTtTQUNGO1FBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQztZQUMvQixZQUFZLEVBQUUsUUFBUTtTQUN0QixDQUFDLENBQUE7SUFDSCxDQUFDO0lBRUQsbUNBQU0sR0FBTjtRQUNDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLENBQUE7SUFDM0IsQ0FBQzs7Z0JBOURnQixhQUFhO2dCQUNULGlCQUFpQjtnQkFDaEIsWUFBWTtnQkFDZixnQkFBZ0I7Z0JBQ2hCLFNBQVM7Z0JBQ0wsWUFBWTtnQkFDVixjQUFjOztJQVQ5QjtRQUFSLEtBQUssRUFBRTs7NERBQWM7SUFkVixrQkFBa0I7UUFKOUIsU0FBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLGtCQUFrQjtZQUM1QiwyN1BBQTBDO1NBQzFDLENBQUM7aURBa0JnQixhQUFhO1lBQ1QsaUJBQWlCO1lBQ2hCLFlBQVk7WUFDZixnQkFBZ0I7WUFDaEIsU0FBUztZQUNMLFlBQVk7WUFDVixjQUFjO09BdkIzQixrQkFBa0IsQ0FnRjlCO0lBQUQseUJBQUM7Q0FBQSxBQWhGRCxJQWdGQztTQWhGWSxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkRlc3Ryb3ksIE9uSW5pdCwgUmVuZGVyZXIyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XHJcbmltcG9ydCB7IExheW91dFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9sYXlvdXQuc2VydmljZSc7XHJcbmltcG9ydCB7IE5hdmlnYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvbmF2aWdhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVGhlbWVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvdGhlbWUuc2VydmljZSc7XHJcbmltcG9ydCB7QWNjb3VudFNlcnZpY2UsIElNZW51SXRlbSwgTG9naW5TZXJ2aWNlfSBmcm9tICdAbGFtaXMvd2ViLWNvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcblx0c2VsZWN0b3I6ICdlZ3JldC1oZWFkZXItdG9wJyxcclxuXHR0ZW1wbGF0ZVVybDogJy4vaGVhZGVyLXRvcC5jb21wb25lbnQuaHRtbCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEhlYWRlclRvcENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuXHRsYXlvdXRDb25mOiBhbnk7XHJcblx0bWVudUl0ZW1zOiBJTWVudUl0ZW1bXTtcclxuXHRtZW51SXRlbVN1YjogU3Vic2NyaXB0aW9uO1xyXG5cdGVncmV0VGhlbWVzOiBhbnlbXSA9IFtdO1xyXG5cdG9wZW5DdXN0b21pemVyID0gZmFsc2U7XHJcblx0Y3VycmVudExhbmcgPSAnZW4nO1xyXG5cdGF2YWlsYWJsZUxhbmdzID0gW3tcclxuXHRcdG5hbWU6ICdFbmdsaXNoJyxcclxuXHRcdGNvZGU6ICdlbicsXHJcblx0fSwge1xyXG5cdFx0bmFtZTogJ1NwYW5pc2gnLFxyXG5cdFx0Y29kZTogJ2VzJyxcclxuXHR9XVxyXG5cdEBJbnB1dCgpIG5vdGlmaWNQYW5lbDtcclxuXHJcblx0Y29uc3RydWN0b3IoXHJcblx0XHRwcml2YXRlIGxheW91dDogTGF5b3V0U2VydmljZSxcclxuXHRcdHByaXZhdGUgbmF2U2VydmljZTogTmF2aWdhdGlvblNlcnZpY2UsXHJcblx0XHRwdWJsaWMgdGhlbWVTZXJ2aWNlOiBUaGVtZVNlcnZpY2UsXHJcblx0XHRwdWJsaWMgdHJhbnNsYXRlOiBUcmFuc2xhdGVTZXJ2aWNlLFxyXG5cdFx0cHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIyLFxyXG5cdFx0cHJpdmF0ZSBsb2dpblNlcnZpY2U6IExvZ2luU2VydmljZSxcclxuXHRcdHByaXZhdGUgYWNjb3VudFNlcnZpY2U6IEFjY291bnRTZXJ2aWNlXHJcblx0KSB7XHJcblx0fVxyXG5cclxuXHRuZ09uSW5pdCgpIHtcclxuXHRcdHRoaXMubGF5b3V0Q29uZiA9IHRoaXMubGF5b3V0LmxheW91dENvbmY7XHJcblx0XHR0aGlzLmVncmV0VGhlbWVzID0gdGhpcy50aGVtZVNlcnZpY2UuZWdyZXRUaGVtZXM7XHJcblx0XHR0aGlzLm1lbnVJdGVtU3ViID0gdGhpcy5uYXZTZXJ2aWNlLm1lbnVJdGVtcyRcclxuXHRcdFx0LnN1YnNjcmliZShyZXMgPT4ge1xyXG5cdFx0XHRcdHJlcyA9IHJlcy5maWx0ZXIoaXRlbSA9PiBpdGVtLnR5cGUgIT09ICdpY29uJyAmJiBpdGVtLnR5cGUgIT09ICdzZXBhcmF0b3InKTtcclxuXHRcdFx0XHRsZXQgbGltaXQgPSA0O1xyXG5cdFx0XHRcdGxldCBtYWluSXRlbXM6IGFueVtdID0gcmVzLnNsaWNlKDAsIGxpbWl0KTtcclxuXHRcdFx0XHRpZiAocmVzLmxlbmd0aCA8PSBsaW1pdCkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIHRoaXMubWVudUl0ZW1zID0gbWFpbkl0ZW1zXHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGxldCBzdWJJdGVtczogYW55W10gPSByZXMuc2xpY2UobGltaXQsIHJlcy5sZW5ndGggLSAxKTtcclxuXHRcdFx0XHRtYWluSXRlbXMucHVzaCh7XHJcblx0XHRcdFx0XHRuYW1lOiAnTW9yZScsXHJcblx0XHRcdFx0XHR0eXBlOiAnZHJvcERvd24nLFxyXG5cdFx0XHRcdFx0dG9vbHRpcDogJ01vcmUnLFxyXG5cdFx0XHRcdFx0aWNvbjogJ21vcmVfaG9yaXonLFxyXG5cdFx0XHRcdFx0c3ViOiBzdWJJdGVtc1xyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHRcdHRoaXMubWVudUl0ZW1zID0gbWFpbkl0ZW1zO1xyXG5cdFx0XHR9KVxyXG5cdH1cclxuXHJcblx0bmdPbkRlc3Ryb3koKSB7XHJcblx0XHR0aGlzLm1lbnVJdGVtU3ViLnVuc3Vic2NyaWJlKClcclxuXHR9XHJcblxyXG5cdHNldExhbmcoKSB7XHJcblx0XHR0aGlzLnRyYW5zbGF0ZS51c2UodGhpcy5jdXJyZW50TGFuZylcclxuXHR9XHJcblxyXG5cdGNoYW5nZVRoZW1lKHRoZW1lKSB7XHJcblx0XHR0aGlzLmxheW91dC5wdWJsaXNoTGF5b3V0Q2hhbmdlKHttYXRUaGVtZTogdGhlbWUubmFtZX0pXHJcblx0fVxyXG5cclxuXHR0b2dnbGVOb3RpZmljKCkge1xyXG5cdFx0dGhpcy5ub3RpZmljUGFuZWwudG9nZ2xlKCk7XHJcblx0fVxyXG5cclxuXHR0b2dnbGVTaWRlbmF2KCkge1xyXG5cdFx0aWYgKHRoaXMubGF5b3V0Q29uZi5zaWRlYmFyU3R5bGUgPT09ICdjbG9zZWQnKSB7XHJcblx0XHRcdHJldHVybiB0aGlzLmxheW91dC5wdWJsaXNoTGF5b3V0Q2hhbmdlKHtcclxuXHRcdFx0XHRzaWRlYmFyU3R5bGU6ICdmdWxsJ1xyXG5cdFx0XHR9KVxyXG5cdFx0fVxyXG5cdFx0dGhpcy5sYXlvdXQucHVibGlzaExheW91dENoYW5nZSh7XHJcblx0XHRcdHNpZGViYXJTdHlsZTogJ2Nsb3NlZCdcclxuXHRcdH0pXHJcblx0fVxyXG5cclxuXHRsb2dvdXQoKSB7XHJcblx0XHR0aGlzLmxvZ2luU2VydmljZS5sb2dvdXQoKVxyXG5cdH1cclxufVxyXG4iXX0=