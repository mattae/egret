import * as tslib_1 from "tslib";
import { ChangeDetectorRef, Component, Directive, ElementRef, HostBinding, HostListener, Input, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { EgretSidebarHelperService } from './egret-sidebar-helper.service';
import { MatchMediaService } from '../../services/match-media.service';
var EgretSidebarComponent = /** @class */ (function () {
    function EgretSidebarComponent(matchMediaService, mediaObserver, sidebarHelperService, _renderer, _elementRef, cdr) {
        this.matchMediaService = matchMediaService;
        this.mediaObserver = mediaObserver;
        this.sidebarHelperService = sidebarHelperService;
        this._renderer = _renderer;
        this._elementRef = _elementRef;
        this.cdr = cdr;
        this.backdrop = null;
        this.lockedBreakpoint = "gt-sm";
        this.unsubscribeAll = new Subject();
    }
    EgretSidebarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sidebarHelperService.setSidebar(this.name, this);
        if (this.mediaObserver.isActive(this.lockedBreakpoint)) {
            this.sidebarLockedOpen = true;
            this.opened = true;
        }
        else {
            this.sidebarLockedOpen = false;
            this.opened = false;
        }
        this.matchMediaService.onMediaChange
            .pipe(takeUntil(this.unsubscribeAll))
            .subscribe(function () {
            // console.log("medua sub");
            if (_this.mediaObserver.isActive(_this.lockedBreakpoint)) {
                _this.sidebarLockedOpen = true;
                _this.opened = true;
            }
            else {
                _this.sidebarLockedOpen = false;
                _this.opened = false;
            }
        });
    };
    EgretSidebarComponent.prototype.open = function () {
        this.opened = true;
        if (!this.sidebarLockedOpen && !this.backdrop) {
            this.showBackdrop();
        }
    };
    EgretSidebarComponent.prototype.close = function () {
        this.opened = false;
        this.hideBackdrop();
    };
    EgretSidebarComponent.prototype.toggle = function () {
        if (this.opened) {
            this.close();
        }
        else {
            this.open();
        }
    };
    EgretSidebarComponent.prototype.showBackdrop = function () {
        var _this = this;
        this.backdrop = this._renderer.createElement("div");
        this.backdrop.classList.add("egret-sidebar-overlay");
        this._renderer.appendChild(this._elementRef.nativeElement.parentElement, this.backdrop);
        // Close sidebar onclick
        this.backdrop.addEventListener("click", function () {
            _this.close();
        });
        this.cdr.markForCheck();
    };
    EgretSidebarComponent.prototype.hideBackdrop = function () {
        if (this.backdrop) {
            this.backdrop.parentNode.removeChild(this.backdrop);
            this.backdrop = null;
        }
        this.cdr.markForCheck();
    };
    EgretSidebarComponent.prototype.ngOnDestroy = function () {
        this.unsubscribeAll.next();
        this.unsubscribeAll.complete();
        this.sidebarHelperService.removeSidebar(this.name);
    };
    EgretSidebarComponent.ctorParameters = function () { return [
        { type: MatchMediaService },
        { type: MediaObserver },
        { type: EgretSidebarHelperService },
        { type: Renderer2 },
        { type: ElementRef },
        { type: ChangeDetectorRef }
    ]; };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], EgretSidebarComponent.prototype, "name", void 0);
    tslib_1.__decorate([
        Input(),
        HostBinding("class.position-right"),
        tslib_1.__metadata("design:type", Boolean)
    ], EgretSidebarComponent.prototype, "right", void 0);
    tslib_1.__decorate([
        HostBinding("class.open"),
        tslib_1.__metadata("design:type", Boolean)
    ], EgretSidebarComponent.prototype, "opened", void 0);
    tslib_1.__decorate([
        HostBinding("class.sidebar-locked-open"),
        tslib_1.__metadata("design:type", Boolean)
    ], EgretSidebarComponent.prototype, "sidebarLockedOpen", void 0);
    tslib_1.__decorate([
        HostBinding("class.is-over"),
        tslib_1.__metadata("design:type", Boolean)
    ], EgretSidebarComponent.prototype, "isOver", void 0);
    EgretSidebarComponent = tslib_1.__decorate([
        Component({
            selector: 'egret-sidebar',
            template: "<div>\r\n  <ng-content></ng-content>\r\n</div>",
            styles: [""]
        }),
        tslib_1.__metadata("design:paramtypes", [MatchMediaService,
            MediaObserver,
            EgretSidebarHelperService,
            Renderer2,
            ElementRef,
            ChangeDetectorRef])
    ], EgretSidebarComponent);
    return EgretSidebarComponent;
}());
export { EgretSidebarComponent };
var EgretSidebarTogglerDirective = /** @class */ (function () {
    function EgretSidebarTogglerDirective(egretSidebarHelperService) {
        this.egretSidebarHelperService = egretSidebarHelperService;
    }
    EgretSidebarTogglerDirective.prototype.onClick = function () {
        this.egretSidebarHelperService.getSidebar(this.id).toggle();
    };
    EgretSidebarTogglerDirective.ctorParameters = function () { return [
        { type: EgretSidebarHelperService }
    ]; };
    tslib_1.__decorate([
        Input("egretSidebarToggler"),
        tslib_1.__metadata("design:type", Object)
    ], EgretSidebarTogglerDirective.prototype, "id", void 0);
    tslib_1.__decorate([
        HostListener("click"),
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", []),
        tslib_1.__metadata("design:returntype", void 0)
    ], EgretSidebarTogglerDirective.prototype, "onClick", null);
    EgretSidebarTogglerDirective = tslib_1.__decorate([
        Directive({
            selector: "[egretSidebarToggler]"
        }),
        tslib_1.__metadata("design:paramtypes", [EgretSidebarHelperService])
    ], EgretSidebarTogglerDirective);
    return EgretSidebarTogglerDirective;
}());
export { EgretSidebarTogglerDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWdyZXQtc2lkZWJhci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvZWdyZXQvIiwic291cmNlcyI6WyJjb21wb25lbnRzL2VncmV0LXNpZGViYXIvZWdyZXQtc2lkZWJhci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFDSCxpQkFBaUIsRUFDakIsU0FBUyxFQUNULFNBQVMsRUFDVCxVQUFVLEVBQ1YsV0FBVyxFQUNYLFlBQVksRUFDWixLQUFLLEVBQ0wsU0FBUyxFQUNULE1BQU0sRUFDTixTQUFTLEVBQ1osTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3JELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBT3ZFO0lBMEJJLCtCQUNZLGlCQUFvQyxFQUNwQyxhQUE0QixFQUM1QixvQkFBK0MsRUFDL0MsU0FBb0IsRUFDcEIsV0FBdUIsRUFDdkIsR0FBc0I7UUFMdEIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1Qix5QkFBb0IsR0FBcEIsb0JBQW9CLENBQTJCO1FBQy9DLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFDcEIsZ0JBQVcsR0FBWCxXQUFXLENBQVk7UUFDdkIsUUFBRyxHQUFILEdBQUcsQ0FBbUI7UUFYMUIsYUFBUSxHQUF1QixJQUFJLENBQUM7UUFFcEMscUJBQWdCLEdBQUcsT0FBTyxDQUFDO1FBVy9CLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztJQUN4QyxDQUFDO0lBRUQsd0NBQVEsR0FBUjtRQUFBLGlCQXVCQztRQXRCRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFdEQsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsRUFBRTtZQUNwRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1lBQzlCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1NBQ3RCO2FBQU07WUFDSCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1lBQy9CLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1NBQ3ZCO1FBRUQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWE7YUFDL0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7YUFDcEMsU0FBUyxDQUFDO1lBQ1AsNEJBQTRCO1lBQzVCLElBQUksS0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLEVBQUU7Z0JBQ3BELEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7Z0JBQzlCLEtBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2FBQ3RCO2lCQUFNO2dCQUNILEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7Z0JBQy9CLEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO2FBQ3ZCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsb0NBQUksR0FBSjtRQUNJLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ25CLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQzNDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztTQUN2QjtJQUNMLENBQUM7SUFFRCxxQ0FBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFRCxzQ0FBTSxHQUFOO1FBQ0ksSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2IsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ2hCO2FBQU07WUFDSCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDZjtJQUNMLENBQUM7SUFFRCw0Q0FBWSxHQUFaO1FBQUEsaUJBZUM7UUFkRyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1FBRXJELElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUN0QixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxhQUFhLEVBQzVDLElBQUksQ0FBQyxRQUFRLENBQ2hCLENBQUM7UUFFRix3QkFBd0I7UUFDeEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUU7WUFDcEMsS0FBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2pCLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBRUQsNENBQVksR0FBWjtRQUNJLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNmLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDcEQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7U0FDeEI7UUFFRCxJQUFJLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQzVCLENBQUM7SUFFRCwyQ0FBVyxHQUFYO1FBQ0ksSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQy9CLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3ZELENBQUM7O2dCQXJGOEIsaUJBQWlCO2dCQUNyQixhQUFhO2dCQUNOLHlCQUF5QjtnQkFDcEMsU0FBUztnQkFDUCxVQUFVO2dCQUNsQixpQkFBaUI7O0lBN0JsQztRQURDLEtBQUssRUFBRTs7dURBQ0s7SUFLYjtRQUZDLEtBQUssRUFBRTtRQUNQLFdBQVcsQ0FBQyxzQkFBc0IsQ0FBQzs7d0RBQ3JCO0lBSWY7UUFEQyxXQUFXLENBQUMsWUFBWSxDQUFDOzt5REFDVjtJQUdoQjtRQURDLFdBQVcsQ0FBQywyQkFBMkIsQ0FBQzs7b0VBQ2Q7SUFJM0I7UUFEQyxXQUFXLENBQUMsZUFBZSxDQUFDOzt5REFDYjtJQW5CUCxxQkFBcUI7UUFMakMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLGVBQWU7WUFDekIsMERBQTZDOztTQUVoRCxDQUFDO2lEQTRCaUMsaUJBQWlCO1lBQ3JCLGFBQWE7WUFDTix5QkFBeUI7WUFDcEMsU0FBUztZQUNQLFVBQVU7WUFDbEIsaUJBQWlCO09BaEN6QixxQkFBcUIsQ0FpSGpDO0lBQUQsNEJBQUM7Q0FBQSxBQWpIRCxJQWlIQztTQWpIWSxxQkFBcUI7QUFzSGxDO0lBSUksc0NBQW9CLHlCQUFvRDtRQUFwRCw4QkFBeUIsR0FBekIseUJBQXlCLENBQTJCO0lBQ3hFLENBQUM7SUFHRCw4Q0FBTyxHQUFQO1FBQ0ksSUFBSSxDQUFDLHlCQUF5QixDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDaEUsQ0FBQzs7Z0JBTjhDLHlCQUF5Qjs7SUFGeEU7UUFEQyxLQUFLLENBQUMscUJBQXFCLENBQUM7OzREQUNkO0lBTWY7UUFEQyxZQUFZLENBQUMsT0FBTyxDQUFDOzs7OytEQUdyQjtJQVZRLDRCQUE0QjtRQUh4QyxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsdUJBQXVCO1NBQ3BDLENBQUM7aURBS2lELHlCQUF5QjtPQUovRCw0QkFBNEIsQ0FXeEM7SUFBRCxtQ0FBQztDQUFBLEFBWEQsSUFXQztTQVhZLDRCQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgICBDaGFuZ2VEZXRlY3RvclJlZixcclxuICAgIENvbXBvbmVudCxcclxuICAgIERpcmVjdGl2ZSxcclxuICAgIEVsZW1lbnRSZWYsXHJcbiAgICBIb3N0QmluZGluZyxcclxuICAgIEhvc3RMaXN0ZW5lcixcclxuICAgIElucHV0LFxyXG4gICAgT25EZXN0cm95LFxyXG4gICAgT25Jbml0LFxyXG4gICAgUmVuZGVyZXIyXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1lZGlhT2JzZXJ2ZXIgfSBmcm9tICdAYW5ndWxhci9mbGV4LWxheW91dCc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBFZ3JldFNpZGViYXJIZWxwZXJTZXJ2aWNlIH0gZnJvbSAnLi9lZ3JldC1zaWRlYmFyLWhlbHBlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTWF0Y2hNZWRpYVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9tYXRjaC1tZWRpYS5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdlZ3JldC1zaWRlYmFyJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9lZ3JldC1zaWRlYmFyLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2VncmV0LXNpZGViYXIuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRWdyZXRTaWRlYmFyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG4gICAgLy8gTmFtZVxyXG4gICAgQElucHV0KClcclxuICAgIG5hbWU6IHN0cmluZztcclxuXHJcbiAgICAvLyByaWdodFxyXG4gICAgQElucHV0KClcclxuICAgIEBIb3N0QmluZGluZyhcImNsYXNzLnBvc2l0aW9uLXJpZ2h0XCIpXHJcbiAgICByaWdodDogYm9vbGVhbjtcclxuXHJcbiAgICAvLyBPcGVuXHJcbiAgICBASG9zdEJpbmRpbmcoXCJjbGFzcy5vcGVuXCIpXHJcbiAgICBvcGVuZWQ6IGJvb2xlYW47XHJcblxyXG4gICAgQEhvc3RCaW5kaW5nKFwiY2xhc3Muc2lkZWJhci1sb2NrZWQtb3BlblwiKVxyXG4gICAgc2lkZWJhckxvY2tlZE9wZW46IGJvb2xlYW47XHJcblxyXG4gICAgLy9tb2RlXHJcbiAgICBASG9zdEJpbmRpbmcoXCJjbGFzcy5pcy1vdmVyXCIpXHJcbiAgICBpc092ZXI6IGJvb2xlYW47XHJcblxyXG4gICAgcHJpdmF0ZSBiYWNrZHJvcDogSFRNTEVsZW1lbnQgfCBudWxsID0gbnVsbDtcclxuXHJcbiAgICBwcml2YXRlIGxvY2tlZEJyZWFrcG9pbnQgPSBcImd0LXNtXCI7XHJcbiAgICBwcml2YXRlIHVuc3Vic2NyaWJlQWxsOiBTdWJqZWN0PGFueT47XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBtYXRjaE1lZGlhU2VydmljZTogTWF0Y2hNZWRpYVNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBtZWRpYU9ic2VydmVyOiBNZWRpYU9ic2VydmVyLFxyXG4gICAgICAgIHByaXZhdGUgc2lkZWJhckhlbHBlclNlcnZpY2U6IEVncmV0U2lkZWJhckhlbHBlclNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfcmVuZGVyZXI6IFJlbmRlcmVyMixcclxuICAgICAgICBwcml2YXRlIF9lbGVtZW50UmVmOiBFbGVtZW50UmVmLFxyXG4gICAgICAgIHByaXZhdGUgY2RyOiBDaGFuZ2VEZXRlY3RvclJlZlxyXG4gICAgKSB7XHJcbiAgICAgICAgdGhpcy51bnN1YnNjcmliZUFsbCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5zaWRlYmFySGVscGVyU2VydmljZS5zZXRTaWRlYmFyKHRoaXMubmFtZSwgdGhpcyk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLm1lZGlhT2JzZXJ2ZXIuaXNBY3RpdmUodGhpcy5sb2NrZWRCcmVha3BvaW50KSkge1xyXG4gICAgICAgICAgICB0aGlzLnNpZGViYXJMb2NrZWRPcGVuID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5vcGVuZWQgPSB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2lkZWJhckxvY2tlZE9wZW4gPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5vcGVuZWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMubWF0Y2hNZWRpYVNlcnZpY2Uub25NZWRpYUNoYW5nZVxyXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy51bnN1YnNjcmliZUFsbCkpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coXCJtZWR1YSBzdWJcIik7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5tZWRpYU9ic2VydmVyLmlzQWN0aXZlKHRoaXMubG9ja2VkQnJlYWtwb2ludCkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNpZGViYXJMb2NrZWRPcGVuID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9wZW5lZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2lkZWJhckxvY2tlZE9wZW4gPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9wZW5lZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBvcGVuKCkge1xyXG4gICAgICAgIHRoaXMub3BlbmVkID0gdHJ1ZTtcclxuICAgICAgICBpZiAoIXRoaXMuc2lkZWJhckxvY2tlZE9wZW4gJiYgIXRoaXMuYmFja2Ryb3ApIHtcclxuICAgICAgICAgICAgdGhpcy5zaG93QmFja2Ryb3AoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY2xvc2UoKSB7XHJcbiAgICAgICAgdGhpcy5vcGVuZWQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmhpZGVCYWNrZHJvcCgpO1xyXG4gICAgfVxyXG5cclxuICAgIHRvZ2dsZSgpIHtcclxuICAgICAgICBpZiAodGhpcy5vcGVuZWQpIHtcclxuICAgICAgICAgICAgdGhpcy5jbG9zZSgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMub3BlbigpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzaG93QmFja2Ryb3AoKSB7XHJcbiAgICAgICAgdGhpcy5iYWNrZHJvcCA9IHRoaXMuX3JlbmRlcmVyLmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7XHJcbiAgICAgICAgdGhpcy5iYWNrZHJvcC5jbGFzc0xpc3QuYWRkKFwiZWdyZXQtc2lkZWJhci1vdmVybGF5XCIpO1xyXG5cclxuICAgICAgICB0aGlzLl9yZW5kZXJlci5hcHBlbmRDaGlsZChcclxuICAgICAgICAgICAgdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LnBhcmVudEVsZW1lbnQsXHJcbiAgICAgICAgICAgIHRoaXMuYmFja2Ryb3BcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICAvLyBDbG9zZSBzaWRlYmFyIG9uY2xpY2tcclxuICAgICAgICB0aGlzLmJhY2tkcm9wLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuY2xvc2UoKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5jZHIubWFya0ZvckNoZWNrKCk7XHJcbiAgICB9XHJcblxyXG4gICAgaGlkZUJhY2tkcm9wKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmJhY2tkcm9wKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYmFja2Ryb3AucGFyZW50Tm9kZS5yZW1vdmVDaGlsZCh0aGlzLmJhY2tkcm9wKTtcclxuICAgICAgICAgICAgdGhpcy5iYWNrZHJvcCA9IG51bGw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmNkci5tYXJrRm9yQ2hlY2soKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnVuc3Vic2NyaWJlQWxsLm5leHQoKTtcclxuICAgICAgICB0aGlzLnVuc3Vic2NyaWJlQWxsLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgdGhpcy5zaWRlYmFySGVscGVyU2VydmljZS5yZW1vdmVTaWRlYmFyKHRoaXMubmFtZSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6IFwiW2VncmV0U2lkZWJhclRvZ2dsZXJdXCJcclxufSlcclxuZXhwb3J0IGNsYXNzIEVncmV0U2lkZWJhclRvZ2dsZXJEaXJlY3RpdmUge1xyXG4gICAgQElucHV0KFwiZWdyZXRTaWRlYmFyVG9nZ2xlclwiKVxyXG4gICAgcHVibGljIGlkOiBhbnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBlZ3JldFNpZGViYXJIZWxwZXJTZXJ2aWNlOiBFZ3JldFNpZGViYXJIZWxwZXJTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcihcImNsaWNrXCIpXHJcbiAgICBvbkNsaWNrKCkge1xyXG4gICAgICAgIHRoaXMuZWdyZXRTaWRlYmFySGVscGVyU2VydmljZS5nZXRTaWRlYmFyKHRoaXMuaWQpLnRvZ2dsZSgpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==