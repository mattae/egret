import * as tslib_1 from "tslib";
import { Injectable } from "@angular/core";
import * as i0 from "@angular/core";
var EgretSidebarHelperService = /** @class */ (function () {
    function EgretSidebarHelperService() {
        this.sidebarList = [];
    }
    EgretSidebarHelperService.prototype.setSidebar = function (name, sidebar) {
        this.sidebarList[name] = sidebar;
    };
    EgretSidebarHelperService.prototype.getSidebar = function (name) {
        return this.sidebarList[name];
    };
    EgretSidebarHelperService.prototype.removeSidebar = function (name) {
        if (!this.sidebarList[name]) {
            console.warn("The sidebar with name '" + name + "' doesn't exist.");
        }
        // remove sidebar
        delete this.sidebarList[name];
    };
    EgretSidebarHelperService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function EgretSidebarHelperService_Factory() { return new EgretSidebarHelperService(); }, token: EgretSidebarHelperService, providedIn: "root" });
    EgretSidebarHelperService = tslib_1.__decorate([
        Injectable({
            providedIn: "root"
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], EgretSidebarHelperService);
    return EgretSidebarHelperService;
}());
export { EgretSidebarHelperService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWdyZXQtc2lkZWJhci1oZWxwZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvZWdyZXQtc2lkZWJhci9lZ3JldC1zaWRlYmFyLWhlbHBlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOztBQU0zQztJQUdJO1FBQ0ksSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUVELDhDQUFVLEdBQVYsVUFBVyxJQUFJLEVBQUUsT0FBTztRQUNwQixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLE9BQU8sQ0FBQztJQUNyQyxDQUFDO0lBRUQsOENBQVUsR0FBVixVQUFXLElBQUk7UUFDWCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUVELGlEQUFhLEdBQWIsVUFBYyxJQUFJO1FBQ2QsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDekIsT0FBTyxDQUFDLElBQUksQ0FBQyw0QkFBMEIsSUFBSSxxQkFBa0IsQ0FBQyxDQUFDO1NBQ2xFO1FBRUQsaUJBQWlCO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNsQyxDQUFDOztJQXRCUSx5QkFBeUI7UUFIckMsVUFBVSxDQUFDO1lBQ1IsVUFBVSxFQUFFLE1BQU07U0FDckIsQ0FBQzs7T0FDVyx5QkFBeUIsQ0F1QnJDO29DQTdCRDtDQTZCQyxBQXZCRCxJQXVCQztTQXZCWSx5QkFBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgRWdyZXRTaWRlYmFyQ29tcG9uZW50IH0gZnJvbSBcIi4vZWdyZXQtc2lkZWJhci5jb21wb25lbnRcIjtcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46IFwicm9vdFwiXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBFZ3JldFNpZGViYXJIZWxwZXJTZXJ2aWNlIHtcclxuICAgIHNpZGViYXJMaXN0OiBFZ3JldFNpZGViYXJDb21wb25lbnRbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICB0aGlzLnNpZGViYXJMaXN0ID0gW107XHJcbiAgICB9XHJcblxyXG4gICAgc2V0U2lkZWJhcihuYW1lLCBzaWRlYmFyKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5zaWRlYmFyTGlzdFtuYW1lXSA9IHNpZGViYXI7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0U2lkZWJhcihuYW1lKTogYW55IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zaWRlYmFyTGlzdFtuYW1lXTtcclxuICAgIH1cclxuXHJcbiAgICByZW1vdmVTaWRlYmFyKG5hbWUpIHtcclxuICAgICAgICBpZiAoIXRoaXMuc2lkZWJhckxpc3RbbmFtZV0pIHtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKGBUaGUgc2lkZWJhciB3aXRoIG5hbWUgJyR7bmFtZX0nIGRvZXNuJ3QgZXhpc3QuYCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyByZW1vdmUgc2lkZWJhclxyXG4gICAgICAgIGRlbGV0ZSB0aGlzLnNpZGViYXJMaXN0W25hbWVdO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==