import * as tslib_1 from "tslib";
import { Component, Input, OnInit, Renderer2 } from '@angular/core';
import { ThemeService } from '../../services/theme.service';
import { LayoutService } from '../../services/layout.service';
import { TranslateService } from '@ngx-translate/core';
var HeaderSideComponent = /** @class */ (function () {
    function HeaderSideComponent(themeService, layout, translate, renderer) {
        this.themeService = themeService;
        this.layout = layout;
        this.translate = translate;
        this.renderer = renderer;
        this.availableLangs = [{
                name: 'EN',
                code: 'en',
                flag: 'flag-icon-us'
            }];
        this.currentLang = this.availableLangs[0];
        this.openCustomizer = false;
    }
    HeaderSideComponent.prototype.ngOnInit = function () {
        this.egretThemes = this.themeService.egretThemes;
        this.layoutConf = this.layout.layoutConf;
        this.translate.use(this.currentLang.code);
    };
    HeaderSideComponent.prototype.setLang = function (lng) {
        this.currentLang = lng;
        this.translate.use(lng.code);
    };
    HeaderSideComponent.prototype.changeTheme = function (theme) {
        // this.themeService.changeTheme(theme);
    };
    HeaderSideComponent.prototype.toggleNotific = function () {
        this.notificPanel.toggle();
    };
    HeaderSideComponent.prototype.toggleSidenav = function () {
        if (this.layoutConf.sidebarStyle === 'closed') {
            return this.layout.publishLayoutChange({
                sidebarStyle: 'full'
            });
        }
        this.layout.publishLayoutChange({
            sidebarStyle: 'closed'
        });
    };
    HeaderSideComponent.prototype.toggleCollapse = function () {
        // compact --> full
        if (this.layoutConf.sidebarStyle === 'compact') {
            return this.layout.publishLayoutChange({
                sidebarStyle: 'full',
                sidebarCompactToggle: false
            }, { transitionClass: true });
        }
        // * --> compact
        this.layout.publishLayoutChange({
            sidebarStyle: 'compact',
            sidebarCompactToggle: true
        }, { transitionClass: true });
    };
    HeaderSideComponent.ctorParameters = function () { return [
        { type: ThemeService },
        { type: LayoutService },
        { type: TranslateService },
        { type: Renderer2 }
    ]; };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], HeaderSideComponent.prototype, "notificPanel", void 0);
    HeaderSideComponent = tslib_1.__decorate([
        Component({
            selector: 'egret-header-side',
            template: "<mat-toolbar class=\"topbar\">\r\n    <!-- Sidenav toggle button -->\r\n    <button\r\n            *ngIf=\"layoutConf.sidebarStyle !== 'compact'\"\r\n            mat-icon-button\r\n            id=\"sidenavToggle\"\r\n            (click)=\"toggleSidenav()\"\r\n            matTooltip=\"Toggle Hide/Open\"\r\n    >\r\n        <mat-icon>menu</mat-icon>\r\n    </button>\r\n\r\n    <!-- Search form -->\r\n    <!-- <div fxFlex fxHide.lt-sm=\"true\" class=\"search-bar\">\r\n      <form class=\"top-search-form\">\r\n        <mat-icon role=\"img\">search</mat-icon>\r\n        <input autofocus=\"true\" placeholder=\"Search\" type=\"text\" />\r\n      </form>\r\n    </div> -->\r\n\r\n    <span fxFlex></span>\r\n    <!-- Language Switcher -->\r\n   <!-- <button mat-button [matMenuTriggerFor]=\"menu\">\r\n        <span class=\"flag-icon {{currentLang.flag}} mr-05\"></span>\r\n        <span>{{currentLang.name}}</span>\r\n    </button>\r\n    <mat-menu #menu=\"matMenu\">\r\n        <button mat-menu-item *ngFor=\"let lang of availableLangs\" (click)=\"setLang(lang)\">\r\n            <span class=\"flag-icon mr-05 {{lang.flag}}\"></span>\r\n            <span>{{lang.name}}</span>\r\n        </button>\r\n    </mat-menu>\r\n    \\-->\r\n    <!-- Open \"views/search-view/result-page.component\" to understand how to subscribe to input field value -->\r\n    <egret-customizer [isCustomizerOpen]=\"openCustomizer\"\r\n                      (customizerClosed)=\"openCustomizer = false\">\r\n    </egret-customizer>\r\n    <!-- Notification toggle button -->\r\n    <button\r\n            mat-icon-button\r\n            matTooltip=\"Notifications\"\r\n            (click)=\"toggleNotific()\"\r\n            [style.overflow]=\"'visible'\"\r\n            class=\"topbar-button-right\"\r\n    >\r\n        <mat-icon>notifications</mat-icon>\r\n        <span class=\"notification-number mat-bg-warn\">3</span>\r\n    </button>\r\n    <!-- Top left user menu -->\r\n    <button\r\n            mat-icon-button\r\n            [matMenuTriggerFor]=\"accountMenu\"\r\n            class=\"topbar-button-right img-button\">\r\n        <img src=\"assets/images/face-7.jpg\" alt=\"\"/>\r\n    </button>\r\n\r\n    <mat-menu #accountMenu=\"matMenu\">\r\n        <button mat-menu-item [routerLink]=\"['/profile/overview']\">\r\n            <mat-icon>account_box</mat-icon>\r\n            <span>Profile</span>\r\n        </button>\r\n        <button mat-menu-item (click)=\"openCustomizer = true\">\r\n            <mat-icon>settings</mat-icon>\r\n            <span>Layout Settings</span>\r\n        </button>\r\n        <button mat-menu-item>\r\n            <mat-icon>notifications_off</mat-icon>\r\n            <span>Disable alerts</span>\r\n        </button>\r\n        <button mat-menu-item [routerLink]=\"['/sessions/signin']\">\r\n            <mat-icon>exit_to_app</mat-icon>\r\n            <span>Sign out</span>\r\n        </button>\r\n    </mat-menu>\r\n</mat-toolbar>\r\n"
        }),
        tslib_1.__metadata("design:paramtypes", [ThemeService,
            LayoutService,
            TranslateService,
            Renderer2])
    ], HeaderSideComponent);
    return HeaderSideComponent;
}());
export { HeaderSideComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLXNpZGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9oZWFkZXItc2lkZS9oZWFkZXItc2lkZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDcEUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQzVELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUM5RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQU12RDtJQWFJLDZCQUNZLFlBQTBCLEVBQzFCLE1BQXFCLEVBQ3RCLFNBQTJCLEVBQzFCLFFBQW1CO1FBSG5CLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzFCLFdBQU0sR0FBTixNQUFNLENBQWU7UUFDdEIsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFDMUIsYUFBUSxHQUFSLFFBQVEsQ0FBVztRQWZ4QixtQkFBYyxHQUFHLENBQUM7Z0JBQ3JCLElBQUksRUFBRSxJQUFJO2dCQUNWLElBQUksRUFBRSxJQUFJO2dCQUNWLElBQUksRUFBRSxjQUFjO2FBQ3ZCLENBQUMsQ0FBQTtRQUNGLGdCQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNyQyxtQkFBYyxHQUFHLEtBQUssQ0FBQztJQVd2QixDQUFDO0lBRUQsc0NBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUM7UUFDakQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQztRQUN6QyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRCxxQ0FBTyxHQUFQLFVBQVEsR0FBRztRQUNQLElBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBRUQseUNBQVcsR0FBWCxVQUFZLEtBQUs7UUFDYix3Q0FBd0M7SUFDNUMsQ0FBQztJQUVELDJDQUFhLEdBQWI7UUFDSSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFFRCwyQ0FBYSxHQUFiO1FBQ0ksSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksS0FBSyxRQUFRLEVBQUU7WUFDM0MsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDO2dCQUNuQyxZQUFZLEVBQUUsTUFBTTthQUN2QixDQUFDLENBQUE7U0FDTDtRQUNELElBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUM7WUFDNUIsWUFBWSxFQUFFLFFBQVE7U0FDekIsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVELDRDQUFjLEdBQWQ7UUFDSSxtQkFBbUI7UUFDbkIsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksS0FBSyxTQUFTLEVBQUU7WUFDNUMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDO2dCQUNuQyxZQUFZLEVBQUUsTUFBTTtnQkFDcEIsb0JBQW9CLEVBQUUsS0FBSzthQUM5QixFQUFFLEVBQUMsZUFBZSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUE7U0FDOUI7UUFFRCxnQkFBZ0I7UUFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQztZQUM1QixZQUFZLEVBQUUsU0FBUztZQUN2QixvQkFBb0IsRUFBRSxJQUFJO1NBQzdCLEVBQUUsRUFBQyxlQUFlLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQTtJQUUvQixDQUFDOztnQkFwRHlCLFlBQVk7Z0JBQ2xCLGFBQWE7Z0JBQ1gsZ0JBQWdCO2dCQUNoQixTQUFTOztJQWhCdEI7UUFBUixLQUFLLEVBQUU7OzZEQUFjO0lBRGIsbUJBQW1CO1FBSi9CLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxtQkFBbUI7WUFDN0IsdzVGQUEwQztTQUM3QyxDQUFDO2lEQWU0QixZQUFZO1lBQ2xCLGFBQWE7WUFDWCxnQkFBZ0I7WUFDaEIsU0FBUztPQWpCdEIsbUJBQW1CLENBbUUvQjtJQUFELDBCQUFDO0NBQUEsQUFuRUQsSUFtRUM7U0FuRVksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0LCBSZW5kZXJlcjIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVGhlbWVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvdGhlbWUuc2VydmljZSc7XHJcbmltcG9ydCB7IExheW91dFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9sYXlvdXQuc2VydmljZSc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdlZ3JldC1oZWFkZXItc2lkZScsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vaGVhZGVyLXNpZGUudGVtcGxhdGUuaHRtbCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEhlYWRlclNpZGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgQElucHV0KCkgbm90aWZpY1BhbmVsO1xyXG4gICAgcHVibGljIGF2YWlsYWJsZUxhbmdzID0gW3tcclxuICAgICAgICBuYW1lOiAnRU4nLFxyXG4gICAgICAgIGNvZGU6ICdlbicsXHJcbiAgICAgICAgZmxhZzogJ2ZsYWctaWNvbi11cydcclxuICAgIH1dXHJcbiAgICBjdXJyZW50TGFuZyA9IHRoaXMuYXZhaWxhYmxlTGFuZ3NbMF07XHJcbiAgICBvcGVuQ3VzdG9taXplciA9IGZhbHNlO1xyXG5cclxuICAgIHB1YmxpYyBlZ3JldFRoZW1lcztcclxuICAgIHB1YmxpYyBsYXlvdXRDb25mOiBhbnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSB0aGVtZVNlcnZpY2U6IFRoZW1lU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIGxheW91dDogTGF5b3V0U2VydmljZSxcclxuICAgICAgICBwdWJsaWMgdHJhbnNsYXRlOiBUcmFuc2xhdGVTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgcmVuZGVyZXI6IFJlbmRlcmVyMlxyXG4gICAgKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5lZ3JldFRoZW1lcyA9IHRoaXMudGhlbWVTZXJ2aWNlLmVncmV0VGhlbWVzO1xyXG4gICAgICAgIHRoaXMubGF5b3V0Q29uZiA9IHRoaXMubGF5b3V0LmxheW91dENvbmY7XHJcbiAgICAgICAgdGhpcy50cmFuc2xhdGUudXNlKHRoaXMuY3VycmVudExhbmcuY29kZSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0TGFuZyhsbmcpIHtcclxuICAgICAgICB0aGlzLmN1cnJlbnRMYW5nID0gbG5nO1xyXG4gICAgICAgIHRoaXMudHJhbnNsYXRlLnVzZShsbmcuY29kZSk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hhbmdlVGhlbWUodGhlbWUpIHtcclxuICAgICAgICAvLyB0aGlzLnRoZW1lU2VydmljZS5jaGFuZ2VUaGVtZSh0aGVtZSk7XHJcbiAgICB9XHJcblxyXG4gICAgdG9nZ2xlTm90aWZpYygpIHtcclxuICAgICAgICB0aGlzLm5vdGlmaWNQYW5lbC50b2dnbGUoKTtcclxuICAgIH1cclxuXHJcbiAgICB0b2dnbGVTaWRlbmF2KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmxheW91dENvbmYuc2lkZWJhclN0eWxlID09PSAnY2xvc2VkJykge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5sYXlvdXQucHVibGlzaExheW91dENoYW5nZSh7XHJcbiAgICAgICAgICAgICAgICBzaWRlYmFyU3R5bGU6ICdmdWxsJ1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmxheW91dC5wdWJsaXNoTGF5b3V0Q2hhbmdlKHtcclxuICAgICAgICAgICAgc2lkZWJhclN0eWxlOiAnY2xvc2VkJ1xyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgdG9nZ2xlQ29sbGFwc2UoKSB7XHJcbiAgICAgICAgLy8gY29tcGFjdCAtLT4gZnVsbFxyXG4gICAgICAgIGlmICh0aGlzLmxheW91dENvbmYuc2lkZWJhclN0eWxlID09PSAnY29tcGFjdCcpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMubGF5b3V0LnB1Ymxpc2hMYXlvdXRDaGFuZ2Uoe1xyXG4gICAgICAgICAgICAgICAgc2lkZWJhclN0eWxlOiAnZnVsbCcsXHJcbiAgICAgICAgICAgICAgICBzaWRlYmFyQ29tcGFjdFRvZ2dsZTogZmFsc2VcclxuICAgICAgICAgICAgfSwge3RyYW5zaXRpb25DbGFzczogdHJ1ZX0pXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyAqIC0tPiBjb21wYWN0XHJcbiAgICAgICAgdGhpcy5sYXlvdXQucHVibGlzaExheW91dENoYW5nZSh7XHJcbiAgICAgICAgICAgIHNpZGViYXJTdHlsZTogJ2NvbXBhY3QnLFxyXG4gICAgICAgICAgICBzaWRlYmFyQ29tcGFjdFRvZ2dsZTogdHJ1ZVxyXG4gICAgICAgIH0sIHt0cmFuc2l0aW9uQ2xhc3M6IHRydWV9KVxyXG5cclxuICAgIH1cclxufVxyXG4iXX0=