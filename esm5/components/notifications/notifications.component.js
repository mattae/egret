import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
var NotificationsComponent = /** @class */ (function () {
    function NotificationsComponent(router) {
        this.router = router;
        // Dummy notifications
        this.notifications = [{
                message: 'New contact added',
                icon: 'assignment_ind',
                time: '1 min ago',
                route: '/inbox',
                color: 'primary'
            }, {
                message: 'New message',
                icon: 'chat',
                time: '4 min ago',
                route: '/chat',
                color: 'accent'
            }, {
                message: 'Server rebooted',
                icon: 'settings_backup_restore',
                time: '12 min ago',
                route: '/charts',
                color: 'warn'
            }];
    }
    NotificationsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.router.events.subscribe(function (routeChange) {
            if (routeChange instanceof NavigationEnd) {
                _this.notificPanel.close();
            }
        });
    };
    NotificationsComponent.prototype.clearAll = function (e) {
        e.preventDefault();
        this.notifications = [];
    };
    NotificationsComponent.ctorParameters = function () { return [
        { type: Router }
    ]; };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], NotificationsComponent.prototype, "notificPanel", void 0);
    NotificationsComponent = tslib_1.__decorate([
        Component({
            selector: 'egret-notifications',
            template: "<div class=\"text-center mat-bg-primary pt-1 pb-1\">\r\n    <h6 class=\"m-0\">Notifications</h6>\r\n</div>\r\n<mat-nav-list class=\"notification-list\" role=\"list\">\r\n    <!-- Notification item -->\r\n    <mat-list-item *ngFor=\"let n of notifications\" class=\"notific-item\" role=\"listitem\" routerLinkActive=\"open\">\r\n        <mat-icon [color]=\"n.color\" class=\"notific-icon mr-1\">{{n.icon}}</mat-icon>\r\n        <a [routerLink]=\"[n.route || '/dashboard']\">\r\n            <div class=\"mat-list-text\">\r\n                <h4 class=\"message\">{{n.message}}</h4>\r\n                <small class=\"time text-muted\">{{n.time}}</small>\r\n            </div>\r\n        </a>\r\n    </mat-list-item>\r\n</mat-nav-list>\r\n<div class=\"text-center mt-1\" *ngIf=\"notifications.length\">\r\n    <small><a href=\"#\" (click)=\"clearAll($event)\">Clear all notifications</a></small>\r\n</div>\r\n"
        }),
        tslib_1.__metadata("design:paramtypes", [Router])
    ], NotificationsComponent);
    return NotificationsComponent;
}());
export { NotificationsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9ucy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvZWdyZXQvIiwic291cmNlcyI6WyJjb21wb25lbnRzL25vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9ucy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ3pELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFNeEQ7SUF3QkksZ0NBQW9CLE1BQWM7UUFBZCxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBckJsQyxzQkFBc0I7UUFDdEIsa0JBQWEsR0FBRyxDQUFDO2dCQUNiLE9BQU8sRUFBRSxtQkFBbUI7Z0JBQzVCLElBQUksRUFBRSxnQkFBZ0I7Z0JBQ3RCLElBQUksRUFBRSxXQUFXO2dCQUNqQixLQUFLLEVBQUUsUUFBUTtnQkFDZixLQUFLLEVBQUUsU0FBUzthQUNuQixFQUFFO2dCQUNDLE9BQU8sRUFBRSxhQUFhO2dCQUN0QixJQUFJLEVBQUUsTUFBTTtnQkFDWixJQUFJLEVBQUUsV0FBVztnQkFDakIsS0FBSyxFQUFFLE9BQU87Z0JBQ2QsS0FBSyxFQUFFLFFBQVE7YUFDbEIsRUFBRTtnQkFDQyxPQUFPLEVBQUUsaUJBQWlCO2dCQUMxQixJQUFJLEVBQUUseUJBQXlCO2dCQUMvQixJQUFJLEVBQUUsWUFBWTtnQkFDbEIsS0FBSyxFQUFFLFNBQVM7Z0JBQ2hCLEtBQUssRUFBRSxNQUFNO2FBQ2hCLENBQUMsQ0FBQTtJQUdGLENBQUM7SUFFRCx5Q0FBUSxHQUFSO1FBQUEsaUJBTUM7UUFMRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsVUFBQyxXQUFXO1lBQ3JDLElBQUksV0FBVyxZQUFZLGFBQWEsRUFBRTtnQkFDdEMsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUM3QjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHlDQUFRLEdBQVIsVUFBUyxDQUFDO1FBQ04sQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO0lBQzVCLENBQUM7O2dCQWQyQixNQUFNOztJQXZCekI7UUFBUixLQUFLLEVBQUU7O2dFQUFjO0lBRGIsc0JBQXNCO1FBSmxDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxxQkFBcUI7WUFDL0IsbzVCQUE2QztTQUNoRCxDQUFDO2lEQXlCOEIsTUFBTTtPQXhCekIsc0JBQXNCLENBdUNsQztJQUFELDZCQUFDO0NBQUEsQUF2Q0QsSUF1Q0M7U0F2Q1ksc0JBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5hdmlnYXRpb25FbmQsIFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnZWdyZXQtbm90aWZpY2F0aW9ucycsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vbm90aWZpY2F0aW9ucy5jb21wb25lbnQuaHRtbCdcclxufSlcclxuZXhwb3J0IGNsYXNzIE5vdGlmaWNhdGlvbnNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgQElucHV0KCkgbm90aWZpY1BhbmVsO1xyXG5cclxuICAgIC8vIER1bW15IG5vdGlmaWNhdGlvbnNcclxuICAgIG5vdGlmaWNhdGlvbnMgPSBbe1xyXG4gICAgICAgIG1lc3NhZ2U6ICdOZXcgY29udGFjdCBhZGRlZCcsXHJcbiAgICAgICAgaWNvbjogJ2Fzc2lnbm1lbnRfaW5kJyxcclxuICAgICAgICB0aW1lOiAnMSBtaW4gYWdvJyxcclxuICAgICAgICByb3V0ZTogJy9pbmJveCcsXHJcbiAgICAgICAgY29sb3I6ICdwcmltYXJ5J1xyXG4gICAgfSwge1xyXG4gICAgICAgIG1lc3NhZ2U6ICdOZXcgbWVzc2FnZScsXHJcbiAgICAgICAgaWNvbjogJ2NoYXQnLFxyXG4gICAgICAgIHRpbWU6ICc0IG1pbiBhZ28nLFxyXG4gICAgICAgIHJvdXRlOiAnL2NoYXQnLFxyXG4gICAgICAgIGNvbG9yOiAnYWNjZW50J1xyXG4gICAgfSwge1xyXG4gICAgICAgIG1lc3NhZ2U6ICdTZXJ2ZXIgcmVib290ZWQnLFxyXG4gICAgICAgIGljb246ICdzZXR0aW5nc19iYWNrdXBfcmVzdG9yZScsXHJcbiAgICAgICAgdGltZTogJzEyIG1pbiBhZ28nLFxyXG4gICAgICAgIHJvdXRlOiAnL2NoYXJ0cycsXHJcbiAgICAgICAgY29sb3I6ICd3YXJuJ1xyXG4gICAgfV1cclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjogUm91dGVyKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5yb3V0ZXIuZXZlbnRzLnN1YnNjcmliZSgocm91dGVDaGFuZ2UpID0+IHtcclxuICAgICAgICAgICAgaWYgKHJvdXRlQ2hhbmdlIGluc3RhbmNlb2YgTmF2aWdhdGlvbkVuZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljUGFuZWwuY2xvc2UoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGNsZWFyQWxsKGUpIHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25zID0gW107XHJcbiAgICB9XHJcbn1cclxuIl19