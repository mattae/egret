import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
// import PerfectScrollbar from 'perfect-scrollbar';
import { NavigationService } from '../../services/navigation.service';
var SidebarTopComponent = /** @class */ (function () {
    function SidebarTopComponent(navService) {
        this.navService = navService;
    }
    SidebarTopComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.menuItemsSub = this.navService.menuItems$.subscribe(function (menuItem) {
            _this.menuItems = menuItem.filter(function (item) { return item.type !== 'icon' && item.type !== 'separator'; });
        });
    };
    SidebarTopComponent.prototype.ngAfterViewInit = function () {
        // setTimeout(() => {
        //   this.sidebarPS = new PerfectScrollbar('#sidebar-top-scroll-area', {
        //     suppressScrollX: true
        //   })
        // })
    };
    SidebarTopComponent.prototype.ngOnDestroy = function () {
        // if(this.sidebarPS) {
        //   this.sidebarPS.destroy();
        // }
        if (this.menuItemsSub) {
            this.menuItemsSub.unsubscribe();
        }
    };
    SidebarTopComponent.ctorParameters = function () { return [
        { type: NavigationService }
    ]; };
    SidebarTopComponent = tslib_1.__decorate([
        Component({
            selector: 'egret-sidebar-top',
            template: "<div class=\"sidebar-panel\">\r\n    <div id=\"sidebar-top-scroll-area\" [perfectScrollbar] class=\"navigation-hold\" fxLayout=\"column\">\r\n        <egret-sidenav [items]=\"menuItems\"></egret-sidenav>\r\n    </div>\r\n</div>\r\n"
        }),
        tslib_1.__metadata("design:paramtypes", [NavigationService])
    ], SidebarTopComponent);
    return SidebarTopComponent;
}());
export { SidebarTopComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lkZWJhci10b3AuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9zaWRlYmFyLXRvcC9zaWRlYmFyLXRvcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBaUIsU0FBUyxFQUFxQixNQUFNLGVBQWUsQ0FBQztBQUM1RSxvREFBb0Q7QUFDcEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFPdEU7SUFLQyw2QkFDUyxVQUE2QjtRQUE3QixlQUFVLEdBQVYsVUFBVSxDQUFtQjtJQUV0QyxDQUFDO0lBRUQsc0NBQVEsR0FBUjtRQUFBLGlCQUlDO1FBSEEsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO1lBQ2hFLEtBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxJQUFJLEtBQUssTUFBTSxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssV0FBVyxFQUFqRCxDQUFpRCxDQUFDLENBQUM7UUFDN0YsQ0FBQyxDQUFDLENBQUM7SUFDSixDQUFDO0lBRUQsNkNBQWUsR0FBZjtRQUNDLHFCQUFxQjtRQUNyQix3RUFBd0U7UUFDeEUsNEJBQTRCO1FBQzVCLE9BQU87UUFDUCxLQUFLO0lBQ04sQ0FBQztJQUVELHlDQUFXLEdBQVg7UUFDQyx1QkFBdUI7UUFDdkIsOEJBQThCO1FBQzlCLElBQUk7UUFDSixJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDdEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQTtTQUMvQjtJQUNGLENBQUM7O2dCQXpCb0IsaUJBQWlCOztJQU4xQixtQkFBbUI7UUFKL0IsU0FBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLG1CQUFtQjtZQUM3QixtUEFBMkM7U0FDM0MsQ0FBQztpREFPb0IsaUJBQWlCO09BTjFCLG1CQUFtQixDQWdDL0I7SUFBRCwwQkFBQztDQUFBLEFBaENELElBZ0NDO1NBaENZLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFmdGVyVmlld0luaXQsIENvbXBvbmVudCwgT25EZXN0cm95LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuLy8gaW1wb3J0IFBlcmZlY3RTY3JvbGxiYXIgZnJvbSAncGVyZmVjdC1zY3JvbGxiYXInO1xyXG5pbXBvcnQgeyBOYXZpZ2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL25hdmlnYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcblx0c2VsZWN0b3I6ICdlZ3JldC1zaWRlYmFyLXRvcCcsXHJcblx0dGVtcGxhdGVVcmw6ICcuL3NpZGViYXItdG9wLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgU2lkZWJhclRvcENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95LCBBZnRlclZpZXdJbml0IHtcclxuXHQvLyBwcml2YXRlIHNpZGViYXJQUzogUGVyZmVjdFNjcm9sbGJhcjtcclxuXHRwdWJsaWMgbWVudUl0ZW1zOiBhbnlbXTtcclxuXHRwcml2YXRlIG1lbnVJdGVtc1N1YjogU3Vic2NyaXB0aW9uO1xyXG5cclxuXHRjb25zdHJ1Y3RvcihcclxuXHRcdHByaXZhdGUgbmF2U2VydmljZTogTmF2aWdhdGlvblNlcnZpY2VcclxuXHQpIHtcclxuXHR9XHJcblxyXG5cdG5nT25Jbml0KCkge1xyXG5cdFx0dGhpcy5tZW51SXRlbXNTdWIgPSB0aGlzLm5hdlNlcnZpY2UubWVudUl0ZW1zJC5zdWJzY3JpYmUobWVudUl0ZW0gPT4ge1xyXG5cdFx0XHR0aGlzLm1lbnVJdGVtcyA9IG1lbnVJdGVtLmZpbHRlcihpdGVtID0+IGl0ZW0udHlwZSAhPT0gJ2ljb24nICYmIGl0ZW0udHlwZSAhPT0gJ3NlcGFyYXRvcicpO1xyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHRuZ0FmdGVyVmlld0luaXQoKSB7XHJcblx0XHQvLyBzZXRUaW1lb3V0KCgpID0+IHtcclxuXHRcdC8vICAgdGhpcy5zaWRlYmFyUFMgPSBuZXcgUGVyZmVjdFNjcm9sbGJhcignI3NpZGViYXItdG9wLXNjcm9sbC1hcmVhJywge1xyXG5cdFx0Ly8gICAgIHN1cHByZXNzU2Nyb2xsWDogdHJ1ZVxyXG5cdFx0Ly8gICB9KVxyXG5cdFx0Ly8gfSlcclxuXHR9XHJcblxyXG5cdG5nT25EZXN0cm95KCkge1xyXG5cdFx0Ly8gaWYodGhpcy5zaWRlYmFyUFMpIHtcclxuXHRcdC8vICAgdGhpcy5zaWRlYmFyUFMuZGVzdHJveSgpO1xyXG5cdFx0Ly8gfVxyXG5cdFx0aWYgKHRoaXMubWVudUl0ZW1zU3ViKSB7XHJcblx0XHRcdHRoaXMubWVudUl0ZW1zU3ViLnVuc3Vic2NyaWJlKClcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuIl19