import * as tslib_1 from "tslib";
import { CoreModule } from '@alfresco/adf-core';
import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { MatButtonModule, MatCardModule, MatCheckboxModule, MatDialogModule, MatGridListModule, MatIconModule, MatListModule, MatMenuModule, MatOptionModule, MatProgressSpinnerModule, MatRadioModule, MatRippleModule, MatSelectModule, MatSidenavModule, MatSnackBarModule, MatToolbarModule, MatTooltipModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
// ONLY FOR DEMO (Removable without changing any layout configuration)
// ONLY REQUIRED FOR **SIDE** NAVIGATION LAYOUT
import { HeaderSideComponent } from './components/header-side/header-side.component';
// ONLY REQUIRED FOR **TOP** NAVIGATION LAYOUT
import { HeaderTopComponent } from './components/header-top/header-top.component';
// ALL TIME REQUIRED
import { AdminLayoutComponent } from './components/layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './components/layouts/auth-layout/auth-layout.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { SidebarSideComponent } from './components/sidebar-side/sidebar-side.component';
import { SidebarTopComponent } from './components/sidebar-top/sidebar-top.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
// DIRECTIVES
import { ExcerptPipe } from './pipes/excerpt.pipe';
// PIPES
import { RelativeTimePipe } from './pipes/relative-time.pipe';
import { LayoutService } from './services/layout.service';
import { NavigationService } from './services/navigation.service';
import { RoutePartsService } from './services/route-parts.service';
// SERVICES
import { ThemeService } from './services/theme.service';
import { LamisCoreModule } from '@lamis/web-core';
import { GetValueByKeyPipe } from './pipes/get-value-by-key.pipe';
import { SharedDirectivesModule } from './directives/shared-directives.module';
import { EgretSidebarComponent } from './components/egret-sidebar/egret-sidebar.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { TranslateModule } from '@ngx-translate/core';
import { CustomizerComponent } from './components/customizer/customizer.component';
var classesToInclude = [
    HeaderTopComponent,
    SidebarTopComponent,
    SidenavComponent,
    NotificationsComponent,
    SidebarSideComponent,
    EgretSidebarComponent,
    HeaderSideComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    BreadcrumbComponent,
    RelativeTimePipe,
    ExcerptPipe,
    GetValueByKeyPipe,
    CustomizerComponent
];
var EgretModule = /** @class */ (function () {
    function EgretModule() {
    }
    EgretModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                RouterModule,
                LamisCoreModule,
                FlexLayoutModule,
                CoreModule,
                MatSidenavModule,
                MatListModule,
                MatTooltipModule,
                MatOptionModule,
                MatSelectModule,
                MatMenuModule,
                MatSnackBarModule,
                MatGridListModule,
                MatToolbarModule,
                MatIconModule,
                MatButtonModule,
                MatRadioModule,
                MatCheckboxModule,
                MatCardModule,
                MatProgressSpinnerModule,
                MatRippleModule,
                MatDialogModule,
                SharedDirectivesModule,
                PerfectScrollbarModule,
                TranslateModule
            ],
            entryComponents: [],
            providers: [
                ThemeService,
                LayoutService,
                NavigationService,
                RoutePartsService
            ],
            declarations: classesToInclude,
            exports: tslib_1.__spread(classesToInclude, [SharedDirectivesModule]),
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
    ], EgretModule);
    return EgretModule;
}());
export { EgretModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWdyZXQubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsiZWdyZXQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDaEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDakUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFDTixlQUFlLEVBQ2YsYUFBYSxFQUNiLGlCQUFpQixFQUNqQixlQUFlLEVBQ2YsaUJBQWlCLEVBQ2pCLGFBQWEsRUFDYixhQUFhLEVBQ2IsYUFBYSxFQUNiLGVBQWUsRUFDZix3QkFBd0IsRUFDeEIsY0FBYyxFQUNkLGVBQWUsRUFDZixlQUFlLEVBQ2YsZ0JBQWdCLEVBQ2hCLGlCQUFpQixFQUNqQixnQkFBZ0IsRUFDaEIsZ0JBQWdCLEVBQ2hCLE1BQU0sbUJBQW1CLENBQUM7QUFDM0IsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQ25GLHNFQUFzRTtBQUN0RSwrQ0FBK0M7QUFDL0MsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDckYsOENBQThDO0FBQzlDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQ2xGLG9CQUFvQjtBQUNwQixPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSwwREFBMEQsQ0FBQztBQUNoRyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx3REFBd0QsQ0FBQztBQUM3RixPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSxvREFBb0QsQ0FBQztBQUM1RixPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUN4RixPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUNyRixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUMxRSxhQUFhO0FBQ2IsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ25ELFFBQVE7QUFDUixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDMUQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDbEUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDbkUsV0FBVztBQUNYLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDbEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDbEUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDL0UsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFDM0YsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDL0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBRW5GLElBQU0sZ0JBQWdCLEdBQUc7SUFDeEIsa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsc0JBQXNCO0lBQ3RCLG9CQUFvQjtJQUNwQixxQkFBcUI7SUFDckIsbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQixtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixXQUFXO0lBQ1gsaUJBQWlCO0lBQ2pCLG1CQUFtQjtDQUNuQixDQUFDO0FBMkNGO0lBQUE7SUFDQSxDQUFDO0lBRFksV0FBVztRQXpDdkIsUUFBUSxDQUFDO1lBQ1QsT0FBTyxFQUFFO2dCQUNSLFlBQVk7Z0JBQ1osV0FBVztnQkFDWCxZQUFZO2dCQUNaLGVBQWU7Z0JBQ2YsZ0JBQWdCO2dCQUNoQixVQUFVO2dCQUNWLGdCQUFnQjtnQkFDaEIsYUFBYTtnQkFDYixnQkFBZ0I7Z0JBQ2hCLGVBQWU7Z0JBQ2YsZUFBZTtnQkFDZixhQUFhO2dCQUNiLGlCQUFpQjtnQkFDakIsaUJBQWlCO2dCQUNqQixnQkFBZ0I7Z0JBQ2hCLGFBQWE7Z0JBQ2IsZUFBZTtnQkFDZixjQUFjO2dCQUNkLGlCQUFpQjtnQkFDakIsYUFBYTtnQkFDYix3QkFBd0I7Z0JBQ3hCLGVBQWU7Z0JBQ2YsZUFBZTtnQkFDZixzQkFBc0I7Z0JBQ3RCLHNCQUFzQjtnQkFDdEIsZUFBZTthQUNmO1lBQ0QsZUFBZSxFQUFFLEVBQ2hCO1lBQ0QsU0FBUyxFQUFFO2dCQUNWLFlBQVk7Z0JBQ1osYUFBYTtnQkFDYixpQkFBaUI7Z0JBQ2pCLGlCQUFpQjthQUNqQjtZQUNELFlBQVksRUFBRSxnQkFBZ0I7WUFDOUIsT0FBTyxtQkFBTSxnQkFBZ0IsR0FBRSxzQkFBc0IsRUFBQztZQUN0RCxPQUFPLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQztTQUNqQyxDQUFDO09BQ1csV0FBVyxDQUN2QjtJQUFELGtCQUFDO0NBQUEsQUFERCxJQUNDO1NBRFksV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvcmVNb2R1bGUgfSBmcm9tICdAYWxmcmVzY28vYWRmLWNvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBDVVNUT01fRUxFTUVOVFNfU0NIRU1BLCBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGbGV4TGF5b3V0TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZmxleC1sYXlvdXQnO1xyXG5pbXBvcnQgeyBGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHtcclxuXHRNYXRCdXR0b25Nb2R1bGUsXHJcblx0TWF0Q2FyZE1vZHVsZSxcclxuXHRNYXRDaGVja2JveE1vZHVsZSxcclxuXHRNYXREaWFsb2dNb2R1bGUsXHJcblx0TWF0R3JpZExpc3RNb2R1bGUsXHJcblx0TWF0SWNvbk1vZHVsZSxcclxuXHRNYXRMaXN0TW9kdWxlLFxyXG5cdE1hdE1lbnVNb2R1bGUsXHJcblx0TWF0T3B0aW9uTW9kdWxlLFxyXG5cdE1hdFByb2dyZXNzU3Bpbm5lck1vZHVsZSxcclxuXHRNYXRSYWRpb01vZHVsZSxcclxuXHRNYXRSaXBwbGVNb2R1bGUsXHJcblx0TWF0U2VsZWN0TW9kdWxlLFxyXG5cdE1hdFNpZGVuYXZNb2R1bGUsXHJcblx0TWF0U25hY2tCYXJNb2R1bGUsXHJcblx0TWF0VG9vbGJhck1vZHVsZSxcclxuXHRNYXRUb29sdGlwTW9kdWxlXHJcbn0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBCcmVhZGNydW1iQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2JyZWFkY3J1bWIvYnJlYWRjcnVtYi5jb21wb25lbnQnO1xyXG4vLyBPTkxZIEZPUiBERU1PIChSZW1vdmFibGUgd2l0aG91dCBjaGFuZ2luZyBhbnkgbGF5b3V0IGNvbmZpZ3VyYXRpb24pXHJcbi8vIE9OTFkgUkVRVUlSRUQgRk9SICoqU0lERSoqIE5BVklHQVRJT04gTEFZT1VUXHJcbmltcG9ydCB7IEhlYWRlclNpZGVDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvaGVhZGVyLXNpZGUvaGVhZGVyLXNpZGUuY29tcG9uZW50JztcclxuLy8gT05MWSBSRVFVSVJFRCBGT1IgKipUT1AqKiBOQVZJR0FUSU9OIExBWU9VVFxyXG5pbXBvcnQgeyBIZWFkZXJUb3BDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvaGVhZGVyLXRvcC9oZWFkZXItdG9wLmNvbXBvbmVudCc7XHJcbi8vIEFMTCBUSU1FIFJFUVVJUkVEXHJcbmltcG9ydCB7IEFkbWluTGF5b3V0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2xheW91dHMvYWRtaW4tbGF5b3V0L2FkbWluLWxheW91dC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBBdXRoTGF5b3V0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2xheW91dHMvYXV0aC1sYXlvdXQvYXV0aC1sYXlvdXQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uc0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9ub3RpZmljYXRpb25zL25vdGlmaWNhdGlvbnMuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU2lkZWJhclNpZGVDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvc2lkZWJhci1zaWRlL3NpZGViYXItc2lkZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTaWRlYmFyVG9wQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3NpZGViYXItdG9wL3NpZGViYXItdG9wLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFNpZGVuYXZDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvc2lkZW5hdi9zaWRlbmF2LmNvbXBvbmVudCc7XHJcbi8vIERJUkVDVElWRVNcclxuaW1wb3J0IHsgRXhjZXJwdFBpcGUgfSBmcm9tICcuL3BpcGVzL2V4Y2VycHQucGlwZSc7XHJcbi8vIFBJUEVTXHJcbmltcG9ydCB7IFJlbGF0aXZlVGltZVBpcGUgfSBmcm9tICcuL3BpcGVzL3JlbGF0aXZlLXRpbWUucGlwZSc7XHJcbmltcG9ydCB7IExheW91dFNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2xheW91dC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTmF2aWdhdGlvblNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL25hdmlnYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IFJvdXRlUGFydHNTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9yb3V0ZS1wYXJ0cy5zZXJ2aWNlJztcclxuLy8gU0VSVklDRVNcclxuaW1wb3J0IHsgVGhlbWVTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy90aGVtZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTGFtaXNDb3JlTW9kdWxlIH0gZnJvbSAnQGxhbWlzL3dlYi1jb3JlJztcclxuaW1wb3J0IHsgR2V0VmFsdWVCeUtleVBpcGUgfSBmcm9tICcuL3BpcGVzL2dldC12YWx1ZS1ieS1rZXkucGlwZSc7XHJcbmltcG9ydCB7IFNoYXJlZERpcmVjdGl2ZXNNb2R1bGUgfSBmcm9tICcuL2RpcmVjdGl2ZXMvc2hhcmVkLWRpcmVjdGl2ZXMubW9kdWxlJztcclxuaW1wb3J0IHsgRWdyZXRTaWRlYmFyQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2VncmV0LXNpZGViYXIvZWdyZXQtc2lkZWJhci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBQZXJmZWN0U2Nyb2xsYmFyTW9kdWxlIH0gZnJvbSAnbmd4LXBlcmZlY3Qtc2Nyb2xsYmFyJztcclxuaW1wb3J0IHsgVHJhbnNsYXRlTW9kdWxlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XHJcbmltcG9ydCB7IEN1c3RvbWl6ZXJDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY3VzdG9taXplci9jdXN0b21pemVyLmNvbXBvbmVudCc7XHJcblxyXG5jb25zdCBjbGFzc2VzVG9JbmNsdWRlID0gW1xyXG5cdEhlYWRlclRvcENvbXBvbmVudCxcclxuXHRTaWRlYmFyVG9wQ29tcG9uZW50LFxyXG5cdFNpZGVuYXZDb21wb25lbnQsXHJcblx0Tm90aWZpY2F0aW9uc0NvbXBvbmVudCxcclxuXHRTaWRlYmFyU2lkZUNvbXBvbmVudCxcclxuXHRFZ3JldFNpZGViYXJDb21wb25lbnQsXHJcblx0SGVhZGVyU2lkZUNvbXBvbmVudCxcclxuXHRBZG1pbkxheW91dENvbXBvbmVudCxcclxuXHRBdXRoTGF5b3V0Q29tcG9uZW50LFxyXG5cdEJyZWFkY3J1bWJDb21wb25lbnQsXHJcblx0UmVsYXRpdmVUaW1lUGlwZSxcclxuXHRFeGNlcnB0UGlwZSxcclxuXHRHZXRWYWx1ZUJ5S2V5UGlwZSxcclxuXHRDdXN0b21pemVyQ29tcG9uZW50XHJcbl07XHJcblxyXG5ATmdNb2R1bGUoe1xyXG5cdGltcG9ydHM6IFtcclxuXHRcdENvbW1vbk1vZHVsZSxcclxuXHRcdEZvcm1zTW9kdWxlLFxyXG5cdFx0Um91dGVyTW9kdWxlLFxyXG5cdFx0TGFtaXNDb3JlTW9kdWxlLFxyXG5cdFx0RmxleExheW91dE1vZHVsZSxcclxuXHRcdENvcmVNb2R1bGUsXHJcblx0XHRNYXRTaWRlbmF2TW9kdWxlLFxyXG5cdFx0TWF0TGlzdE1vZHVsZSxcclxuXHRcdE1hdFRvb2x0aXBNb2R1bGUsXHJcblx0XHRNYXRPcHRpb25Nb2R1bGUsXHJcblx0XHRNYXRTZWxlY3RNb2R1bGUsXHJcblx0XHRNYXRNZW51TW9kdWxlLFxyXG5cdFx0TWF0U25hY2tCYXJNb2R1bGUsXHJcblx0XHRNYXRHcmlkTGlzdE1vZHVsZSxcclxuXHRcdE1hdFRvb2xiYXJNb2R1bGUsXHJcblx0XHRNYXRJY29uTW9kdWxlLFxyXG5cdFx0TWF0QnV0dG9uTW9kdWxlLFxyXG5cdFx0TWF0UmFkaW9Nb2R1bGUsXHJcblx0XHRNYXRDaGVja2JveE1vZHVsZSxcclxuXHRcdE1hdENhcmRNb2R1bGUsXHJcblx0XHRNYXRQcm9ncmVzc1NwaW5uZXJNb2R1bGUsXHJcblx0XHRNYXRSaXBwbGVNb2R1bGUsXHJcblx0XHRNYXREaWFsb2dNb2R1bGUsXHJcblx0XHRTaGFyZWREaXJlY3RpdmVzTW9kdWxlLFxyXG5cdFx0UGVyZmVjdFNjcm9sbGJhck1vZHVsZSxcclxuXHRcdFRyYW5zbGF0ZU1vZHVsZVxyXG5cdF0sXHJcblx0ZW50cnlDb21wb25lbnRzOiBbXHJcblx0XSxcclxuXHRwcm92aWRlcnM6IFtcclxuXHRcdFRoZW1lU2VydmljZSxcclxuXHRcdExheW91dFNlcnZpY2UsXHJcblx0XHROYXZpZ2F0aW9uU2VydmljZSxcclxuXHRcdFJvdXRlUGFydHNTZXJ2aWNlXHJcblx0XSxcclxuXHRkZWNsYXJhdGlvbnM6IGNsYXNzZXNUb0luY2x1ZGUsXHJcblx0ZXhwb3J0czogWy4uLmNsYXNzZXNUb0luY2x1ZGUsIFNoYXJlZERpcmVjdGl2ZXNNb2R1bGVdLFxyXG5cdHNjaGVtYXM6IFtDVVNUT01fRUxFTUVOVFNfU0NIRU1BXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRWdyZXRNb2R1bGUge1xyXG59XHJcbiJdfQ==