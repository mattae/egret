import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { AppConfirmComponent } from './app-confirm.component';
let AppConfirmService = class AppConfirmService {
    constructor(dialog) {
        this.dialog = dialog;
    }
    confirm(data = {}) {
        data.title = data.title || 'Confirm';
        data.message = data.message || 'Are you sure?';
        let dialogRef;
        dialogRef = this.dialog.open(AppConfirmComponent, {
            width: '380px',
            disableClose: true,
            data: { title: data.title, message: data.message }
        });
        return dialogRef.afterClosed();
    }
};
AppConfirmService.ctorParameters = () => [
    { type: MatDialog }
];
AppConfirmService = tslib_1.__decorate([
    Injectable(),
    tslib_1.__metadata("design:paramtypes", [MatDialog])
], AppConfirmService);
export { AppConfirmService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLWNvbmZpcm0uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2FwcC1jb25maXJtL2FwcC1jb25maXJtLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUc1RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQVE5RCxJQUFhLGlCQUFpQixHQUE5QixNQUFhLGlCQUFpQjtJQUUxQixZQUFvQixNQUFpQjtRQUFqQixXQUFNLEdBQU4sTUFBTSxDQUFXO0lBQ3JDLENBQUM7SUFFTSxPQUFPLENBQUMsT0FBb0IsRUFBRTtRQUNqQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLElBQUksU0FBUyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sSUFBSSxlQUFlLENBQUM7UUFDL0MsSUFBSSxTQUE0QyxDQUFDO1FBQ2pELFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtZQUM5QyxLQUFLLEVBQUUsT0FBTztZQUNkLFlBQVksRUFBRSxJQUFJO1lBQ2xCLElBQUksRUFBRSxFQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFDO1NBQ25ELENBQUMsQ0FBQztRQUNILE9BQU8sU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ25DLENBQUM7Q0FDSixDQUFBOztZQWQrQixTQUFTOztBQUY1QixpQkFBaUI7SUFEN0IsVUFBVSxFQUFFOzZDQUdtQixTQUFTO0dBRjVCLGlCQUFpQixDQWdCN0I7U0FoQlksaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2csIE1hdERpYWxvZ1JlZiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQXBwQ29uZmlybUNvbXBvbmVudCB9IGZyb20gJy4vYXBwLWNvbmZpcm0uY29tcG9uZW50JztcclxuXHJcbmludGVyZmFjZSBjb25maXJtRGF0YSB7XHJcbiAgICB0aXRsZT86IHN0cmluZyxcclxuICAgIG1lc3NhZ2U/OiBzdHJpbmdcclxufVxyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQXBwQ29uZmlybVNlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgZGlhbG9nOiBNYXREaWFsb2cpIHtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgY29uZmlybShkYXRhOiBjb25maXJtRGF0YSA9IHt9KTogT2JzZXJ2YWJsZTxib29sZWFuPiB7XHJcbiAgICAgICAgZGF0YS50aXRsZSA9IGRhdGEudGl0bGUgfHwgJ0NvbmZpcm0nO1xyXG4gICAgICAgIGRhdGEubWVzc2FnZSA9IGRhdGEubWVzc2FnZSB8fCAnQXJlIHlvdSBzdXJlPyc7XHJcbiAgICAgICAgbGV0IGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPEFwcENvbmZpcm1Db21wb25lbnQ+O1xyXG4gICAgICAgIGRpYWxvZ1JlZiA9IHRoaXMuZGlhbG9nLm9wZW4oQXBwQ29uZmlybUNvbXBvbmVudCwge1xyXG4gICAgICAgICAgICB3aWR0aDogJzM4MHB4JyxcclxuICAgICAgICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxyXG4gICAgICAgICAgICBkYXRhOiB7dGl0bGU6IGRhdGEudGl0bGUsIG1lc3NhZ2U6IGRhdGEubWVzc2FnZX1cclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gZGlhbG9nUmVmLmFmdGVyQ2xvc2VkKCk7XHJcbiAgICB9XHJcbn1cclxuIl19