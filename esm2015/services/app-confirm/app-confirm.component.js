import * as tslib_1 from "tslib";
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
let AppConfirmComponent = class AppConfirmComponent {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
};
AppConfirmComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
];
AppConfirmComponent = tslib_1.__decorate([
    Component({
        selector: 'app-confirm',
        template: `<h1 matDialogTitle>{{ data.title }}</h1>
    <div mat-dialog-content>{{ data.message }}</div>
    <div mat-dialog-actions>
        <button
            type="button"
            mat-raised-button
            color="primary"
            (click)="dialogRef.close(true)">OK
        </button>
        &nbsp;
        <span fxFlex></span>
        <button
            type="button"
            color="accent"
            mat-raised-button
            (click)="dialogRef.close(false)">Cancel
        </button>
    </div>`
    }),
    tslib_1.__param(1, Inject(MAT_DIALOG_DATA)),
    tslib_1.__metadata("design:paramtypes", [MatDialogRef, Object])
], AppConfirmComponent);
export { AppConfirmComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLWNvbmZpcm0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsic2VydmljZXMvYXBwLWNvbmZpcm0vYXBwLWNvbmZpcm0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNsRCxPQUFPLEVBQUUsZUFBZSxFQUFFLFlBQVksRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBdUJsRSxJQUFhLG1CQUFtQixHQUFoQyxNQUFhLG1CQUFtQjtJQUM1QixZQUNXLFNBQTRDLEVBQ25CLElBQVM7UUFEbEMsY0FBUyxHQUFULFNBQVMsQ0FBbUM7UUFDbkIsU0FBSSxHQUFKLElBQUksQ0FBSztJQUU3QyxDQUFDO0NBQ0osQ0FBQTs7WUFKeUIsWUFBWTs0Q0FDN0IsTUFBTSxTQUFDLGVBQWU7O0FBSGxCLG1CQUFtQjtJQXJCL0IsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLGFBQWE7UUFDdkIsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7OztXQWlCSDtLQUNWLENBQUM7SUFJTyxtQkFBQSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUE7NkNBRE4sWUFBWTtHQUZ6QixtQkFBbUIsQ0FNL0I7U0FOWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNQVRfRElBTE9HX0RBVEEsIE1hdERpYWxvZ1JlZiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhcHAtY29uZmlybScsXHJcbiAgICB0ZW1wbGF0ZTogYDxoMSBtYXREaWFsb2dUaXRsZT57eyBkYXRhLnRpdGxlIH19PC9oMT5cclxuICAgIDxkaXYgbWF0LWRpYWxvZy1jb250ZW50Pnt7IGRhdGEubWVzc2FnZSB9fTwvZGl2PlxyXG4gICAgPGRpdiBtYXQtZGlhbG9nLWFjdGlvbnM+XHJcbiAgICAgICAgPGJ1dHRvblxyXG4gICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcclxuICAgICAgICAgICAgbWF0LXJhaXNlZC1idXR0b25cclxuICAgICAgICAgICAgY29sb3I9XCJwcmltYXJ5XCJcclxuICAgICAgICAgICAgKGNsaWNrKT1cImRpYWxvZ1JlZi5jbG9zZSh0cnVlKVwiPk9LXHJcbiAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgJm5ic3A7XHJcbiAgICAgICAgPHNwYW4gZnhGbGV4Pjwvc3Bhbj5cclxuICAgICAgICA8YnV0dG9uXHJcbiAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxyXG4gICAgICAgICAgICBjb2xvcj1cImFjY2VudFwiXHJcbiAgICAgICAgICAgIG1hdC1yYWlzZWQtYnV0dG9uXHJcbiAgICAgICAgICAgIChjbGljayk9XCJkaWFsb2dSZWYuY2xvc2UoZmFsc2UpXCI+Q2FuY2VsXHJcbiAgICAgICAgPC9idXR0b24+XHJcbiAgICA8L2Rpdj5gLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgQXBwQ29uZmlybUNvbXBvbmVudCB7XHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8QXBwQ29uZmlybUNvbXBvbmVudD4sXHJcbiAgICAgICAgQEluamVjdChNQVRfRElBTE9HX0RBVEEpIHB1YmxpYyBkYXRhOiBhbnlcclxuICAgICkge1xyXG4gICAgfVxyXG59XHJcbiJdfQ==