import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { MenuService } from '@lamis/web-core';
let NavigationService = class NavigationService {
    constructor(menuService) {
        this.menuService = menuService;
        // This title will appear if any icon type item is present in menu.
        this.iconTypeMenuTitle = 'Frequently Accessed';
        this.menuItems$ = menuService.getMenus();
    }
    publishNavigationChange(menuType) {
        /*switch (menuType) {
            case "separator-menu":
                this.menuItems.next(this.separatorMenu);
                break;
            case "icon-menu":
                this.menuItems.next(this.iconMenu);
                break;
            default:
                this.menuItems.next(this.plainMenu);
        }*/
    }
};
NavigationService.ctorParameters = () => [
    { type: MenuService }
];
NavigationService = tslib_1.__decorate([
    Injectable(),
    tslib_1.__metadata("design:paramtypes", [MenuService])
], NavigationService);
export { NavigationService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsic2VydmljZXMvbmF2aWdhdGlvbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUc5QyxJQUFhLGlCQUFpQixHQUE5QixNQUFhLGlCQUFpQjtJQU8xQixZQUFvQixXQUF3QjtRQUF4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUw1QyxtRUFBbUU7UUFDbkUsc0JBQWlCLEdBQVcscUJBQXFCLENBQUM7UUFLOUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDN0MsQ0FBQztJQUVELHVCQUF1QixDQUFDLFFBQWdCO1FBQ3BDOzs7Ozs7Ozs7V0FTRztJQUNQLENBQUM7Q0FDSixDQUFBOztZQWhCb0MsV0FBVzs7QUFQbkMsaUJBQWlCO0lBRDdCLFVBQVUsRUFBRTs2Q0FRd0IsV0FBVztHQVBuQyxpQkFBaUIsQ0F1QjdCO1NBdkJZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWVudVNlcnZpY2UgfSBmcm9tICdAbGFtaXMvd2ViLWNvcmUnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgTmF2aWdhdGlvblNlcnZpY2Uge1xyXG5cclxuICAgIC8vIFRoaXMgdGl0bGUgd2lsbCBhcHBlYXIgaWYgYW55IGljb24gdHlwZSBpdGVtIGlzIHByZXNlbnQgaW4gbWVudS5cclxuICAgIGljb25UeXBlTWVudVRpdGxlOiBzdHJpbmcgPSAnRnJlcXVlbnRseSBBY2Nlc3NlZCc7XHJcbiAgICAvLyBuYXZpZ2F0aW9uIGNvbXBvbmVudCBoYXMgc3Vic2NyaWJlZCB0byB0aGlzIE9ic2VydmFibGVcclxuICAgIG1lbnVJdGVtcyQ7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBtZW51U2VydmljZTogTWVudVNlcnZpY2UpIHtcclxuICAgICAgICB0aGlzLm1lbnVJdGVtcyQgPSBtZW51U2VydmljZS5nZXRNZW51cygpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1Ymxpc2hOYXZpZ2F0aW9uQ2hhbmdlKG1lbnVUeXBlOiBzdHJpbmcpIHtcclxuICAgICAgICAvKnN3aXRjaCAobWVudVR5cGUpIHtcclxuICAgICAgICAgICAgY2FzZSBcInNlcGFyYXRvci1tZW51XCI6XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1lbnVJdGVtcy5uZXh0KHRoaXMuc2VwYXJhdG9yTWVudSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBcImljb24tbWVudVwiOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5tZW51SXRlbXMubmV4dCh0aGlzLmljb25NZW51KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgdGhpcy5tZW51SXRlbXMubmV4dCh0aGlzLnBsYWluTWVudSk7XHJcbiAgICAgICAgfSovXHJcbiAgICB9XHJcbn1cclxuIl19