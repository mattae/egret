import * as tslib_1 from "tslib";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { LayoutService } from "./layout.service";
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "./layout.service";
let CustomizerService = class CustomizerService {
    constructor(router, layout) {
        this.router = router;
        this.layout = layout;
        this.colors = [
            {
                class: "black",
                active: false
            },
            {
                class: "white",
                active: false
            },
            {
                class: "dark-blue",
                active: false
            },
            {
                class: "grey",
                active: false
            },
            {
                class: "brown",
                active: false
            },
            {
                class: "gray",
                active: false
            },
            {
                class: "purple",
                active: false
            },
            {
                class: "blue",
                active: false
            },
            {
                class: "indigo",
                active: false
            },
            {
                class: "yellow",
                active: false
            },
            {
                class: "green",
                active: false
            },
            {
                class: "pink",
                active: false
            },
            {
                class: "red",
                active: false
            }
        ];
        this.topbarColors = this.getTopbarColors();
        this.sidebarColors = this.getSidebarColors();
    }
    getSidebarColors() {
        let sidebarColors = ['black', 'white', 'grey', 'brown', 'purple', 'dark-blue',];
        return this.colors.filter(color => {
            return sidebarColors.includes(color.class);
        })
            .map(c => {
            c.active = c.class === this.layout.layoutConf.sidebarColor;
            return Object.assign({}, c);
        });
    }
    getTopbarColors() {
        let topbarColors = ['black', 'white', 'dark-gray', 'purple', 'dark-blue', 'indigo', 'pink', 'red', 'yellow', 'green'];
        return this.colors.filter(color => {
            return topbarColors.includes(color.class);
        })
            .map(c => {
            c.active = c.class === this.layout.layoutConf.topbarColor;
            return Object.assign({}, c);
        });
    }
    changeSidebarColor(color) {
        this.layout.publishLayoutChange({ sidebarColor: color.class });
        this.sidebarColors = this.getSidebarColors();
    }
    changeTopbarColor(color) {
        this.layout.publishLayoutChange({ topbarColor: color.class });
        this.topbarColors = this.getTopbarColors();
    }
    removeClass(el, className) {
        if (!el || el.length === 0)
            return;
        if (!el.length) {
            el.classList.remove(className);
        }
        else {
            for (var i = 0; i < el.length; i++) {
                el[i].classList.remove(className);
            }
        }
    }
    addClass(el, className) {
        if (!el)
            return;
        if (!el.length) {
            el.classList.add(className);
        }
        else {
            for (var i = 0; i < el.length; i++) {
                el[i].classList.add(className);
            }
        }
    }
    findClosest(el, className) {
        if (!el)
            return;
        while (el) {
            var parent = el.parentElement;
            if (parent && this.hasClass(parent, className)) {
                return parent;
            }
            el = parent;
        }
    }
    hasClass(el, className) {
        if (!el)
            return;
        return (` ${el.className} `.replace(/[\n\t]/g, " ").indexOf(` ${className} `) > -1);
    }
    toggleClass(el, className) {
        if (!el)
            return;
        if (this.hasClass(el, className)) {
            this.removeClass(el, className);
        }
        else {
            this.addClass(el, className);
        }
    }
};
CustomizerService.ctorParameters = () => [
    { type: Router },
    { type: LayoutService }
];
CustomizerService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function CustomizerService_Factory() { return new CustomizerService(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.LayoutService)); }, token: CustomizerService, providedIn: "root" });
CustomizerService = tslib_1.__decorate([
    Injectable({
        providedIn: "root"
    }),
    tslib_1.__metadata("design:paramtypes", [Router,
        LayoutService])
], CustomizerService);
export { CustomizerService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9taXplci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsic2VydmljZXMvY3VzdG9taXplci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7Ozs7QUFLakQsSUFBYSxpQkFBaUIsR0FBOUIsTUFBYSxpQkFBaUI7SUE0RDdCLFlBQ1MsTUFBYyxFQUNkLE1BQXFCO1FBRHJCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxXQUFNLEdBQU4sTUFBTSxDQUFlO1FBN0Q5QixXQUFNLEdBQUc7WUFDUjtnQkFDQyxLQUFLLEVBQUUsT0FBTztnQkFDZCxNQUFNLEVBQUUsS0FBSzthQUNiO1lBQ0Q7Z0JBQ0MsS0FBSyxFQUFFLE9BQU87Z0JBQ2QsTUFBTSxFQUFFLEtBQUs7YUFDYjtZQUNEO2dCQUNDLEtBQUssRUFBRSxXQUFXO2dCQUNsQixNQUFNLEVBQUUsS0FBSzthQUNiO1lBQ0Q7Z0JBQ0MsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsTUFBTSxFQUFFLEtBQUs7YUFDYjtZQUNEO2dCQUNDLEtBQUssRUFBRSxPQUFPO2dCQUNkLE1BQU0sRUFBRSxLQUFLO2FBQ2I7WUFDRDtnQkFDQyxLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsS0FBSzthQUNiO1lBQ0Q7Z0JBQ0MsS0FBSyxFQUFFLFFBQVE7Z0JBQ2YsTUFBTSxFQUFFLEtBQUs7YUFDYjtZQUNEO2dCQUNDLEtBQUssRUFBRSxNQUFNO2dCQUNiLE1BQU0sRUFBRSxLQUFLO2FBQ2I7WUFFRDtnQkFDQyxLQUFLLEVBQUUsUUFBUTtnQkFDZixNQUFNLEVBQUUsS0FBSzthQUNiO1lBQ0Q7Z0JBQ0MsS0FBSyxFQUFFLFFBQVE7Z0JBQ2YsTUFBTSxFQUFFLEtBQUs7YUFDYjtZQUNEO2dCQUNDLEtBQUssRUFBRSxPQUFPO2dCQUNkLE1BQU0sRUFBRSxLQUFLO2FBQ2I7WUFDRDtnQkFDQyxLQUFLLEVBQUUsTUFBTTtnQkFDYixNQUFNLEVBQUUsS0FBSzthQUNiO1lBQ0Q7Z0JBQ0MsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osTUFBTSxFQUFFLEtBQUs7YUFDYjtTQUNELENBQUM7UUFTRCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUMzQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzlDLENBQUM7SUFFRCxnQkFBZ0I7UUFDZixJQUFJLGFBQWEsR0FBRyxDQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLENBQUM7UUFDaEYsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNqQyxPQUFPLGFBQWEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzVDLENBQUMsQ0FBQzthQUNBLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUNSLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUM7WUFDM0QseUJBQVcsQ0FBQyxFQUFFO1FBQ2YsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsZUFBZTtRQUNkLElBQUksWUFBWSxHQUFHLENBQUMsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDdEgsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNqQyxPQUFPLFlBQVksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzNDLENBQUMsQ0FBQzthQUNBLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUNSLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUM7WUFDMUQseUJBQVcsQ0FBQyxFQUFFO1FBQ2YsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsa0JBQWtCLENBQUMsS0FBSztRQUN2QixJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEVBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxLQUFLLEVBQUMsQ0FBQyxDQUFDO1FBQzdELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDOUMsQ0FBQztJQUVELGlCQUFpQixDQUFDLEtBQUs7UUFDdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxFQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsS0FBSyxFQUFDLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUM1QyxDQUFDO0lBRUQsV0FBVyxDQUFDLEVBQUUsRUFBRSxTQUFTO1FBQ3hCLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLE1BQU0sS0FBSyxDQUFDO1lBQUUsT0FBTztRQUNuQyxJQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRTtZQUNmLEVBQUUsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQy9CO2FBQU07WUFDTixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDbkMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDbEM7U0FDRDtJQUNGLENBQUM7SUFFRCxRQUFRLENBQUMsRUFBRSxFQUFFLFNBQVM7UUFDckIsSUFBSSxDQUFDLEVBQUU7WUFBRSxPQUFPO1FBQ2hCLElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFO1lBQ2YsRUFBRSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDNUI7YUFBTTtZQUNOLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNuQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUMvQjtTQUNEO0lBQ0YsQ0FBQztJQUVELFdBQVcsQ0FBQyxFQUFFLEVBQUUsU0FBUztRQUN4QixJQUFJLENBQUMsRUFBRTtZQUFFLE9BQU87UUFDaEIsT0FBTyxFQUFFLEVBQUU7WUFDVixJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsYUFBYSxDQUFDO1lBQzlCLElBQUksTUFBTSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxFQUFFO2dCQUMvQyxPQUFPLE1BQU0sQ0FBQzthQUNkO1lBQ0QsRUFBRSxHQUFHLE1BQU0sQ0FBQztTQUNaO0lBQ0YsQ0FBQztJQUVELFFBQVEsQ0FBQyxFQUFFLEVBQUUsU0FBUztRQUNyQixJQUFJLENBQUMsRUFBRTtZQUFFLE9BQU87UUFDaEIsT0FBTyxDQUNOLElBQUksRUFBRSxDQUFDLFNBQVMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksU0FBUyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FDMUUsQ0FBQztJQUNILENBQUM7SUFFRCxXQUFXLENBQUMsRUFBRSxFQUFFLFNBQVM7UUFDeEIsSUFBSSxDQUFDLEVBQUU7WUFBRSxPQUFPO1FBQ2hCLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUUsU0FBUyxDQUFDLEVBQUU7WUFDakMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFLEVBQUUsU0FBUyxDQUFDLENBQUM7U0FDaEM7YUFBTTtZQUNOLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1NBQzdCO0lBQ0YsQ0FBQztDQUVELENBQUE7O1lBeEZpQixNQUFNO1lBQ04sYUFBYTs7O0FBOURsQixpQkFBaUI7SUFIN0IsVUFBVSxDQUFDO1FBQ1gsVUFBVSxFQUFFLE1BQU07S0FDbEIsQ0FBQzs2Q0E4RGdCLE1BQU07UUFDTixhQUFhO0dBOURsQixpQkFBaUIsQ0FxSjdCO1NBckpZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IExheW91dFNlcnZpY2UgfSBmcm9tIFwiLi9sYXlvdXQuc2VydmljZVwiO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG5cdHByb3ZpZGVkSW46IFwicm9vdFwiXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDdXN0b21pemVyU2VydmljZSB7XHJcblx0Y29sb3JzID0gW1xyXG5cdFx0e1xyXG5cdFx0XHRjbGFzczogXCJibGFja1wiLFxyXG5cdFx0XHRhY3RpdmU6IGZhbHNlXHJcblx0XHR9LFxyXG5cdFx0e1xyXG5cdFx0XHRjbGFzczogXCJ3aGl0ZVwiLFxyXG5cdFx0XHRhY3RpdmU6IGZhbHNlXHJcblx0XHR9LFxyXG5cdFx0e1xyXG5cdFx0XHRjbGFzczogXCJkYXJrLWJsdWVcIixcclxuXHRcdFx0YWN0aXZlOiBmYWxzZVxyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0Y2xhc3M6IFwiZ3JleVwiLFxyXG5cdFx0XHRhY3RpdmU6IGZhbHNlXHJcblx0XHR9LFxyXG5cdFx0e1xyXG5cdFx0XHRjbGFzczogXCJicm93blwiLFxyXG5cdFx0XHRhY3RpdmU6IGZhbHNlXHJcblx0XHR9LFxyXG5cdFx0e1xyXG5cdFx0XHRjbGFzczogXCJncmF5XCIsXHJcblx0XHRcdGFjdGl2ZTogZmFsc2VcclxuXHRcdH0sXHJcblx0XHR7XHJcblx0XHRcdGNsYXNzOiBcInB1cnBsZVwiLFxyXG5cdFx0XHRhY3RpdmU6IGZhbHNlXHJcblx0XHR9LFxyXG5cdFx0e1xyXG5cdFx0XHRjbGFzczogXCJibHVlXCIsXHJcblx0XHRcdGFjdGl2ZTogZmFsc2VcclxuXHRcdH0sXHJcblxyXG5cdFx0e1xyXG5cdFx0XHRjbGFzczogXCJpbmRpZ29cIixcclxuXHRcdFx0YWN0aXZlOiBmYWxzZVxyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0Y2xhc3M6IFwieWVsbG93XCIsXHJcblx0XHRcdGFjdGl2ZTogZmFsc2VcclxuXHRcdH0sXHJcblx0XHR7XHJcblx0XHRcdGNsYXNzOiBcImdyZWVuXCIsXHJcblx0XHRcdGFjdGl2ZTogZmFsc2VcclxuXHRcdH0sXHJcblx0XHR7XHJcblx0XHRcdGNsYXNzOiBcInBpbmtcIixcclxuXHRcdFx0YWN0aXZlOiBmYWxzZVxyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0Y2xhc3M6IFwicmVkXCIsXHJcblx0XHRcdGFjdGl2ZTogZmFsc2VcclxuXHRcdH1cclxuXHRdO1xyXG5cdHNlbGVjdGVkU2lkZWJhckNvbG9yO1xyXG5cdHRvcGJhckNvbG9yczogYW55W107XHJcblx0c2lkZWJhckNvbG9yczogYW55W107XHJcblxyXG5cdGNvbnN0cnVjdG9yKFxyXG5cdFx0cHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuXHRcdHByaXZhdGUgbGF5b3V0OiBMYXlvdXRTZXJ2aWNlLFxyXG5cdCkge1xyXG5cdFx0dGhpcy50b3BiYXJDb2xvcnMgPSB0aGlzLmdldFRvcGJhckNvbG9ycygpO1xyXG5cdFx0dGhpcy5zaWRlYmFyQ29sb3JzID0gdGhpcy5nZXRTaWRlYmFyQ29sb3JzKCk7XHJcblx0fVxyXG5cclxuXHRnZXRTaWRlYmFyQ29sb3JzKCkge1xyXG5cdFx0bGV0IHNpZGViYXJDb2xvcnMgPSBbJ2JsYWNrJywgJ3doaXRlJywgJ2dyZXknLCAnYnJvd24nLCAncHVycGxlJywgJ2RhcmstYmx1ZScsXTtcclxuXHRcdHJldHVybiB0aGlzLmNvbG9ycy5maWx0ZXIoY29sb3IgPT4ge1xyXG5cdFx0XHRyZXR1cm4gc2lkZWJhckNvbG9ycy5pbmNsdWRlcyhjb2xvci5jbGFzcyk7XHJcblx0XHR9KVxyXG5cdFx0XHQubWFwKGMgPT4ge1xyXG5cdFx0XHRcdGMuYWN0aXZlID0gYy5jbGFzcyA9PT0gdGhpcy5sYXlvdXQubGF5b3V0Q29uZi5zaWRlYmFyQ29sb3I7XHJcblx0XHRcdFx0cmV0dXJuIHsuLi5jfTtcclxuXHRcdFx0fSk7XHJcblx0fVxyXG5cclxuXHRnZXRUb3BiYXJDb2xvcnMoKSB7XHJcblx0XHRsZXQgdG9wYmFyQ29sb3JzID0gWydibGFjaycsICd3aGl0ZScsICdkYXJrLWdyYXknLCAncHVycGxlJywgJ2RhcmstYmx1ZScsICdpbmRpZ28nLCAncGluaycsICdyZWQnLCAneWVsbG93JywgJ2dyZWVuJ107XHJcblx0XHRyZXR1cm4gdGhpcy5jb2xvcnMuZmlsdGVyKGNvbG9yID0+IHtcclxuXHRcdFx0cmV0dXJuIHRvcGJhckNvbG9ycy5pbmNsdWRlcyhjb2xvci5jbGFzcyk7XHJcblx0XHR9KVxyXG5cdFx0XHQubWFwKGMgPT4ge1xyXG5cdFx0XHRcdGMuYWN0aXZlID0gYy5jbGFzcyA9PT0gdGhpcy5sYXlvdXQubGF5b3V0Q29uZi50b3BiYXJDb2xvcjtcclxuXHRcdFx0XHRyZXR1cm4gey4uLmN9O1xyXG5cdFx0XHR9KTtcclxuXHR9XHJcblxyXG5cdGNoYW5nZVNpZGViYXJDb2xvcihjb2xvcikge1xyXG5cdFx0dGhpcy5sYXlvdXQucHVibGlzaExheW91dENoYW5nZSh7c2lkZWJhckNvbG9yOiBjb2xvci5jbGFzc30pO1xyXG5cdFx0dGhpcy5zaWRlYmFyQ29sb3JzID0gdGhpcy5nZXRTaWRlYmFyQ29sb3JzKCk7XHJcblx0fVxyXG5cclxuXHRjaGFuZ2VUb3BiYXJDb2xvcihjb2xvcikge1xyXG5cdFx0dGhpcy5sYXlvdXQucHVibGlzaExheW91dENoYW5nZSh7dG9wYmFyQ29sb3I6IGNvbG9yLmNsYXNzfSk7XHJcblx0XHR0aGlzLnRvcGJhckNvbG9ycyA9IHRoaXMuZ2V0VG9wYmFyQ29sb3JzKCk7XHJcblx0fVxyXG5cclxuXHRyZW1vdmVDbGFzcyhlbCwgY2xhc3NOYW1lKSB7XHJcblx0XHRpZiAoIWVsIHx8IGVsLmxlbmd0aCA9PT0gMCkgcmV0dXJuO1xyXG5cdFx0aWYgKCFlbC5sZW5ndGgpIHtcclxuXHRcdFx0ZWwuY2xhc3NMaXN0LnJlbW92ZShjbGFzc05hbWUpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCBlbC5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRcdGVsW2ldLmNsYXNzTGlzdC5yZW1vdmUoY2xhc3NOYW1lKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0YWRkQ2xhc3MoZWwsIGNsYXNzTmFtZSkge1xyXG5cdFx0aWYgKCFlbCkgcmV0dXJuO1xyXG5cdFx0aWYgKCFlbC5sZW5ndGgpIHtcclxuXHRcdFx0ZWwuY2xhc3NMaXN0LmFkZChjbGFzc05hbWUpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCBlbC5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRcdGVsW2ldLmNsYXNzTGlzdC5hZGQoY2xhc3NOYW1lKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0ZmluZENsb3Nlc3QoZWwsIGNsYXNzTmFtZSkge1xyXG5cdFx0aWYgKCFlbCkgcmV0dXJuO1xyXG5cdFx0d2hpbGUgKGVsKSB7XHJcblx0XHRcdHZhciBwYXJlbnQgPSBlbC5wYXJlbnRFbGVtZW50O1xyXG5cdFx0XHRpZiAocGFyZW50ICYmIHRoaXMuaGFzQ2xhc3MocGFyZW50LCBjbGFzc05hbWUpKSB7XHJcblx0XHRcdFx0cmV0dXJuIHBhcmVudDtcclxuXHRcdFx0fVxyXG5cdFx0XHRlbCA9IHBhcmVudDtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdGhhc0NsYXNzKGVsLCBjbGFzc05hbWUpIHtcclxuXHRcdGlmICghZWwpIHJldHVybjtcclxuXHRcdHJldHVybiAoXHJcblx0XHRcdGAgJHtlbC5jbGFzc05hbWV9IGAucmVwbGFjZSgvW1xcblxcdF0vZywgXCIgXCIpLmluZGV4T2YoYCAke2NsYXNzTmFtZX0gYCkgPiAtMVxyXG5cdFx0KTtcclxuXHR9XHJcblxyXG5cdHRvZ2dsZUNsYXNzKGVsLCBjbGFzc05hbWUpIHtcclxuXHRcdGlmICghZWwpIHJldHVybjtcclxuXHRcdGlmICh0aGlzLmhhc0NsYXNzKGVsLCBjbGFzc05hbWUpKSB7XHJcblx0XHRcdHRoaXMucmVtb3ZlQ2xhc3MoZWwsIGNsYXNzTmFtZSk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHR0aGlzLmFkZENsYXNzKGVsLCBjbGFzc05hbWUpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcbn1cclxuIl19