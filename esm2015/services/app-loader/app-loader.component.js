import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';
let AppLoaderComponent = class AppLoaderComponent {
    constructor(dialogRef) {
        this.dialogRef = dialogRef;
    }
    ngOnInit() {
    }
};
AppLoaderComponent.ctorParameters = () => [
    { type: MatDialogRef }
];
AppLoaderComponent = tslib_1.__decorate([
    Component({
        selector: 'egret-app-loader',
        template: "<div class=\"text-center\">\r\n    <h6 class=\"m-0 pb-1\">{{ title }}</h6>\r\n    <div mat-dialog-content>\r\n        <mat-spinner [style.margin]=\"'auto'\"></mat-spinner>\r\n    </div>\r\n</div>\r\n",
        styles: [".mat-dialog-content{min-height:122px}"]
    }),
    tslib_1.__metadata("design:paramtypes", [MatDialogRef])
], AppLoaderComponent);
export { AppLoaderComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLWxvYWRlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvZWdyZXQvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9hcHAtbG9hZGVyL2FwcC1sb2FkZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ2xELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQU9qRCxJQUFhLGtCQUFrQixHQUEvQixNQUFhLGtCQUFrQjtJQUkzQixZQUFtQixTQUEyQztRQUEzQyxjQUFTLEdBQVQsU0FBUyxDQUFrQztJQUM5RCxDQUFDO0lBRUQsUUFBUTtJQUNSLENBQUM7Q0FFSixDQUFBOztZQU5pQyxZQUFZOztBQUpqQyxrQkFBa0I7SUFMOUIsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLGtCQUFrQjtRQUM1QixtTkFBMEM7O0tBRTdDLENBQUM7NkNBS2dDLFlBQVk7R0FKakMsa0JBQWtCLENBVTlCO1NBVlksa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0RGlhbG9nUmVmIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2VncmV0LWFwcC1sb2FkZXInLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2FwcC1sb2FkZXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vYXBwLWxvYWRlci5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEFwcExvYWRlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICB0aXRsZTtcclxuICAgIG1lc3NhZ2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPEFwcExvYWRlckNvbXBvbmVudD4pIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgIH1cclxuXHJcbn1cclxuIl19