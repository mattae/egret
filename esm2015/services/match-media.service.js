import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { MediaObserver, MediaChange } from '@angular/flex-layout';
import { BehaviorSubject } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/core";
let MatchMediaService = class MatchMediaService {
    constructor(media) {
        this.media = media;
        this.onMediaChange = new BehaviorSubject('');
        this.activeMediaQuery = '';
        this.init();
    }
    init() {
        this.media
            .asObservable()
            .subscribe((change) => {
            if (this.activeMediaQuery !== change[0].mqAlias) {
                this.activeMediaQuery = change[0].mqAlias;
                this.onMediaChange.next(change[0].mqAlias);
            }
        });
    }
};
MatchMediaService.ctorParameters = () => [
    { type: MediaObserver }
];
MatchMediaService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function MatchMediaService_Factory() { return new MatchMediaService(i0.ɵɵinject(i1.MediaObserver)); }, token: MatchMediaService, providedIn: "root" });
MatchMediaService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [MediaObserver])
], MatchMediaService);
export { MatchMediaService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0Y2gtbWVkaWEuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL21hdGNoLW1lZGlhLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGFBQWEsRUFBRSxXQUFXLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNsRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sTUFBTSxDQUFDOzs7QUFLdkMsSUFBYSxpQkFBaUIsR0FBOUIsTUFBYSxpQkFBaUI7SUFJNUIsWUFDVSxLQUFvQjtRQUFwQixVQUFLLEdBQUwsS0FBSyxDQUFlO1FBSDlCLGtCQUFhLEdBQTRCLElBQUksZUFBZSxDQUFTLEVBQUUsQ0FBQyxDQUFDO1FBS3ZFLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2QsQ0FBQztJQUdPLElBQUk7UUFFUixJQUFJLENBQUMsS0FBSzthQUNMLFlBQVksRUFBRTthQUNkLFNBQVMsQ0FBQyxDQUFDLE1BQXFCLEVBQUUsRUFBRTtZQUNqQyxJQUFLLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUNoRDtnQkFDSSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztnQkFDMUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQzlDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0NBQ0YsQ0FBQTs7WUFuQmtCLGFBQWE7OztBQUxuQixpQkFBaUI7SUFIN0IsVUFBVSxDQUFDO1FBQ1YsVUFBVSxFQUFFLE1BQU07S0FDbkIsQ0FBQzs2Q0FNaUIsYUFBYTtHQUxuQixpQkFBaUIsQ0F3QjdCO1NBeEJZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWVkaWFPYnNlcnZlciwgTWVkaWFDaGFuZ2UgfSBmcm9tICdAYW5ndWxhci9mbGV4LWxheW91dCc7XHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTWF0Y2hNZWRpYVNlcnZpY2Uge1xyXG4gIGFjdGl2ZU1lZGlhUXVlcnk6IHN0cmluZztcclxuICBvbk1lZGlhQ2hhbmdlOiBCZWhhdmlvclN1YmplY3Q8c3RyaW5nPiA9IG5ldyBCZWhhdmlvclN1YmplY3Q8c3RyaW5nPignJyk7XHJcbiAgXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIG1lZGlhOiBNZWRpYU9ic2VydmVyXHJcbiAgKSB7XHJcbiAgICB0aGlzLmFjdGl2ZU1lZGlhUXVlcnkgPSAnJztcclxuICAgIHRoaXMuaW5pdCgpO1xyXG4gIH1cclxuXHJcblxyXG4gIHByaXZhdGUgaW5pdCgpOiB2b2lkXHJcbiAge1xyXG4gICAgICB0aGlzLm1lZGlhXHJcbiAgICAgICAgICAuYXNPYnNlcnZhYmxlKClcclxuICAgICAgICAgIC5zdWJzY3JpYmUoKGNoYW5nZTogTWVkaWFDaGFuZ2VbXSkgPT4ge1xyXG4gICAgICAgICAgICAgIGlmICggdGhpcy5hY3RpdmVNZWRpYVF1ZXJ5ICE9PSBjaGFuZ2VbMF0ubXFBbGlhcyApXHJcbiAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLmFjdGl2ZU1lZGlhUXVlcnkgPSBjaGFuZ2VbMF0ubXFBbGlhcztcclxuICAgICAgICAgICAgICAgICAgdGhpcy5vbk1lZGlhQ2hhbmdlLm5leHQoY2hhbmdlWzBdLm1xQWxpYXMpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=