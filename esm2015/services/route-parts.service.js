import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Params, Router } from "@angular/router";
let RoutePartsService = class RoutePartsService {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    generateRouteParts(snapshot) {
        let routeParts = [];
        if (snapshot) {
            if (snapshot.firstChild) {
                routeParts = routeParts.concat(this.generateRouteParts(snapshot.firstChild));
            }
            if (snapshot.data['title'] && snapshot.url.length) {
                // console.log(snapshot.data['title'], snapshot.url)
                routeParts.push({
                    title: snapshot.data['title'],
                    breadcrumb: snapshot.data['breadcrumb'],
                    url: snapshot.url[0].path,
                    urlSegments: snapshot.url,
                    params: snapshot.params
                });
            }
        }
        return routeParts;
    }
};
RoutePartsService.ctorParameters = () => [
    { type: Router }
];
RoutePartsService = tslib_1.__decorate([
    Injectable(),
    tslib_1.__metadata("design:paramtypes", [Router])
], RoutePartsService);
export { RoutePartsService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm91dGUtcGFydHMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL3JvdXRlLXBhcnRzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQVd6RSxJQUFhLGlCQUFpQixHQUE5QixNQUFhLGlCQUFpQjtJQUc3QixZQUFvQixNQUFjO1FBQWQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtJQUNsQyxDQUFDO0lBRUQsUUFBUTtJQUNSLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxRQUFnQztRQUNsRCxJQUFJLFVBQVUsR0FBaUIsRUFBRSxDQUFDO1FBQ2xDLElBQUksUUFBUSxFQUFFO1lBQ2IsSUFBSSxRQUFRLENBQUMsVUFBVSxFQUFFO2dCQUN4QixVQUFVLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7YUFDN0U7WUFDRCxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUU7Z0JBQ2xELG9EQUFvRDtnQkFDcEQsVUFBVSxDQUFDLElBQUksQ0FBQztvQkFDZixLQUFLLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7b0JBQzdCLFVBQVUsRUFBRSxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztvQkFDdkMsR0FBRyxFQUFFLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtvQkFDekIsV0FBVyxFQUFFLFFBQVEsQ0FBQyxHQUFHO29CQUN6QixNQUFNLEVBQUUsUUFBUSxDQUFDLE1BQU07aUJBQ3ZCLENBQUMsQ0FBQzthQUNIO1NBQ0Q7UUFDRCxPQUFPLFVBQVUsQ0FBQztJQUNuQixDQUFDO0NBQ0QsQ0FBQTs7WUF6QjRCLE1BQU07O0FBSHRCLGlCQUFpQjtJQUQ3QixVQUFVLEVBQUU7NkNBSWdCLE1BQU07R0FIdEIsaUJBQWlCLENBNEI3QjtTQTVCWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIFBhcmFtcywgUm91dGVyIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5cclxuaW50ZXJmYWNlIElSb3V0ZVBhcnQge1xyXG5cdHRpdGxlOiBzdHJpbmcsXHJcblx0YnJlYWRjcnVtYjogc3RyaW5nLFxyXG5cdHBhcmFtcz86IFBhcmFtcyxcclxuXHR1cmw6IHN0cmluZyxcclxuXHR1cmxTZWdtZW50czogYW55W11cclxufVxyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgUm91dGVQYXJ0c1NlcnZpY2Uge1xyXG5cdHB1YmxpYyByb3V0ZVBhcnRzOiBJUm91dGVQYXJ0W107XHJcblxyXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOiBSb3V0ZXIpIHtcclxuXHR9XHJcblxyXG5cdG5nT25Jbml0KCkge1xyXG5cdH1cclxuXHJcblx0Z2VuZXJhdGVSb3V0ZVBhcnRzKHNuYXBzaG90OiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90KTogSVJvdXRlUGFydFtdIHtcclxuXHRcdGxldCByb3V0ZVBhcnRzID0gPElSb3V0ZVBhcnRbXT5bXTtcclxuXHRcdGlmIChzbmFwc2hvdCkge1xyXG5cdFx0XHRpZiAoc25hcHNob3QuZmlyc3RDaGlsZCkge1xyXG5cdFx0XHRcdHJvdXRlUGFydHMgPSByb3V0ZVBhcnRzLmNvbmNhdCh0aGlzLmdlbmVyYXRlUm91dGVQYXJ0cyhzbmFwc2hvdC5maXJzdENoaWxkKSk7XHJcblx0XHRcdH1cclxuXHRcdFx0aWYgKHNuYXBzaG90LmRhdGFbJ3RpdGxlJ10gJiYgc25hcHNob3QudXJsLmxlbmd0aCkge1xyXG5cdFx0XHRcdC8vIGNvbnNvbGUubG9nKHNuYXBzaG90LmRhdGFbJ3RpdGxlJ10sIHNuYXBzaG90LnVybClcclxuXHRcdFx0XHRyb3V0ZVBhcnRzLnB1c2goe1xyXG5cdFx0XHRcdFx0dGl0bGU6IHNuYXBzaG90LmRhdGFbJ3RpdGxlJ10sXHJcblx0XHRcdFx0XHRicmVhZGNydW1iOiBzbmFwc2hvdC5kYXRhWydicmVhZGNydW1iJ10sXHJcblx0XHRcdFx0XHR1cmw6IHNuYXBzaG90LnVybFswXS5wYXRoLFxyXG5cdFx0XHRcdFx0dXJsU2VnbWVudHM6IHNuYXBzaG90LnVybCxcclxuXHRcdFx0XHRcdHBhcmFtczogc25hcHNob3QucGFyYW1zXHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdHJldHVybiByb3V0ZVBhcnRzO1xyXG5cdH1cclxufVxyXG4iXX0=