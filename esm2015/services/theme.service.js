import * as tslib_1 from "tslib";
import { Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { getQueryParam } from '../helpers/url.helper';
let ThemeService = class ThemeService {
    constructor(document) {
        this.document = document;
        this.egretThemes = [{
                "name": "egret-dark-purple",
                "baseColor": "#9c27b0",
                "isActive": false
            }, {
                "name": "egret-dark-pink",
                "baseColor": "#e91e63",
                "isActive": false
            }, {
                "name": "egret-blue",
                "baseColor": "#03a9f4",
                "isActive": true
            }, {
                "name": "egret-navy",
                "baseColor": "#10174c",
                "isActive": false
            }];
    }
    // Invoked in AppComponent and apply 'activatedTheme' on startup
    applyMatTheme(r, themeName) {
        this.renderer = r;
        this.activatedTheme = this.egretThemes[2];
        // *********** ONLY FOR DEMO **********
        this.setThemeFromQuery();
        // ************************************
        // this.changeTheme(themeName);
        this.renderer.addClass(this.document.body, themeName);
    }
    changeTheme(prevTheme, themeName) {
        this.renderer.removeClass(this.document.body, prevTheme);
        this.renderer.addClass(this.document.body, themeName);
        this.flipActiveFlag(themeName);
    }
    flipActiveFlag(themeName) {
        this.egretThemes.forEach((t) => {
            t.isActive = false;
            if (t.name === themeName) {
                t.isActive = true;
                this.activatedTheme = t;
            }
        });
    }
    // *********** ONLY FOR DEMO **********
    setThemeFromQuery() {
        let themeStr = getQueryParam('theme');
        try {
            this.activatedTheme = JSON.parse(themeStr);
            this.flipActiveFlag(this.activatedTheme.name);
        }
        catch (e) {
        }
    }
};
ThemeService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] }
];
ThemeService = tslib_1.__decorate([
    Injectable(),
    tslib_1.__param(0, Inject(DOCUMENT)),
    tslib_1.__metadata("design:paramtypes", [Object])
], ThemeService);
export { ThemeService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGhlbWUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL3RoZW1lLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFhLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMzQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFTdEQsSUFBYSxZQUFZLEdBQXpCLE1BQWEsWUFBWTtJQXFCeEIsWUFBc0MsUUFBYTtRQUFiLGFBQVEsR0FBUixRQUFRLENBQUs7UUFwQjVDLGdCQUFXLEdBQWEsQ0FBQztnQkFDL0IsTUFBTSxFQUFFLG1CQUFtQjtnQkFDM0IsV0FBVyxFQUFFLFNBQVM7Z0JBQ3RCLFVBQVUsRUFBRSxLQUFLO2FBQ2pCLEVBQUU7Z0JBQ0YsTUFBTSxFQUFFLGlCQUFpQjtnQkFDekIsV0FBVyxFQUFFLFNBQVM7Z0JBQ3RCLFVBQVUsRUFBRSxLQUFLO2FBQ2pCLEVBQUU7Z0JBQ0YsTUFBTSxFQUFFLFlBQVk7Z0JBQ3BCLFdBQVcsRUFBRSxTQUFTO2dCQUN0QixVQUFVLEVBQUUsSUFBSTthQUNoQixFQUFFO2dCQUNGLE1BQU0sRUFBRSxZQUFZO2dCQUNwQixXQUFXLEVBQUUsU0FBUztnQkFDdEIsVUFBVSxFQUFFLEtBQUs7YUFDakIsQ0FBQyxDQUFDO0lBS0gsQ0FBQztJQUVELGdFQUFnRTtJQUNoRSxhQUFhLENBQUMsQ0FBWSxFQUFFLFNBQWlCO1FBQzVDLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDO1FBRWxCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUUxQyx1Q0FBdUM7UUFDdkMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFDekIsdUNBQXVDO1FBRXZDLCtCQUErQjtRQUMvQixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztJQUV2RCxDQUFDO0lBRUQsV0FBVyxDQUFDLFNBQVMsRUFBRSxTQUFpQjtRQUN2QyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztRQUN6RCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFFRCxjQUFjLENBQUMsU0FBaUI7UUFDL0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUM5QixDQUFDLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztZQUNuQixJQUFJLENBQUMsQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFO2dCQUN6QixDQUFDLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztnQkFDbEIsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUM7YUFDeEI7UUFDRixDQUFDLENBQUMsQ0FBQztJQUNKLENBQUM7SUFFRCx1Q0FBdUM7SUFDdkMsaUJBQWlCO1FBQ2hCLElBQUksUUFBUSxHQUFHLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN0QyxJQUFJO1lBQ0gsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM5QztRQUFDLE9BQU8sQ0FBQyxFQUFFO1NBQ1g7SUFDRixDQUFDO0NBQ0QsQ0FBQTs7NENBM0NhLE1BQU0sU0FBQyxRQUFROztBQXJCaEIsWUFBWTtJQUR4QixVQUFVLEVBQUU7SUFzQkMsbUJBQUEsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFBOztHQXJCakIsWUFBWSxDQWdFeEI7U0FoRVksWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSwgUmVuZGVyZXIyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERPQ1VNRU5UIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgZ2V0UXVlcnlQYXJhbSB9IGZyb20gJy4uL2hlbHBlcnMvdXJsLmhlbHBlcic7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIElUaGVtZSB7XHJcblx0bmFtZTogc3RyaW5nLFxyXG5cdGJhc2VDb2xvcj86IHN0cmluZyxcclxuXHRpc0FjdGl2ZT86IGJvb2xlYW5cclxufVxyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgVGhlbWVTZXJ2aWNlIHtcclxuXHRwdWJsaWMgZWdyZXRUaGVtZXM6IElUaGVtZVtdID0gW3tcclxuXHRcdFwibmFtZVwiOiBcImVncmV0LWRhcmstcHVycGxlXCIsXHJcblx0XHRcImJhc2VDb2xvclwiOiBcIiM5YzI3YjBcIixcclxuXHRcdFwiaXNBY3RpdmVcIjogZmFsc2VcclxuXHR9LCB7XHJcblx0XHRcIm5hbWVcIjogXCJlZ3JldC1kYXJrLXBpbmtcIixcclxuXHRcdFwiYmFzZUNvbG9yXCI6IFwiI2U5MWU2M1wiLFxyXG5cdFx0XCJpc0FjdGl2ZVwiOiBmYWxzZVxyXG5cdH0sIHtcclxuXHRcdFwibmFtZVwiOiBcImVncmV0LWJsdWVcIixcclxuXHRcdFwiYmFzZUNvbG9yXCI6IFwiIzAzYTlmNFwiLFxyXG5cdFx0XCJpc0FjdGl2ZVwiOiB0cnVlXHJcblx0fSwge1xyXG5cdFx0XCJuYW1lXCI6IFwiZWdyZXQtbmF2eVwiLFxyXG5cdFx0XCJiYXNlQ29sb3JcIjogXCIjMTAxNzRjXCIsXHJcblx0XHRcImlzQWN0aXZlXCI6IGZhbHNlXHJcblx0fV07XHJcblx0cHVibGljIGFjdGl2YXRlZFRoZW1lOiBJVGhlbWU7XHJcblx0cHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIyO1xyXG5cclxuXHRjb25zdHJ1Y3RvcihASW5qZWN0KERPQ1VNRU5UKSBwcml2YXRlIGRvY3VtZW50OiBhbnkpIHtcclxuXHR9XHJcblxyXG5cdC8vIEludm9rZWQgaW4gQXBwQ29tcG9uZW50IGFuZCBhcHBseSAnYWN0aXZhdGVkVGhlbWUnIG9uIHN0YXJ0dXBcclxuXHRhcHBseU1hdFRoZW1lKHI6IFJlbmRlcmVyMiwgdGhlbWVOYW1lOiBzdHJpbmcpIHtcclxuXHRcdHRoaXMucmVuZGVyZXIgPSByO1xyXG5cclxuXHRcdHRoaXMuYWN0aXZhdGVkVGhlbWUgPSB0aGlzLmVncmV0VGhlbWVzWzJdO1xyXG5cclxuXHRcdC8vICoqKioqKioqKioqIE9OTFkgRk9SIERFTU8gKioqKioqKioqKlxyXG5cdFx0dGhpcy5zZXRUaGVtZUZyb21RdWVyeSgpO1xyXG5cdFx0Ly8gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcblxyXG5cdFx0Ly8gdGhpcy5jaGFuZ2VUaGVtZSh0aGVtZU5hbWUpO1xyXG5cdFx0dGhpcy5yZW5kZXJlci5hZGRDbGFzcyh0aGlzLmRvY3VtZW50LmJvZHksIHRoZW1lTmFtZSk7XHJcblxyXG5cdH1cclxuXHJcblx0Y2hhbmdlVGhlbWUocHJldlRoZW1lLCB0aGVtZU5hbWU6IHN0cmluZykge1xyXG5cdFx0dGhpcy5yZW5kZXJlci5yZW1vdmVDbGFzcyh0aGlzLmRvY3VtZW50LmJvZHksIHByZXZUaGVtZSk7XHJcblx0XHR0aGlzLnJlbmRlcmVyLmFkZENsYXNzKHRoaXMuZG9jdW1lbnQuYm9keSwgdGhlbWVOYW1lKTtcclxuXHRcdHRoaXMuZmxpcEFjdGl2ZUZsYWcodGhlbWVOYW1lKTtcclxuXHR9XHJcblxyXG5cdGZsaXBBY3RpdmVGbGFnKHRoZW1lTmFtZTogc3RyaW5nKSB7XHJcblx0XHR0aGlzLmVncmV0VGhlbWVzLmZvckVhY2goKHQpID0+IHtcclxuXHRcdFx0dC5pc0FjdGl2ZSA9IGZhbHNlO1xyXG5cdFx0XHRpZiAodC5uYW1lID09PSB0aGVtZU5hbWUpIHtcclxuXHRcdFx0XHR0LmlzQWN0aXZlID0gdHJ1ZTtcclxuXHRcdFx0XHR0aGlzLmFjdGl2YXRlZFRoZW1lID0gdDtcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHQvLyAqKioqKioqKioqKiBPTkxZIEZPUiBERU1PICoqKioqKioqKipcclxuXHRzZXRUaGVtZUZyb21RdWVyeSgpIHtcclxuXHRcdGxldCB0aGVtZVN0ciA9IGdldFF1ZXJ5UGFyYW0oJ3RoZW1lJyk7XHJcblx0XHR0cnkge1xyXG5cdFx0XHR0aGlzLmFjdGl2YXRlZFRoZW1lID0gSlNPTi5wYXJzZSh0aGVtZVN0cik7XHJcblx0XHRcdHRoaXMuZmxpcEFjdGl2ZUZsYWcodGhpcy5hY3RpdmF0ZWRUaGVtZS5uYW1lKTtcclxuXHRcdH0gY2F0Y2ggKGUpIHtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuIl19