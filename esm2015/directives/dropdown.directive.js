import * as tslib_1 from "tslib";
import { Directive } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
let AppDropdownDirective = class AppDropdownDirective {
    constructor(router) {
        this.router = router;
        this.navlinks = [];
    }
    closeOtherLinks(openLink) {
        this.navlinks.forEach((link) => {
            if (link !== openLink) {
                link.open = false;
            }
        });
    }
    addLink(link) {
        this.navlinks.push(link);
    }
    removeGroup(link) {
        const index = this.navlinks.indexOf(link);
        if (index !== -1) {
            this.navlinks.splice(index, 1);
        }
    }
    getUrl() {
        return this.router.url;
    }
    ngOnInit() {
        this._router = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((event) => {
            this.navlinks.forEach((link) => {
                if (link.group) {
                    const routeUrl = this.getUrl();
                    const currentUrl = routeUrl.split('/');
                    if (currentUrl.indexOf(link.group) > 0) {
                        link.open = true;
                        this.closeOtherLinks(link);
                    }
                }
            });
        });
    }
};
AppDropdownDirective.ctorParameters = () => [
    { type: Router }
];
AppDropdownDirective = tslib_1.__decorate([
    Directive({
        selector: '[appDropdown]'
    }),
    tslib_1.__metadata("design:paramtypes", [Router])
], AppDropdownDirective);
export { AppDropdownDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24uZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsiZGlyZWN0aXZlcy9kcm9wZG93bi5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDMUMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUd4RCxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFLeEMsSUFBYSxvQkFBb0IsR0FBakMsTUFBYSxvQkFBb0I7SUEyQ2hDLFlBQW9CLE1BQWM7UUFBZCxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBMUN4QixhQUFRLEdBQWlDLEVBQUUsQ0FBQztJQTJDdEQsQ0FBQztJQXZDTSxlQUFlLENBQUMsUUFBK0I7UUFDckQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUEyQixFQUFFLEVBQUU7WUFDckQsSUFBSSxJQUFJLEtBQUssUUFBUSxFQUFFO2dCQUN0QixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQzthQUNsQjtRQUNGLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUVNLE9BQU8sQ0FBQyxJQUEyQjtRQUN6QyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBRU0sV0FBVyxDQUFDLElBQTJCO1FBQzdDLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFDLElBQUksS0FBSyxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQ2pCLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztTQUMvQjtJQUNGLENBQUM7SUFFTSxNQUFNO1FBQ1osT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQztJQUN4QixDQUFDO0lBRU0sUUFBUTtRQUNkLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssWUFBWSxhQUFhLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEtBQW9CLEVBQUUsRUFBRTtZQUMxSCxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQTJCLEVBQUUsRUFBRTtnQkFDckQsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO29CQUNmLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztvQkFDL0IsTUFBTSxVQUFVLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDdkMsSUFBSSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQ3ZDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO3dCQUNqQixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUMzQjtpQkFDRDtZQUNGLENBQUMsQ0FBQyxDQUFDO1FBQ0osQ0FBQyxDQUFDLENBQUM7SUFDSixDQUFDO0NBS0QsQ0FBQTs7WUFINEIsTUFBTTs7QUEzQ3RCLG9CQUFvQjtJQUhoQyxTQUFTLENBQUM7UUFDVixRQUFRLEVBQUUsZUFBZTtLQUN6QixDQUFDOzZDQTRDMkIsTUFBTTtHQTNDdEIsb0JBQW9CLENBOENoQztTQTlDWSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTmF2aWdhdGlvbkVuZCwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgRHJvcGRvd25MaW5rRGlyZWN0aXZlIH0gZnJvbSAnLi9kcm9wZG93bi1saW5rLmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBmaWx0ZXIgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuXHRzZWxlY3RvcjogJ1thcHBEcm9wZG93bl0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBcHBEcm9wZG93bkRpcmVjdGl2ZSB7XHJcblx0cHJvdGVjdGVkIG5hdmxpbmtzOiBBcnJheTxEcm9wZG93bkxpbmtEaXJlY3RpdmU+ID0gW107XHJcblxyXG5cdHByaXZhdGUgX3JvdXRlcjogU3Vic2NyaXB0aW9uO1xyXG5cclxuXHRwdWJsaWMgY2xvc2VPdGhlckxpbmtzKG9wZW5MaW5rOiBEcm9wZG93bkxpbmtEaXJlY3RpdmUpOiB2b2lkIHtcclxuXHRcdHRoaXMubmF2bGlua3MuZm9yRWFjaCgobGluazogRHJvcGRvd25MaW5rRGlyZWN0aXZlKSA9PiB7XHJcblx0XHRcdGlmIChsaW5rICE9PSBvcGVuTGluaykge1xyXG5cdFx0XHRcdGxpbmsub3BlbiA9IGZhbHNlO1xyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG5cdHB1YmxpYyBhZGRMaW5rKGxpbms6IERyb3Bkb3duTGlua0RpcmVjdGl2ZSk6IHZvaWQge1xyXG5cdFx0dGhpcy5uYXZsaW5rcy5wdXNoKGxpbmspO1xyXG5cdH1cclxuXHJcblx0cHVibGljIHJlbW92ZUdyb3VwKGxpbms6IERyb3Bkb3duTGlua0RpcmVjdGl2ZSk6IHZvaWQge1xyXG5cdFx0Y29uc3QgaW5kZXggPSB0aGlzLm5hdmxpbmtzLmluZGV4T2YobGluayk7XHJcblx0XHRpZiAoaW5kZXggIT09IC0xKSB7XHJcblx0XHRcdHRoaXMubmF2bGlua3Muc3BsaWNlKGluZGV4LCAxKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHB1YmxpYyBnZXRVcmwoKSB7XHJcblx0XHRyZXR1cm4gdGhpcy5yb3V0ZXIudXJsO1xyXG5cdH1cclxuXHJcblx0cHVibGljIG5nT25Jbml0KCk6IGFueSB7XHJcblx0XHR0aGlzLl9yb3V0ZXIgPSB0aGlzLnJvdXRlci5ldmVudHMucGlwZShmaWx0ZXIoZXZlbnQgPT4gZXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uRW5kKSkuc3Vic2NyaWJlKChldmVudDogTmF2aWdhdGlvbkVuZCkgPT4ge1xyXG5cdFx0XHR0aGlzLm5hdmxpbmtzLmZvckVhY2goKGxpbms6IERyb3Bkb3duTGlua0RpcmVjdGl2ZSkgPT4ge1xyXG5cdFx0XHRcdGlmIChsaW5rLmdyb3VwKSB7XHJcblx0XHRcdFx0XHRjb25zdCByb3V0ZVVybCA9IHRoaXMuZ2V0VXJsKCk7XHJcblx0XHRcdFx0XHRjb25zdCBjdXJyZW50VXJsID0gcm91dGVVcmwuc3BsaXQoJy8nKTtcclxuXHRcdFx0XHRcdGlmIChjdXJyZW50VXJsLmluZGV4T2YobGluay5ncm91cCkgPiAwKSB7XHJcblx0XHRcdFx0XHRcdGxpbmsub3BlbiA9IHRydWU7XHJcblx0XHRcdFx0XHRcdHRoaXMuY2xvc2VPdGhlckxpbmtzKGxpbmspO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOiBSb3V0ZXIpIHtcclxuXHR9XHJcblxyXG59XHJcbiJdfQ==