import * as tslib_1 from "tslib";
var ScrollToDirective_1;
import { Attribute, Directive, ElementRef, HostListener, OnInit } from '@angular/core';
let ScrollToDirective = ScrollToDirective_1 = class ScrollToDirective {
    constructor(elmID, el) {
        this.elmID = elmID;
        this.el = el;
    }
    ngOnInit() {
    }
    static currentYPosition() {
        // Firefox, Chrome, Opera, Safari
        if (self.pageYOffset)
            return self.pageYOffset;
        // Internet Explorer 6 - standards mode
        if (document.documentElement && document.documentElement.scrollTop)
            return document.documentElement.scrollTop;
        // Internet Explorer 6, 7 and 8
        if (document.body.scrollTop)
            return document.body.scrollTop;
        return 0;
    }
    ;
    static elmYPosition(eID) {
        const elm = document.getElementById(eID);
        let y = elm.offsetTop;
        let node = elm;
        while (node.offsetParent && node.offsetParent != document.body) {
            node = node.offsetParent;
            y += node.offsetTop;
        }
        return y;
    }
    ;
    smoothScroll() {
        let i;
        if (!this.elmID)
            return;
        const startY = ScrollToDirective_1.currentYPosition();
        const stopY = ScrollToDirective_1.elmYPosition(this.elmID);
        const distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY);
            return;
        }
        let speed = Math.round(distance / 50);
        if (speed >= 20)
            speed = 20;
        let step = Math.round(distance / 25);
        let leapY = stopY > startY ? startY + step : startY - step;
        let timer = 0;
        if (stopY > startY) {
            for (i = startY; i < stopY; i += step) {
                setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                leapY += step;
                if (leapY > stopY)
                    leapY = stopY;
                timer++;
            }
            return;
        }
        for (i = startY; i > stopY; i -= step) {
            setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
            leapY -= step;
            if (leapY < stopY)
                leapY = stopY;
            timer++;
        }
        return false;
    }
    ;
};
ScrollToDirective.ctorParameters = () => [
    { type: String, decorators: [{ type: Attribute, args: ['scrollTo',] }] },
    { type: ElementRef }
];
tslib_1.__decorate([
    HostListener('click', ['$event']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], ScrollToDirective.prototype, "smoothScroll", null);
ScrollToDirective = ScrollToDirective_1 = tslib_1.__decorate([
    Directive({ selector: '[scrollTo]' }),
    tslib_1.__param(0, Attribute('scrollTo')),
    tslib_1.__metadata("design:paramtypes", [String, ElementRef])
], ScrollToDirective);
export { ScrollToDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Nyb2xsLXRvLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvc2Nyb2xsLXRvLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBR3ZGLElBQWEsaUJBQWlCLHlCQUE5QixNQUFhLGlCQUFpQjtJQUM3QixZQUEwQyxLQUFhLEVBQVUsRUFBYztRQUFyQyxVQUFLLEdBQUwsS0FBSyxDQUFRO1FBQVUsT0FBRSxHQUFGLEVBQUUsQ0FBWTtJQUMvRSxDQUFDO0lBRUQsUUFBUTtJQUNSLENBQUM7SUFFRCxNQUFNLENBQUMsZ0JBQWdCO1FBQ3RCLGlDQUFpQztRQUNqQyxJQUFJLElBQUksQ0FBQyxXQUFXO1lBQUUsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQzlDLHVDQUF1QztRQUN2QyxJQUFJLFFBQVEsQ0FBQyxlQUFlLElBQUksUUFBUSxDQUFDLGVBQWUsQ0FBQyxTQUFTO1lBQ2pFLE9BQU8sUUFBUSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUM7UUFDM0MsK0JBQStCO1FBQy9CLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTO1lBQUUsT0FBTyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUM1RCxPQUFPLENBQUMsQ0FBQztJQUNWLENBQUM7SUFBQSxDQUFDO0lBRUYsTUFBTSxDQUFDLFlBQVksQ0FBQyxHQUFHO1FBQ3RCLE1BQU0sR0FBRyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLFNBQVMsQ0FBQztRQUN0QixJQUFJLElBQUksR0FBUSxHQUFHLENBQUM7UUFDcEIsT0FBTyxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxZQUFZLElBQUksUUFBUSxDQUFDLElBQUksRUFBRTtZQUMvRCxJQUFJLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUN6QixDQUFDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQztTQUNwQjtRQUNELE9BQU8sQ0FBQyxDQUFDO0lBQ1YsQ0FBQztJQUFBLENBQUM7SUFHRixZQUFZO1FBQ1gsSUFBSSxDQUFDLENBQUM7UUFDTixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUs7WUFDZCxPQUFPO1FBQ1IsTUFBTSxNQUFNLEdBQUcsbUJBQWlCLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUNwRCxNQUFNLEtBQUssR0FBRyxtQkFBaUIsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3pELE1BQU0sUUFBUSxHQUFHLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDbEUsSUFBSSxRQUFRLEdBQUcsR0FBRyxFQUFFO1lBQ25CLFFBQVEsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDbkIsT0FBTztTQUNQO1FBQ0QsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDdEMsSUFBSSxLQUFLLElBQUksRUFBRTtZQUFFLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDNUIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDckMsSUFBSSxLQUFLLEdBQUcsS0FBSyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMzRCxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDZCxJQUFJLEtBQUssR0FBRyxNQUFNLEVBQUU7WUFDbkIsS0FBSyxDQUFDLEdBQUcsTUFBTSxFQUFFLENBQUMsR0FBRyxLQUFLLEVBQUUsQ0FBQyxJQUFJLElBQUksRUFBRTtnQkFDdEMsVUFBVSxDQUFDLHFCQUFxQixHQUFHLEtBQUssR0FBRyxHQUFHLEVBQUUsS0FBSyxHQUFHLEtBQUssQ0FBQyxDQUFDO2dCQUMvRCxLQUFLLElBQUksSUFBSSxDQUFDO2dCQUNkLElBQUksS0FBSyxHQUFHLEtBQUs7b0JBQUUsS0FBSyxHQUFHLEtBQUssQ0FBQztnQkFDakMsS0FBSyxFQUFFLENBQUM7YUFDUjtZQUNELE9BQU87U0FDUDtRQUNELEtBQUssQ0FBQyxHQUFHLE1BQU0sRUFBRSxDQUFDLEdBQUcsS0FBSyxFQUFFLENBQUMsSUFBSSxJQUFJLEVBQUU7WUFDdEMsVUFBVSxDQUFDLHFCQUFxQixHQUFHLEtBQUssR0FBRyxHQUFHLEVBQUUsS0FBSyxHQUFHLEtBQUssQ0FBQyxDQUFDO1lBQy9ELEtBQUssSUFBSSxJQUFJLENBQUM7WUFDZCxJQUFJLEtBQUssR0FBRyxLQUFLO2dCQUFFLEtBQUssR0FBRyxLQUFLLENBQUM7WUFDakMsS0FBSyxFQUFFLENBQUM7U0FDUjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2QsQ0FBQztJQUFBLENBQUM7Q0FDRixDQUFBOzt5Q0E5RGEsU0FBUyxTQUFDLFVBQVU7WUFBb0MsVUFBVTs7QUE2Qi9FO0lBREMsWUFBWSxDQUFDLE9BQU8sRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDOzs7O3FEQWlDakM7QUE5RFcsaUJBQWlCO0lBRDdCLFNBQVMsQ0FBQyxFQUFDLFFBQVEsRUFBRSxZQUFZLEVBQUMsQ0FBQztJQUV0QixtQkFBQSxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUE7cURBQW1DLFVBQVU7R0FEbkUsaUJBQWlCLENBK0Q3QjtTQS9EWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBdHRyaWJ1dGUsIERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgSG9zdExpc3RlbmVyLCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBEaXJlY3RpdmUoe3NlbGVjdG9yOiAnW3Njcm9sbFRvXSd9KVxyXG5leHBvcnQgY2xhc3MgU2Nyb2xsVG9EaXJlY3RpdmUgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cdGNvbnN0cnVjdG9yKEBBdHRyaWJ1dGUoJ3Njcm9sbFRvJykgcHVibGljIGVsbUlEOiBzdHJpbmcsIHByaXZhdGUgZWw6IEVsZW1lbnRSZWYpIHtcclxuXHR9XHJcblxyXG5cdG5nT25Jbml0KCkge1xyXG5cdH1cclxuXHJcblx0c3RhdGljIGN1cnJlbnRZUG9zaXRpb24oKSB7XHJcblx0XHQvLyBGaXJlZm94LCBDaHJvbWUsIE9wZXJhLCBTYWZhcmlcclxuXHRcdGlmIChzZWxmLnBhZ2VZT2Zmc2V0KSByZXR1cm4gc2VsZi5wYWdlWU9mZnNldDtcclxuXHRcdC8vIEludGVybmV0IEV4cGxvcmVyIDYgLSBzdGFuZGFyZHMgbW9kZVxyXG5cdFx0aWYgKGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCAmJiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wKVxyXG5cdFx0XHRyZXR1cm4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcDtcclxuXHRcdC8vIEludGVybmV0IEV4cGxvcmVyIDYsIDcgYW5kIDhcclxuXHRcdGlmIChkb2N1bWVudC5ib2R5LnNjcm9sbFRvcCkgcmV0dXJuIGRvY3VtZW50LmJvZHkuc2Nyb2xsVG9wO1xyXG5cdFx0cmV0dXJuIDA7XHJcblx0fTtcclxuXHJcblx0c3RhdGljIGVsbVlQb3NpdGlvbihlSUQpIHtcclxuXHRcdGNvbnN0IGVsbSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGVJRCk7XHJcblx0XHRsZXQgeSA9IGVsbS5vZmZzZXRUb3A7XHJcblx0XHRsZXQgbm9kZTogYW55ID0gZWxtO1xyXG5cdFx0d2hpbGUgKG5vZGUub2Zmc2V0UGFyZW50ICYmIG5vZGUub2Zmc2V0UGFyZW50ICE9IGRvY3VtZW50LmJvZHkpIHtcclxuXHRcdFx0bm9kZSA9IG5vZGUub2Zmc2V0UGFyZW50O1xyXG5cdFx0XHR5ICs9IG5vZGUub2Zmc2V0VG9wO1xyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIHk7XHJcblx0fTtcclxuXHJcblx0QEhvc3RMaXN0ZW5lcignY2xpY2snLCBbJyRldmVudCddKVxyXG5cdHNtb290aFNjcm9sbCgpIHtcclxuXHRcdGxldCBpO1xyXG5cdFx0aWYgKCF0aGlzLmVsbUlEKVxyXG5cdFx0XHRyZXR1cm47XHJcblx0XHRjb25zdCBzdGFydFkgPSBTY3JvbGxUb0RpcmVjdGl2ZS5jdXJyZW50WVBvc2l0aW9uKCk7XHJcblx0XHRjb25zdCBzdG9wWSA9IFNjcm9sbFRvRGlyZWN0aXZlLmVsbVlQb3NpdGlvbih0aGlzLmVsbUlEKTtcclxuXHRcdGNvbnN0IGRpc3RhbmNlID0gc3RvcFkgPiBzdGFydFkgPyBzdG9wWSAtIHN0YXJ0WSA6IHN0YXJ0WSAtIHN0b3BZO1xyXG5cdFx0aWYgKGRpc3RhbmNlIDwgMTAwKSB7XHJcblx0XHRcdHNjcm9sbFRvKDAsIHN0b3BZKTtcclxuXHRcdFx0cmV0dXJuO1xyXG5cdFx0fVxyXG5cdFx0bGV0IHNwZWVkID0gTWF0aC5yb3VuZChkaXN0YW5jZSAvIDUwKTtcclxuXHRcdGlmIChzcGVlZCA+PSAyMCkgc3BlZWQgPSAyMDtcclxuXHRcdGxldCBzdGVwID0gTWF0aC5yb3VuZChkaXN0YW5jZSAvIDI1KTtcclxuXHRcdGxldCBsZWFwWSA9IHN0b3BZID4gc3RhcnRZID8gc3RhcnRZICsgc3RlcCA6IHN0YXJ0WSAtIHN0ZXA7XHJcblx0XHRsZXQgdGltZXIgPSAwO1xyXG5cdFx0aWYgKHN0b3BZID4gc3RhcnRZKSB7XHJcblx0XHRcdGZvciAoaSA9IHN0YXJ0WTsgaSA8IHN0b3BZOyBpICs9IHN0ZXApIHtcclxuXHRcdFx0XHRzZXRUaW1lb3V0KFwid2luZG93LnNjcm9sbFRvKDAsIFwiICsgbGVhcFkgKyBcIilcIiwgdGltZXIgKiBzcGVlZCk7XHJcblx0XHRcdFx0bGVhcFkgKz0gc3RlcDtcclxuXHRcdFx0XHRpZiAobGVhcFkgPiBzdG9wWSkgbGVhcFkgPSBzdG9wWTtcclxuXHRcdFx0XHR0aW1lcisrO1xyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybjtcclxuXHRcdH1cclxuXHRcdGZvciAoaSA9IHN0YXJ0WTsgaSA+IHN0b3BZOyBpIC09IHN0ZXApIHtcclxuXHRcdFx0c2V0VGltZW91dChcIndpbmRvdy5zY3JvbGxUbygwLCBcIiArIGxlYXBZICsgXCIpXCIsIHRpbWVyICogc3BlZWQpO1xyXG5cdFx0XHRsZWFwWSAtPSBzdGVwO1xyXG5cdFx0XHRpZiAobGVhcFkgPCBzdG9wWSkgbGVhcFkgPSBzdG9wWTtcclxuXHRcdFx0dGltZXIrKztcclxuXHRcdH1cclxuXHRcdHJldHVybiBmYWxzZTtcclxuXHR9O1xyXG59XHJcbiJdfQ==