import * as tslib_1 from "tslib";
import { Directive, HostBinding, HostListener, Input } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { EgretSidenavHelperService } from './egret-sidenav-helper.service';
import { MatSidenav } from '@angular/material';
import { MediaObserver } from '@angular/flex-layout';
import { MatchMediaService } from '../../services/match-media.service';
let EgretSidenavHelperDirective = class EgretSidenavHelperDirective {
    constructor(matchMediaService, egretSidenavHelperService, matSidenav, media) {
        this.matchMediaService = matchMediaService;
        this.egretSidenavHelperService = egretSidenavHelperService;
        this.matSidenav = matSidenav;
        this.media = media;
        // Set the default value
        this.isOpen = true;
        this.unsubscribeAll = new Subject();
    }
    ngOnInit() {
        this.egretSidenavHelperService.setSidenav(this.id, this.matSidenav);
        if (this.media.isActive(this.isOpenBreakpoint)) {
            this.isOpen = true;
            this.matSidenav.mode = 'side';
            this.matSidenav.toggle(true);
        }
        else {
            this.isOpen = false;
            this.matSidenav.mode = 'over';
            this.matSidenav.toggle(false);
        }
        this.matchMediaService.onMediaChange
            .pipe(takeUntil(this.unsubscribeAll))
            .subscribe(() => {
            if (this.media.isActive(this.isOpenBreakpoint)) {
                this.isOpen = true;
                this.matSidenav.mode = 'side';
                this.matSidenav.toggle(true);
            }
            else {
                this.isOpen = false;
                this.matSidenav.mode = 'over';
                this.matSidenav.toggle(false);
            }
        });
    }
    ngOnDestroy() {
        this.unsubscribeAll.next();
        this.unsubscribeAll.complete();
    }
};
EgretSidenavHelperDirective.ctorParameters = () => [
    { type: MatchMediaService },
    { type: EgretSidenavHelperService },
    { type: MatSidenav },
    { type: MediaObserver }
];
tslib_1.__decorate([
    HostBinding('class.is-open'),
    tslib_1.__metadata("design:type", Boolean)
], EgretSidenavHelperDirective.prototype, "isOpen", void 0);
tslib_1.__decorate([
    Input('egretSidenavHelper'),
    tslib_1.__metadata("design:type", String)
], EgretSidenavHelperDirective.prototype, "id", void 0);
tslib_1.__decorate([
    Input('isOpen'),
    tslib_1.__metadata("design:type", String)
], EgretSidenavHelperDirective.prototype, "isOpenBreakpoint", void 0);
EgretSidenavHelperDirective = tslib_1.__decorate([
    Directive({
        selector: '[egretSidenavHelper]'
    }),
    tslib_1.__metadata("design:paramtypes", [MatchMediaService,
        EgretSidenavHelperService,
        MatSidenav,
        MediaObserver])
], EgretSidenavHelperDirective);
export { EgretSidenavHelperDirective };
let EgretSidenavTogglerDirective = class EgretSidenavTogglerDirective {
    constructor(egretSidenavHelperService) {
        this.egretSidenavHelperService = egretSidenavHelperService;
    }
    onClick() {
        // console.log(this.egretSidenavHelperService.getSidenav(this.id))
        this.egretSidenavHelperService.getSidenav(this.id).toggle();
    }
};
EgretSidenavTogglerDirective.ctorParameters = () => [
    { type: EgretSidenavHelperService }
];
tslib_1.__decorate([
    Input('egretSidenavToggler'),
    tslib_1.__metadata("design:type", Object)
], EgretSidenavTogglerDirective.prototype, "id", void 0);
tslib_1.__decorate([
    HostListener('click'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], EgretSidenavTogglerDirective.prototype, "onClick", null);
EgretSidenavTogglerDirective = tslib_1.__decorate([
    Directive({
        selector: '[egretSidenavToggler]'
    }),
    tslib_1.__metadata("design:paramtypes", [EgretSidenavHelperService])
], EgretSidenavTogglerDirective);
export { EgretSidenavTogglerDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWdyZXQtc2lkZW5hdi1oZWxwZXIuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsiZGlyZWN0aXZlcy9lZ3JldC1zaWRlbmF2LWhlbHBlci9lZ3JldC1zaWRlbmF2LWhlbHBlci5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQXFCLE1BQU0sZUFBZSxDQUFDO0FBQy9GLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQy9CLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDckQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFLdkUsSUFBYSwyQkFBMkIsR0FBeEMsTUFBYSwyQkFBMkI7SUFZdkMsWUFDUyxpQkFBb0MsRUFDcEMseUJBQW9ELEVBQ3BELFVBQXNCLEVBQ3RCLEtBQW9CO1FBSHBCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsOEJBQXlCLEdBQXpCLHlCQUF5QixDQUEyQjtRQUNwRCxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLFVBQUssR0FBTCxLQUFLLENBQWU7UUFFNUIsd0JBQXdCO1FBQ3hCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBRW5CLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztJQUNyQyxDQUFDO0lBRUQsUUFBUTtRQUNQLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFcEUsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsRUFBRTtZQUMvQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUNuQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7WUFDOUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDN0I7YUFBTTtZQUNOLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3BCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQztZQUM5QixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM5QjtRQUVELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhO2FBQ2xDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2FBQ3BDLFNBQVMsQ0FBQyxHQUFHLEVBQUU7WUFDZixJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFO2dCQUMvQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztnQkFDbkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDO2dCQUM5QixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUM3QjtpQkFBTTtnQkFDTixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztnQkFDcEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDO2dCQUM5QixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUM5QjtRQUNGLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELFdBQVc7UUFDVixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDaEMsQ0FBQztDQUNELENBQUE7O1lBM0M0QixpQkFBaUI7WUFDVCx5QkFBeUI7WUFDeEMsVUFBVTtZQUNmLGFBQWE7O0FBZDdCO0lBREMsV0FBVyxDQUFDLGVBQWUsQ0FBQzs7MkRBQ2I7QUFHaEI7SUFEQyxLQUFLLENBQUMsb0JBQW9CLENBQUM7O3VEQUNqQjtBQUdYO0lBREMsS0FBSyxDQUFDLFFBQVEsQ0FBQzs7cUVBQ1M7QUFSYiwyQkFBMkI7SUFIdkMsU0FBUyxDQUFDO1FBQ1YsUUFBUSxFQUFFLHNCQUFzQjtLQUNoQyxDQUFDOzZDQWMyQixpQkFBaUI7UUFDVCx5QkFBeUI7UUFDeEMsVUFBVTtRQUNmLGFBQWE7R0FoQmpCLDJCQUEyQixDQXdEdkM7U0F4RFksMkJBQTJCO0FBNkR4QyxJQUFhLDRCQUE0QixHQUF6QyxNQUFhLDRCQUE0QjtJQUl4QyxZQUFvQix5QkFBb0Q7UUFBcEQsOEJBQXlCLEdBQXpCLHlCQUF5QixDQUEyQjtJQUN4RSxDQUFDO0lBR0QsT0FBTztRQUNOLGtFQUFrRTtRQUNsRSxJQUFJLENBQUMseUJBQXlCLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUM3RCxDQUFDO0NBQ0QsQ0FBQTs7WUFSK0MseUJBQXlCOztBQUZ4RTtJQURDLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQzs7d0RBQ2Q7QUFNZjtJQURDLFlBQVksQ0FBQyxPQUFPLENBQUM7Ozs7MkRBSXJCO0FBWFcsNEJBQTRCO0lBSHhDLFNBQVMsQ0FBQztRQUNWLFFBQVEsRUFBRSx1QkFBdUI7S0FDakMsQ0FBQzs2Q0FLOEMseUJBQXlCO0dBSjVELDRCQUE0QixDQVl4QztTQVpZLDRCQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgSG9zdEJpbmRpbmcsIEhvc3RMaXN0ZW5lciwgSW5wdXQsIE9uRGVzdHJveSwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBFZ3JldFNpZGVuYXZIZWxwZXJTZXJ2aWNlIH0gZnJvbSAnLi9lZ3JldC1zaWRlbmF2LWhlbHBlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTWF0U2lkZW5hdiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgTWVkaWFPYnNlcnZlciB9IGZyb20gJ0Bhbmd1bGFyL2ZsZXgtbGF5b3V0JztcclxuaW1wb3J0IHsgTWF0Y2hNZWRpYVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9tYXRjaC1tZWRpYS5zZXJ2aWNlJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG5cdHNlbGVjdG9yOiAnW2VncmV0U2lkZW5hdkhlbHBlcl0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBFZ3JldFNpZGVuYXZIZWxwZXJEaXJlY3RpdmUgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcblx0QEhvc3RCaW5kaW5nKCdjbGFzcy5pcy1vcGVuJylcclxuXHRpc09wZW46IGJvb2xlYW47XHJcblxyXG5cdEBJbnB1dCgnZWdyZXRTaWRlbmF2SGVscGVyJylcclxuXHRpZDogc3RyaW5nO1xyXG5cclxuXHRASW5wdXQoJ2lzT3BlbicpXHJcblx0aXNPcGVuQnJlYWtwb2ludDogc3RyaW5nO1xyXG5cclxuXHRwcml2YXRlIHVuc3Vic2NyaWJlQWxsOiBTdWJqZWN0PGFueT47XHJcblxyXG5cdGNvbnN0cnVjdG9yKFxyXG5cdFx0cHJpdmF0ZSBtYXRjaE1lZGlhU2VydmljZTogTWF0Y2hNZWRpYVNlcnZpY2UsXHJcblx0XHRwcml2YXRlIGVncmV0U2lkZW5hdkhlbHBlclNlcnZpY2U6IEVncmV0U2lkZW5hdkhlbHBlclNlcnZpY2UsXHJcblx0XHRwcml2YXRlIG1hdFNpZGVuYXY6IE1hdFNpZGVuYXYsXHJcblx0XHRwcml2YXRlIG1lZGlhOiBNZWRpYU9ic2VydmVyXHJcblx0KSB7XHJcblx0XHQvLyBTZXQgdGhlIGRlZmF1bHQgdmFsdWVcclxuXHRcdHRoaXMuaXNPcGVuID0gdHJ1ZTtcclxuXHJcblx0XHR0aGlzLnVuc3Vic2NyaWJlQWxsID0gbmV3IFN1YmplY3QoKTtcclxuXHR9XHJcblxyXG5cdG5nT25Jbml0KCk6IHZvaWQge1xyXG5cdFx0dGhpcy5lZ3JldFNpZGVuYXZIZWxwZXJTZXJ2aWNlLnNldFNpZGVuYXYodGhpcy5pZCwgdGhpcy5tYXRTaWRlbmF2KTtcclxuXHJcblx0XHRpZiAodGhpcy5tZWRpYS5pc0FjdGl2ZSh0aGlzLmlzT3BlbkJyZWFrcG9pbnQpKSB7XHJcblx0XHRcdHRoaXMuaXNPcGVuID0gdHJ1ZTtcclxuXHRcdFx0dGhpcy5tYXRTaWRlbmF2Lm1vZGUgPSAnc2lkZSc7XHJcblx0XHRcdHRoaXMubWF0U2lkZW5hdi50b2dnbGUodHJ1ZSk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHR0aGlzLmlzT3BlbiA9IGZhbHNlO1xyXG5cdFx0XHR0aGlzLm1hdFNpZGVuYXYubW9kZSA9ICdvdmVyJztcclxuXHRcdFx0dGhpcy5tYXRTaWRlbmF2LnRvZ2dsZShmYWxzZSk7XHJcblx0XHR9XHJcblxyXG5cdFx0dGhpcy5tYXRjaE1lZGlhU2VydmljZS5vbk1lZGlhQ2hhbmdlXHJcblx0XHRcdC5waXBlKHRha2VVbnRpbCh0aGlzLnVuc3Vic2NyaWJlQWxsKSlcclxuXHRcdFx0LnN1YnNjcmliZSgoKSA9PiB7XHJcblx0XHRcdFx0aWYgKHRoaXMubWVkaWEuaXNBY3RpdmUodGhpcy5pc09wZW5CcmVha3BvaW50KSkge1xyXG5cdFx0XHRcdFx0dGhpcy5pc09wZW4gPSB0cnVlO1xyXG5cdFx0XHRcdFx0dGhpcy5tYXRTaWRlbmF2Lm1vZGUgPSAnc2lkZSc7XHJcblx0XHRcdFx0XHR0aGlzLm1hdFNpZGVuYXYudG9nZ2xlKHRydWUpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHR0aGlzLmlzT3BlbiA9IGZhbHNlO1xyXG5cdFx0XHRcdFx0dGhpcy5tYXRTaWRlbmF2Lm1vZGUgPSAnb3Zlcic7XHJcblx0XHRcdFx0XHR0aGlzLm1hdFNpZGVuYXYudG9nZ2xlKGZhbHNlKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cdH1cclxuXHJcblx0bmdPbkRlc3Ryb3koKTogdm9pZCB7XHJcblx0XHR0aGlzLnVuc3Vic2NyaWJlQWxsLm5leHQoKTtcclxuXHRcdHRoaXMudW5zdWJzY3JpYmVBbGwuY29tcGxldGUoKTtcclxuXHR9XHJcbn1cclxuXHJcbkBEaXJlY3RpdmUoe1xyXG5cdHNlbGVjdG9yOiAnW2VncmV0U2lkZW5hdlRvZ2dsZXJdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRWdyZXRTaWRlbmF2VG9nZ2xlckRpcmVjdGl2ZSB7XHJcblx0QElucHV0KCdlZ3JldFNpZGVuYXZUb2dnbGVyJylcclxuXHRwdWJsaWMgaWQ6IGFueTtcclxuXHJcblx0Y29uc3RydWN0b3IocHJpdmF0ZSBlZ3JldFNpZGVuYXZIZWxwZXJTZXJ2aWNlOiBFZ3JldFNpZGVuYXZIZWxwZXJTZXJ2aWNlKSB7XHJcblx0fVxyXG5cclxuXHRASG9zdExpc3RlbmVyKCdjbGljaycpXHJcblx0b25DbGljaygpIHtcclxuXHRcdC8vIGNvbnNvbGUubG9nKHRoaXMuZWdyZXRTaWRlbmF2SGVscGVyU2VydmljZS5nZXRTaWRlbmF2KHRoaXMuaWQpKVxyXG5cdFx0dGhpcy5lZ3JldFNpZGVuYXZIZWxwZXJTZXJ2aWNlLmdldFNpZGVuYXYodGhpcy5pZCkudG9nZ2xlKCk7XHJcblx0fVxyXG59XHJcbiJdfQ==