import * as tslib_1 from "tslib";
import { Directive, HostBinding, Inject, Input } from '@angular/core';
import { AppDropdownDirective } from './dropdown.directive';
let DropdownLinkDirective = class DropdownLinkDirective {
    constructor(nav) {
        this.nav = nav;
    }
    get open() {
        return this._open;
    }
    set open(value) {
        this._open = value;
        if (value) {
            this.nav.closeOtherLinks(this);
        }
    }
    ngOnInit() {
        this.nav.addLink(this);
    }
    ngOnDestroy() {
        this.nav.removeGroup(this);
    }
    toggle() {
        this.open = !this.open;
    }
};
DropdownLinkDirective.ctorParameters = () => [
    { type: AppDropdownDirective, decorators: [{ type: Inject, args: [AppDropdownDirective,] }] }
];
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], DropdownLinkDirective.prototype, "group", void 0);
tslib_1.__decorate([
    HostBinding('class.open'),
    Input(),
    tslib_1.__metadata("design:type", Boolean),
    tslib_1.__metadata("design:paramtypes", [Boolean])
], DropdownLinkDirective.prototype, "open", null);
DropdownLinkDirective = tslib_1.__decorate([
    Directive({
        selector: '[appDropdownLink]'
    }),
    tslib_1.__param(0, Inject(AppDropdownDirective)),
    tslib_1.__metadata("design:paramtypes", [AppDropdownDirective])
], DropdownLinkDirective);
export { DropdownLinkDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24tbGluay5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvZWdyZXQvIiwic291cmNlcyI6WyJkaXJlY3RpdmVzL2Ryb3Bkb3duLWxpbmsuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXRFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBSzVELElBQWEscUJBQXFCLEdBQWxDLE1BQWEscUJBQXFCO0lBb0JqQyxZQUFpRCxHQUF5QjtRQUN6RSxJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztJQUNoQixDQUFDO0lBaEJELElBQUksSUFBSTtRQUNQLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUNuQixDQUFDO0lBRUQsSUFBSSxJQUFJLENBQUMsS0FBYztRQUN0QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixJQUFJLEtBQUssRUFBRTtZQUNWLElBQUksQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQy9CO0lBQ0YsQ0FBQztJQVNNLFFBQVE7UUFDZCxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN4QixDQUFDO0lBRU0sV0FBVztRQUNqQixJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRU0sTUFBTTtRQUNaLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ3hCLENBQUM7Q0FFRCxDQUFBOztZQWhCc0Qsb0JBQW9CLHVCQUF0RCxNQUFNLFNBQUMsb0JBQW9COztBQWxCdEM7SUFBUixLQUFLLEVBQUU7O29EQUFtQjtBQUkzQjtJQUZDLFdBQVcsQ0FBQyxZQUFZLENBQUM7SUFDekIsS0FBSyxFQUFFOzs7aURBR1A7QUFSVyxxQkFBcUI7SUFIakMsU0FBUyxDQUFDO1FBQ1YsUUFBUSxFQUFFLG1CQUFtQjtLQUM3QixDQUFDO0lBcUJtQixtQkFBQSxNQUFNLENBQUMsb0JBQW9CLENBQUMsQ0FBQTs2Q0FBTSxvQkFBb0I7R0FwQjlELHFCQUFxQixDQW9DakM7U0FwQ1kscUJBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBIb3N0QmluZGluZywgSW5qZWN0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgQXBwRHJvcGRvd25EaXJlY3RpdmUgfSBmcm9tICcuL2Ryb3Bkb3duLmRpcmVjdGl2ZSc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuXHRzZWxlY3RvcjogJ1thcHBEcm9wZG93bkxpbmtdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRHJvcGRvd25MaW5rRGlyZWN0aXZlIHtcclxuXHJcblx0QElucHV0KCkgcHVibGljIGdyb3VwOiBhbnk7XHJcblxyXG5cdEBIb3N0QmluZGluZygnY2xhc3Mub3BlbicpXHJcblx0QElucHV0KClcclxuXHRnZXQgb3BlbigpOiBib29sZWFuIHtcclxuXHRcdHJldHVybiB0aGlzLl9vcGVuO1xyXG5cdH1cclxuXHJcblx0c2V0IG9wZW4odmFsdWU6IGJvb2xlYW4pIHtcclxuXHRcdHRoaXMuX29wZW4gPSB2YWx1ZTtcclxuXHRcdGlmICh2YWx1ZSkge1xyXG5cdFx0XHR0aGlzLm5hdi5jbG9zZU90aGVyTGlua3ModGhpcyk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRwcm90ZWN0ZWQgX29wZW46IGJvb2xlYW47XHJcblx0cHJvdGVjdGVkIG5hdjogQXBwRHJvcGRvd25EaXJlY3RpdmU7XHJcblxyXG5cdHB1YmxpYyBjb25zdHJ1Y3RvcihASW5qZWN0KEFwcERyb3Bkb3duRGlyZWN0aXZlKSBuYXY6IEFwcERyb3Bkb3duRGlyZWN0aXZlKSB7XHJcblx0XHR0aGlzLm5hdiA9IG5hdjtcclxuXHR9XHJcblxyXG5cdHB1YmxpYyBuZ09uSW5pdCgpOiBhbnkge1xyXG5cdFx0dGhpcy5uYXYuYWRkTGluayh0aGlzKTtcclxuXHR9XHJcblxyXG5cdHB1YmxpYyBuZ09uRGVzdHJveSgpOiBhbnkge1xyXG5cdFx0dGhpcy5uYXYucmVtb3ZlR3JvdXAodGhpcyk7XHJcblx0fVxyXG5cclxuXHRwdWJsaWMgdG9nZ2xlKCk6IGFueSB7XHJcblx0XHR0aGlzLm9wZW4gPSAhdGhpcy5vcGVuO1xyXG5cdH1cclxuXHJcbn1cclxuIl19