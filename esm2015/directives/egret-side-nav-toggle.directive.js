import * as tslib_1 from "tslib";
import { Directive, Host, Optional, Self } from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { MatSidenav } from '@angular/material';
let EgretSideNavToggleDirective = class EgretSideNavToggleDirective {
    constructor(media, sideNav) {
        this.media = media;
        this.sideNav = sideNav;
    }
    ngOnInit() {
        this.initSideNav();
    }
    ngOnDestroy() {
        if (this.screenSizeWatcher) {
            this.screenSizeWatcher.unsubscribe();
        }
    }
    updateSidenav() {
        var self = this;
        setTimeout(() => {
            self.sideNav.opened = !self.isMobile;
            self.sideNav.mode = self.isMobile ? 'over' : 'side';
        });
    }
    initSideNav() {
        this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
        // console.log(this.isMobile)
        this.updateSidenav();
        this.screenSizeWatcher = this.media.asObservable().subscribe((change) => {
            this.isMobile = (change[0].mqAlias == 'xs') || (change[0].mqAlias == 'sm');
            this.updateSidenav();
        });
    }
};
EgretSideNavToggleDirective.ctorParameters = () => [
    { type: MediaObserver },
    { type: MatSidenav, decorators: [{ type: Host }, { type: Self }, { type: Optional }] }
];
EgretSideNavToggleDirective = tslib_1.__decorate([
    Directive({
        selector: '[EgretSideNavToggle]'
    }),
    tslib_1.__param(1, Host()), tslib_1.__param(1, Self()), tslib_1.__param(1, Optional()),
    tslib_1.__metadata("design:paramtypes", [MediaObserver,
        MatSidenav])
], EgretSideNavToggleDirective);
export { EgretSideNavToggleDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWdyZXQtc2lkZS1uYXYtdG9nZ2xlLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvZWdyZXQtc2lkZS1uYXYtdG9nZ2xlLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQXFCLFFBQVEsRUFBRSxJQUFJLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkYsT0FBTyxFQUFFLFdBQVcsRUFBRSxhQUFhLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUVsRSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFNL0MsSUFBYSwyQkFBMkIsR0FBeEMsTUFBYSwyQkFBMkI7SUFJdkMsWUFDUyxLQUFvQixFQUNPLE9BQW1CO1FBRDlDLFVBQUssR0FBTCxLQUFLLENBQWU7UUFDTyxZQUFPLEdBQVAsT0FBTyxDQUFZO0lBRXZELENBQUM7SUFFRCxRQUFRO1FBQ1AsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ3BCLENBQUM7SUFFRCxXQUFXO1FBQ1YsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDM0IsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsRUFBRSxDQUFBO1NBQ3BDO0lBQ0YsQ0FBQztJQUVELGFBQWE7UUFDWixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNmLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUNyQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUNyRCxDQUFDLENBQUMsQ0FBQTtJQUNILENBQUM7SUFFRCxXQUFXO1FBQ1YsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2RSw2QkFBNkI7UUFDN0IsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLE1BQXFCLEVBQUUsRUFBRTtZQUN0RixJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLENBQUM7WUFDM0UsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3RCLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQztDQUVELENBQUE7O1lBakNnQixhQUFhO1lBQ2dCLFVBQVUsdUJBQXJELElBQUksWUFBSSxJQUFJLFlBQUksUUFBUTs7QUFOZCwyQkFBMkI7SUFIdkMsU0FBUyxDQUFDO1FBQ1YsUUFBUSxFQUFFLHNCQUFzQjtLQUNoQyxDQUFDO0lBT0MsbUJBQUEsSUFBSSxFQUFFLENBQUEsRUFBRSxtQkFBQSxJQUFJLEVBQUUsQ0FBQSxFQUFFLG1CQUFBLFFBQVEsRUFBRSxDQUFBOzZDQURaLGFBQWE7UUFDZ0IsVUFBVTtHQU4zQywyQkFBMkIsQ0FzQ3ZDO1NBdENZLDJCQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgSG9zdCwgT25EZXN0cm95LCBPbkluaXQsIE9wdGlvbmFsLCBTZWxmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1lZGlhQ2hhbmdlLCBNZWRpYU9ic2VydmVyIH0gZnJvbSAnQGFuZ3VsYXIvZmxleC1sYXlvdXQnO1xyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgTWF0U2lkZW5hdiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuXHJcblxyXG5ARGlyZWN0aXZlKHtcclxuXHRzZWxlY3RvcjogJ1tFZ3JldFNpZGVOYXZUb2dnbGVdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRWdyZXRTaWRlTmF2VG9nZ2xlRGlyZWN0aXZlIGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG5cdGlzTW9iaWxlO1xyXG5cdHNjcmVlblNpemVXYXRjaGVyOiBTdWJzY3JpcHRpb247XHJcblxyXG5cdGNvbnN0cnVjdG9yKFxyXG5cdFx0cHJpdmF0ZSBtZWRpYTogTWVkaWFPYnNlcnZlcixcclxuXHRcdEBIb3N0KCkgQFNlbGYoKSBAT3B0aW9uYWwoKSBwdWJsaWMgc2lkZU5hdjogTWF0U2lkZW5hdlxyXG5cdCkge1xyXG5cdH1cclxuXHJcblx0bmdPbkluaXQoKSB7XHJcblx0XHR0aGlzLmluaXRTaWRlTmF2KCk7XHJcblx0fVxyXG5cclxuXHRuZ09uRGVzdHJveSgpIHtcclxuXHRcdGlmICh0aGlzLnNjcmVlblNpemVXYXRjaGVyKSB7XHJcblx0XHRcdHRoaXMuc2NyZWVuU2l6ZVdhdGNoZXIudW5zdWJzY3JpYmUoKVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0dXBkYXRlU2lkZW5hdigpIHtcclxuXHRcdHZhciBzZWxmID0gdGhpcztcclxuXHRcdHNldFRpbWVvdXQoKCkgPT4ge1xyXG5cdFx0XHRzZWxmLnNpZGVOYXYub3BlbmVkID0gIXNlbGYuaXNNb2JpbGU7XHJcblx0XHRcdHNlbGYuc2lkZU5hdi5tb2RlID0gc2VsZi5pc01vYmlsZSA/ICdvdmVyJyA6ICdzaWRlJztcclxuXHRcdH0pXHJcblx0fVxyXG5cclxuXHRpbml0U2lkZU5hdigpIHtcclxuXHRcdHRoaXMuaXNNb2JpbGUgPSB0aGlzLm1lZGlhLmlzQWN0aXZlKCd4cycpIHx8IHRoaXMubWVkaWEuaXNBY3RpdmUoJ3NtJyk7XHJcblx0XHQvLyBjb25zb2xlLmxvZyh0aGlzLmlzTW9iaWxlKVxyXG5cdFx0dGhpcy51cGRhdGVTaWRlbmF2KCk7XHJcblx0XHR0aGlzLnNjcmVlblNpemVXYXRjaGVyID0gdGhpcy5tZWRpYS5hc09ic2VydmFibGUoKS5zdWJzY3JpYmUoKGNoYW5nZTogTWVkaWFDaGFuZ2VbXSkgPT4ge1xyXG5cdFx0XHR0aGlzLmlzTW9iaWxlID0gKGNoYW5nZVswXS5tcUFsaWFzID09ICd4cycpIHx8IChjaGFuZ2VbMF0ubXFBbGlhcyA9PSAnc20nKTtcclxuXHRcdFx0dGhpcy51cGRhdGVTaWRlbmF2KCk7XHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG59XHJcbiJdfQ==