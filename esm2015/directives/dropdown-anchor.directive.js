import * as tslib_1 from "tslib";
import { Directive, HostListener, Inject } from '@angular/core';
import { DropdownLinkDirective } from './dropdown-link.directive';
let DropdownAnchorDirective = class DropdownAnchorDirective {
    constructor(navlink) {
        this.navlink = navlink;
    }
    onClick(e) {
        this.navlink.toggle();
    }
};
DropdownAnchorDirective.ctorParameters = () => [
    { type: DropdownLinkDirective, decorators: [{ type: Inject, args: [DropdownLinkDirective,] }] }
];
tslib_1.__decorate([
    HostListener('click', ['$event']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], DropdownAnchorDirective.prototype, "onClick", null);
DropdownAnchorDirective = tslib_1.__decorate([
    Directive({
        selector: '[appDropdownToggle]'
    }),
    tslib_1.__param(0, Inject(DropdownLinkDirective)),
    tslib_1.__metadata("design:paramtypes", [DropdownLinkDirective])
], DropdownAnchorDirective);
export { DropdownAnchorDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24tYW5jaG9yLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvZHJvcGRvd24tYW5jaG9yLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBS2xFLElBQWEsdUJBQXVCLEdBQXBDLE1BQWEsdUJBQXVCO0lBSW5DLFlBQTJDLE9BQThCO1FBQ3hFLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO0lBQ3hCLENBQUM7SUFHRCxPQUFPLENBQUMsQ0FBTTtRQUNiLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDdkIsQ0FBQztDQUNELENBQUE7O1lBUm9ELHFCQUFxQix1QkFBNUQsTUFBTSxTQUFDLHFCQUFxQjs7QUFLekM7SUFEQyxZQUFZLENBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUM7Ozs7c0RBR2pDO0FBWFcsdUJBQXVCO0lBSG5DLFNBQVMsQ0FBQztRQUNWLFFBQVEsRUFBRSxxQkFBcUI7S0FDL0IsQ0FBQztJQUtZLG1CQUFBLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxDQUFBOzZDQUFVLHFCQUFxQjtHQUo3RCx1QkFBdUIsQ0FZbkM7U0FaWSx1QkFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEhvc3RMaXN0ZW5lciwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERyb3Bkb3duTGlua0RpcmVjdGl2ZSB9IGZyb20gJy4vZHJvcGRvd24tbGluay5kaXJlY3RpdmUnO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcblx0c2VsZWN0b3I6ICdbYXBwRHJvcGRvd25Ub2dnbGVdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRHJvcGRvd25BbmNob3JEaXJlY3RpdmUge1xyXG5cclxuXHRwcm90ZWN0ZWQgbmF2bGluazogRHJvcGRvd25MaW5rRGlyZWN0aXZlO1xyXG5cclxuXHRjb25zdHJ1Y3RvcihASW5qZWN0KERyb3Bkb3duTGlua0RpcmVjdGl2ZSkgbmF2bGluazogRHJvcGRvd25MaW5rRGlyZWN0aXZlKSB7XHJcblx0XHR0aGlzLm5hdmxpbmsgPSBuYXZsaW5rO1xyXG5cdH1cclxuXHJcblx0QEhvc3RMaXN0ZW5lcignY2xpY2snLCBbJyRldmVudCddKVxyXG5cdG9uQ2xpY2soZTogYW55KSB7XHJcblx0XHR0aGlzLm5hdmxpbmsudG9nZ2xlKCk7XHJcblx0fVxyXG59XHJcbiJdfQ==