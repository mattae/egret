import * as tslib_1 from "tslib";
import { Attribute, Directive, ElementRef, OnInit } from '@angular/core';
let FontSizeDirective = class FontSizeDirective {
    constructor(fontSize, el) {
        this.fontSize = fontSize;
        this.el = el;
    }
    ngOnInit() {
        this.el.nativeElement.fontSize = this.fontSize;
    }
};
FontSizeDirective.ctorParameters = () => [
    { type: String, decorators: [{ type: Attribute, args: ['fontSize',] }] },
    { type: ElementRef }
];
FontSizeDirective = tslib_1.__decorate([
    Directive({ selector: '[fontSize]' }),
    tslib_1.__param(0, Attribute('fontSize')),
    tslib_1.__metadata("design:paramtypes", [String, ElementRef])
], FontSizeDirective);
export { FontSizeDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9udC1zaXplLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvZm9udC1zaXplLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUd6RSxJQUFhLGlCQUFpQixHQUE5QixNQUFhLGlCQUFpQjtJQUM1QixZQUEyQyxRQUFnQixFQUFVLEVBQWM7UUFBeEMsYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUFVLE9BQUUsR0FBRixFQUFFLENBQVk7SUFBSSxDQUFDO0lBQ3hGLFFBQVE7UUFDTixJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUNqRCxDQUFDO0NBQ0YsQ0FBQTs7eUNBSmUsU0FBUyxTQUFDLFVBQVU7WUFBdUMsVUFBVTs7QUFEeEUsaUJBQWlCO0lBRDdCLFNBQVMsQ0FBQyxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztJQUV0QixtQkFBQSxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUE7cURBQXNDLFVBQVU7R0FEeEUsaUJBQWlCLENBSzdCO1NBTFksaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQXR0cmlidXRlLCBEaXJlY3RpdmUsIEVsZW1lbnRSZWYsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQERpcmVjdGl2ZSh7IHNlbGVjdG9yOiAnW2ZvbnRTaXplXScgfSlcclxuZXhwb3J0IGNsYXNzIEZvbnRTaXplRGlyZWN0aXZlIGltcGxlbWVudHMgT25Jbml0IHtcclxuICBjb25zdHJ1Y3RvciggQEF0dHJpYnV0ZSgnZm9udFNpemUnKSBwdWJsaWMgZm9udFNpemU6IHN0cmluZywgcHJpdmF0ZSBlbDogRWxlbWVudFJlZikgeyB9XHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuZm9udFNpemUgPSB0aGlzLmZvbnRTaXplO1xyXG4gIH1cclxufVxyXG4iXX0=