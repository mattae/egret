import * as tslib_1 from "tslib";
import { Injectable } from "@angular/core";
import * as i0 from "@angular/core";
let EgretSidebarHelperService = class EgretSidebarHelperService {
    constructor() {
        this.sidebarList = [];
    }
    setSidebar(name, sidebar) {
        this.sidebarList[name] = sidebar;
    }
    getSidebar(name) {
        return this.sidebarList[name];
    }
    removeSidebar(name) {
        if (!this.sidebarList[name]) {
            console.warn(`The sidebar with name '${name}' doesn't exist.`);
        }
        // remove sidebar
        delete this.sidebarList[name];
    }
};
EgretSidebarHelperService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function EgretSidebarHelperService_Factory() { return new EgretSidebarHelperService(); }, token: EgretSidebarHelperService, providedIn: "root" });
EgretSidebarHelperService = tslib_1.__decorate([
    Injectable({
        providedIn: "root"
    }),
    tslib_1.__metadata("design:paramtypes", [])
], EgretSidebarHelperService);
export { EgretSidebarHelperService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWdyZXQtc2lkZWJhci1oZWxwZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvZWdyZXQtc2lkZWJhci9lZ3JldC1zaWRlYmFyLWhlbHBlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOztBQU0zQyxJQUFhLHlCQUF5QixHQUF0QyxNQUFhLHlCQUF5QjtJQUdsQztRQUNJLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFRCxVQUFVLENBQUMsSUFBSSxFQUFFLE9BQU87UUFDcEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxPQUFPLENBQUM7SUFDckMsQ0FBQztJQUVELFVBQVUsQ0FBQyxJQUFJO1FBQ1gsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFFRCxhQUFhLENBQUMsSUFBSTtRQUNkLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3pCLE9BQU8sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLElBQUksa0JBQWtCLENBQUMsQ0FBQztTQUNsRTtRQUVELGlCQUFpQjtRQUNqQixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbEMsQ0FBQztDQUNKLENBQUE7O0FBdkJZLHlCQUF5QjtJQUhyQyxVQUFVLENBQUM7UUFDUixVQUFVLEVBQUUsTUFBTTtLQUNyQixDQUFDOztHQUNXLHlCQUF5QixDQXVCckM7U0F2QlkseUJBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IEVncmV0U2lkZWJhckNvbXBvbmVudCB9IGZyb20gXCIuL2VncmV0LXNpZGViYXIuY29tcG9uZW50XCI7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiBcInJvb3RcIlxyXG59KVxyXG5leHBvcnQgY2xhc3MgRWdyZXRTaWRlYmFySGVscGVyU2VydmljZSB7XHJcbiAgICBzaWRlYmFyTGlzdDogRWdyZXRTaWRlYmFyQ29tcG9uZW50W107XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy5zaWRlYmFyTGlzdCA9IFtdO1xyXG4gICAgfVxyXG5cclxuICAgIHNldFNpZGViYXIobmFtZSwgc2lkZWJhcik6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuc2lkZWJhckxpc3RbbmFtZV0gPSBzaWRlYmFyO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFNpZGViYXIobmFtZSk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2lkZWJhckxpc3RbbmFtZV07XHJcbiAgICB9XHJcblxyXG4gICAgcmVtb3ZlU2lkZWJhcihuYW1lKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLnNpZGViYXJMaXN0W25hbWVdKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihgVGhlIHNpZGViYXIgd2l0aCBuYW1lICcke25hbWV9JyBkb2Vzbid0IGV4aXN0LmApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gcmVtb3ZlIHNpZGViYXJcclxuICAgICAgICBkZWxldGUgdGhpcy5zaWRlYmFyTGlzdFtuYW1lXTtcclxuICAgIH1cclxufVxyXG4iXX0=