import * as tslib_1 from "tslib";
import { ChangeDetectorRef, Component, Directive, ElementRef, HostBinding, HostListener, Input, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { EgretSidebarHelperService } from './egret-sidebar-helper.service';
import { MatchMediaService } from '../../services/match-media.service';
let EgretSidebarComponent = class EgretSidebarComponent {
    constructor(matchMediaService, mediaObserver, sidebarHelperService, _renderer, _elementRef, cdr) {
        this.matchMediaService = matchMediaService;
        this.mediaObserver = mediaObserver;
        this.sidebarHelperService = sidebarHelperService;
        this._renderer = _renderer;
        this._elementRef = _elementRef;
        this.cdr = cdr;
        this.backdrop = null;
        this.lockedBreakpoint = "gt-sm";
        this.unsubscribeAll = new Subject();
    }
    ngOnInit() {
        this.sidebarHelperService.setSidebar(this.name, this);
        if (this.mediaObserver.isActive(this.lockedBreakpoint)) {
            this.sidebarLockedOpen = true;
            this.opened = true;
        }
        else {
            this.sidebarLockedOpen = false;
            this.opened = false;
        }
        this.matchMediaService.onMediaChange
            .pipe(takeUntil(this.unsubscribeAll))
            .subscribe(() => {
            // console.log("medua sub");
            if (this.mediaObserver.isActive(this.lockedBreakpoint)) {
                this.sidebarLockedOpen = true;
                this.opened = true;
            }
            else {
                this.sidebarLockedOpen = false;
                this.opened = false;
            }
        });
    }
    open() {
        this.opened = true;
        if (!this.sidebarLockedOpen && !this.backdrop) {
            this.showBackdrop();
        }
    }
    close() {
        this.opened = false;
        this.hideBackdrop();
    }
    toggle() {
        if (this.opened) {
            this.close();
        }
        else {
            this.open();
        }
    }
    showBackdrop() {
        this.backdrop = this._renderer.createElement("div");
        this.backdrop.classList.add("egret-sidebar-overlay");
        this._renderer.appendChild(this._elementRef.nativeElement.parentElement, this.backdrop);
        // Close sidebar onclick
        this.backdrop.addEventListener("click", () => {
            this.close();
        });
        this.cdr.markForCheck();
    }
    hideBackdrop() {
        if (this.backdrop) {
            this.backdrop.parentNode.removeChild(this.backdrop);
            this.backdrop = null;
        }
        this.cdr.markForCheck();
    }
    ngOnDestroy() {
        this.unsubscribeAll.next();
        this.unsubscribeAll.complete();
        this.sidebarHelperService.removeSidebar(this.name);
    }
};
EgretSidebarComponent.ctorParameters = () => [
    { type: MatchMediaService },
    { type: MediaObserver },
    { type: EgretSidebarHelperService },
    { type: Renderer2 },
    { type: ElementRef },
    { type: ChangeDetectorRef }
];
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], EgretSidebarComponent.prototype, "name", void 0);
tslib_1.__decorate([
    Input(),
    HostBinding("class.position-right"),
    tslib_1.__metadata("design:type", Boolean)
], EgretSidebarComponent.prototype, "right", void 0);
tslib_1.__decorate([
    HostBinding("class.open"),
    tslib_1.__metadata("design:type", Boolean)
], EgretSidebarComponent.prototype, "opened", void 0);
tslib_1.__decorate([
    HostBinding("class.sidebar-locked-open"),
    tslib_1.__metadata("design:type", Boolean)
], EgretSidebarComponent.prototype, "sidebarLockedOpen", void 0);
tslib_1.__decorate([
    HostBinding("class.is-over"),
    tslib_1.__metadata("design:type", Boolean)
], EgretSidebarComponent.prototype, "isOver", void 0);
EgretSidebarComponent = tslib_1.__decorate([
    Component({
        selector: 'egret-sidebar',
        template: "<div>\r\n  <ng-content></ng-content>\r\n</div>",
        styles: [""]
    }),
    tslib_1.__metadata("design:paramtypes", [MatchMediaService,
        MediaObserver,
        EgretSidebarHelperService,
        Renderer2,
        ElementRef,
        ChangeDetectorRef])
], EgretSidebarComponent);
export { EgretSidebarComponent };
let EgretSidebarTogglerDirective = class EgretSidebarTogglerDirective {
    constructor(egretSidebarHelperService) {
        this.egretSidebarHelperService = egretSidebarHelperService;
    }
    onClick() {
        this.egretSidebarHelperService.getSidebar(this.id).toggle();
    }
};
EgretSidebarTogglerDirective.ctorParameters = () => [
    { type: EgretSidebarHelperService }
];
tslib_1.__decorate([
    Input("egretSidebarToggler"),
    tslib_1.__metadata("design:type", Object)
], EgretSidebarTogglerDirective.prototype, "id", void 0);
tslib_1.__decorate([
    HostListener("click"),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], EgretSidebarTogglerDirective.prototype, "onClick", null);
EgretSidebarTogglerDirective = tslib_1.__decorate([
    Directive({
        selector: "[egretSidebarToggler]"
    }),
    tslib_1.__metadata("design:paramtypes", [EgretSidebarHelperService])
], EgretSidebarTogglerDirective);
export { EgretSidebarTogglerDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWdyZXQtc2lkZWJhci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvZWdyZXQvIiwic291cmNlcyI6WyJjb21wb25lbnRzL2VncmV0LXNpZGViYXIvZWdyZXQtc2lkZWJhci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFDSCxpQkFBaUIsRUFDakIsU0FBUyxFQUNULFNBQVMsRUFDVCxVQUFVLEVBQ1YsV0FBVyxFQUNYLFlBQVksRUFDWixLQUFLLEVBQ0wsU0FBUyxFQUNULE1BQU0sRUFDTixTQUFTLEVBQ1osTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3JELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBT3ZFLElBQWEscUJBQXFCLEdBQWxDLE1BQWEscUJBQXFCO0lBMEI5QixZQUNZLGlCQUFvQyxFQUNwQyxhQUE0QixFQUM1QixvQkFBK0MsRUFDL0MsU0FBb0IsRUFDcEIsV0FBdUIsRUFDdkIsR0FBc0I7UUFMdEIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1Qix5QkFBb0IsR0FBcEIsb0JBQW9CLENBQTJCO1FBQy9DLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFDcEIsZ0JBQVcsR0FBWCxXQUFXLENBQVk7UUFDdkIsUUFBRyxHQUFILEdBQUcsQ0FBbUI7UUFYMUIsYUFBUSxHQUF1QixJQUFJLENBQUM7UUFFcEMscUJBQWdCLEdBQUcsT0FBTyxDQUFDO1FBVy9CLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztJQUN4QyxDQUFDO0lBRUQsUUFBUTtRQUNKLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztRQUV0RCxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFO1lBQ3BELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7WUFDOUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7U0FDdEI7YUFBTTtZQUNILElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7WUFDL0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7U0FDdkI7UUFFRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYTthQUMvQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQzthQUNwQyxTQUFTLENBQUMsR0FBRyxFQUFFO1lBQ1osNEJBQTRCO1lBQzVCLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEVBQUU7Z0JBQ3BELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7Z0JBQzlCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2FBQ3RCO2lCQUFNO2dCQUNILElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7Z0JBQy9CLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO2FBQ3ZCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsSUFBSTtRQUNBLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ25CLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQzNDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztTQUN2QjtJQUNMLENBQUM7SUFFRCxLQUFLO1FBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFRCxNQUFNO1FBQ0YsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2IsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ2hCO2FBQU07WUFDSCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDZjtJQUNMLENBQUM7SUFFRCxZQUFZO1FBQ1IsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwRCxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQztRQUVyRCxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FDdEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsYUFBYSxFQUM1QyxJQUFJLENBQUMsUUFBUSxDQUNoQixDQUFDO1FBRUYsd0JBQXdCO1FBQ3hCLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLEdBQUcsRUFBRTtZQUN6QyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDakIsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQzVCLENBQUM7SUFFRCxZQUFZO1FBQ1IsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNwRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztTQUN4QjtRQUVELElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUVELFdBQVc7UUFDUCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDL0IsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDdkQsQ0FBQztDQUNKLENBQUE7O1lBdEZrQyxpQkFBaUI7WUFDckIsYUFBYTtZQUNOLHlCQUF5QjtZQUNwQyxTQUFTO1lBQ1AsVUFBVTtZQUNsQixpQkFBaUI7O0FBN0JsQztJQURDLEtBQUssRUFBRTs7bURBQ0s7QUFLYjtJQUZDLEtBQUssRUFBRTtJQUNQLFdBQVcsQ0FBQyxzQkFBc0IsQ0FBQzs7b0RBQ3JCO0FBSWY7SUFEQyxXQUFXLENBQUMsWUFBWSxDQUFDOztxREFDVjtBQUdoQjtJQURDLFdBQVcsQ0FBQywyQkFBMkIsQ0FBQzs7Z0VBQ2Q7QUFJM0I7SUFEQyxXQUFXLENBQUMsZUFBZSxDQUFDOztxREFDYjtBQW5CUCxxQkFBcUI7SUFMakMsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLGVBQWU7UUFDekIsMERBQTZDOztLQUVoRCxDQUFDOzZDQTRCaUMsaUJBQWlCO1FBQ3JCLGFBQWE7UUFDTix5QkFBeUI7UUFDcEMsU0FBUztRQUNQLFVBQVU7UUFDbEIsaUJBQWlCO0dBaEN6QixxQkFBcUIsQ0FpSGpDO1NBakhZLHFCQUFxQjtBQXNIbEMsSUFBYSw0QkFBNEIsR0FBekMsTUFBYSw0QkFBNEI7SUFJckMsWUFBb0IseUJBQW9EO1FBQXBELDhCQUF5QixHQUF6Qix5QkFBeUIsQ0FBMkI7SUFDeEUsQ0FBQztJQUdELE9BQU87UUFDSCxJQUFJLENBQUMseUJBQXlCLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUNoRSxDQUFDO0NBQ0osQ0FBQTs7WUFQa0QseUJBQXlCOztBQUZ4RTtJQURDLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQzs7d0RBQ2Q7QUFNZjtJQURDLFlBQVksQ0FBQyxPQUFPLENBQUM7Ozs7MkRBR3JCO0FBVlEsNEJBQTRCO0lBSHhDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSx1QkFBdUI7S0FDcEMsQ0FBQzs2Q0FLaUQseUJBQXlCO0dBSi9ELDRCQUE0QixDQVd4QztTQVhZLDRCQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgICBDaGFuZ2VEZXRlY3RvclJlZixcclxuICAgIENvbXBvbmVudCxcclxuICAgIERpcmVjdGl2ZSxcclxuICAgIEVsZW1lbnRSZWYsXHJcbiAgICBIb3N0QmluZGluZyxcclxuICAgIEhvc3RMaXN0ZW5lcixcclxuICAgIElucHV0LFxyXG4gICAgT25EZXN0cm95LFxyXG4gICAgT25Jbml0LFxyXG4gICAgUmVuZGVyZXIyXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1lZGlhT2JzZXJ2ZXIgfSBmcm9tICdAYW5ndWxhci9mbGV4LWxheW91dCc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBFZ3JldFNpZGViYXJIZWxwZXJTZXJ2aWNlIH0gZnJvbSAnLi9lZ3JldC1zaWRlYmFyLWhlbHBlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTWF0Y2hNZWRpYVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9tYXRjaC1tZWRpYS5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdlZ3JldC1zaWRlYmFyJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9lZ3JldC1zaWRlYmFyLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2VncmV0LXNpZGViYXIuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRWdyZXRTaWRlYmFyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG4gICAgLy8gTmFtZVxyXG4gICAgQElucHV0KClcclxuICAgIG5hbWU6IHN0cmluZztcclxuXHJcbiAgICAvLyByaWdodFxyXG4gICAgQElucHV0KClcclxuICAgIEBIb3N0QmluZGluZyhcImNsYXNzLnBvc2l0aW9uLXJpZ2h0XCIpXHJcbiAgICByaWdodDogYm9vbGVhbjtcclxuXHJcbiAgICAvLyBPcGVuXHJcbiAgICBASG9zdEJpbmRpbmcoXCJjbGFzcy5vcGVuXCIpXHJcbiAgICBvcGVuZWQ6IGJvb2xlYW47XHJcblxyXG4gICAgQEhvc3RCaW5kaW5nKFwiY2xhc3Muc2lkZWJhci1sb2NrZWQtb3BlblwiKVxyXG4gICAgc2lkZWJhckxvY2tlZE9wZW46IGJvb2xlYW47XHJcblxyXG4gICAgLy9tb2RlXHJcbiAgICBASG9zdEJpbmRpbmcoXCJjbGFzcy5pcy1vdmVyXCIpXHJcbiAgICBpc092ZXI6IGJvb2xlYW47XHJcblxyXG4gICAgcHJpdmF0ZSBiYWNrZHJvcDogSFRNTEVsZW1lbnQgfCBudWxsID0gbnVsbDtcclxuXHJcbiAgICBwcml2YXRlIGxvY2tlZEJyZWFrcG9pbnQgPSBcImd0LXNtXCI7XHJcbiAgICBwcml2YXRlIHVuc3Vic2NyaWJlQWxsOiBTdWJqZWN0PGFueT47XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBtYXRjaE1lZGlhU2VydmljZTogTWF0Y2hNZWRpYVNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBtZWRpYU9ic2VydmVyOiBNZWRpYU9ic2VydmVyLFxyXG4gICAgICAgIHByaXZhdGUgc2lkZWJhckhlbHBlclNlcnZpY2U6IEVncmV0U2lkZWJhckhlbHBlclNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBfcmVuZGVyZXI6IFJlbmRlcmVyMixcclxuICAgICAgICBwcml2YXRlIF9lbGVtZW50UmVmOiBFbGVtZW50UmVmLFxyXG4gICAgICAgIHByaXZhdGUgY2RyOiBDaGFuZ2VEZXRlY3RvclJlZlxyXG4gICAgKSB7XHJcbiAgICAgICAgdGhpcy51bnN1YnNjcmliZUFsbCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5zaWRlYmFySGVscGVyU2VydmljZS5zZXRTaWRlYmFyKHRoaXMubmFtZSwgdGhpcyk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLm1lZGlhT2JzZXJ2ZXIuaXNBY3RpdmUodGhpcy5sb2NrZWRCcmVha3BvaW50KSkge1xyXG4gICAgICAgICAgICB0aGlzLnNpZGViYXJMb2NrZWRPcGVuID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5vcGVuZWQgPSB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2lkZWJhckxvY2tlZE9wZW4gPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5vcGVuZWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMubWF0Y2hNZWRpYVNlcnZpY2Uub25NZWRpYUNoYW5nZVxyXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy51bnN1YnNjcmliZUFsbCkpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coXCJtZWR1YSBzdWJcIik7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5tZWRpYU9ic2VydmVyLmlzQWN0aXZlKHRoaXMubG9ja2VkQnJlYWtwb2ludCkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNpZGViYXJMb2NrZWRPcGVuID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9wZW5lZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2lkZWJhckxvY2tlZE9wZW4gPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9wZW5lZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBvcGVuKCkge1xyXG4gICAgICAgIHRoaXMub3BlbmVkID0gdHJ1ZTtcclxuICAgICAgICBpZiAoIXRoaXMuc2lkZWJhckxvY2tlZE9wZW4gJiYgIXRoaXMuYmFja2Ryb3ApIHtcclxuICAgICAgICAgICAgdGhpcy5zaG93QmFja2Ryb3AoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY2xvc2UoKSB7XHJcbiAgICAgICAgdGhpcy5vcGVuZWQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmhpZGVCYWNrZHJvcCgpO1xyXG4gICAgfVxyXG5cclxuICAgIHRvZ2dsZSgpIHtcclxuICAgICAgICBpZiAodGhpcy5vcGVuZWQpIHtcclxuICAgICAgICAgICAgdGhpcy5jbG9zZSgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMub3BlbigpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzaG93QmFja2Ryb3AoKSB7XHJcbiAgICAgICAgdGhpcy5iYWNrZHJvcCA9IHRoaXMuX3JlbmRlcmVyLmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7XHJcbiAgICAgICAgdGhpcy5iYWNrZHJvcC5jbGFzc0xpc3QuYWRkKFwiZWdyZXQtc2lkZWJhci1vdmVybGF5XCIpO1xyXG5cclxuICAgICAgICB0aGlzLl9yZW5kZXJlci5hcHBlbmRDaGlsZChcclxuICAgICAgICAgICAgdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LnBhcmVudEVsZW1lbnQsXHJcbiAgICAgICAgICAgIHRoaXMuYmFja2Ryb3BcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICAvLyBDbG9zZSBzaWRlYmFyIG9uY2xpY2tcclxuICAgICAgICB0aGlzLmJhY2tkcm9wLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuY2xvc2UoKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5jZHIubWFya0ZvckNoZWNrKCk7XHJcbiAgICB9XHJcblxyXG4gICAgaGlkZUJhY2tkcm9wKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmJhY2tkcm9wKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYmFja2Ryb3AucGFyZW50Tm9kZS5yZW1vdmVDaGlsZCh0aGlzLmJhY2tkcm9wKTtcclxuICAgICAgICAgICAgdGhpcy5iYWNrZHJvcCA9IG51bGw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmNkci5tYXJrRm9yQ2hlY2soKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnVuc3Vic2NyaWJlQWxsLm5leHQoKTtcclxuICAgICAgICB0aGlzLnVuc3Vic2NyaWJlQWxsLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgdGhpcy5zaWRlYmFySGVscGVyU2VydmljZS5yZW1vdmVTaWRlYmFyKHRoaXMubmFtZSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6IFwiW2VncmV0U2lkZWJhclRvZ2dsZXJdXCJcclxufSlcclxuZXhwb3J0IGNsYXNzIEVncmV0U2lkZWJhclRvZ2dsZXJEaXJlY3RpdmUge1xyXG4gICAgQElucHV0KFwiZWdyZXRTaWRlYmFyVG9nZ2xlclwiKVxyXG4gICAgcHVibGljIGlkOiBhbnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBlZ3JldFNpZGViYXJIZWxwZXJTZXJ2aWNlOiBFZ3JldFNpZGViYXJIZWxwZXJTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcihcImNsaWNrXCIpXHJcbiAgICBvbkNsaWNrKCkge1xyXG4gICAgICAgIHRoaXMuZWdyZXRTaWRlYmFySGVscGVyU2VydmljZS5nZXRTaWRlYmFyKHRoaXMuaWQpLnRvZ2dsZSgpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==