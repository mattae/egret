import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
// import PerfectScrollbar from 'perfect-scrollbar';
import { NavigationService } from '../../services/navigation.service';
let SidebarTopComponent = class SidebarTopComponent {
    constructor(navService) {
        this.navService = navService;
    }
    ngOnInit() {
        this.menuItemsSub = this.navService.menuItems$.subscribe(menuItem => {
            this.menuItems = menuItem.filter(item => item.type !== 'icon' && item.type !== 'separator');
        });
    }
    ngAfterViewInit() {
        // setTimeout(() => {
        //   this.sidebarPS = new PerfectScrollbar('#sidebar-top-scroll-area', {
        //     suppressScrollX: true
        //   })
        // })
    }
    ngOnDestroy() {
        // if(this.sidebarPS) {
        //   this.sidebarPS.destroy();
        // }
        if (this.menuItemsSub) {
            this.menuItemsSub.unsubscribe();
        }
    }
};
SidebarTopComponent.ctorParameters = () => [
    { type: NavigationService }
];
SidebarTopComponent = tslib_1.__decorate([
    Component({
        selector: 'egret-sidebar-top',
        template: "<div class=\"sidebar-panel\">\r\n    <div id=\"sidebar-top-scroll-area\" [perfectScrollbar] class=\"navigation-hold\" fxLayout=\"column\">\r\n        <egret-sidenav [items]=\"menuItems\"></egret-sidenav>\r\n    </div>\r\n</div>\r\n"
    }),
    tslib_1.__metadata("design:paramtypes", [NavigationService])
], SidebarTopComponent);
export { SidebarTopComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lkZWJhci10b3AuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9zaWRlYmFyLXRvcC9zaWRlYmFyLXRvcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBaUIsU0FBUyxFQUFxQixNQUFNLGVBQWUsQ0FBQztBQUM1RSxvREFBb0Q7QUFDcEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFPdEUsSUFBYSxtQkFBbUIsR0FBaEMsTUFBYSxtQkFBbUI7SUFLL0IsWUFDUyxVQUE2QjtRQUE3QixlQUFVLEdBQVYsVUFBVSxDQUFtQjtJQUV0QyxDQUFDO0lBRUQsUUFBUTtRQUNQLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ25FLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssTUFBTSxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssV0FBVyxDQUFDLENBQUM7UUFDN0YsQ0FBQyxDQUFDLENBQUM7SUFDSixDQUFDO0lBRUQsZUFBZTtRQUNkLHFCQUFxQjtRQUNyQix3RUFBd0U7UUFDeEUsNEJBQTRCO1FBQzVCLE9BQU87UUFDUCxLQUFLO0lBQ04sQ0FBQztJQUVELFdBQVc7UUFDVix1QkFBdUI7UUFDdkIsOEJBQThCO1FBQzlCLElBQUk7UUFDSixJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDdEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQTtTQUMvQjtJQUNGLENBQUM7Q0FDRCxDQUFBOztZQTFCcUIsaUJBQWlCOztBQU4xQixtQkFBbUI7SUFKL0IsU0FBUyxDQUFDO1FBQ1YsUUFBUSxFQUFFLG1CQUFtQjtRQUM3QixtUEFBMkM7S0FDM0MsQ0FBQzs2Q0FPb0IsaUJBQWlCO0dBTjFCLG1CQUFtQixDQWdDL0I7U0FoQ1ksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQWZ0ZXJWaWV3SW5pdCwgQ29tcG9uZW50LCBPbkRlc3Ryb3ksIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG4vLyBpbXBvcnQgUGVyZmVjdFNjcm9sbGJhciBmcm9tICdwZXJmZWN0LXNjcm9sbGJhcic7XHJcbmltcG9ydCB7IE5hdmlnYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvbmF2aWdhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuXHRzZWxlY3RvcjogJ2VncmV0LXNpZGViYXItdG9wJyxcclxuXHR0ZW1wbGF0ZVVybDogJy4vc2lkZWJhci10b3AuY29tcG9uZW50Lmh0bWwnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTaWRlYmFyVG9wQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3ksIEFmdGVyVmlld0luaXQge1xyXG5cdC8vIHByaXZhdGUgc2lkZWJhclBTOiBQZXJmZWN0U2Nyb2xsYmFyO1xyXG5cdHB1YmxpYyBtZW51SXRlbXM6IGFueVtdO1xyXG5cdHByaXZhdGUgbWVudUl0ZW1zU3ViOiBTdWJzY3JpcHRpb247XHJcblxyXG5cdGNvbnN0cnVjdG9yKFxyXG5cdFx0cHJpdmF0ZSBuYXZTZXJ2aWNlOiBOYXZpZ2F0aW9uU2VydmljZVxyXG5cdCkge1xyXG5cdH1cclxuXHJcblx0bmdPbkluaXQoKSB7XHJcblx0XHR0aGlzLm1lbnVJdGVtc1N1YiA9IHRoaXMubmF2U2VydmljZS5tZW51SXRlbXMkLnN1YnNjcmliZShtZW51SXRlbSA9PiB7XHJcblx0XHRcdHRoaXMubWVudUl0ZW1zID0gbWVudUl0ZW0uZmlsdGVyKGl0ZW0gPT4gaXRlbS50eXBlICE9PSAnaWNvbicgJiYgaXRlbS50eXBlICE9PSAnc2VwYXJhdG9yJyk7XHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG5cdG5nQWZ0ZXJWaWV3SW5pdCgpIHtcclxuXHRcdC8vIHNldFRpbWVvdXQoKCkgPT4ge1xyXG5cdFx0Ly8gICB0aGlzLnNpZGViYXJQUyA9IG5ldyBQZXJmZWN0U2Nyb2xsYmFyKCcjc2lkZWJhci10b3Atc2Nyb2xsLWFyZWEnLCB7XHJcblx0XHQvLyAgICAgc3VwcHJlc3NTY3JvbGxYOiB0cnVlXHJcblx0XHQvLyAgIH0pXHJcblx0XHQvLyB9KVxyXG5cdH1cclxuXHJcblx0bmdPbkRlc3Ryb3koKSB7XHJcblx0XHQvLyBpZih0aGlzLnNpZGViYXJQUykge1xyXG5cdFx0Ly8gICB0aGlzLnNpZGViYXJQUy5kZXN0cm95KCk7XHJcblx0XHQvLyB9XHJcblx0XHRpZiAodGhpcy5tZW51SXRlbXNTdWIpIHtcclxuXHRcdFx0dGhpcy5tZW51SXRlbXNTdWIudW5zdWJzY3JpYmUoKVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iXX0=