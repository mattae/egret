import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
let AuthLayoutComponent = class AuthLayoutComponent {
    constructor() {
    }
    ngOnInit() {
    }
};
AuthLayoutComponent = tslib_1.__decorate([
    Component({
        selector: 'egret-auth-layout',
        template: "<router-outlet></router-outlet>"
    }),
    tslib_1.__metadata("design:paramtypes", [])
], AuthLayoutComponent);
export { AuthLayoutComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1sYXlvdXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9sYXlvdXRzL2F1dGgtbGF5b3V0L2F1dGgtbGF5b3V0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQztBQU1sRCxJQUFhLG1CQUFtQixHQUFoQyxNQUFhLG1CQUFtQjtJQUU1QjtJQUNBLENBQUM7SUFFRCxRQUFRO0lBQ1IsQ0FBQztDQUVKLENBQUE7QUFSWSxtQkFBbUI7SUFKL0IsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLG1CQUFtQjtRQUM3QiwyQ0FBMkM7S0FDOUMsQ0FBQzs7R0FDVyxtQkFBbUIsQ0FRL0I7U0FSWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2VncmV0LWF1dGgtbGF5b3V0JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9hdXRoLWxheW91dC5jb21wb25lbnQuaHRtbCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEF1dGhMYXlvdXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=