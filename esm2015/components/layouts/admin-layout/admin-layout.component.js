import * as tslib_1 from "tslib";
import { Component, HostListener } from '@angular/core';
import { NavigationEnd, ResolveEnd, ResolveStart, RouteConfigLoadEnd, RouteConfigLoadStart, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { filter } from 'rxjs/operators';
import { LayoutService } from '../../../services/layout.service';
import { ThemeService } from '../../../services/theme.service';
let AdminLayoutComponent = class AdminLayoutComponent {
    constructor(router, translate, themeService, layout) {
        this.router = router;
        this.translate = translate;
        this.themeService = themeService;
        this.layout = layout;
        this.isModuleLoading = false;
        this.scrollConfig = {};
        this.layoutConf = {};
        // Close sidenav after route change in mobile
        this.routerEventSub = router.events.pipe(filter(event => event instanceof NavigationEnd))
            .subscribe((routeChange) => {
            this.layout.adjustLayout({ route: routeChange.url });
        });
        // Translator init
        const browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    }
    ngOnInit() {
        // this.layoutConf = this.layout.layoutConf;
        this.layoutConfSub = this.layout.layoutConf$.subscribe((layoutConf) => {
            this.layoutConf = layoutConf;
        });
        // FOR MODULE LOADER FLAG
        this.moduleLoaderSub = this.router.events.subscribe(event => {
            if (event instanceof RouteConfigLoadStart || event instanceof ResolveStart) {
                this.isModuleLoading = true;
            }
            if (event instanceof RouteConfigLoadEnd || event instanceof ResolveEnd) {
                this.isModuleLoading = false;
            }
        });
    }
    onResize(event) {
        this.layout.adjustLayout(event);
    }
    ngAfterViewInit() {
    }
    scrollToTop(selector) {
        if (document) {
            let element = document.querySelector(selector);
            element.scrollTop = 0;
        }
    }
    ngOnDestroy() {
        if (this.moduleLoaderSub) {
            this.moduleLoaderSub.unsubscribe();
        }
        if (this.layoutConfSub) {
            this.layoutConfSub.unsubscribe();
        }
        if (this.routerEventSub) {
            this.routerEventSub.unsubscribe();
        }
    }
    closeSidebar() {
        this.layout.publishLayoutChange({
            sidebarStyle: 'closed'
        });
    }
    sidebarMouseenter(e) {
        // console.log(this.layoutConf);
        if (this.layoutConf.sidebarStyle === 'compact') {
            this.layout.publishLayoutChange({ sidebarStyle: 'full' }, { transitionClass: true });
        }
    }
    sidebarMouseleave(e) {
        // console.log(this.layoutConf);
        if (this.layoutConf.sidebarStyle === 'full' &&
            this.layoutConf.sidebarCompactToggle) {
            this.layout.publishLayoutChange({ sidebarStyle: 'compact' }, { transitionClass: true });
        }
    }
};
AdminLayoutComponent.ctorParameters = () => [
    { type: Router },
    { type: TranslateService },
    { type: ThemeService },
    { type: LayoutService }
];
tslib_1.__decorate([
    HostListener('window:resize', ['$event']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], AdminLayoutComponent.prototype, "onResize", null);
AdminLayoutComponent = tslib_1.__decorate([
    Component({
        selector: 'egret-admin-layout',
        template: "<div class=\"app-admin-wrap\" [dir]='layoutConf?.dir'>\r\n    <!-- Header for top navigation layout -->\r\n    <!-- ONLY REQUIRED FOR **TOP** NAVIGATION LAYOUT -->\r\n    <egret-header-top\r\n            *ngIf=\"layoutConf.navigationPos === 'top'\"\r\n            [notificPanel]=\"notificationPanel\">\r\n    </egret-header-top>\r\n    <!-- Main Container -->\r\n    <mat-sidenav-container\r\n            [dir]='layoutConf.dir'\r\n            class=\"app-admin-container app-side-nav-container mat-drawer-transition sidebar-{{layoutConf?.sidebarColor}} topbar-{{layoutConf?.topbarColor}}\"\r\n            [ngClass]=\"{\r\n    'navigation-top': layoutConf.navigationPos === 'top',\r\n    'sidebar-full': layoutConf.sidebarStyle === 'full',\r\n    'sidebar-compact': layoutConf.sidebarStyle === 'compact' && layoutConf.navigationPos === 'side',\r\n    'compact-toggle-active': layoutConf.sidebarCompactToggle,\r\n    'sidebar-compact-big': layoutConf.sidebarStyle === 'compact-big' && layoutConf.navigationPos === 'side',\r\n    'sidebar-opened': layoutConf.sidebarStyle !== 'closed' && layoutConf.navigationPos === 'side',\r\n    'sidebar-closed': layoutConf.sidebarStyle === 'closed',\r\n    'fixed-topbar': layoutConf.topbarFixed && layoutConf.navigationPos === 'side'\r\n  }\">\r\n        <!-- SIDEBAR -->\r\n        <!-- ONLY REQUIRED FOR **SIDE** NAVIGATION LAYOUT -->\r\n        <egret-sidebar-side\r\n                *ngIf=\"layoutConf.navigationPos === 'side'\"\r\n                (mouseenter)=\"sidebarMouseenter($event)\"\r\n                (mouseleave)=\"sidebarMouseleave($event)\">\r\n        </egret-sidebar-side>\r\n\r\n        <!-- Top navigation layout (navigation for mobile screen) -->\r\n        <!-- ONLY REQUIRED FOR **TOP** NAVIGATION MOBILE LAYOUT -->\r\n        <egret-sidebar-top *ngIf=\"layoutConf.navigationPos === 'top' && layoutConf.isMobile\"></egret-sidebar-top>\r\n\r\n        <!-- App content -->\r\n        <div class=\"main-content-wrap\" id=\"main-content-wrap\" [perfectScrollbar]=\"\"\r\n             [disabled]=\"layoutConf.topbarFixed || !layoutConf.perfectScrollbar\" style=\"display: block;\">\r\n            <!-- Header for side navigation layout -->\r\n            <!-- ONLY REQUIRED FOR **SIDE** NAVIGATION LAYOUT -->\r\n            <egret-header-side\r\n                    *ngIf=\"layoutConf.navigationPos === 'side'\"\r\n                    [notificPanel]=\"notificationPanel\">\r\n            </egret-header-side>\r\n\r\n            <div class=\"rightside-content-hold\" id=\"rightside-content-hold\" [perfectScrollbar]=\"scrollConfig\"\r\n                 [disabled]=\"!layoutConf.topbarFixed || !layoutConf.perfectScrollbar\">\r\n                <!-- View Loader -->\r\n                <div class=\"view-loader\" *ngIf=\"isModuleLoading\">\r\n                    <div class=\"spinner\">\r\n                        <div class=\"double-bounce1 mat-bg-accent\"></div>\r\n                        <div class=\"double-bounce2 mat-bg-primary\"></div>\r\n                    </div>\r\n                </div>\r\n                <!-- Breadcrumb -->\r\n                <egret-breadcrumb></egret-breadcrumb>\r\n                <!-- View outlet -->\r\n                <router-outlet></router-outlet>\r\n            </div>\r\n        </div>\r\n        <!-- View overlay for mobile navigation -->\r\n        <div class=\"sidebar-backdrop\"\r\n             [ngClass]=\"{'visible': layoutConf.sidebarStyle !== 'closed' && layoutConf.isMobile}\"\r\n             (click)=\"closeSidebar()\">\r\n        </div>\r\n\r\n        <!-- Notificaation bar -->\r\n        <mat-sidenav #notificationPanel mode=\"over\" class=\"\" position=\"end\">\r\n            <div class=\"nofication-panel\" fxLayout=\"column\">\r\n                <egret-notifications [notificPanel]=\"notificationPanel\"></egret-notifications>\r\n            </div>\r\n        </mat-sidenav>\r\n    </mat-sidenav-container>\r\n</div>\r\n\r\n\r\n<!-- Only for demo purpose -->\r\n<!-- Remove this from your production version -->\r\n"
    }),
    tslib_1.__metadata("design:paramtypes", [Router,
        TranslateService,
        ThemeService,
        LayoutService])
], AdminLayoutComponent);
export { AdminLayoutComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRtaW4tbGF5b3V0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvbGF5b3V0cy9hZG1pbi1sYXlvdXQvYWRtaW4tbGF5b3V0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFpQixTQUFTLEVBQUUsWUFBWSxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFDTixhQUFhLEVBQ2IsVUFBVSxFQUNWLFlBQVksRUFDWixrQkFBa0IsRUFDbEIsb0JBQW9CLEVBQ3BCLE1BQU0sRUFDTixNQUFNLGlCQUFpQixDQUFDO0FBQ3pCLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBRXZELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN4QyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDakUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBTS9ELElBQWEsb0JBQW9CLEdBQWpDLE1BQWEsb0JBQW9CO0lBU2hDLFlBQ1MsTUFBYyxFQUNmLFNBQTJCLEVBQzNCLFlBQTBCLEVBQ3pCLE1BQXFCO1FBSHJCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZixjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQUMzQixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUN6QixXQUFNLEdBQU4sTUFBTSxDQUFlO1FBWnZCLG9CQUFlLEdBQVksS0FBSyxDQUFDO1FBS2pDLGlCQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLGVBQVUsR0FBUSxFQUFFLENBQUM7UUFRM0IsNkNBQTZDO1FBQzdDLElBQUksQ0FBQyxjQUFjLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsS0FBSyxZQUFZLGFBQWEsQ0FBQyxDQUFDO2FBQ3ZGLFNBQVMsQ0FBQyxDQUFDLFdBQTBCLEVBQUUsRUFBRTtZQUN6QyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxFQUFDLEtBQUssRUFBRSxXQUFXLENBQUMsR0FBRyxFQUFDLENBQUMsQ0FBQztRQUNwRCxDQUFDLENBQUMsQ0FBQztRQUVKLGtCQUFrQjtRQUNsQixNQUFNLFdBQVcsR0FBVyxTQUFTLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdkQsU0FBUyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2hFLENBQUM7SUFFRCxRQUFRO1FBQ1AsNENBQTRDO1FBQzVDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUMsVUFBVSxFQUFFLEVBQUU7WUFDckUsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDOUIsQ0FBQyxDQUFDLENBQUM7UUFDSCx5QkFBeUI7UUFDekIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDM0QsSUFBSSxLQUFLLFlBQVksb0JBQW9CLElBQUksS0FBSyxZQUFZLFlBQVksRUFBRTtnQkFDM0UsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7YUFDNUI7WUFDRCxJQUFJLEtBQUssWUFBWSxrQkFBa0IsSUFBSSxLQUFLLFlBQVksVUFBVSxFQUFFO2dCQUN2RSxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQzthQUM3QjtRQUNGLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUdELFFBQVEsQ0FBQyxLQUFLO1FBQ2IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVELGVBQWU7SUFFZixDQUFDO0lBRUQsV0FBVyxDQUFDLFFBQWdCO1FBQzNCLElBQUksUUFBUSxFQUFFO1lBQ2IsSUFBSSxPQUFPLEdBQWdCLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDNUQsT0FBTyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7U0FDdEI7SUFDRixDQUFDO0lBRUQsV0FBVztRQUNWLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN6QixJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ25DO1FBQ0QsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDakM7UUFDRCxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDeEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNsQztJQUNGLENBQUM7SUFFRCxZQUFZO1FBQ1gsSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQztZQUMvQixZQUFZLEVBQUUsUUFBUTtTQUN0QixDQUFDLENBQUE7SUFDSCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsQ0FBQztRQUNsQixnQ0FBZ0M7UUFDaEMsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksS0FBSyxTQUFTLEVBQUU7WUFDL0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxFQUFDLFlBQVksRUFBRSxNQUFNLEVBQUMsRUFBRSxFQUFDLGVBQWUsRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFDO1NBQ2pGO0lBQ0YsQ0FBQztJQUVELGlCQUFpQixDQUFDLENBQUM7UUFDbEIsZ0NBQWdDO1FBQ2hDLElBQ0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEtBQUssTUFBTTtZQUN2QyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixFQUNuQztZQUNELElBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUMsRUFBQyxZQUFZLEVBQUUsU0FBUyxFQUFDLEVBQUUsRUFBQyxlQUFlLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztTQUNwRjtJQUNGLENBQUM7Q0FDRCxDQUFBOztZQWxGaUIsTUFBTTtZQUNKLGdCQUFnQjtZQUNiLFlBQVk7WUFDakIsYUFBYTs7QUE4QjlCO0lBREMsWUFBWSxDQUFDLGVBQWUsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDOzs7O29EQUd6QztBQTdDVyxvQkFBb0I7SUFKaEMsU0FBUyxDQUFDO1FBQ1YsUUFBUSxFQUFFLG9CQUFvQjtRQUM5QixxOEhBQTJDO0tBQzNDLENBQUM7NkNBV2dCLE1BQU07UUFDSixnQkFBZ0I7UUFDYixZQUFZO1FBQ2pCLGFBQWE7R0FibEIsb0JBQW9CLENBNEZoQztTQTVGWSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBZnRlclZpZXdJbml0LCBDb21wb25lbnQsIEhvc3RMaXN0ZW5lciwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7XHJcblx0TmF2aWdhdGlvbkVuZCxcclxuXHRSZXNvbHZlRW5kLFxyXG5cdFJlc29sdmVTdGFydCxcclxuXHRSb3V0ZUNvbmZpZ0xvYWRFbmQsXHJcblx0Um91dGVDb25maWdMb2FkU3RhcnQsXHJcblx0Um91dGVyXHJcbn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tIFwicnhqc1wiO1xyXG5pbXBvcnQgeyBmaWx0ZXIgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IExheW91dFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9sYXlvdXQuc2VydmljZSc7XHJcbmltcG9ydCB7IFRoZW1lU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NlcnZpY2VzL3RoZW1lLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcblx0c2VsZWN0b3I6ICdlZ3JldC1hZG1pbi1sYXlvdXQnLFxyXG5cdHRlbXBsYXRlVXJsOiAnLi9hZG1pbi1sYXlvdXQudGVtcGxhdGUuaHRtbCcsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBZG1pbkxheW91dENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCB7XHJcblx0cHVibGljIGlzTW9kdWxlTG9hZGluZzogQm9vbGVhbiA9IGZhbHNlO1xyXG5cdHByaXZhdGUgbW9kdWxlTG9hZGVyU3ViOiBTdWJzY3JpcHRpb247XHJcblx0cHJpdmF0ZSBsYXlvdXRDb25mU3ViOiBTdWJzY3JpcHRpb247XHJcblx0cHJpdmF0ZSByb3V0ZXJFdmVudFN1YjogU3Vic2NyaXB0aW9uO1xyXG5cclxuXHRwdWJsaWMgc2Nyb2xsQ29uZmlnID0ge307XHJcblx0cHVibGljIGxheW91dENvbmY6IGFueSA9IHt9O1xyXG5cclxuXHRjb25zdHJ1Y3RvcihcclxuXHRcdHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcblx0XHRwdWJsaWMgdHJhbnNsYXRlOiBUcmFuc2xhdGVTZXJ2aWNlLFxyXG5cdFx0cHVibGljIHRoZW1lU2VydmljZTogVGhlbWVTZXJ2aWNlLFxyXG5cdFx0cHJpdmF0ZSBsYXlvdXQ6IExheW91dFNlcnZpY2VcclxuXHQpIHtcclxuXHRcdC8vIENsb3NlIHNpZGVuYXYgYWZ0ZXIgcm91dGUgY2hhbmdlIGluIG1vYmlsZVxyXG5cdFx0dGhpcy5yb3V0ZXJFdmVudFN1YiA9IHJvdXRlci5ldmVudHMucGlwZShmaWx0ZXIoZXZlbnQgPT4gZXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uRW5kKSlcclxuXHRcdFx0LnN1YnNjcmliZSgocm91dGVDaGFuZ2U6IE5hdmlnYXRpb25FbmQpID0+IHtcclxuXHRcdFx0XHR0aGlzLmxheW91dC5hZGp1c3RMYXlvdXQoe3JvdXRlOiByb3V0ZUNoYW5nZS51cmx9KTtcclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0Ly8gVHJhbnNsYXRvciBpbml0XHJcblx0XHRjb25zdCBicm93c2VyTGFuZzogc3RyaW5nID0gdHJhbnNsYXRlLmdldEJyb3dzZXJMYW5nKCk7XHJcblx0XHR0cmFuc2xhdGUudXNlKGJyb3dzZXJMYW5nLm1hdGNoKC9lbnxmci8pID8gYnJvd3NlckxhbmcgOiAnZW4nKTtcclxuXHR9XHJcblxyXG5cdG5nT25Jbml0KCkge1xyXG5cdFx0Ly8gdGhpcy5sYXlvdXRDb25mID0gdGhpcy5sYXlvdXQubGF5b3V0Q29uZjtcclxuXHRcdHRoaXMubGF5b3V0Q29uZlN1YiA9IHRoaXMubGF5b3V0LmxheW91dENvbmYkLnN1YnNjcmliZSgobGF5b3V0Q29uZikgPT4ge1xyXG5cdFx0XHR0aGlzLmxheW91dENvbmYgPSBsYXlvdXRDb25mO1xyXG5cdFx0fSk7XHJcblx0XHQvLyBGT1IgTU9EVUxFIExPQURFUiBGTEFHXHJcblx0XHR0aGlzLm1vZHVsZUxvYWRlclN1YiA9IHRoaXMucm91dGVyLmV2ZW50cy5zdWJzY3JpYmUoZXZlbnQgPT4ge1xyXG5cdFx0XHRpZiAoZXZlbnQgaW5zdGFuY2VvZiBSb3V0ZUNvbmZpZ0xvYWRTdGFydCB8fCBldmVudCBpbnN0YW5jZW9mIFJlc29sdmVTdGFydCkge1xyXG5cdFx0XHRcdHRoaXMuaXNNb2R1bGVMb2FkaW5nID0gdHJ1ZTtcclxuXHRcdFx0fVxyXG5cdFx0XHRpZiAoZXZlbnQgaW5zdGFuY2VvZiBSb3V0ZUNvbmZpZ0xvYWRFbmQgfHwgZXZlbnQgaW5zdGFuY2VvZiBSZXNvbHZlRW5kKSB7XHJcblx0XHRcdFx0dGhpcy5pc01vZHVsZUxvYWRpbmcgPSBmYWxzZTtcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHRASG9zdExpc3RlbmVyKCd3aW5kb3c6cmVzaXplJywgWyckZXZlbnQnXSlcclxuXHRvblJlc2l6ZShldmVudCkge1xyXG5cdFx0dGhpcy5sYXlvdXQuYWRqdXN0TGF5b3V0KGV2ZW50KTtcclxuXHR9XHJcblxyXG5cdG5nQWZ0ZXJWaWV3SW5pdCgpIHtcclxuXHJcblx0fVxyXG5cclxuXHRzY3JvbGxUb1RvcChzZWxlY3Rvcjogc3RyaW5nKSB7XHJcblx0XHRpZiAoZG9jdW1lbnQpIHtcclxuXHRcdFx0bGV0IGVsZW1lbnQgPSA8SFRNTEVsZW1lbnQ+ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihzZWxlY3Rvcik7XHJcblx0XHRcdGVsZW1lbnQuc2Nyb2xsVG9wID0gMDtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdG5nT25EZXN0cm95KCkge1xyXG5cdFx0aWYgKHRoaXMubW9kdWxlTG9hZGVyU3ViKSB7XHJcblx0XHRcdHRoaXMubW9kdWxlTG9hZGVyU3ViLnVuc3Vic2NyaWJlKCk7XHJcblx0XHR9XHJcblx0XHRpZiAodGhpcy5sYXlvdXRDb25mU3ViKSB7XHJcblx0XHRcdHRoaXMubGF5b3V0Q29uZlN1Yi51bnN1YnNjcmliZSgpO1xyXG5cdFx0fVxyXG5cdFx0aWYgKHRoaXMucm91dGVyRXZlbnRTdWIpIHtcclxuXHRcdFx0dGhpcy5yb3V0ZXJFdmVudFN1Yi51bnN1YnNjcmliZSgpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0Y2xvc2VTaWRlYmFyKCkge1xyXG5cdFx0dGhpcy5sYXlvdXQucHVibGlzaExheW91dENoYW5nZSh7XHJcblx0XHRcdHNpZGViYXJTdHlsZTogJ2Nsb3NlZCdcclxuXHRcdH0pXHJcblx0fVxyXG5cclxuXHRzaWRlYmFyTW91c2VlbnRlcihlKSB7XHJcblx0XHQvLyBjb25zb2xlLmxvZyh0aGlzLmxheW91dENvbmYpO1xyXG5cdFx0aWYgKHRoaXMubGF5b3V0Q29uZi5zaWRlYmFyU3R5bGUgPT09ICdjb21wYWN0Jykge1xyXG5cdFx0XHR0aGlzLmxheW91dC5wdWJsaXNoTGF5b3V0Q2hhbmdlKHtzaWRlYmFyU3R5bGU6ICdmdWxsJ30sIHt0cmFuc2l0aW9uQ2xhc3M6IHRydWV9KTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHNpZGViYXJNb3VzZWxlYXZlKGUpIHtcclxuXHRcdC8vIGNvbnNvbGUubG9nKHRoaXMubGF5b3V0Q29uZik7XHJcblx0XHRpZiAoXHJcblx0XHRcdHRoaXMubGF5b3V0Q29uZi5zaWRlYmFyU3R5bGUgPT09ICdmdWxsJyAmJlxyXG5cdFx0XHR0aGlzLmxheW91dENvbmYuc2lkZWJhckNvbXBhY3RUb2dnbGVcclxuXHRcdCkge1xyXG5cdFx0XHR0aGlzLmxheW91dC5wdWJsaXNoTGF5b3V0Q2hhbmdlKHtzaWRlYmFyU3R5bGU6ICdjb21wYWN0J30sIHt0cmFuc2l0aW9uQ2xhc3M6IHRydWV9KTtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuIl19