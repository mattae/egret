import * as tslib_1 from "tslib";
import { Component, Input, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LayoutService } from '../../services/layout.service';
import { NavigationService } from '../../services/navigation.service';
import { ThemeService } from '../../services/theme.service';
import { AccountService, IMenuItem, LoginService } from '@lamis/web-core';
let HeaderTopComponent = class HeaderTopComponent {
    constructor(layout, navService, themeService, translate, renderer, loginService, accountService) {
        this.layout = layout;
        this.navService = navService;
        this.themeService = themeService;
        this.translate = translate;
        this.renderer = renderer;
        this.loginService = loginService;
        this.accountService = accountService;
        this.egretThemes = [];
        this.openCustomizer = false;
        this.currentLang = 'en';
        this.availableLangs = [{
                name: 'English',
                code: 'en',
            }, {
                name: 'Spanish',
                code: 'es',
            }];
    }
    ngOnInit() {
        this.layoutConf = this.layout.layoutConf;
        this.egretThemes = this.themeService.egretThemes;
        this.menuItemSub = this.navService.menuItems$
            .subscribe(res => {
            res = res.filter(item => item.type !== 'icon' && item.type !== 'separator');
            let limit = 4;
            let mainItems = res.slice(0, limit);
            if (res.length <= limit) {
                return this.menuItems = mainItems;
            }
            let subItems = res.slice(limit, res.length - 1);
            mainItems.push({
                name: 'More',
                type: 'dropDown',
                tooltip: 'More',
                icon: 'more_horiz',
                sub: subItems
            });
            this.menuItems = mainItems;
        });
    }
    ngOnDestroy() {
        this.menuItemSub.unsubscribe();
    }
    setLang() {
        this.translate.use(this.currentLang);
    }
    changeTheme(theme) {
        this.layout.publishLayoutChange({ matTheme: theme.name });
    }
    toggleNotific() {
        this.notificPanel.toggle();
    }
    toggleSidenav() {
        if (this.layoutConf.sidebarStyle === 'closed') {
            return this.layout.publishLayoutChange({
                sidebarStyle: 'full'
            });
        }
        this.layout.publishLayoutChange({
            sidebarStyle: 'closed'
        });
    }
    logout() {
        this.loginService.logout();
    }
};
HeaderTopComponent.ctorParameters = () => [
    { type: LayoutService },
    { type: NavigationService },
    { type: ThemeService },
    { type: TranslateService },
    { type: Renderer2 },
    { type: LoginService },
    { type: AccountService }
];
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], HeaderTopComponent.prototype, "notificPanel", void 0);
HeaderTopComponent = tslib_1.__decorate([
    Component({
        selector: 'egret-header-top',
        template: "<div class=\"header-topnav mat-elevation-z2\">\r\n    <div class=\"container\">\r\n        <div class=\"topnav\">\r\n            <!-- App Logo -->\r\n            <div class=\"topbar-branding\">\r\n                <img src=\"assets/images/logo.png\" alt=\"\" class=\"app-logo\">\r\n            </div>\r\n\r\n            <ul class=\"menu\" *ngIf=\"!layoutConf.isMobile\">\r\n                <li *ngFor=\"let item of menuItems; let i = index;\">\r\n                    <ng-container *jhiHasAnyAuthority=\"item.authorities\">\r\n                        <div *ngIf=\"item.type !== 'separator'\" routerLinkActive=\"open\">\r\n                            <a matRipple routerLink=\"/{{item.state}}\" *ngIf=\"item.type === 'link'\">\r\n                                <mat-icon>{{item.icon}}</mat-icon>\r\n                                {{item.name | translate}}\r\n                            </a>\r\n                            <div *ngIf=\"item.type === 'dropDown'\">\r\n                                <label matRipple for=\"drop-{{i}}\" class=\"toggle\">\r\n                                    <mat-icon>{{item.icon}}</mat-icon>\r\n                                    {{item.name | translate}}</label>\r\n                                <a matRipple>\r\n                                    <mat-icon>{{item.icon}}</mat-icon>\r\n                                    {{item.name | translate}}</a>\r\n                                <input type=\"checkbox\" id=\"drop-{{i}}\"/>\r\n                                <ul>\r\n                                    <li *ngFor=\"let itemLvL2 of item.subs; let j = index;\" routerLinkActive=\"open\">\r\n                                        <a matRipple\r\n                                           routerLink=\"{{item.state ? '/'+item.state : ''}}/{{itemLvL2.state}}\"\r\n                                           *ngIf=\"itemLvL2.type !== 'dropDown'\">\r\n                                            <mat-icon *ngIf=\"itemLvL2.icon\">{{itemLvL2.icon}}</mat-icon>\r\n                                            {{itemLvL2.name | translate}}\r\n                                        </a>\r\n\r\n                                        <div *ngIf=\"itemLvL2.type === 'dropDown'\">\r\n                                            <label matRipple for=\"drop-{{i}}{{j}}\"\r\n                                                   class=\"toggle\">{{itemLvL2.name | translate}}</label>\r\n                                            <a matRipple>\r\n                                                <mat-icon *ngIf=\"itemLvL2.icon\">{{itemLvL2.icon}}</mat-icon>\r\n                                                {{itemLvL2.name | translate}}</a>\r\n                                            <input type=\"checkbox\" id=\"drop-{{i}}{{j}}\"/>\r\n                                            <!-- Level 3 -->\r\n                                            <ul>\r\n                                                <li *ngFor=\"let itemLvL3 of itemLvL2.subs\" routerLinkActive=\"open\">\r\n                                                    <a matRipple\r\n                                                       routerLink=\"{{item.state ? '/'+item.state : ''}}{{itemLvL2.state ? '/'+itemLvL2.state : ''}}/{{itemLvL3.state}}\">\r\n                                                        <mat-icon *ngIf=\"itemLvL3.icon\">{{itemLvL3.icon}}</mat-icon>\r\n                                                        {{itemLvL3.name | translate}}\r\n                                                    </a>\r\n                                                </li>\r\n                                            </ul>\r\n                                        </div>\r\n                                    </li>\r\n                                </ul>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </ng-container>\r\n                </li>\r\n            </ul>\r\n            <egret-customizer [isCustomizerOpen]=\"openCustomizer\"\r\n                              (customizerClosed)=\"openCustomizer = false\"></egret-customizer>\r\n            <span fxFlex></span>\r\n            <!-- End Navigation -->\r\n\r\n            <!-- Language Switcher -->\r\n            <mat-select\r\n                    *ngIf=\"!layoutConf.isMobile\"\r\n                    placeholder=\"\"\r\n                    id=\"langToggle\"\r\n                    [style.width]=\"'auto'\"\r\n                    name=\"currentLang\"\r\n                    [(ngModel)]=\"currentLang\"\r\n                    (selectionChange)=\"setLang()\"\r\n                    class=\"topbar-button-right\">\r\n                <mat-option\r\n                        *ngFor=\"let lang of availableLangs\"\r\n                        [value]=\"lang.code\" ngDefaultControl>{{ lang.name }}</mat-option>\r\n            </mat-select>\r\n            <!-- Theme Switcher -->\r\n            <button\r\n                    mat-icon-button\r\n                    id=\"schemeToggle\"\r\n                    [style.overflow]=\"'visible'\"\r\n                    matTooltip=\"Color Schemes\"\r\n                    [matMenuTriggerFor]=\"themeMenu\"\r\n                    class=\"topbar-button-right\">\r\n                <mat-icon>format_color_fill</mat-icon>\r\n            </button>\r\n            <mat-menu #themeMenu=\"matMenu\">\r\n                <mat-grid-list\r\n                        class=\"theme-list\"\r\n                        cols=\"2\"\r\n                        rowHeight=\"48px\">\r\n                    <mat-grid-tile\r\n                            *ngFor=\"let theme of egretThemes\"\r\n                            (click)=\"changeTheme(theme)\">\r\n                        <div mat-menu-item [title]=\"theme.name\">\r\n                            <div [style.background]=\"theme.baseColor\" class=\"egret-swatch\"></div>\r\n                            <mat-icon class=\"active-icon\" *ngIf=\"theme.isActive\">check</mat-icon>\r\n                        </div>\r\n                    </mat-grid-tile>\r\n                </mat-grid-list>\r\n            </mat-menu>\r\n            <!-- Notification toggle button -->\r\n            <button\r\n                    mat-icon-button\r\n                    matTooltip=\"Notifications\"\r\n                    (click)=\"toggleNotific()\"\r\n                    [style.overflow]=\"'visible'\"\r\n                    class=\"topbar-button-right\">\r\n                <mat-icon>notifications</mat-icon>\r\n                <span class=\"notification-number mat-bg-warn\">3</span>\r\n            </button>\r\n            <!-- Top left user menu -->\r\n            <button mat-icon-button [matMenuTriggerFor]=\"accountMenu\" class=\"topbar-button-right mr-1 img-button\">\r\n                <img src=\"assets/images/face-7.jpg\" alt=\"\">\r\n            </button>\r\n            <mat-menu #accountMenu=\"matMenu\">\r\n                <button mat-menu-item [routerLink]=\"['/profile/overview']\">\r\n                    <mat-icon>account_box</mat-icon>\r\n                    <span>Profile</span>\r\n                </button>\r\n                <button mat-menu-item (click)=\"openCustomizer = true\">\r\n                    <mat-icon>settings</mat-icon>\r\n                    <span>Layout Settings</span>\r\n                </button>\r\n                <button mat-menu-item>\r\n                    <mat-icon>notifications_off</mat-icon>\r\n                    <span>Disable alerts</span>\r\n                </button>\r\n                <button mat-menu-item\r\n                        (click)=\"logout()\">\r\n                    <mat-icon>exit_to_app</mat-icon>\r\n                    <span>Sign out</span>\r\n                </button>\r\n            </mat-menu>\r\n            <!-- Mobile screen menu toggle -->\r\n            <button\r\n                    mat-icon-button\r\n                    class=\"mr-1\"\r\n                    (click)=\"toggleSidenav()\"\r\n                    *ngIf=\"layoutConf.isMobile\">\r\n                <mat-icon>menu</mat-icon>\r\n            </button>\r\n        </div>\r\n    </div>\r\n</div>\r\n"
    }),
    tslib_1.__metadata("design:paramtypes", [LayoutService,
        NavigationService,
        ThemeService,
        TranslateService,
        Renderer2,
        LoginService,
        AccountService])
], HeaderTopComponent);
export { HeaderTopComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLXRvcC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvZWdyZXQvIiwic291cmNlcyI6WyJjb21wb25lbnRzL2hlYWRlci10b3AvaGVhZGVyLXRvcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRS9FLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUM5RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUN0RSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDNUQsT0FBTyxFQUFDLGNBQWMsRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFNeEUsSUFBYSxrQkFBa0IsR0FBL0IsTUFBYSxrQkFBa0I7SUFnQjlCLFlBQ1MsTUFBcUIsRUFDckIsVUFBNkIsRUFDOUIsWUFBMEIsRUFDMUIsU0FBMkIsRUFDMUIsUUFBbUIsRUFDbkIsWUFBMEIsRUFDMUIsY0FBOEI7UUFOOUIsV0FBTSxHQUFOLE1BQU0sQ0FBZTtRQUNyQixlQUFVLEdBQVYsVUFBVSxDQUFtQjtRQUM5QixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQUMxQixhQUFRLEdBQVIsUUFBUSxDQUFXO1FBQ25CLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzFCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQW5CdkMsZ0JBQVcsR0FBVSxFQUFFLENBQUM7UUFDeEIsbUJBQWMsR0FBRyxLQUFLLENBQUM7UUFDdkIsZ0JBQVcsR0FBRyxJQUFJLENBQUM7UUFDbkIsbUJBQWMsR0FBRyxDQUFDO2dCQUNqQixJQUFJLEVBQUUsU0FBUztnQkFDZixJQUFJLEVBQUUsSUFBSTthQUNWLEVBQUU7Z0JBQ0YsSUFBSSxFQUFFLFNBQVM7Z0JBQ2YsSUFBSSxFQUFFLElBQUk7YUFDVixDQUFDLENBQUE7SUFZRixDQUFDO0lBRUQsUUFBUTtRQUNQLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUM7UUFDekMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQztRQUNqRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVTthQUMzQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDaEIsR0FBRyxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLE1BQU0sSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLFdBQVcsQ0FBQyxDQUFDO1lBQzVFLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztZQUNkLElBQUksU0FBUyxHQUFVLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQzNDLElBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxLQUFLLEVBQUU7Z0JBQ3hCLE9BQU8sSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUE7YUFDakM7WUFDRCxJQUFJLFFBQVEsR0FBVSxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3ZELFNBQVMsQ0FBQyxJQUFJLENBQUM7Z0JBQ2QsSUFBSSxFQUFFLE1BQU07Z0JBQ1osSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLE9BQU8sRUFBRSxNQUFNO2dCQUNmLElBQUksRUFBRSxZQUFZO2dCQUNsQixHQUFHLEVBQUUsUUFBUTthQUNiLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO1FBQzVCLENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUVELFdBQVc7UUFDVixJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFBO0lBQy9CLENBQUM7SUFFRCxPQUFPO1FBQ04sSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFBO0lBQ3JDLENBQUM7SUFFRCxXQUFXLENBQUMsS0FBSztRQUNoQixJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEVBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxJQUFJLEVBQUMsQ0FBQyxDQUFBO0lBQ3hELENBQUM7SUFFRCxhQUFhO1FBQ1osSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBRUQsYUFBYTtRQUNaLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEtBQUssUUFBUSxFQUFFO1lBQzlDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQztnQkFDdEMsWUFBWSxFQUFFLE1BQU07YUFDcEIsQ0FBQyxDQUFBO1NBQ0Y7UUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDO1lBQy9CLFlBQVksRUFBRSxRQUFRO1NBQ3RCLENBQUMsQ0FBQTtJQUNILENBQUM7SUFFRCxNQUFNO1FBQ0wsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsQ0FBQTtJQUMzQixDQUFDO0NBQ0QsQ0FBQTs7WUEvRGlCLGFBQWE7WUFDVCxpQkFBaUI7WUFDaEIsWUFBWTtZQUNmLGdCQUFnQjtZQUNoQixTQUFTO1lBQ0wsWUFBWTtZQUNWLGNBQWM7O0FBVDlCO0lBQVIsS0FBSyxFQUFFOzt3REFBYztBQWRWLGtCQUFrQjtJQUo5QixTQUFTLENBQUM7UUFDVixRQUFRLEVBQUUsa0JBQWtCO1FBQzVCLDI3UEFBMEM7S0FDMUMsQ0FBQzs2Q0FrQmdCLGFBQWE7UUFDVCxpQkFBaUI7UUFDaEIsWUFBWTtRQUNmLGdCQUFnQjtRQUNoQixTQUFTO1FBQ0wsWUFBWTtRQUNWLGNBQWM7R0F2QjNCLGtCQUFrQixDQWdGOUI7U0FoRlksa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25EZXN0cm95LCBPbkluaXQsIFJlbmRlcmVyMiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5pbXBvcnQgeyBMYXlvdXRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvbGF5b3V0LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOYXZpZ2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL25hdmlnYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IFRoZW1lU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL3RoZW1lLnNlcnZpY2UnO1xyXG5pbXBvcnQge0FjY291bnRTZXJ2aWNlLCBJTWVudUl0ZW0sIExvZ2luU2VydmljZX0gZnJvbSAnQGxhbWlzL3dlYi1jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG5cdHNlbGVjdG9yOiAnZWdyZXQtaGVhZGVyLXRvcCcsXHJcblx0dGVtcGxhdGVVcmw6ICcuL2hlYWRlci10b3AuY29tcG9uZW50Lmh0bWwnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBIZWFkZXJUb3BDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcblx0bGF5b3V0Q29uZjogYW55O1xyXG5cdG1lbnVJdGVtczogSU1lbnVJdGVtW107XHJcblx0bWVudUl0ZW1TdWI6IFN1YnNjcmlwdGlvbjtcclxuXHRlZ3JldFRoZW1lczogYW55W10gPSBbXTtcclxuXHRvcGVuQ3VzdG9taXplciA9IGZhbHNlO1xyXG5cdGN1cnJlbnRMYW5nID0gJ2VuJztcclxuXHRhdmFpbGFibGVMYW5ncyA9IFt7XHJcblx0XHRuYW1lOiAnRW5nbGlzaCcsXHJcblx0XHRjb2RlOiAnZW4nLFxyXG5cdH0sIHtcclxuXHRcdG5hbWU6ICdTcGFuaXNoJyxcclxuXHRcdGNvZGU6ICdlcycsXHJcblx0fV1cclxuXHRASW5wdXQoKSBub3RpZmljUGFuZWw7XHJcblxyXG5cdGNvbnN0cnVjdG9yKFxyXG5cdFx0cHJpdmF0ZSBsYXlvdXQ6IExheW91dFNlcnZpY2UsXHJcblx0XHRwcml2YXRlIG5hdlNlcnZpY2U6IE5hdmlnYXRpb25TZXJ2aWNlLFxyXG5cdFx0cHVibGljIHRoZW1lU2VydmljZTogVGhlbWVTZXJ2aWNlLFxyXG5cdFx0cHVibGljIHRyYW5zbGF0ZTogVHJhbnNsYXRlU2VydmljZSxcclxuXHRcdHByaXZhdGUgcmVuZGVyZXI6IFJlbmRlcmVyMixcclxuXHRcdHByaXZhdGUgbG9naW5TZXJ2aWNlOiBMb2dpblNlcnZpY2UsXHJcblx0XHRwcml2YXRlIGFjY291bnRTZXJ2aWNlOiBBY2NvdW50U2VydmljZVxyXG5cdCkge1xyXG5cdH1cclxuXHJcblx0bmdPbkluaXQoKSB7XHJcblx0XHR0aGlzLmxheW91dENvbmYgPSB0aGlzLmxheW91dC5sYXlvdXRDb25mO1xyXG5cdFx0dGhpcy5lZ3JldFRoZW1lcyA9IHRoaXMudGhlbWVTZXJ2aWNlLmVncmV0VGhlbWVzO1xyXG5cdFx0dGhpcy5tZW51SXRlbVN1YiA9IHRoaXMubmF2U2VydmljZS5tZW51SXRlbXMkXHJcblx0XHRcdC5zdWJzY3JpYmUocmVzID0+IHtcclxuXHRcdFx0XHRyZXMgPSByZXMuZmlsdGVyKGl0ZW0gPT4gaXRlbS50eXBlICE9PSAnaWNvbicgJiYgaXRlbS50eXBlICE9PSAnc2VwYXJhdG9yJyk7XHJcblx0XHRcdFx0bGV0IGxpbWl0ID0gNDtcclxuXHRcdFx0XHRsZXQgbWFpbkl0ZW1zOiBhbnlbXSA9IHJlcy5zbGljZSgwLCBsaW1pdCk7XHJcblx0XHRcdFx0aWYgKHJlcy5sZW5ndGggPD0gbGltaXQpIHtcclxuXHRcdFx0XHRcdHJldHVybiB0aGlzLm1lbnVJdGVtcyA9IG1haW5JdGVtc1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRsZXQgc3ViSXRlbXM6IGFueVtdID0gcmVzLnNsaWNlKGxpbWl0LCByZXMubGVuZ3RoIC0gMSk7XHJcblx0XHRcdFx0bWFpbkl0ZW1zLnB1c2goe1xyXG5cdFx0XHRcdFx0bmFtZTogJ01vcmUnLFxyXG5cdFx0XHRcdFx0dHlwZTogJ2Ryb3BEb3duJyxcclxuXHRcdFx0XHRcdHRvb2x0aXA6ICdNb3JlJyxcclxuXHRcdFx0XHRcdGljb246ICdtb3JlX2hvcml6JyxcclxuXHRcdFx0XHRcdHN1Yjogc3ViSXRlbXNcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHR0aGlzLm1lbnVJdGVtcyA9IG1haW5JdGVtcztcclxuXHRcdFx0fSlcclxuXHR9XHJcblxyXG5cdG5nT25EZXN0cm95KCkge1xyXG5cdFx0dGhpcy5tZW51SXRlbVN1Yi51bnN1YnNjcmliZSgpXHJcblx0fVxyXG5cclxuXHRzZXRMYW5nKCkge1xyXG5cdFx0dGhpcy50cmFuc2xhdGUudXNlKHRoaXMuY3VycmVudExhbmcpXHJcblx0fVxyXG5cclxuXHRjaGFuZ2VUaGVtZSh0aGVtZSkge1xyXG5cdFx0dGhpcy5sYXlvdXQucHVibGlzaExheW91dENoYW5nZSh7bWF0VGhlbWU6IHRoZW1lLm5hbWV9KVxyXG5cdH1cclxuXHJcblx0dG9nZ2xlTm90aWZpYygpIHtcclxuXHRcdHRoaXMubm90aWZpY1BhbmVsLnRvZ2dsZSgpO1xyXG5cdH1cclxuXHJcblx0dG9nZ2xlU2lkZW5hdigpIHtcclxuXHRcdGlmICh0aGlzLmxheW91dENvbmYuc2lkZWJhclN0eWxlID09PSAnY2xvc2VkJykge1xyXG5cdFx0XHRyZXR1cm4gdGhpcy5sYXlvdXQucHVibGlzaExheW91dENoYW5nZSh7XHJcblx0XHRcdFx0c2lkZWJhclN0eWxlOiAnZnVsbCdcclxuXHRcdFx0fSlcclxuXHRcdH1cclxuXHRcdHRoaXMubGF5b3V0LnB1Ymxpc2hMYXlvdXRDaGFuZ2Uoe1xyXG5cdFx0XHRzaWRlYmFyU3R5bGU6ICdjbG9zZWQnXHJcblx0XHR9KVxyXG5cdH1cclxuXHJcblx0bG9nb3V0KCkge1xyXG5cdFx0dGhpcy5sb2dpblNlcnZpY2UubG9nb3V0KClcclxuXHR9XHJcbn1cclxuIl19