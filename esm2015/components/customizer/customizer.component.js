import * as tslib_1 from "tslib";
import { Component, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { NavigationService } from '../../services/navigation.service';
import { LayoutService } from '../../services/layout.service';
import { CustomizerService } from '../../services/customizer.service';
import { ITheme, ThemeService } from '../../services/theme.service';
let CustomizerComponent = class CustomizerComponent {
    constructor(navService, layout, themeService, customizer, renderer) {
        this.navService = navService;
        this.layout = layout;
        this.themeService = themeService;
        this.customizer = customizer;
        this.renderer = renderer;
        this.isCustomizerOpen = false;
        this.customizerClosed = new EventEmitter();
        this.sidenavTypes = [
            {
                name: 'Default Menu',
                value: 'default-menu'
            },
            {
                name: 'Separator Menu',
                value: 'separator-menu'
            },
            {
                name: 'Icon Menu',
                value: 'icon-menu'
            }
        ];
        this.selectedMenu = 'icon-menu';
        this.isTopbarFixed = false;
        this.isRTL = false;
        this.perfectScrollbarEnabled = true;
    }
    ngOnInit() {
        this.layoutConf = this.layout.layoutConf;
        this.selectedLayout = this.layoutConf.navigationPos;
        this.isTopbarFixed = this.layoutConf.topbarFixed;
        this.isRTL = this.layoutConf.dir === 'rtl';
        this.egretThemes = this.themeService.egretThemes;
    }
    changeTheme(theme) {
        // this.themeService.changeTheme(theme);
        this.layout.publishLayoutChange({ matTheme: theme.name });
    }
    changeLayoutStyle(data) {
        this.layout.publishLayoutChange({ navigationPos: this.selectedLayout });
    }
    changeSidenav(data) {
        this.navService.publishNavigationChange(data.value);
    }
    toggleBreadcrumb(data) {
        this.layout.publishLayoutChange({ useBreadcrumb: data.checked });
    }
    sidebarCompactToggle(data) {
        this.layout.publishLayoutChange({ sidebarCompactToggle: data.checked });
    }
    toggleTopbarFixed(data) {
        this.layout.publishLayoutChange({ topbarFixed: data.checked });
    }
    toggleDir(data) {
        let dir = data.checked ? 'rtl' : 'ltr';
        this.layout.publishLayoutChange({ dir: dir });
    }
    tooglePerfectScrollbar(data) {
        this.layout.publishLayoutChange({ perfectScrollbar: this.perfectScrollbarEnabled });
    }
    close() {
        this.isCustomizerOpen = false;
        this.customizerClosed.emit(null);
    }
};
CustomizerComponent.ctorParameters = () => [
    { type: NavigationService },
    { type: LayoutService },
    { type: ThemeService },
    { type: CustomizerService },
    { type: Renderer2 }
];
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Boolean)
], CustomizerComponent.prototype, "isCustomizerOpen", void 0);
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", EventEmitter)
], CustomizerComponent.prototype, "customizerClosed", void 0);
CustomizerComponent = tslib_1.__decorate([
    Component({
        selector: 'egret-customizer',
        template: "<div id=\"app-customizer\" *ngIf=\"isCustomizerOpen\">\r\n    <mat-card class=\"p-0\">\r\n        <mat-card-title class=\"m-0 light-gray\">\r\n            <div class=\"card-title-text\" fxLayout=\"row wrap\" fxLayoutAlign=\"center center\">\r\n                <span fxFlex></span>\r\n                <button\r\n                        class=\"card-control\"\r\n                        mat-icon-button\r\n                        (click)=\"close()\">\r\n                    <mat-icon>close</mat-icon>\r\n                </button>\r\n            </div>\r\n        </mat-card-title>\r\n\r\n        <mat-card-content [perfectScrollbar]>\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Layouts</h6>\r\n                <mat-radio-group fxLayout=\"column\" [(ngModel)]=\"selectedLayout\" (change)=\"changeLayoutStyle($event)\">\r\n                    <mat-radio-button [value]=\"'top'\"> Top Navigation</mat-radio-button>\r\n                    <mat-radio-button [value]=\"'side'\"> Side Navigation</mat-radio-button>\r\n                </mat-radio-group>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Header Colors</h6>\r\n                <div class=\"mb-1\">\r\n                    <mat-checkbox [(ngModel)]=\"isTopbarFixed\" (change)=\"toggleTopbarFixed($event)\"\r\n                                  [disabled]=\"selectedLayout === 'top'\" [value]=\"selectedLayout !== 'top'\">Fixed Header\r\n                    </mat-checkbox>\r\n                </div>\r\n                <div class=\"colors\">\r\n                    <div\r\n                            class=\"color {{c.class}}\"\r\n                            *ngFor=\"let c of customizer.topbarColors\"\r\n                            (click)=\"customizer.changeTopbarColor(c)\">\r\n                        <mat-icon class=\"active-icon\" *ngIf=\"c.active\">check</mat-icon>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Sidebar colors</h6>\r\n                <div class=\"colors\">\r\n                    <div\r\n                            class=\"color {{c.class}}\"\r\n                            *ngFor=\"let c of customizer.sidebarColors\"\r\n                            (click)=\"customizer.changeSidebarColor(c)\">\r\n                        <mat-icon class=\"active-icon\" *ngIf=\"c.active\">check</mat-icon>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Material Themes</h6>\r\n                <div class=\"colors\">\r\n                    <div class=\"color\" *ngFor=\"let theme of egretThemes\"\r\n                         (click)=\"changeTheme(theme)\" [style.background]=\"theme.baseColor\">\r\n                        <mat-icon class=\"active-icon\" *ngIf=\"theme.isActive\">check</mat-icon>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Sidebar Toggle</h6>\r\n                <div class=\"mb-1\">\r\n                    <mat-checkbox [(ngModel)]=\"layoutConf.sidebarCompactToggle\" (change)=\"sidebarCompactToggle($event)\">\r\n                        Toggle Sidebar\r\n                    </mat-checkbox>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Breadcrumb</h6>\r\n                <div class=\"mb-1\">\r\n                    <mat-checkbox [(ngModel)]=\"layoutConf.useBreadcrumb\" (change)=\"toggleBreadcrumb($event)\">Use\r\n                        breadcrumb\r\n                    </mat-checkbox>\r\n                </div>\r\n                <small class=\"text-muted\">Breadcrumb types</small>\r\n                <mat-radio-group fxLayout=\"column\" [(ngModel)]=\"layoutConf.breadcrumb\"\r\n                                 [disabled]=\"!layoutConf.useBreadcrumb\">\r\n                    <mat-radio-button [value]=\"'simple'\"> Simple</mat-radio-button>\r\n                    <mat-radio-button [value]=\"'title'\"> Simple with title</mat-radio-button>\r\n                </mat-radio-group>\r\n            </div>\r\n            <div class=\"pb-1 pos-rel mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Navigation</h6>\r\n                <mat-radio-group\r\n                        fxLayout=\"column\"\r\n                        [(ngModel)]=\"selectedMenu\"\r\n                        (change)=\"changeSidenav($event)\"\r\n                        [disabled]=\"selectedLayout === 'top'\">\r\n                    <mat-radio-button\r\n                            *ngFor=\"let type of sidenavTypes\"\r\n                            [value]=\"type.value\">\r\n                        {{type.name}}\r\n                    </mat-radio-button>\r\n                </mat-radio-group>\r\n            </div>\r\n\r\n            <div class=\"pb-1 \">\r\n                <mat-checkbox [(ngModel)]=\"isRTL\" (change)=\"toggleDir($event)\">RTL</mat-checkbox>\r\n            </div>\r\n        </mat-card-content>\r\n    </mat-card>\r\n</div>\r\n",
        styles: [".handle{position:fixed;bottom:30px;right:30px;z-index:99}#app-customizer{position:fixed;bottom:0;top:0;right:0;min-width:180px;max-width:280px;z-index:999}#app-customizer .title{text-transform:uppercase;font-size:12px;font-weight:700;margin:0 0 1rem}#app-customizer .mat-card{margin:0;border-radius:0}#app-customizer .mat-card-content{padding:1rem 1.5rem 2rem;max-height:calc(100vh - 80px)}.pos-rel{position:relative;z-index:99}.pos-rel .olay{position:absolute;width:100%;height:100%;background:rgba(0,0,0,.5);z-index:100}.colors{display:flex;flex-wrap:wrap}.colors .color{position:relative;width:36px;height:36px;display:inline-block;border-radius:50%;margin:8px;text-align:center;box-shadow:0 4px 20px 1px rgba(0,0,0,.06),0 1px 4px rgba(0,0,0,.03);cursor:pointer}.colors .color .active-icon{position:absolute;left:0;right:0;margin:auto;top:6px}"]
    }),
    tslib_1.__metadata("design:paramtypes", [NavigationService,
        LayoutService,
        ThemeService,
        CustomizerService,
        Renderer2])
], CustomizerComponent);
export { CustomizerComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9taXplci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvZWdyZXQvIiwic291cmNlcyI6WyJjb21wb25lbnRzL2N1c3RvbWl6ZXIvY3VzdG9taXplci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxRixPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUN0RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDOUQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDdEUsT0FBTyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQU9wRSxJQUFhLG1CQUFtQixHQUFoQyxNQUFhLG1CQUFtQjtJQThCL0IsWUFDUyxVQUE2QixFQUM3QixNQUFxQixFQUNyQixZQUEwQixFQUMzQixVQUE2QixFQUM1QixRQUFtQjtRQUpuQixlQUFVLEdBQVYsVUFBVSxDQUFtQjtRQUM3QixXQUFNLEdBQU4sTUFBTSxDQUFlO1FBQ3JCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzNCLGVBQVUsR0FBVixVQUFVLENBQW1CO1FBQzVCLGFBQVEsR0FBUixRQUFRLENBQVc7UUFqQzVCLHFCQUFnQixHQUFZLEtBQUssQ0FBQztRQUVsQyxxQkFBZ0IsR0FBc0IsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN6RCxpQkFBWSxHQUFHO1lBQ2Q7Z0JBQ0MsSUFBSSxFQUFFLGNBQWM7Z0JBQ3BCLEtBQUssRUFBRSxjQUFjO2FBQ3JCO1lBQ0Q7Z0JBQ0MsSUFBSSxFQUFFLGdCQUFnQjtnQkFDdEIsS0FBSyxFQUFFLGdCQUFnQjthQUN2QjtZQUNEO2dCQUNDLElBQUksRUFBRSxXQUFXO2dCQUNqQixLQUFLLEVBQUUsV0FBVzthQUNsQjtTQUNELENBQUM7UUFLRixpQkFBWSxHQUFXLFdBQVcsQ0FBQztRQUVuQyxrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QixVQUFLLEdBQUcsS0FBSyxDQUFDO1FBRWQsNEJBQXVCLEdBQVksSUFBSSxDQUFDO0lBU3hDLENBQUM7SUFFRCxRQUFRO1FBQ1AsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQztRQUN6QyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDO1FBQ3BELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUM7UUFDakQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsS0FBSyxLQUFLLENBQUM7UUFDM0MsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQztJQUNsRCxDQUFDO0lBRUQsV0FBVyxDQUFDLEtBQUs7UUFDaEIsd0NBQXdDO1FBQ3hDLElBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUMsRUFBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLElBQUksRUFBQyxDQUFDLENBQUE7SUFDeEQsQ0FBQztJQUVELGlCQUFpQixDQUFDLElBQUk7UUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxFQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFDLENBQUMsQ0FBQztJQUN2RSxDQUFDO0lBRUQsYUFBYSxDQUFDLElBQUk7UUFDakIsSUFBSSxDQUFDLFVBQVUsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckQsQ0FBQztJQUVELGdCQUFnQixDQUFDLElBQUk7UUFDcEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxFQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFDLENBQUMsQ0FBQztJQUNoRSxDQUFDO0lBRUQsb0JBQW9CLENBQUMsSUFBSTtRQUN4QixJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEVBQUMsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBQyxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVELGlCQUFpQixDQUFDLElBQUk7UUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxFQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFDLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRUQsU0FBUyxDQUFDLElBQUk7UUFDYixJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUN2QyxJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEVBQUMsR0FBRyxFQUFFLEdBQUcsRUFBQyxDQUFDLENBQUM7SUFDN0MsQ0FBQztJQUVELHNCQUFzQixDQUFDLElBQUk7UUFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxFQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyx1QkFBdUIsRUFBQyxDQUFDLENBQUE7SUFDbEYsQ0FBQztJQUVELEtBQUs7UUFDSixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQzlCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbEMsQ0FBQztDQUNELENBQUE7O1lBdERxQixpQkFBaUI7WUFDckIsYUFBYTtZQUNQLFlBQVk7WUFDZixpQkFBaUI7WUFDbEIsU0FBUzs7QUFqQzVCO0lBREMsS0FBSyxFQUFFOzs2REFDMEI7QUFFbEM7SUFEQyxNQUFNLEVBQUU7c0NBQ1MsWUFBWTs2REFBMkI7QUFKN0MsbUJBQW1CO0lBTC9CLFNBQVMsQ0FBQztRQUNWLFFBQVEsRUFBRSxrQkFBa0I7UUFDNUIsc3lLQUEwQzs7S0FFMUMsQ0FBQzs2Q0FnQ29CLGlCQUFpQjtRQUNyQixhQUFhO1FBQ1AsWUFBWTtRQUNmLGlCQUFpQjtRQUNsQixTQUFTO0dBbkNoQixtQkFBbUIsQ0FxRi9CO1NBckZZLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPdXRwdXQsIFJlbmRlcmVyMiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOYXZpZ2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL25hdmlnYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IExheW91dFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9sYXlvdXQuc2VydmljZSc7XHJcbmltcG9ydCB7IEN1c3RvbWl6ZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvY3VzdG9taXplci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSVRoZW1lLCBUaGVtZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy90aGVtZS5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG5cdHNlbGVjdG9yOiAnZWdyZXQtY3VzdG9taXplcicsXHJcblx0dGVtcGxhdGVVcmw6ICcuL2N1c3RvbWl6ZXIuY29tcG9uZW50Lmh0bWwnLFxyXG5cdHN0eWxlVXJsczogWycuL2N1c3RvbWl6ZXIuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ3VzdG9taXplckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblx0QElucHV0KClcclxuXHRpc0N1c3RvbWl6ZXJPcGVuOiBib29sZWFuID0gZmFsc2U7XHJcblx0QE91dHB1dCgpXHJcblx0Y3VzdG9taXplckNsb3NlZDogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcblx0c2lkZW5hdlR5cGVzID0gW1xyXG5cdFx0e1xyXG5cdFx0XHRuYW1lOiAnRGVmYXVsdCBNZW51JyxcclxuXHRcdFx0dmFsdWU6ICdkZWZhdWx0LW1lbnUnXHJcblx0XHR9LFxyXG5cdFx0e1xyXG5cdFx0XHRuYW1lOiAnU2VwYXJhdG9yIE1lbnUnLFxyXG5cdFx0XHR2YWx1ZTogJ3NlcGFyYXRvci1tZW51J1xyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0bmFtZTogJ0ljb24gTWVudScsXHJcblx0XHRcdHZhbHVlOiAnaWNvbi1tZW51J1xyXG5cdFx0fVxyXG5cdF07XHJcblx0c2lkZWJhckNvbG9yczogYW55W107XHJcblx0dG9wYmFyQ29sb3JzOiBhbnlbXTtcclxuXHJcblx0bGF5b3V0Q29uZjtcclxuXHRzZWxlY3RlZE1lbnU6IHN0cmluZyA9ICdpY29uLW1lbnUnO1xyXG5cdHNlbGVjdGVkTGF5b3V0OiBzdHJpbmc7XHJcblx0aXNUb3BiYXJGaXhlZCA9IGZhbHNlO1xyXG5cdGlzUlRMID0gZmFsc2U7XHJcblx0ZWdyZXRUaGVtZXM6IElUaGVtZVtdO1xyXG5cdHBlcmZlY3RTY3JvbGxiYXJFbmFibGVkOiBib29sZWFuID0gdHJ1ZTtcclxuXHJcblx0Y29uc3RydWN0b3IoXHJcblx0XHRwcml2YXRlIG5hdlNlcnZpY2U6IE5hdmlnYXRpb25TZXJ2aWNlLFxyXG5cdFx0cHJpdmF0ZSBsYXlvdXQ6IExheW91dFNlcnZpY2UsXHJcblx0XHRwcml2YXRlIHRoZW1lU2VydmljZTogVGhlbWVTZXJ2aWNlLFxyXG5cdFx0cHVibGljIGN1c3RvbWl6ZXI6IEN1c3RvbWl6ZXJTZXJ2aWNlLFxyXG5cdFx0cHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIyXHJcblx0KSB7XHJcblx0fVxyXG5cclxuXHRuZ09uSW5pdCgpIHtcclxuXHRcdHRoaXMubGF5b3V0Q29uZiA9IHRoaXMubGF5b3V0LmxheW91dENvbmY7XHJcblx0XHR0aGlzLnNlbGVjdGVkTGF5b3V0ID0gdGhpcy5sYXlvdXRDb25mLm5hdmlnYXRpb25Qb3M7XHJcblx0XHR0aGlzLmlzVG9wYmFyRml4ZWQgPSB0aGlzLmxheW91dENvbmYudG9wYmFyRml4ZWQ7XHJcblx0XHR0aGlzLmlzUlRMID0gdGhpcy5sYXlvdXRDb25mLmRpciA9PT0gJ3J0bCc7XHJcblx0XHR0aGlzLmVncmV0VGhlbWVzID0gdGhpcy50aGVtZVNlcnZpY2UuZWdyZXRUaGVtZXM7XHJcblx0fVxyXG5cclxuXHRjaGFuZ2VUaGVtZSh0aGVtZSkge1xyXG5cdFx0Ly8gdGhpcy50aGVtZVNlcnZpY2UuY2hhbmdlVGhlbWUodGhlbWUpO1xyXG5cdFx0dGhpcy5sYXlvdXQucHVibGlzaExheW91dENoYW5nZSh7bWF0VGhlbWU6IHRoZW1lLm5hbWV9KVxyXG5cdH1cclxuXHJcblx0Y2hhbmdlTGF5b3V0U3R5bGUoZGF0YSkge1xyXG5cdFx0dGhpcy5sYXlvdXQucHVibGlzaExheW91dENoYW5nZSh7bmF2aWdhdGlvblBvczogdGhpcy5zZWxlY3RlZExheW91dH0pO1xyXG5cdH1cclxuXHJcblx0Y2hhbmdlU2lkZW5hdihkYXRhKSB7XHJcblx0XHR0aGlzLm5hdlNlcnZpY2UucHVibGlzaE5hdmlnYXRpb25DaGFuZ2UoZGF0YS52YWx1ZSk7XHJcblx0fVxyXG5cclxuXHR0b2dnbGVCcmVhZGNydW1iKGRhdGEpIHtcclxuXHRcdHRoaXMubGF5b3V0LnB1Ymxpc2hMYXlvdXRDaGFuZ2Uoe3VzZUJyZWFkY3J1bWI6IGRhdGEuY2hlY2tlZH0pO1xyXG5cdH1cclxuXHJcblx0c2lkZWJhckNvbXBhY3RUb2dnbGUoZGF0YSkge1xyXG5cdFx0dGhpcy5sYXlvdXQucHVibGlzaExheW91dENoYW5nZSh7c2lkZWJhckNvbXBhY3RUb2dnbGU6IGRhdGEuY2hlY2tlZH0pO1xyXG5cdH1cclxuXHJcblx0dG9nZ2xlVG9wYmFyRml4ZWQoZGF0YSkge1xyXG5cdFx0dGhpcy5sYXlvdXQucHVibGlzaExheW91dENoYW5nZSh7dG9wYmFyRml4ZWQ6IGRhdGEuY2hlY2tlZH0pO1xyXG5cdH1cclxuXHJcblx0dG9nZ2xlRGlyKGRhdGEpIHtcclxuXHRcdGxldCBkaXIgPSBkYXRhLmNoZWNrZWQgPyAncnRsJyA6ICdsdHInO1xyXG5cdFx0dGhpcy5sYXlvdXQucHVibGlzaExheW91dENoYW5nZSh7ZGlyOiBkaXJ9KTtcclxuXHR9XHJcblxyXG5cdHRvb2dsZVBlcmZlY3RTY3JvbGxiYXIoZGF0YSkge1xyXG5cdFx0dGhpcy5sYXlvdXQucHVibGlzaExheW91dENoYW5nZSh7cGVyZmVjdFNjcm9sbGJhcjogdGhpcy5wZXJmZWN0U2Nyb2xsYmFyRW5hYmxlZH0pXHJcblx0fVxyXG5cclxuXHRjbG9zZSgpIHtcclxuXHRcdHRoaXMuaXNDdXN0b21pemVyT3BlbiA9IGZhbHNlO1xyXG5cdFx0dGhpcy5jdXN0b21pemVyQ2xvc2VkLmVtaXQobnVsbCk7XHJcblx0fVxyXG59XHJcbiJdfQ==