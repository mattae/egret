import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ThemeService } from '../../services/theme.service';
import { Account, AccountService, IMenuItem, LoginService } from '@lamis/web-core';
import { NavigationService } from '../../services/navigation.service';
import { ILayoutConf, LayoutService } from '../../services/layout.service';
let SidebarSideComponent = class SidebarSideComponent {
    constructor(navService, themeService, layout, loginService, accountService) {
        this.navService = navService;
        this.themeService = themeService;
        this.layout = layout;
        this.loginService = loginService;
        this.accountService = accountService;
    }
    ngOnInit() {
        this.iconTypeMenuTitle = this.navService.iconTypeMenuTitle;
        this.menuItemsSub = this.navService.menuItems$.subscribe(menuItem => {
            this.menuItems = menuItem;
            //Checks item list has any icon type.
            this.hasIconTypeMenuItem = !!this.menuItems.filter(item => item.type === "icon").length;
        });
        this.layoutConf = this.layout.layoutConf;
        this.accountService.fetch().subscribe((res) => this.account = res.body);
    }
    ngAfterViewInit() {
    }
    ngOnDestroy() {
        if (this.menuItemsSub) {
            this.menuItemsSub.unsubscribe();
        }
    }
    toggleCollapse() {
        if (this.layoutConf.sidebarCompactToggle) {
            this.layout.publishLayoutChange({
                sidebarCompactToggle: false
            });
        }
        else {
            this.layout.publishLayoutChange({
                // sidebarStyle: "compact",
                sidebarCompactToggle: true
            });
        }
    }
    logout() {
        this.loginService.logout();
    }
};
SidebarSideComponent.ctorParameters = () => [
    { type: NavigationService },
    { type: ThemeService },
    { type: LayoutService },
    { type: LoginService },
    { type: AccountService }
];
SidebarSideComponent = tslib_1.__decorate([
    Component({
        selector: 'egret-sidebar-side',
        template: "<div class=\"sidebar-panel\">\r\n    <div id=\"scroll-area\" [perfectScrollbar] class=\"navigation-hold\" fxLayout=\"column\">\r\n        <div class=\"sidebar-hold\">\r\n\r\n            <!-- App Logo -->\r\n            <div class=\"branding\">\r\n                <img src=\"assets/images/logo.png\" alt=\"\" class=\"app-logo\">\r\n                <span class=\"app-logo-text\">LAMIS</span>\r\n\r\n                <span style=\"margin: auto\" *ngIf=\"layoutConf.sidebarStyle !== 'compact'\"></span>\r\n                <div\r\n                        class=\"sidebar-compact-switch\"\r\n                        [ngClass]=\"{active: layoutConf.sidebarCompactToggle}\"\r\n                        (click)=\"toggleCollapse()\"\r\n                        *ngIf=\"layoutConf.sidebarStyle !== 'compact'\"><span></span></div>\r\n            </div>\r\n\r\n            <!-- Sidebar user -->\r\n            <div class=\"app-user\">\r\n                <div class=\"app-user-photo\">\r\n                    <img src=\"assets/images/face-7.jpg\" class=\"mat-elevation-z1\" alt=\"\">\r\n                </div>\r\n                <span class=\"app-user-name mb-05\">\r\n                <mat-icon class=\"icon-xs text-muted\">lock</mat-icon>\r\n                {{account?.firstName}} {{account?.lastName}}\r\n                </span>\r\n                <!-- Small buttons -->\r\n                <div class=\"app-user-controls\">\r\n                    <button\r\n                            class=\"text-muted\"\r\n                            mat-icon-button\r\n                            mat-xs-button\r\n                            [matMenuTriggerFor]=\"appUserMenu\">\r\n                        <mat-icon>settings</mat-icon>\r\n                    </button>\r\n                    <button\r\n                            class=\"text-muted\"\r\n                            mat-icon-button\r\n                            mat-xs-button\r\n                            matTooltip=\"Inbox\"\r\n                            routerLink=\"/inbox\">\r\n                        <mat-icon>email</mat-icon>\r\n                    </button>\r\n                    <button\r\n                            class=\"text-muted\"\r\n                            mat-icon-button\r\n                            mat-xs-button\r\n                            (click)=\"logout()\"\r\n                            matTooltip=\"Sign Out\">\r\n                        <mat-icon>exit_to_app</mat-icon>\r\n                    </button>\r\n                    <mat-menu #appUserMenu=\"matMenu\">\r\n                        <button mat-menu-item routerLink=\"/profile/overview\">\r\n                            <mat-icon>account_box</mat-icon>\r\n                            <span>Profile</span>\r\n                        </button>\r\n                        <button mat-menu-item routerLink=\"/profile/settings\">\r\n                            <mat-icon>settings</mat-icon>\r\n                            <span>Account Settings</span>\r\n                        </button>\r\n                        <button mat-menu-item\r\n                                (click)=\"logout()\">\r\n                            <mat-icon>exit_to_app</mat-icon>\r\n                            <span>Sign out</span>\r\n                        </button>\r\n                    </mat-menu>\r\n                </div>\r\n            </div>\r\n            <!-- Navigation -->\r\n            <egret-sidenav [items]=\"menuItems\" [hasIconMenu]=\"hasIconTypeMenuItem\"\r\n                           [iconMenuTitle]=\"iconTypeMenuTitle\"></egret-sidenav>\r\n        </div>\r\n    </div>\r\n</div>\r\n"
    }),
    tslib_1.__metadata("design:paramtypes", [NavigationService,
        ThemeService,
        LayoutService,
        LoginService,
        AccountService])
], SidebarSideComponent);
export { SidebarSideComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lkZWJhci1zaWRlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy9lZ3JldC8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvc2lkZWJhci1zaWRlL3NpZGViYXItc2lkZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBaUIsU0FBUyxFQUFxQixNQUFNLGVBQWUsQ0FBQztBQUU1RSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDNUQsT0FBTyxFQUFFLE9BQU8sRUFBRSxjQUFjLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ25GLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3RFLE9BQU8sRUFBRSxXQUFXLEVBQUUsYUFBYSxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFNM0UsSUFBYSxvQkFBb0IsR0FBakMsTUFBYSxvQkFBb0I7SUFRN0IsWUFDWSxVQUE2QixFQUM5QixZQUEwQixFQUN6QixNQUFxQixFQUNyQixZQUEwQixFQUMxQixjQUE4QjtRQUo5QixlQUFVLEdBQVYsVUFBVSxDQUFtQjtRQUM5QixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUN6QixXQUFNLEdBQU4sTUFBTSxDQUFlO1FBQ3JCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzFCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtJQUUxQyxDQUFDO0lBRUQsUUFBUTtRQUNKLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDO1FBQzNELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2hFLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDO1lBQzFCLHFDQUFxQztZQUNyQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUM5QyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssTUFBTSxDQUMvQixDQUFDLE1BQU0sQ0FBQztRQUNiLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQztRQUN6QyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDNUUsQ0FBQztJQUVELGVBQWU7SUFDZixDQUFDO0lBRUQsV0FBVztRQUNQLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNuQixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ25DO0lBQ0wsQ0FBQztJQUVELGNBQWM7UUFDVixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLEVBQUU7WUFDdEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQztnQkFDNUIsb0JBQW9CLEVBQUUsS0FBSzthQUM5QixDQUFDLENBQUM7U0FDTjthQUFNO1lBQ0gsSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQztnQkFDNUIsMkJBQTJCO2dCQUMzQixvQkFBb0IsRUFBRSxJQUFJO2FBQzdCLENBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQztJQUVELE1BQU07UUFDRixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFBO0lBQzlCLENBQUM7Q0FDSixDQUFBOztZQTlDMkIsaUJBQWlCO1lBQ2hCLFlBQVk7WUFDakIsYUFBYTtZQUNQLFlBQVk7WUFDVixjQUFjOztBQWJqQyxvQkFBb0I7SUFKaEMsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLG9CQUFvQjtRQUM5QixpakhBQTRDO0tBQy9DLENBQUM7NkNBVTBCLGlCQUFpQjtRQUNoQixZQUFZO1FBQ2pCLGFBQWE7UUFDUCxZQUFZO1FBQ1YsY0FBYztHQWJqQyxvQkFBb0IsQ0F1RGhDO1NBdkRZLG9CQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFmdGVyVmlld0luaXQsIENvbXBvbmVudCwgT25EZXN0cm95LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFRoZW1lU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL3RoZW1lLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBY2NvdW50LCBBY2NvdW50U2VydmljZSwgSU1lbnVJdGVtLCBMb2dpblNlcnZpY2UgfSBmcm9tICdAbGFtaXMvd2ViLWNvcmUnO1xyXG5pbXBvcnQgeyBOYXZpZ2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL25hdmlnYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IElMYXlvdXRDb25mLCBMYXlvdXRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvbGF5b3V0LnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2VncmV0LXNpZGViYXItc2lkZScsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vc2lkZWJhci1zaWRlLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgU2lkZWJhclNpZGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSwgQWZ0ZXJWaWV3SW5pdCB7XHJcbiAgICBwdWJsaWMgbWVudUl0ZW1zOiBJTWVudUl0ZW1bXTtcclxuICAgIHB1YmxpYyBoYXNJY29uVHlwZU1lbnVJdGVtOiBib29sZWFuO1xyXG4gICAgcHVibGljIGljb25UeXBlTWVudVRpdGxlOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIG1lbnVJdGVtc1N1YjogU3Vic2NyaXB0aW9uO1xyXG4gICAgcHVibGljIGxheW91dENvbmY6IElMYXlvdXRDb25mO1xyXG4gICAgcHVibGljIGFjY291bnQ6IEFjY291bnQ7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBuYXZTZXJ2aWNlOiBOYXZpZ2F0aW9uU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgdGhlbWVTZXJ2aWNlOiBUaGVtZVNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBsYXlvdXQ6IExheW91dFNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBsb2dpblNlcnZpY2U6IExvZ2luU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIGFjY291bnRTZXJ2aWNlOiBBY2NvdW50U2VydmljZVxyXG4gICAgKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5pY29uVHlwZU1lbnVUaXRsZSA9IHRoaXMubmF2U2VydmljZS5pY29uVHlwZU1lbnVUaXRsZTtcclxuICAgICAgICB0aGlzLm1lbnVJdGVtc1N1YiA9IHRoaXMubmF2U2VydmljZS5tZW51SXRlbXMkLnN1YnNjcmliZShtZW51SXRlbSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubWVudUl0ZW1zID0gbWVudUl0ZW07XHJcbiAgICAgICAgICAgIC8vQ2hlY2tzIGl0ZW0gbGlzdCBoYXMgYW55IGljb24gdHlwZS5cclxuICAgICAgICAgICAgdGhpcy5oYXNJY29uVHlwZU1lbnVJdGVtID0gISF0aGlzLm1lbnVJdGVtcy5maWx0ZXIoXHJcbiAgICAgICAgICAgICAgICBpdGVtID0+IGl0ZW0udHlwZSA9PT0gXCJpY29uXCJcclxuICAgICAgICAgICAgKS5sZW5ndGg7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5sYXlvdXRDb25mID0gdGhpcy5sYXlvdXQubGF5b3V0Q29uZjtcclxuICAgICAgICB0aGlzLmFjY291bnRTZXJ2aWNlLmZldGNoKCkuc3Vic2NyaWJlKChyZXMpID0+IHRoaXMuYWNjb3VudCA9IHJlcy5ib2R5KTtcclxuICAgIH1cclxuXHJcbiAgICBuZ0FmdGVyVmlld0luaXQoKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICAgICAgaWYgKHRoaXMubWVudUl0ZW1zU3ViKSB7XHJcbiAgICAgICAgICAgIHRoaXMubWVudUl0ZW1zU3ViLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHRvZ2dsZUNvbGxhcHNlKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmxheW91dENvbmYuc2lkZWJhckNvbXBhY3RUb2dnbGUpIHtcclxuICAgICAgICAgICAgdGhpcy5sYXlvdXQucHVibGlzaExheW91dENoYW5nZSh7XHJcbiAgICAgICAgICAgICAgICBzaWRlYmFyQ29tcGFjdFRvZ2dsZTogZmFsc2VcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5sYXlvdXQucHVibGlzaExheW91dENoYW5nZSh7XHJcbiAgICAgICAgICAgICAgICAvLyBzaWRlYmFyU3R5bGU6IFwiY29tcGFjdFwiLFxyXG4gICAgICAgICAgICAgICAgc2lkZWJhckNvbXBhY3RUb2dnbGU6IHRydWVcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGxvZ291dCgpIHtcclxuICAgICAgICB0aGlzLmxvZ2luU2VydmljZS5sb2dvdXQoKVxyXG4gICAgfVxyXG59XHJcbiJdfQ==