import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
let NotificationsComponent = class NotificationsComponent {
    constructor(router) {
        this.router = router;
        // Dummy notifications
        this.notifications = [{
                message: 'New contact added',
                icon: 'assignment_ind',
                time: '1 min ago',
                route: '/inbox',
                color: 'primary'
            }, {
                message: 'New message',
                icon: 'chat',
                time: '4 min ago',
                route: '/chat',
                color: 'accent'
            }, {
                message: 'Server rebooted',
                icon: 'settings_backup_restore',
                time: '12 min ago',
                route: '/charts',
                color: 'warn'
            }];
    }
    ngOnInit() {
        this.router.events.subscribe((routeChange) => {
            if (routeChange instanceof NavigationEnd) {
                this.notificPanel.close();
            }
        });
    }
    clearAll(e) {
        e.preventDefault();
        this.notifications = [];
    }
};
NotificationsComponent.ctorParameters = () => [
    { type: Router }
];
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], NotificationsComponent.prototype, "notificPanel", void 0);
NotificationsComponent = tslib_1.__decorate([
    Component({
        selector: 'egret-notifications',
        template: "<div class=\"text-center mat-bg-primary pt-1 pb-1\">\r\n    <h6 class=\"m-0\">Notifications</h6>\r\n</div>\r\n<mat-nav-list class=\"notification-list\" role=\"list\">\r\n    <!-- Notification item -->\r\n    <mat-list-item *ngFor=\"let n of notifications\" class=\"notific-item\" role=\"listitem\" routerLinkActive=\"open\">\r\n        <mat-icon [color]=\"n.color\" class=\"notific-icon mr-1\">{{n.icon}}</mat-icon>\r\n        <a [routerLink]=\"[n.route || '/dashboard']\">\r\n            <div class=\"mat-list-text\">\r\n                <h4 class=\"message\">{{n.message}}</h4>\r\n                <small class=\"time text-muted\">{{n.time}}</small>\r\n            </div>\r\n        </a>\r\n    </mat-list-item>\r\n</mat-nav-list>\r\n<div class=\"text-center mt-1\" *ngIf=\"notifications.length\">\r\n    <small><a href=\"#\" (click)=\"clearAll($event)\">Clear all notifications</a></small>\r\n</div>\r\n"
    }),
    tslib_1.__metadata("design:paramtypes", [Router])
], NotificationsComponent);
export { NotificationsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9ucy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvZWdyZXQvIiwic291cmNlcyI6WyJjb21wb25lbnRzL25vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9ucy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ3pELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFNeEQsSUFBYSxzQkFBc0IsR0FBbkMsTUFBYSxzQkFBc0I7SUF3Qi9CLFlBQW9CLE1BQWM7UUFBZCxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBckJsQyxzQkFBc0I7UUFDdEIsa0JBQWEsR0FBRyxDQUFDO2dCQUNiLE9BQU8sRUFBRSxtQkFBbUI7Z0JBQzVCLElBQUksRUFBRSxnQkFBZ0I7Z0JBQ3RCLElBQUksRUFBRSxXQUFXO2dCQUNqQixLQUFLLEVBQUUsUUFBUTtnQkFDZixLQUFLLEVBQUUsU0FBUzthQUNuQixFQUFFO2dCQUNDLE9BQU8sRUFBRSxhQUFhO2dCQUN0QixJQUFJLEVBQUUsTUFBTTtnQkFDWixJQUFJLEVBQUUsV0FBVztnQkFDakIsS0FBSyxFQUFFLE9BQU87Z0JBQ2QsS0FBSyxFQUFFLFFBQVE7YUFDbEIsRUFBRTtnQkFDQyxPQUFPLEVBQUUsaUJBQWlCO2dCQUMxQixJQUFJLEVBQUUseUJBQXlCO2dCQUMvQixJQUFJLEVBQUUsWUFBWTtnQkFDbEIsS0FBSyxFQUFFLFNBQVM7Z0JBQ2hCLEtBQUssRUFBRSxNQUFNO2FBQ2hCLENBQUMsQ0FBQTtJQUdGLENBQUM7SUFFRCxRQUFRO1FBQ0osSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsV0FBVyxFQUFFLEVBQUU7WUFDekMsSUFBSSxXQUFXLFlBQVksYUFBYSxFQUFFO2dCQUN0QyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQzdCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsUUFBUSxDQUFDLENBQUM7UUFDTixDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7SUFDNUIsQ0FBQztDQUNKLENBQUE7O1lBZitCLE1BQU07O0FBdkJ6QjtJQUFSLEtBQUssRUFBRTs7NERBQWM7QUFEYixzQkFBc0I7SUFKbEMsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLHFCQUFxQjtRQUMvQixvNUJBQTZDO0tBQ2hELENBQUM7NkNBeUI4QixNQUFNO0dBeEJ6QixzQkFBc0IsQ0F1Q2xDO1NBdkNZLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOYXZpZ2F0aW9uRW5kLCBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2VncmV0LW5vdGlmaWNhdGlvbnMnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL25vdGlmaWNhdGlvbnMuY29tcG9uZW50Lmh0bWwnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOb3RpZmljYXRpb25zQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIEBJbnB1dCgpIG5vdGlmaWNQYW5lbDtcclxuXHJcbiAgICAvLyBEdW1teSBub3RpZmljYXRpb25zXHJcbiAgICBub3RpZmljYXRpb25zID0gW3tcclxuICAgICAgICBtZXNzYWdlOiAnTmV3IGNvbnRhY3QgYWRkZWQnLFxyXG4gICAgICAgIGljb246ICdhc3NpZ25tZW50X2luZCcsXHJcbiAgICAgICAgdGltZTogJzEgbWluIGFnbycsXHJcbiAgICAgICAgcm91dGU6ICcvaW5ib3gnLFxyXG4gICAgICAgIGNvbG9yOiAncHJpbWFyeSdcclxuICAgIH0sIHtcclxuICAgICAgICBtZXNzYWdlOiAnTmV3IG1lc3NhZ2UnLFxyXG4gICAgICAgIGljb246ICdjaGF0JyxcclxuICAgICAgICB0aW1lOiAnNCBtaW4gYWdvJyxcclxuICAgICAgICByb3V0ZTogJy9jaGF0JyxcclxuICAgICAgICBjb2xvcjogJ2FjY2VudCdcclxuICAgIH0sIHtcclxuICAgICAgICBtZXNzYWdlOiAnU2VydmVyIHJlYm9vdGVkJyxcclxuICAgICAgICBpY29uOiAnc2V0dGluZ3NfYmFja3VwX3Jlc3RvcmUnLFxyXG4gICAgICAgIHRpbWU6ICcxMiBtaW4gYWdvJyxcclxuICAgICAgICByb3V0ZTogJy9jaGFydHMnLFxyXG4gICAgICAgIGNvbG9yOiAnd2FybidcclxuICAgIH1dXHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMucm91dGVyLmV2ZW50cy5zdWJzY3JpYmUoKHJvdXRlQ2hhbmdlKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyb3V0ZUNoYW5nZSBpbnN0YW5jZW9mIE5hdmlnYXRpb25FbmQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY1BhbmVsLmNsb3NlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBjbGVhckFsbChlKSB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9ucyA9IFtdO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==