import * as tslib_1 from "tslib";
import { Component, Input, OnInit, Renderer2 } from '@angular/core';
import { ThemeService } from '../../services/theme.service';
import { LayoutService } from '../../services/layout.service';
import { TranslateService } from '@ngx-translate/core';
let HeaderSideComponent = class HeaderSideComponent {
    constructor(themeService, layout, translate, renderer) {
        this.themeService = themeService;
        this.layout = layout;
        this.translate = translate;
        this.renderer = renderer;
        this.availableLangs = [{
                name: 'EN',
                code: 'en',
                flag: 'flag-icon-us'
            }];
        this.currentLang = this.availableLangs[0];
        this.openCustomizer = false;
    }
    ngOnInit() {
        this.egretThemes = this.themeService.egretThemes;
        this.layoutConf = this.layout.layoutConf;
        this.translate.use(this.currentLang.code);
    }
    setLang(lng) {
        this.currentLang = lng;
        this.translate.use(lng.code);
    }
    changeTheme(theme) {
        // this.themeService.changeTheme(theme);
    }
    toggleNotific() {
        this.notificPanel.toggle();
    }
    toggleSidenav() {
        if (this.layoutConf.sidebarStyle === 'closed') {
            return this.layout.publishLayoutChange({
                sidebarStyle: 'full'
            });
        }
        this.layout.publishLayoutChange({
            sidebarStyle: 'closed'
        });
    }
    toggleCollapse() {
        // compact --> full
        if (this.layoutConf.sidebarStyle === 'compact') {
            return this.layout.publishLayoutChange({
                sidebarStyle: 'full',
                sidebarCompactToggle: false
            }, { transitionClass: true });
        }
        // * --> compact
        this.layout.publishLayoutChange({
            sidebarStyle: 'compact',
            sidebarCompactToggle: true
        }, { transitionClass: true });
    }
};
HeaderSideComponent.ctorParameters = () => [
    { type: ThemeService },
    { type: LayoutService },
    { type: TranslateService },
    { type: Renderer2 }
];
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], HeaderSideComponent.prototype, "notificPanel", void 0);
HeaderSideComponent = tslib_1.__decorate([
    Component({
        selector: 'egret-header-side',
        template: "<mat-toolbar class=\"topbar\">\r\n    <!-- Sidenav toggle button -->\r\n    <button\r\n            *ngIf=\"layoutConf.sidebarStyle !== 'compact'\"\r\n            mat-icon-button\r\n            id=\"sidenavToggle\"\r\n            (click)=\"toggleSidenav()\"\r\n            matTooltip=\"Toggle Hide/Open\"\r\n    >\r\n        <mat-icon>menu</mat-icon>\r\n    </button>\r\n\r\n    <!-- Search form -->\r\n    <!-- <div fxFlex fxHide.lt-sm=\"true\" class=\"search-bar\">\r\n      <form class=\"top-search-form\">\r\n        <mat-icon role=\"img\">search</mat-icon>\r\n        <input autofocus=\"true\" placeholder=\"Search\" type=\"text\" />\r\n      </form>\r\n    </div> -->\r\n\r\n    <span fxFlex></span>\r\n    <!-- Language Switcher -->\r\n   <!-- <button mat-button [matMenuTriggerFor]=\"menu\">\r\n        <span class=\"flag-icon {{currentLang.flag}} mr-05\"></span>\r\n        <span>{{currentLang.name}}</span>\r\n    </button>\r\n    <mat-menu #menu=\"matMenu\">\r\n        <button mat-menu-item *ngFor=\"let lang of availableLangs\" (click)=\"setLang(lang)\">\r\n            <span class=\"flag-icon mr-05 {{lang.flag}}\"></span>\r\n            <span>{{lang.name}}</span>\r\n        </button>\r\n    </mat-menu>\r\n    \\-->\r\n    <!-- Open \"views/search-view/result-page.component\" to understand how to subscribe to input field value -->\r\n    <egret-customizer [isCustomizerOpen]=\"openCustomizer\"\r\n                      (customizerClosed)=\"openCustomizer = false\">\r\n    </egret-customizer>\r\n    <!-- Notification toggle button -->\r\n    <button\r\n            mat-icon-button\r\n            matTooltip=\"Notifications\"\r\n            (click)=\"toggleNotific()\"\r\n            [style.overflow]=\"'visible'\"\r\n            class=\"topbar-button-right\"\r\n    >\r\n        <mat-icon>notifications</mat-icon>\r\n        <span class=\"notification-number mat-bg-warn\">3</span>\r\n    </button>\r\n    <!-- Top left user menu -->\r\n    <button\r\n            mat-icon-button\r\n            [matMenuTriggerFor]=\"accountMenu\"\r\n            class=\"topbar-button-right img-button\">\r\n        <img src=\"assets/images/face-7.jpg\" alt=\"\"/>\r\n    </button>\r\n\r\n    <mat-menu #accountMenu=\"matMenu\">\r\n        <button mat-menu-item [routerLink]=\"['/profile/overview']\">\r\n            <mat-icon>account_box</mat-icon>\r\n            <span>Profile</span>\r\n        </button>\r\n        <button mat-menu-item (click)=\"openCustomizer = true\">\r\n            <mat-icon>settings</mat-icon>\r\n            <span>Layout Settings</span>\r\n        </button>\r\n        <button mat-menu-item>\r\n            <mat-icon>notifications_off</mat-icon>\r\n            <span>Disable alerts</span>\r\n        </button>\r\n        <button mat-menu-item [routerLink]=\"['/sessions/signin']\">\r\n            <mat-icon>exit_to_app</mat-icon>\r\n            <span>Sign out</span>\r\n        </button>\r\n    </mat-menu>\r\n</mat-toolbar>\r\n"
    }),
    tslib_1.__metadata("design:paramtypes", [ThemeService,
        LayoutService,
        TranslateService,
        Renderer2])
], HeaderSideComponent);
export { HeaderSideComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLXNpZGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9oZWFkZXItc2lkZS9oZWFkZXItc2lkZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDcEUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQzVELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUM5RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQU12RCxJQUFhLG1CQUFtQixHQUFoQyxNQUFhLG1CQUFtQjtJQWE1QixZQUNZLFlBQTBCLEVBQzFCLE1BQXFCLEVBQ3RCLFNBQTJCLEVBQzFCLFFBQW1CO1FBSG5CLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzFCLFdBQU0sR0FBTixNQUFNLENBQWU7UUFDdEIsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFDMUIsYUFBUSxHQUFSLFFBQVEsQ0FBVztRQWZ4QixtQkFBYyxHQUFHLENBQUM7Z0JBQ3JCLElBQUksRUFBRSxJQUFJO2dCQUNWLElBQUksRUFBRSxJQUFJO2dCQUNWLElBQUksRUFBRSxjQUFjO2FBQ3ZCLENBQUMsQ0FBQTtRQUNGLGdCQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNyQyxtQkFBYyxHQUFHLEtBQUssQ0FBQztJQVd2QixDQUFDO0lBRUQsUUFBUTtRQUNKLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUM7UUFDakQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQztRQUN6QyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRCxPQUFPLENBQUMsR0FBRztRQUNQLElBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBRUQsV0FBVyxDQUFDLEtBQUs7UUFDYix3Q0FBd0M7SUFDNUMsQ0FBQztJQUVELGFBQWE7UUFDVCxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFFRCxhQUFhO1FBQ1QsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksS0FBSyxRQUFRLEVBQUU7WUFDM0MsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDO2dCQUNuQyxZQUFZLEVBQUUsTUFBTTthQUN2QixDQUFDLENBQUE7U0FDTDtRQUNELElBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUM7WUFDNUIsWUFBWSxFQUFFLFFBQVE7U0FDekIsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVELGNBQWM7UUFDVixtQkFBbUI7UUFDbkIsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksS0FBSyxTQUFTLEVBQUU7WUFDNUMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDO2dCQUNuQyxZQUFZLEVBQUUsTUFBTTtnQkFDcEIsb0JBQW9CLEVBQUUsS0FBSzthQUM5QixFQUFFLEVBQUMsZUFBZSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUE7U0FDOUI7UUFFRCxnQkFBZ0I7UUFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQztZQUM1QixZQUFZLEVBQUUsU0FBUztZQUN2QixvQkFBb0IsRUFBRSxJQUFJO1NBQzdCLEVBQUUsRUFBQyxlQUFlLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQTtJQUUvQixDQUFDO0NBQ0osQ0FBQTs7WUFyRDZCLFlBQVk7WUFDbEIsYUFBYTtZQUNYLGdCQUFnQjtZQUNoQixTQUFTOztBQWhCdEI7SUFBUixLQUFLLEVBQUU7O3lEQUFjO0FBRGIsbUJBQW1CO0lBSi9CLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxtQkFBbUI7UUFDN0IsdzVGQUEwQztLQUM3QyxDQUFDOzZDQWU0QixZQUFZO1FBQ2xCLGFBQWE7UUFDWCxnQkFBZ0I7UUFDaEIsU0FBUztHQWpCdEIsbUJBQW1CLENBbUUvQjtTQW5FWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQsIFJlbmRlcmVyMiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBUaGVtZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy90aGVtZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTGF5b3V0U2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2xheW91dC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2VncmV0LWhlYWRlci1zaWRlJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9oZWFkZXItc2lkZS50ZW1wbGF0ZS5odG1sJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgSGVhZGVyU2lkZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBASW5wdXQoKSBub3RpZmljUGFuZWw7XHJcbiAgICBwdWJsaWMgYXZhaWxhYmxlTGFuZ3MgPSBbe1xyXG4gICAgICAgIG5hbWU6ICdFTicsXHJcbiAgICAgICAgY29kZTogJ2VuJyxcclxuICAgICAgICBmbGFnOiAnZmxhZy1pY29uLXVzJ1xyXG4gICAgfV1cclxuICAgIGN1cnJlbnRMYW5nID0gdGhpcy5hdmFpbGFibGVMYW5nc1swXTtcclxuICAgIG9wZW5DdXN0b21pemVyID0gZmFsc2U7XHJcblxyXG4gICAgcHVibGljIGVncmV0VGhlbWVzO1xyXG4gICAgcHVibGljIGxheW91dENvbmY6IGFueTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIHRoZW1lU2VydmljZTogVGhlbWVTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgbGF5b3V0OiBMYXlvdXRTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIyXHJcbiAgICApIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLmVncmV0VGhlbWVzID0gdGhpcy50aGVtZVNlcnZpY2UuZWdyZXRUaGVtZXM7XHJcbiAgICAgICAgdGhpcy5sYXlvdXRDb25mID0gdGhpcy5sYXlvdXQubGF5b3V0Q29uZjtcclxuICAgICAgICB0aGlzLnRyYW5zbGF0ZS51c2UodGhpcy5jdXJyZW50TGFuZy5jb2RlKTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRMYW5nKGxuZykge1xyXG4gICAgICAgIHRoaXMuY3VycmVudExhbmcgPSBsbmc7XHJcbiAgICAgICAgdGhpcy50cmFuc2xhdGUudXNlKGxuZy5jb2RlKTtcclxuICAgIH1cclxuXHJcbiAgICBjaGFuZ2VUaGVtZSh0aGVtZSkge1xyXG4gICAgICAgIC8vIHRoaXMudGhlbWVTZXJ2aWNlLmNoYW5nZVRoZW1lKHRoZW1lKTtcclxuICAgIH1cclxuXHJcbiAgICB0b2dnbGVOb3RpZmljKCkge1xyXG4gICAgICAgIHRoaXMubm90aWZpY1BhbmVsLnRvZ2dsZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHRvZ2dsZVNpZGVuYXYoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMubGF5b3V0Q29uZi5zaWRlYmFyU3R5bGUgPT09ICdjbG9zZWQnKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmxheW91dC5wdWJsaXNoTGF5b3V0Q2hhbmdlKHtcclxuICAgICAgICAgICAgICAgIHNpZGViYXJTdHlsZTogJ2Z1bGwnXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMubGF5b3V0LnB1Ymxpc2hMYXlvdXRDaGFuZ2Uoe1xyXG4gICAgICAgICAgICBzaWRlYmFyU3R5bGU6ICdjbG9zZWQnXHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICB0b2dnbGVDb2xsYXBzZSgpIHtcclxuICAgICAgICAvLyBjb21wYWN0IC0tPiBmdWxsXHJcbiAgICAgICAgaWYgKHRoaXMubGF5b3V0Q29uZi5zaWRlYmFyU3R5bGUgPT09ICdjb21wYWN0Jykge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5sYXlvdXQucHVibGlzaExheW91dENoYW5nZSh7XHJcbiAgICAgICAgICAgICAgICBzaWRlYmFyU3R5bGU6ICdmdWxsJyxcclxuICAgICAgICAgICAgICAgIHNpZGViYXJDb21wYWN0VG9nZ2xlOiBmYWxzZVxyXG4gICAgICAgICAgICB9LCB7dHJhbnNpdGlvbkNsYXNzOiB0cnVlfSlcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vICogLS0+IGNvbXBhY3RcclxuICAgICAgICB0aGlzLmxheW91dC5wdWJsaXNoTGF5b3V0Q2hhbmdlKHtcclxuICAgICAgICAgICAgc2lkZWJhclN0eWxlOiAnY29tcGFjdCcsXHJcbiAgICAgICAgICAgIHNpZGViYXJDb21wYWN0VG9nZ2xlOiB0cnVlXHJcbiAgICAgICAgfSwge3RyYW5zaXRpb25DbGFzczogdHJ1ZX0pXHJcblxyXG4gICAgfVxyXG59XHJcbiJdfQ==