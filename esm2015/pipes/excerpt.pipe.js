import * as tslib_1 from "tslib";
import { Pipe } from "@angular/core";
let ExcerptPipe = class ExcerptPipe {
    transform(text, limit = 5) {
        if (text.length <= limit)
            return text;
        return text.substring(0, limit) + '...';
    }
};
ExcerptPipe = tslib_1.__decorate([
    Pipe({ name: 'excerpt' })
], ExcerptPipe);
export { ExcerptPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhjZXJwdC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsicGlwZXMvZXhjZXJwdC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUdwRCxJQUFhLFdBQVcsR0FBeEIsTUFBYSxXQUFXO0lBQ3BCLFNBQVMsQ0FBQyxJQUFZLEVBQUUsUUFBZ0IsQ0FBQztRQUNyQyxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksS0FBSztZQUNwQixPQUFPLElBQUksQ0FBQztRQUNoQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxHQUFHLEtBQUssQ0FBQztJQUM1QyxDQUFDO0NBQ0osQ0FBQTtBQU5ZLFdBQVc7SUFEdkIsSUFBSSxDQUFDLEVBQUMsSUFBSSxFQUFFLFNBQVMsRUFBQyxDQUFDO0dBQ1gsV0FBVyxDQU12QjtTQU5ZLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuXHJcbkBQaXBlKHtuYW1lOiAnZXhjZXJwdCd9KVxyXG5leHBvcnQgY2xhc3MgRXhjZXJwdFBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuICAgIHRyYW5zZm9ybSh0ZXh0OiBzdHJpbmcsIGxpbWl0OiBudW1iZXIgPSA1KSB7XHJcbiAgICAgICAgaWYgKHRleHQubGVuZ3RoIDw9IGxpbWl0KVxyXG4gICAgICAgICAgICByZXR1cm4gdGV4dDtcclxuICAgICAgICByZXR1cm4gdGV4dC5zdWJzdHJpbmcoMCwgbGltaXQpICsgJy4uLic7XHJcbiAgICB9XHJcbn1cclxuIl19