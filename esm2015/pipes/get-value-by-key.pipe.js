import * as tslib_1 from "tslib";
import { Pipe } from "@angular/core";
let GetValueByKeyPipe = class GetValueByKeyPipe {
    transform(value, id, property) {
        const filteredObj = value.find(item => {
            if (item.id !== undefined) {
                return item.id === id;
            }
            return false;
        });
        if (filteredObj) {
            return filteredObj[property];
        }
    }
};
GetValueByKeyPipe = tslib_1.__decorate([
    Pipe({
        name: "getValueByKey",
        pure: false
    })
], GetValueByKeyPipe);
export { GetValueByKeyPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2V0LXZhbHVlLWJ5LWtleS5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL2VncmV0LyIsInNvdXJjZXMiOlsicGlwZXMvZ2V0LXZhbHVlLWJ5LWtleS5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQU1wRCxJQUFhLGlCQUFpQixHQUE5QixNQUFhLGlCQUFpQjtJQUM1QixTQUFTLENBQUMsS0FBWSxFQUFFLEVBQVUsRUFBRSxRQUFnQjtRQUNsRCxNQUFNLFdBQVcsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3BDLElBQUksSUFBSSxDQUFDLEVBQUUsS0FBSyxTQUFTLEVBQUU7Z0JBQ3pCLE9BQU8sSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLENBQUM7YUFDdkI7WUFFRCxPQUFPLEtBQUssQ0FBQztRQUNmLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxXQUFXLEVBQUU7WUFDZixPQUFPLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUM5QjtJQUNILENBQUM7Q0FDRixDQUFBO0FBZFksaUJBQWlCO0lBSjdCLElBQUksQ0FBQztRQUNKLElBQUksRUFBRSxlQUFlO1FBQ3JCLElBQUksRUFBRSxLQUFLO0tBQ1osQ0FBQztHQUNXLGlCQUFpQixDQWM3QjtTQWRZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5cclxuQFBpcGUoe1xyXG4gIG5hbWU6IFwiZ2V0VmFsdWVCeUtleVwiLFxyXG4gIHB1cmU6IGZhbHNlXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBHZXRWYWx1ZUJ5S2V5UGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xyXG4gIHRyYW5zZm9ybSh2YWx1ZTogYW55W10sIGlkOiBudW1iZXIsIHByb3BlcnR5OiBzdHJpbmcpOiBhbnkge1xyXG4gICAgY29uc3QgZmlsdGVyZWRPYmogPSB2YWx1ZS5maW5kKGl0ZW0gPT4ge1xyXG4gICAgICBpZiAoaXRlbS5pZCAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgcmV0dXJuIGl0ZW0uaWQgPT09IGlkO1xyXG4gICAgICB9XHJcblxyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9KTtcclxuXHJcbiAgICBpZiAoZmlsdGVyZWRPYmopIHtcclxuICAgICAgcmV0dXJuIGZpbHRlcmVkT2JqW3Byb3BlcnR5XTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19