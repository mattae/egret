(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/animations'), require('@angular/core'), require('@angular/router'), require('@ngx-translate/core'), require('rxjs/operators'), require('rxjs'), require('@angular/common'), require('@angular/common/http'), require('@lamis/web-core'), require('@angular/flex-layout'), require('@angular/flex-layout/core'), require('@angular/material'), require('@alfresco/adf-core'), require('@angular/forms'), require('ngx-perfect-scrollbar')) :
    typeof define === 'function' && define.amd ? define('@lamis/egret', ['exports', '@angular/animations', '@angular/core', '@angular/router', '@ngx-translate/core', 'rxjs/operators', 'rxjs', '@angular/common', '@angular/common/http', '@lamis/web-core', '@angular/flex-layout', '@angular/flex-layout/core', '@angular/material', '@alfresco/adf-core', '@angular/forms', 'ngx-perfect-scrollbar'], factory) :
    (global = global || self, factory((global.lamis = global.lamis || {}, global.lamis.egret = {}), global.ng.animations, global.ng.core, global.ng.router, global.core$1, global.rxjs.operators, global.rxjs, global.ng.common, global.ng.common.http, global.webCore, global.ng['flex-layout'], global.ng['flex-layout'].core, global.ng.material, global.adfCore, global.ng.forms, global.ngxPerfectScrollbar));
}(this, function (exports, animations, core, router, core$1, operators, rxjs, common, http, webCore, flexLayout, core$2, material, adfCore, forms, ngxPerfectScrollbar) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __exportStar(m, exports) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
        if (m) return m.call(o);
        return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    var reusable = animations.animation([
        animations.style({
            opacity: "{{opacity}}",
            transform: "scale({{scale}}) translate3d({{x}}, {{y}}, {{z}})"
        }),
        animations.animate("{{duration}} {{delay}} cubic-bezier(0.0, 0.0, 0.2, 1)", animations.style("*"))
    ], {
        params: {
            duration: "200ms",
            delay: "0ms",
            opacity: "0",
            scale: "1",
            x: "0",
            y: "0",
            z: "0"
        }
    });
    var egretAnimations = [
        animations.trigger("animate", [animations.transition("void => *", [animations.useAnimation(reusable)])]),
        animations.trigger("fadeInOut", [
            animations.state("0", animations.style({
                opacity: 0,
                display: "none"
            })),
            animations.state("1", animations.style({
                opacity: 1,
                display: "block"
            })),
            animations.transition("0 => 1", animations.animate("300ms")),
            animations.transition("1 => 0", animations.animate("300ms"))
        ])
    ];

    function getQueryParam(prop) {
        var params = {};
        var search = decodeURIComponent(window.location.href.slice(window.location.href.indexOf('?') + 1));
        var definitions = search.split('&');
        definitions.forEach(function (val, key) {
            var parts = val.split('=', 2);
            params[parts[0]] = parts[1];
        });
        return (prop && prop in params) ? params[prop] : params;
    }

    var ThemeService = /** @class */ (function () {
        function ThemeService(document) {
            this.document = document;
            this.egretThemes = [{
                    "name": "egret-dark-purple",
                    "baseColor": "#9c27b0",
                    "isActive": false
                }, {
                    "name": "egret-dark-pink",
                    "baseColor": "#e91e63",
                    "isActive": false
                }, {
                    "name": "egret-blue",
                    "baseColor": "#03a9f4",
                    "isActive": true
                }, {
                    "name": "egret-navy",
                    "baseColor": "#10174c",
                    "isActive": false
                }];
        }
        // Invoked in AppComponent and apply 'activatedTheme' on startup
        ThemeService.prototype.applyMatTheme = function (r, themeName) {
            this.renderer = r;
            this.activatedTheme = this.egretThemes[2];
            // *********** ONLY FOR DEMO **********
            this.setThemeFromQuery();
            // ************************************
            // this.changeTheme(themeName);
            this.renderer.addClass(this.document.body, themeName);
        };
        ThemeService.prototype.changeTheme = function (prevTheme, themeName) {
            this.renderer.removeClass(this.document.body, prevTheme);
            this.renderer.addClass(this.document.body, themeName);
            this.flipActiveFlag(themeName);
        };
        ThemeService.prototype.flipActiveFlag = function (themeName) {
            var _this = this;
            this.egretThemes.forEach(function (t) {
                t.isActive = false;
                if (t.name === themeName) {
                    t.isActive = true;
                    _this.activatedTheme = t;
                }
            });
        };
        // *********** ONLY FOR DEMO **********
        ThemeService.prototype.setThemeFromQuery = function () {
            var themeStr = getQueryParam('theme');
            try {
                this.activatedTheme = JSON.parse(themeStr);
                this.flipActiveFlag(this.activatedTheme.name);
            }
            catch (e) {
            }
        };
        ThemeService.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] }
        ]; };
        ThemeService = __decorate([
            core.Injectable(),
            __param(0, core.Inject(common.DOCUMENT)),
            __metadata("design:paramtypes", [Object])
        ], ThemeService);
        return ThemeService;
    }());

    var LayoutService = /** @class */ (function () {
        function LayoutService(themeService, http, apiUrlConfig, accountService) {
            this.themeService = themeService;
            this.http = http;
            this.apiUrlConfig = apiUrlConfig;
            this.accountService = accountService;
            this.layoutConfSubject = new rxjs.BehaviorSubject(this.layoutConf);
            this.layoutConf$ = this.layoutConfSubject.asObservable();
            this.username = 'anonymous';
            this.fullWidthRoutes = ['shop'];
            this.setAppLayout();
        }
        LayoutService.prototype.setAppLayout = function () {
            //******** SET YOUR LAYOUT OPTIONS HERE *********
            this.layoutConf = {
                "navigationPos": "side",
                "sidebarStyle": "full",
                "sidebarColor": "white",
                "sidebarCompactToggle": false,
                "dir": "ltr",
                "useBreadcrumb": true,
                "topbarFixed": false,
                "topbarColor": "white",
                "matTheme": "egret-blue",
                "breadcrumb": "simple",
                "perfectScrollbar": true
            };
            // this.getLayoutConfig();
        };
        LayoutService.prototype.publishLayoutChange = function (lc, opt) {
            if (opt === void 0) { opt = {}; }
            if (this.layoutConf.matTheme !== lc.matTheme && lc.matTheme) {
                this.themeService.changeTheme(this.layoutConf.matTheme, lc.matTheme);
            }
            this.layoutConf = Object.assign(this.layoutConf, lc);
            this.layoutConfSubject.next(this.layoutConf);
            //this.saveLayoutConfig();
        };
        LayoutService.prototype.applyMatTheme = function (r) {
            this.themeService.applyMatTheme(r, this.layoutConf.matTheme);
        };
        LayoutService.prototype.adjustLayout = function (options) {
            var _this = this;
            if (options === void 0) { options = {}; }
            var sidebarStyle;
            this.isMobile = this.isSm();
            this.currentRoute = options.route || this.currentRoute;
            sidebarStyle = this.isMobile ? 'closed' : this.layoutConf.sidebarStyle;
            if (this.currentRoute) {
                this.fullWidthRoutes.forEach(function (route) {
                    if (_this.currentRoute.indexOf(route) !== -1) {
                        sidebarStyle = 'closed';
                    }
                });
            }
            this.publishLayoutChange({
                isMobile: this.isMobile,
                sidebarStyle: sidebarStyle
            });
        };
        LayoutService.prototype.isSm = function () {
            return window.matchMedia("(max-width: 959px)").matches;
        };
        LayoutService.prototype.getLayoutConfig = function () {
            var _this = this;
            this.accountService.fetch().subscribe(function (account) {
                _this.username = account.body.login;
                _this.http.get(_this.apiUrlConfig.SERVER_API_URL + ("api/layout-config/" + _this.username), { observe: 'body' }).subscribe(function (res) {
                    _this.layoutConf = res.config || _this.layoutConf;
                });
            });
        };
        LayoutService.prototype.saveLayoutConfig = function () {
            var _this = this;
            this.http.put(this.apiUrlConfig.SERVER_API_URL + ("api/layout-config/" + this.username), {
                config: this.layoutConf
            }, { observe: 'response' })
                .subscribe(function (config) {
                _this.layoutConf = config.body;
            });
        };
        LayoutService.ctorParameters = function () { return [
            { type: ThemeService },
            { type: http.HttpClient },
            { type: undefined, decorators: [{ type: core.Inject, args: [webCore.SERVER_API_URL_CONFIG,] }] },
            { type: webCore.AccountService }
        ]; };
        LayoutService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function LayoutService_Factory() { return new LayoutService(core.ɵɵinject(ThemeService), core.ɵɵinject(http.HttpClient), core.ɵɵinject(webCore.SERVER_API_URL_CONFIG), core.ɵɵinject(webCore.AccountService)); }, token: LayoutService, providedIn: "root" });
        LayoutService = __decorate([
            core.Injectable({
                providedIn: 'root'
            }),
            __param(2, core.Inject(webCore.SERVER_API_URL_CONFIG)),
            __metadata("design:paramtypes", [ThemeService,
                http.HttpClient, Object, webCore.AccountService])
        ], LayoutService);
        return LayoutService;
    }());

    var AdminLayoutComponent = /** @class */ (function () {
        function AdminLayoutComponent(router$1, translate, themeService, layout) {
            var _this = this;
            this.router = router$1;
            this.translate = translate;
            this.themeService = themeService;
            this.layout = layout;
            this.isModuleLoading = false;
            this.scrollConfig = {};
            this.layoutConf = {};
            // Close sidenav after route change in mobile
            this.routerEventSub = router$1.events.pipe(operators.filter(function (event) { return event instanceof router.NavigationEnd; }))
                .subscribe(function (routeChange) {
                _this.layout.adjustLayout({ route: routeChange.url });
            });
            // Translator init
            var browserLang = translate.getBrowserLang();
            translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
        }
        AdminLayoutComponent.prototype.ngOnInit = function () {
            var _this = this;
            // this.layoutConf = this.layout.layoutConf;
            this.layoutConfSub = this.layout.layoutConf$.subscribe(function (layoutConf) {
                _this.layoutConf = layoutConf;
            });
            // FOR MODULE LOADER FLAG
            this.moduleLoaderSub = this.router.events.subscribe(function (event) {
                if (event instanceof router.RouteConfigLoadStart || event instanceof router.ResolveStart) {
                    _this.isModuleLoading = true;
                }
                if (event instanceof router.RouteConfigLoadEnd || event instanceof router.ResolveEnd) {
                    _this.isModuleLoading = false;
                }
            });
        };
        AdminLayoutComponent.prototype.onResize = function (event) {
            this.layout.adjustLayout(event);
        };
        AdminLayoutComponent.prototype.ngAfterViewInit = function () {
        };
        AdminLayoutComponent.prototype.scrollToTop = function (selector) {
            if (document) {
                var element = document.querySelector(selector);
                element.scrollTop = 0;
            }
        };
        AdminLayoutComponent.prototype.ngOnDestroy = function () {
            if (this.moduleLoaderSub) {
                this.moduleLoaderSub.unsubscribe();
            }
            if (this.layoutConfSub) {
                this.layoutConfSub.unsubscribe();
            }
            if (this.routerEventSub) {
                this.routerEventSub.unsubscribe();
            }
        };
        AdminLayoutComponent.prototype.closeSidebar = function () {
            this.layout.publishLayoutChange({
                sidebarStyle: 'closed'
            });
        };
        AdminLayoutComponent.prototype.sidebarMouseenter = function (e) {
            // console.log(this.layoutConf);
            if (this.layoutConf.sidebarStyle === 'compact') {
                this.layout.publishLayoutChange({ sidebarStyle: 'full' }, { transitionClass: true });
            }
        };
        AdminLayoutComponent.prototype.sidebarMouseleave = function (e) {
            // console.log(this.layoutConf);
            if (this.layoutConf.sidebarStyle === 'full' &&
                this.layoutConf.sidebarCompactToggle) {
                this.layout.publishLayoutChange({ sidebarStyle: 'compact' }, { transitionClass: true });
            }
        };
        AdminLayoutComponent.ctorParameters = function () { return [
            { type: router.Router },
            { type: core$1.TranslateService },
            { type: ThemeService },
            { type: LayoutService }
        ]; };
        __decorate([
            core.HostListener('window:resize', ['$event']),
            __metadata("design:type", Function),
            __metadata("design:paramtypes", [Object]),
            __metadata("design:returntype", void 0)
        ], AdminLayoutComponent.prototype, "onResize", null);
        AdminLayoutComponent = __decorate([
            core.Component({
                selector: 'egret-admin-layout',
                template: "<div class=\"app-admin-wrap\" [dir]='layoutConf?.dir'>\r\n    <!-- Header for top navigation layout -->\r\n    <!-- ONLY REQUIRED FOR **TOP** NAVIGATION LAYOUT -->\r\n    <egret-header-top\r\n            *ngIf=\"layoutConf.navigationPos === 'top'\"\r\n            [notificPanel]=\"notificationPanel\">\r\n    </egret-header-top>\r\n    <!-- Main Container -->\r\n    <mat-sidenav-container\r\n            [dir]='layoutConf.dir'\r\n            class=\"app-admin-container app-side-nav-container mat-drawer-transition sidebar-{{layoutConf?.sidebarColor}} topbar-{{layoutConf?.topbarColor}}\"\r\n            [ngClass]=\"{\r\n    'navigation-top': layoutConf.navigationPos === 'top',\r\n    'sidebar-full': layoutConf.sidebarStyle === 'full',\r\n    'sidebar-compact': layoutConf.sidebarStyle === 'compact' && layoutConf.navigationPos === 'side',\r\n    'compact-toggle-active': layoutConf.sidebarCompactToggle,\r\n    'sidebar-compact-big': layoutConf.sidebarStyle === 'compact-big' && layoutConf.navigationPos === 'side',\r\n    'sidebar-opened': layoutConf.sidebarStyle !== 'closed' && layoutConf.navigationPos === 'side',\r\n    'sidebar-closed': layoutConf.sidebarStyle === 'closed',\r\n    'fixed-topbar': layoutConf.topbarFixed && layoutConf.navigationPos === 'side'\r\n  }\">\r\n        <!-- SIDEBAR -->\r\n        <!-- ONLY REQUIRED FOR **SIDE** NAVIGATION LAYOUT -->\r\n        <egret-sidebar-side\r\n                *ngIf=\"layoutConf.navigationPos === 'side'\"\r\n                (mouseenter)=\"sidebarMouseenter($event)\"\r\n                (mouseleave)=\"sidebarMouseleave($event)\">\r\n        </egret-sidebar-side>\r\n\r\n        <!-- Top navigation layout (navigation for mobile screen) -->\r\n        <!-- ONLY REQUIRED FOR **TOP** NAVIGATION MOBILE LAYOUT -->\r\n        <egret-sidebar-top *ngIf=\"layoutConf.navigationPos === 'top' && layoutConf.isMobile\"></egret-sidebar-top>\r\n\r\n        <!-- App content -->\r\n        <div class=\"main-content-wrap\" id=\"main-content-wrap\" [perfectScrollbar]=\"\"\r\n             [disabled]=\"layoutConf.topbarFixed || !layoutConf.perfectScrollbar\" style=\"display: block;\">\r\n            <!-- Header for side navigation layout -->\r\n            <!-- ONLY REQUIRED FOR **SIDE** NAVIGATION LAYOUT -->\r\n            <egret-header-side\r\n                    *ngIf=\"layoutConf.navigationPos === 'side'\"\r\n                    [notificPanel]=\"notificationPanel\">\r\n            </egret-header-side>\r\n\r\n            <div class=\"rightside-content-hold\" id=\"rightside-content-hold\" [perfectScrollbar]=\"scrollConfig\"\r\n                 [disabled]=\"!layoutConf.topbarFixed || !layoutConf.perfectScrollbar\">\r\n                <!-- View Loader -->\r\n                <div class=\"view-loader\" *ngIf=\"isModuleLoading\">\r\n                    <div class=\"spinner\">\r\n                        <div class=\"double-bounce1 mat-bg-accent\"></div>\r\n                        <div class=\"double-bounce2 mat-bg-primary\"></div>\r\n                    </div>\r\n                </div>\r\n                <!-- Breadcrumb -->\r\n                <egret-breadcrumb></egret-breadcrumb>\r\n                <!-- View outlet -->\r\n                <router-outlet></router-outlet>\r\n            </div>\r\n        </div>\r\n        <!-- View overlay for mobile navigation -->\r\n        <div class=\"sidebar-backdrop\"\r\n             [ngClass]=\"{'visible': layoutConf.sidebarStyle !== 'closed' && layoutConf.isMobile}\"\r\n             (click)=\"closeSidebar()\">\r\n        </div>\r\n\r\n        <!-- Notificaation bar -->\r\n        <mat-sidenav #notificationPanel mode=\"over\" class=\"\" position=\"end\">\r\n            <div class=\"nofication-panel\" fxLayout=\"column\">\r\n                <egret-notifications [notificPanel]=\"notificationPanel\"></egret-notifications>\r\n            </div>\r\n        </mat-sidenav>\r\n    </mat-sidenav-container>\r\n</div>\r\n\r\n\r\n<!-- Only for demo purpose -->\r\n<!-- Remove this from your production version -->\r\n"
            }),
            __metadata("design:paramtypes", [router.Router,
                core$1.TranslateService,
                ThemeService,
                LayoutService])
        ], AdminLayoutComponent);
        return AdminLayoutComponent;
    }());

    var AuthLayoutComponent = /** @class */ (function () {
        function AuthLayoutComponent() {
        }
        AuthLayoutComponent.prototype.ngOnInit = function () {
        };
        AuthLayoutComponent = __decorate([
            core.Component({
                selector: 'egret-auth-layout',
                template: "<router-outlet></router-outlet>"
            }),
            __metadata("design:paramtypes", [])
        ], AuthLayoutComponent);
        return AuthLayoutComponent;
    }());

    var GetValueByKeyPipe = /** @class */ (function () {
        function GetValueByKeyPipe() {
        }
        GetValueByKeyPipe.prototype.transform = function (value, id, property) {
            var filteredObj = value.find(function (item) {
                if (item.id !== undefined) {
                    return item.id === id;
                }
                return false;
            });
            if (filteredObj) {
                return filteredObj[property];
            }
        };
        GetValueByKeyPipe = __decorate([
            core.Pipe({
                name: "getValueByKey",
                pure: false
            })
        ], GetValueByKeyPipe);
        return GetValueByKeyPipe;
    }());

    var ExcerptPipe = /** @class */ (function () {
        function ExcerptPipe() {
        }
        ExcerptPipe.prototype.transform = function (text, limit) {
            if (limit === void 0) { limit = 5; }
            if (text.length <= limit)
                return text;
            return text.substring(0, limit) + '...';
        };
        ExcerptPipe = __decorate([
            core.Pipe({ name: 'excerpt' })
        ], ExcerptPipe);
        return ExcerptPipe;
    }());

    var RelativeTimePipe = /** @class */ (function () {
        function RelativeTimePipe() {
        }
        RelativeTimePipe.prototype.transform = function (value) {
            if (!(value instanceof Date))
                value = new Date(value);
            var seconds = Math.floor(((new Date()).getTime() - value.getTime()) / 1000);
            var interval = Math.floor(seconds / 31536000);
            if (interval > 1) {
                return interval + " years ago";
            }
            interval = Math.floor(seconds / 2592000);
            if (interval > 1) {
                return interval + " months ago";
            }
            interval = Math.floor(seconds / 86400);
            if (interval > 1) {
                return interval + " days ago";
            }
            interval = Math.floor(seconds / 3600);
            if (interval > 1) {
                return interval + " hours ago";
            }
            interval = Math.floor(seconds / 60);
            if (interval > 1) {
                return interval + " minutes ago";
            }
            return Math.floor(seconds) + " seconds ago";
        };
        RelativeTimePipe = __decorate([
            core.Pipe({ name: 'relativeTime' })
        ], RelativeTimePipe);
        return RelativeTimePipe;
    }());

    var RoutePartsService = /** @class */ (function () {
        function RoutePartsService(router) {
            this.router = router;
        }
        RoutePartsService.prototype.ngOnInit = function () {
        };
        RoutePartsService.prototype.generateRouteParts = function (snapshot) {
            var routeParts = [];
            if (snapshot) {
                if (snapshot.firstChild) {
                    routeParts = routeParts.concat(this.generateRouteParts(snapshot.firstChild));
                }
                if (snapshot.data['title'] && snapshot.url.length) {
                    // console.log(snapshot.data['title'], snapshot.url)
                    routeParts.push({
                        title: snapshot.data['title'],
                        breadcrumb: snapshot.data['breadcrumb'],
                        url: snapshot.url[0].path,
                        urlSegments: snapshot.url,
                        params: snapshot.params
                    });
                }
            }
            return routeParts;
        };
        RoutePartsService.ctorParameters = function () { return [
            { type: router.Router }
        ]; };
        RoutePartsService = __decorate([
            core.Injectable(),
            __metadata("design:paramtypes", [router.Router])
        ], RoutePartsService);
        return RoutePartsService;
    }());

    var NavigationService = /** @class */ (function () {
        function NavigationService(menuService) {
            this.menuService = menuService;
            // This title will appear if any icon type item is present in menu.
            this.iconTypeMenuTitle = 'Frequently Accessed';
            this.menuItems$ = menuService.getMenus();
        }
        NavigationService.prototype.publishNavigationChange = function (menuType) {
            /*switch (menuType) {
                case "separator-menu":
                    this.menuItems.next(this.separatorMenu);
                    break;
                case "icon-menu":
                    this.menuItems.next(this.iconMenu);
                    break;
                default:
                    this.menuItems.next(this.plainMenu);
            }*/
        };
        NavigationService.ctorParameters = function () { return [
            { type: webCore.MenuService }
        ]; };
        NavigationService = __decorate([
            core.Injectable(),
            __metadata("design:paramtypes", [webCore.MenuService])
        ], NavigationService);
        return NavigationService;
    }());

    var MatchMediaService = /** @class */ (function () {
        function MatchMediaService(media) {
            this.media = media;
            this.onMediaChange = new rxjs.BehaviorSubject('');
            this.activeMediaQuery = '';
            this.init();
        }
        MatchMediaService.prototype.init = function () {
            var _this = this;
            this.media
                .asObservable()
                .subscribe(function (change) {
                if (_this.activeMediaQuery !== change[0].mqAlias) {
                    _this.activeMediaQuery = change[0].mqAlias;
                    _this.onMediaChange.next(change[0].mqAlias);
                }
            });
        };
        MatchMediaService.ctorParameters = function () { return [
            { type: flexLayout.MediaObserver }
        ]; };
        MatchMediaService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function MatchMediaService_Factory() { return new MatchMediaService(core.ɵɵinject(core$2.MediaObserver)); }, token: MatchMediaService, providedIn: "root" });
        MatchMediaService = __decorate([
            core.Injectable({
                providedIn: 'root'
            }),
            __metadata("design:paramtypes", [flexLayout.MediaObserver])
        ], MatchMediaService);
        return MatchMediaService;
    }());

    var AppDropdownDirective = /** @class */ (function () {
        function AppDropdownDirective(router) {
            this.router = router;
            this.navlinks = [];
        }
        AppDropdownDirective.prototype.closeOtherLinks = function (openLink) {
            this.navlinks.forEach(function (link) {
                if (link !== openLink) {
                    link.open = false;
                }
            });
        };
        AppDropdownDirective.prototype.addLink = function (link) {
            this.navlinks.push(link);
        };
        AppDropdownDirective.prototype.removeGroup = function (link) {
            var index = this.navlinks.indexOf(link);
            if (index !== -1) {
                this.navlinks.splice(index, 1);
            }
        };
        AppDropdownDirective.prototype.getUrl = function () {
            return this.router.url;
        };
        AppDropdownDirective.prototype.ngOnInit = function () {
            var _this = this;
            this._router = this.router.events.pipe(operators.filter(function (event) { return event instanceof router.NavigationEnd; })).subscribe(function (event) {
                _this.navlinks.forEach(function (link) {
                    if (link.group) {
                        var routeUrl = _this.getUrl();
                        var currentUrl = routeUrl.split('/');
                        if (currentUrl.indexOf(link.group) > 0) {
                            link.open = true;
                            _this.closeOtherLinks(link);
                        }
                    }
                });
            });
        };
        AppDropdownDirective.ctorParameters = function () { return [
            { type: router.Router }
        ]; };
        AppDropdownDirective = __decorate([
            core.Directive({
                selector: '[appDropdown]'
            }),
            __metadata("design:paramtypes", [router.Router])
        ], AppDropdownDirective);
        return AppDropdownDirective;
    }());

    var DropdownLinkDirective = /** @class */ (function () {
        function DropdownLinkDirective(nav) {
            this.nav = nav;
        }
        Object.defineProperty(DropdownLinkDirective.prototype, "open", {
            get: function () {
                return this._open;
            },
            set: function (value) {
                this._open = value;
                if (value) {
                    this.nav.closeOtherLinks(this);
                }
            },
            enumerable: true,
            configurable: true
        });
        DropdownLinkDirective.prototype.ngOnInit = function () {
            this.nav.addLink(this);
        };
        DropdownLinkDirective.prototype.ngOnDestroy = function () {
            this.nav.removeGroup(this);
        };
        DropdownLinkDirective.prototype.toggle = function () {
            this.open = !this.open;
        };
        DropdownLinkDirective.ctorParameters = function () { return [
            { type: AppDropdownDirective, decorators: [{ type: core.Inject, args: [AppDropdownDirective,] }] }
        ]; };
        __decorate([
            core.Input(),
            __metadata("design:type", Object)
        ], DropdownLinkDirective.prototype, "group", void 0);
        __decorate([
            core.HostBinding('class.open'),
            core.Input(),
            __metadata("design:type", Boolean),
            __metadata("design:paramtypes", [Boolean])
        ], DropdownLinkDirective.prototype, "open", null);
        DropdownLinkDirective = __decorate([
            core.Directive({
                selector: '[appDropdownLink]'
            }),
            __param(0, core.Inject(AppDropdownDirective)),
            __metadata("design:paramtypes", [AppDropdownDirective])
        ], DropdownLinkDirective);
        return DropdownLinkDirective;
    }());

    var DropdownAnchorDirective = /** @class */ (function () {
        function DropdownAnchorDirective(navlink) {
            this.navlink = navlink;
        }
        DropdownAnchorDirective.prototype.onClick = function (e) {
            this.navlink.toggle();
        };
        DropdownAnchorDirective.ctorParameters = function () { return [
            { type: DropdownLinkDirective, decorators: [{ type: core.Inject, args: [DropdownLinkDirective,] }] }
        ]; };
        __decorate([
            core.HostListener('click', ['$event']),
            __metadata("design:type", Function),
            __metadata("design:paramtypes", [Object]),
            __metadata("design:returntype", void 0)
        ], DropdownAnchorDirective.prototype, "onClick", null);
        DropdownAnchorDirective = __decorate([
            core.Directive({
                selector: '[appDropdownToggle]'
            }),
            __param(0, core.Inject(DropdownLinkDirective)),
            __metadata("design:paramtypes", [DropdownLinkDirective])
        ], DropdownAnchorDirective);
        return DropdownAnchorDirective;
    }());

    var EgretSideNavToggleDirective = /** @class */ (function () {
        function EgretSideNavToggleDirective(media, sideNav) {
            this.media = media;
            this.sideNav = sideNav;
        }
        EgretSideNavToggleDirective.prototype.ngOnInit = function () {
            this.initSideNav();
        };
        EgretSideNavToggleDirective.prototype.ngOnDestroy = function () {
            if (this.screenSizeWatcher) {
                this.screenSizeWatcher.unsubscribe();
            }
        };
        EgretSideNavToggleDirective.prototype.updateSidenav = function () {
            var self = this;
            setTimeout(function () {
                self.sideNav.opened = !self.isMobile;
                self.sideNav.mode = self.isMobile ? 'over' : 'side';
            });
        };
        EgretSideNavToggleDirective.prototype.initSideNav = function () {
            var _this = this;
            this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
            // console.log(this.isMobile)
            this.updateSidenav();
            this.screenSizeWatcher = this.media.asObservable().subscribe(function (change) {
                _this.isMobile = (change[0].mqAlias == 'xs') || (change[0].mqAlias == 'sm');
                _this.updateSidenav();
            });
        };
        EgretSideNavToggleDirective.ctorParameters = function () { return [
            { type: flexLayout.MediaObserver },
            { type: material.MatSidenav, decorators: [{ type: core.Host }, { type: core.Self }, { type: core.Optional }] }
        ]; };
        EgretSideNavToggleDirective = __decorate([
            core.Directive({
                selector: '[EgretSideNavToggle]'
            }),
            __param(1, core.Host()), __param(1, core.Self()), __param(1, core.Optional()),
            __metadata("design:paramtypes", [flexLayout.MediaObserver,
                material.MatSidenav])
        ], EgretSideNavToggleDirective);
        return EgretSideNavToggleDirective;
    }());

    var EgretSidenavHelperService = /** @class */ (function () {
        function EgretSidenavHelperService() {
            this.sidenavList = [];
        }
        EgretSidenavHelperService.prototype.setSidenav = function (id, sidenav) {
            this.sidenavList[id] = sidenav;
        };
        EgretSidenavHelperService.prototype.getSidenav = function (id) {
            return this.sidenavList[id];
        };
        EgretSidenavHelperService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function EgretSidenavHelperService_Factory() { return new EgretSidenavHelperService(); }, token: EgretSidenavHelperService, providedIn: "root" });
        EgretSidenavHelperService = __decorate([
            core.Injectable({
                providedIn: "root"
            }),
            __metadata("design:paramtypes", [])
        ], EgretSidenavHelperService);
        return EgretSidenavHelperService;
    }());

    var EgretSidenavHelperDirective = /** @class */ (function () {
        function EgretSidenavHelperDirective(matchMediaService, egretSidenavHelperService, matSidenav, media) {
            this.matchMediaService = matchMediaService;
            this.egretSidenavHelperService = egretSidenavHelperService;
            this.matSidenav = matSidenav;
            this.media = media;
            // Set the default value
            this.isOpen = true;
            this.unsubscribeAll = new rxjs.Subject();
        }
        EgretSidenavHelperDirective.prototype.ngOnInit = function () {
            var _this = this;
            this.egretSidenavHelperService.setSidenav(this.id, this.matSidenav);
            if (this.media.isActive(this.isOpenBreakpoint)) {
                this.isOpen = true;
                this.matSidenav.mode = 'side';
                this.matSidenav.toggle(true);
            }
            else {
                this.isOpen = false;
                this.matSidenav.mode = 'over';
                this.matSidenav.toggle(false);
            }
            this.matchMediaService.onMediaChange
                .pipe(operators.takeUntil(this.unsubscribeAll))
                .subscribe(function () {
                if (_this.media.isActive(_this.isOpenBreakpoint)) {
                    _this.isOpen = true;
                    _this.matSidenav.mode = 'side';
                    _this.matSidenav.toggle(true);
                }
                else {
                    _this.isOpen = false;
                    _this.matSidenav.mode = 'over';
                    _this.matSidenav.toggle(false);
                }
            });
        };
        EgretSidenavHelperDirective.prototype.ngOnDestroy = function () {
            this.unsubscribeAll.next();
            this.unsubscribeAll.complete();
        };
        EgretSidenavHelperDirective.ctorParameters = function () { return [
            { type: MatchMediaService },
            { type: EgretSidenavHelperService },
            { type: material.MatSidenav },
            { type: flexLayout.MediaObserver }
        ]; };
        __decorate([
            core.HostBinding('class.is-open'),
            __metadata("design:type", Boolean)
        ], EgretSidenavHelperDirective.prototype, "isOpen", void 0);
        __decorate([
            core.Input('egretSidenavHelper'),
            __metadata("design:type", String)
        ], EgretSidenavHelperDirective.prototype, "id", void 0);
        __decorate([
            core.Input('isOpen'),
            __metadata("design:type", String)
        ], EgretSidenavHelperDirective.prototype, "isOpenBreakpoint", void 0);
        EgretSidenavHelperDirective = __decorate([
            core.Directive({
                selector: '[egretSidenavHelper]'
            }),
            __metadata("design:paramtypes", [MatchMediaService,
                EgretSidenavHelperService,
                material.MatSidenav,
                flexLayout.MediaObserver])
        ], EgretSidenavHelperDirective);
        return EgretSidenavHelperDirective;
    }());
    var EgretSidenavTogglerDirective = /** @class */ (function () {
        function EgretSidenavTogglerDirective(egretSidenavHelperService) {
            this.egretSidenavHelperService = egretSidenavHelperService;
        }
        EgretSidenavTogglerDirective.prototype.onClick = function () {
            // console.log(this.egretSidenavHelperService.getSidenav(this.id))
            this.egretSidenavHelperService.getSidenav(this.id).toggle();
        };
        EgretSidenavTogglerDirective.ctorParameters = function () { return [
            { type: EgretSidenavHelperService }
        ]; };
        __decorate([
            core.Input('egretSidenavToggler'),
            __metadata("design:type", Object)
        ], EgretSidenavTogglerDirective.prototype, "id", void 0);
        __decorate([
            core.HostListener('click'),
            __metadata("design:type", Function),
            __metadata("design:paramtypes", []),
            __metadata("design:returntype", void 0)
        ], EgretSidenavTogglerDirective.prototype, "onClick", null);
        EgretSidenavTogglerDirective = __decorate([
            core.Directive({
                selector: '[egretSidenavToggler]'
            }),
            __metadata("design:paramtypes", [EgretSidenavHelperService])
        ], EgretSidenavTogglerDirective);
        return EgretSidenavTogglerDirective;
    }());

    var FontSizeDirective = /** @class */ (function () {
        function FontSizeDirective(fontSize, el) {
            this.fontSize = fontSize;
            this.el = el;
        }
        FontSizeDirective.prototype.ngOnInit = function () {
            this.el.nativeElement.fontSize = this.fontSize;
        };
        FontSizeDirective.ctorParameters = function () { return [
            { type: String, decorators: [{ type: core.Attribute, args: ['fontSize',] }] },
            { type: core.ElementRef }
        ]; };
        FontSizeDirective = __decorate([
            core.Directive({ selector: '[fontSize]' }),
            __param(0, core.Attribute('fontSize')),
            __metadata("design:paramtypes", [String, core.ElementRef])
        ], FontSizeDirective);
        return FontSizeDirective;
    }());

    var ScrollToDirective = /** @class */ (function () {
        function ScrollToDirective(elmID, el) {
            this.elmID = elmID;
            this.el = el;
        }
        ScrollToDirective_1 = ScrollToDirective;
        ScrollToDirective.prototype.ngOnInit = function () {
        };
        ScrollToDirective.currentYPosition = function () {
            // Firefox, Chrome, Opera, Safari
            if (self.pageYOffset)
                return self.pageYOffset;
            // Internet Explorer 6 - standards mode
            if (document.documentElement && document.documentElement.scrollTop)
                return document.documentElement.scrollTop;
            // Internet Explorer 6, 7 and 8
            if (document.body.scrollTop)
                return document.body.scrollTop;
            return 0;
        };
        ;
        ScrollToDirective.elmYPosition = function (eID) {
            var elm = document.getElementById(eID);
            var y = elm.offsetTop;
            var node = elm;
            while (node.offsetParent && node.offsetParent != document.body) {
                node = node.offsetParent;
                y += node.offsetTop;
            }
            return y;
        };
        ;
        ScrollToDirective.prototype.smoothScroll = function () {
            var i;
            if (!this.elmID)
                return;
            var startY = ScrollToDirective_1.currentYPosition();
            var stopY = ScrollToDirective_1.elmYPosition(this.elmID);
            var distance = stopY > startY ? stopY - startY : startY - stopY;
            if (distance < 100) {
                scrollTo(0, stopY);
                return;
            }
            var speed = Math.round(distance / 50);
            if (speed >= 20)
                speed = 20;
            var step = Math.round(distance / 25);
            var leapY = stopY > startY ? startY + step : startY - step;
            var timer = 0;
            if (stopY > startY) {
                for (i = startY; i < stopY; i += step) {
                    setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                    leapY += step;
                    if (leapY > stopY)
                        leapY = stopY;
                    timer++;
                }
                return;
            }
            for (i = startY; i > stopY; i -= step) {
                setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                leapY -= step;
                if (leapY < stopY)
                    leapY = stopY;
                timer++;
            }
            return false;
        };
        ;
        var ScrollToDirective_1;
        ScrollToDirective.ctorParameters = function () { return [
            { type: String, decorators: [{ type: core.Attribute, args: ['scrollTo',] }] },
            { type: core.ElementRef }
        ]; };
        __decorate([
            core.HostListener('click', ['$event']),
            __metadata("design:type", Function),
            __metadata("design:paramtypes", []),
            __metadata("design:returntype", void 0)
        ], ScrollToDirective.prototype, "smoothScroll", null);
        ScrollToDirective = ScrollToDirective_1 = __decorate([
            core.Directive({ selector: '[scrollTo]' }),
            __param(0, core.Attribute('scrollTo')),
            __metadata("design:paramtypes", [String, core.ElementRef])
        ], ScrollToDirective);
        return ScrollToDirective;
    }());

    var EgretSidebarHelperService = /** @class */ (function () {
        function EgretSidebarHelperService() {
            this.sidebarList = [];
        }
        EgretSidebarHelperService.prototype.setSidebar = function (name, sidebar) {
            this.sidebarList[name] = sidebar;
        };
        EgretSidebarHelperService.prototype.getSidebar = function (name) {
            return this.sidebarList[name];
        };
        EgretSidebarHelperService.prototype.removeSidebar = function (name) {
            if (!this.sidebarList[name]) {
                console.warn("The sidebar with name '" + name + "' doesn't exist.");
            }
            // remove sidebar
            delete this.sidebarList[name];
        };
        EgretSidebarHelperService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function EgretSidebarHelperService_Factory() { return new EgretSidebarHelperService(); }, token: EgretSidebarHelperService, providedIn: "root" });
        EgretSidebarHelperService = __decorate([
            core.Injectable({
                providedIn: "root"
            }),
            __metadata("design:paramtypes", [])
        ], EgretSidebarHelperService);
        return EgretSidebarHelperService;
    }());

    var EgretSidebarComponent = /** @class */ (function () {
        function EgretSidebarComponent(matchMediaService, mediaObserver, sidebarHelperService, _renderer, _elementRef, cdr) {
            this.matchMediaService = matchMediaService;
            this.mediaObserver = mediaObserver;
            this.sidebarHelperService = sidebarHelperService;
            this._renderer = _renderer;
            this._elementRef = _elementRef;
            this.cdr = cdr;
            this.backdrop = null;
            this.lockedBreakpoint = "gt-sm";
            this.unsubscribeAll = new rxjs.Subject();
        }
        EgretSidebarComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.sidebarHelperService.setSidebar(this.name, this);
            if (this.mediaObserver.isActive(this.lockedBreakpoint)) {
                this.sidebarLockedOpen = true;
                this.opened = true;
            }
            else {
                this.sidebarLockedOpen = false;
                this.opened = false;
            }
            this.matchMediaService.onMediaChange
                .pipe(operators.takeUntil(this.unsubscribeAll))
                .subscribe(function () {
                // console.log("medua sub");
                if (_this.mediaObserver.isActive(_this.lockedBreakpoint)) {
                    _this.sidebarLockedOpen = true;
                    _this.opened = true;
                }
                else {
                    _this.sidebarLockedOpen = false;
                    _this.opened = false;
                }
            });
        };
        EgretSidebarComponent.prototype.open = function () {
            this.opened = true;
            if (!this.sidebarLockedOpen && !this.backdrop) {
                this.showBackdrop();
            }
        };
        EgretSidebarComponent.prototype.close = function () {
            this.opened = false;
            this.hideBackdrop();
        };
        EgretSidebarComponent.prototype.toggle = function () {
            if (this.opened) {
                this.close();
            }
            else {
                this.open();
            }
        };
        EgretSidebarComponent.prototype.showBackdrop = function () {
            var _this = this;
            this.backdrop = this._renderer.createElement("div");
            this.backdrop.classList.add("egret-sidebar-overlay");
            this._renderer.appendChild(this._elementRef.nativeElement.parentElement, this.backdrop);
            // Close sidebar onclick
            this.backdrop.addEventListener("click", function () {
                _this.close();
            });
            this.cdr.markForCheck();
        };
        EgretSidebarComponent.prototype.hideBackdrop = function () {
            if (this.backdrop) {
                this.backdrop.parentNode.removeChild(this.backdrop);
                this.backdrop = null;
            }
            this.cdr.markForCheck();
        };
        EgretSidebarComponent.prototype.ngOnDestroy = function () {
            this.unsubscribeAll.next();
            this.unsubscribeAll.complete();
            this.sidebarHelperService.removeSidebar(this.name);
        };
        EgretSidebarComponent.ctorParameters = function () { return [
            { type: MatchMediaService },
            { type: flexLayout.MediaObserver },
            { type: EgretSidebarHelperService },
            { type: core.Renderer2 },
            { type: core.ElementRef },
            { type: core.ChangeDetectorRef }
        ]; };
        __decorate([
            core.Input(),
            __metadata("design:type", String)
        ], EgretSidebarComponent.prototype, "name", void 0);
        __decorate([
            core.Input(),
            core.HostBinding("class.position-right"),
            __metadata("design:type", Boolean)
        ], EgretSidebarComponent.prototype, "right", void 0);
        __decorate([
            core.HostBinding("class.open"),
            __metadata("design:type", Boolean)
        ], EgretSidebarComponent.prototype, "opened", void 0);
        __decorate([
            core.HostBinding("class.sidebar-locked-open"),
            __metadata("design:type", Boolean)
        ], EgretSidebarComponent.prototype, "sidebarLockedOpen", void 0);
        __decorate([
            core.HostBinding("class.is-over"),
            __metadata("design:type", Boolean)
        ], EgretSidebarComponent.prototype, "isOver", void 0);
        EgretSidebarComponent = __decorate([
            core.Component({
                selector: 'egret-sidebar',
                template: "<div>\r\n  <ng-content></ng-content>\r\n</div>",
                styles: [""]
            }),
            __metadata("design:paramtypes", [MatchMediaService,
                flexLayout.MediaObserver,
                EgretSidebarHelperService,
                core.Renderer2,
                core.ElementRef,
                core.ChangeDetectorRef])
        ], EgretSidebarComponent);
        return EgretSidebarComponent;
    }());
    var EgretSidebarTogglerDirective = /** @class */ (function () {
        function EgretSidebarTogglerDirective(egretSidebarHelperService) {
            this.egretSidebarHelperService = egretSidebarHelperService;
        }
        EgretSidebarTogglerDirective.prototype.onClick = function () {
            this.egretSidebarHelperService.getSidebar(this.id).toggle();
        };
        EgretSidebarTogglerDirective.ctorParameters = function () { return [
            { type: EgretSidebarHelperService }
        ]; };
        __decorate([
            core.Input("egretSidebarToggler"),
            __metadata("design:type", Object)
        ], EgretSidebarTogglerDirective.prototype, "id", void 0);
        __decorate([
            core.HostListener("click"),
            __metadata("design:type", Function),
            __metadata("design:paramtypes", []),
            __metadata("design:returntype", void 0)
        ], EgretSidebarTogglerDirective.prototype, "onClick", null);
        EgretSidebarTogglerDirective = __decorate([
            core.Directive({
                selector: "[egretSidebarToggler]"
            }),
            __metadata("design:paramtypes", [EgretSidebarHelperService])
        ], EgretSidebarTogglerDirective);
        return EgretSidebarTogglerDirective;
    }());

    var directives = [
        FontSizeDirective,
        ScrollToDirective,
        AppDropdownDirective,
        DropdownAnchorDirective,
        DropdownLinkDirective,
        EgretSideNavToggleDirective,
        EgretSidenavHelperDirective,
        EgretSidenavTogglerDirective,
        EgretSidebarTogglerDirective
    ];
    var SharedDirectivesModule = /** @class */ (function () {
        function SharedDirectivesModule() {
        }
        SharedDirectivesModule = __decorate([
            core.NgModule({
                imports: [
                    common.CommonModule
                ],
                declarations: directives,
                exports: directives
            })
        ], SharedDirectivesModule);
        return SharedDirectivesModule;
    }());

    var BreadcrumbComponent = /** @class */ (function () {
        // public isEnabled: boolean = true;
        function BreadcrumbComponent(router$1, routePartsService, activeRoute, layout) {
            var _this = this;
            this.router = router$1;
            this.routePartsService = routePartsService;
            this.activeRoute = activeRoute;
            this.layout = layout;
            this.routerEventSub = this.router.events
                .pipe(operators.filter(function (event) { return event instanceof router.NavigationEnd; }))
                .subscribe(function (routeChange) {
                _this.routeParts = _this.routePartsService.generateRouteParts(_this.activeRoute.snapshot);
                // generate url from parts
                _this.routeParts.reverse().map(function (item, i) {
                    item.breadcrumb = _this.parseText(item);
                    item.urlSegments.forEach(function (urlSegment, j) {
                        if (j === 0)
                            return item.url = "" + urlSegment.path;
                        item.url += "/" + urlSegment.path;
                    });
                    if (i === 0) {
                        return item;
                    }
                    // prepend previous part to current part
                    item.url = _this.routeParts[i - 1].url + "/" + item.url;
                    return item;
                });
            });
        }
        BreadcrumbComponent.prototype.ngOnInit = function () {
        };
        BreadcrumbComponent.prototype.ngOnDestroy = function () {
            if (this.routerEventSub) {
                this.routerEventSub.unsubscribe();
            }
        };
        BreadcrumbComponent.prototype.parseText = function (part) {
            if (!part.breadcrumb) {
                return '';
            }
            part.breadcrumb = part.breadcrumb.replace(/{{([^{}]*)}}/g, function (a, b) {
                var r = part.params[b];
                return typeof r === 'string' ? r : a;
            });
            return part.breadcrumb;
        };
        BreadcrumbComponent.ctorParameters = function () { return [
            { type: router.Router },
            { type: RoutePartsService },
            { type: router.ActivatedRoute },
            { type: LayoutService }
        ]; };
        BreadcrumbComponent = __decorate([
            core.Component({
                selector: 'egret-breadcrumb',
                template: "<ng-container *ngIf=\"routeParts && routeParts.length > 0\">\r\n    <div class=\"breadcrumb-bar\" *ngIf=\"layout.layoutConf.useBreadcrumb && layout.layoutConf.breadcrumb === 'simple'\">\r\n        <ul class=\"breadcrumb\">\r\n            <li *ngFor=\"let part of routeParts\">\r\n                <a routerLink=\"/{{part.url}}\">{{part.breadcrumb | translate}}</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n\r\n    <div class=\"breadcrumb-title\" *ngIf=\"layout.layoutConf.useBreadcrumb && layout.layoutConf.breadcrumb === 'title'\">\r\n        <h1 class=\"bc-title\">{{routeParts[routeParts.length - 1]?.breadcrumb | translate}}</h1>\r\n        <ul class=\"breadcrumb\" *ngIf=\"routeParts.length > 1\">\r\n            <li *ngFor=\"let part of routeParts\">\r\n                <a routerLink=\"/{{part.url}}\" class=\"text-muted\">{{part.breadcrumb | translate}}</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</ng-container>\r\n",
                styles: [""]
            }),
            __metadata("design:paramtypes", [router.Router,
                RoutePartsService,
                router.ActivatedRoute,
                LayoutService])
        ], BreadcrumbComponent);
        return BreadcrumbComponent;
    }());

    var HeaderSideComponent = /** @class */ (function () {
        function HeaderSideComponent(themeService, layout, translate, renderer) {
            this.themeService = themeService;
            this.layout = layout;
            this.translate = translate;
            this.renderer = renderer;
            this.availableLangs = [{
                    name: 'EN',
                    code: 'en',
                    flag: 'flag-icon-us'
                }];
            this.currentLang = this.availableLangs[0];
            this.openCustomizer = false;
        }
        HeaderSideComponent.prototype.ngOnInit = function () {
            this.egretThemes = this.themeService.egretThemes;
            this.layoutConf = this.layout.layoutConf;
            this.translate.use(this.currentLang.code);
        };
        HeaderSideComponent.prototype.setLang = function (lng) {
            this.currentLang = lng;
            this.translate.use(lng.code);
        };
        HeaderSideComponent.prototype.changeTheme = function (theme) {
            // this.themeService.changeTheme(theme);
        };
        HeaderSideComponent.prototype.toggleNotific = function () {
            this.notificPanel.toggle();
        };
        HeaderSideComponent.prototype.toggleSidenav = function () {
            if (this.layoutConf.sidebarStyle === 'closed') {
                return this.layout.publishLayoutChange({
                    sidebarStyle: 'full'
                });
            }
            this.layout.publishLayoutChange({
                sidebarStyle: 'closed'
            });
        };
        HeaderSideComponent.prototype.toggleCollapse = function () {
            // compact --> full
            if (this.layoutConf.sidebarStyle === 'compact') {
                return this.layout.publishLayoutChange({
                    sidebarStyle: 'full',
                    sidebarCompactToggle: false
                }, { transitionClass: true });
            }
            // * --> compact
            this.layout.publishLayoutChange({
                sidebarStyle: 'compact',
                sidebarCompactToggle: true
            }, { transitionClass: true });
        };
        HeaderSideComponent.ctorParameters = function () { return [
            { type: ThemeService },
            { type: LayoutService },
            { type: core$1.TranslateService },
            { type: core.Renderer2 }
        ]; };
        __decorate([
            core.Input(),
            __metadata("design:type", Object)
        ], HeaderSideComponent.prototype, "notificPanel", void 0);
        HeaderSideComponent = __decorate([
            core.Component({
                selector: 'egret-header-side',
                template: "<mat-toolbar class=\"topbar\">\r\n    <!-- Sidenav toggle button -->\r\n    <button\r\n            *ngIf=\"layoutConf.sidebarStyle !== 'compact'\"\r\n            mat-icon-button\r\n            id=\"sidenavToggle\"\r\n            (click)=\"toggleSidenav()\"\r\n            matTooltip=\"Toggle Hide/Open\"\r\n    >\r\n        <mat-icon>menu</mat-icon>\r\n    </button>\r\n\r\n    <!-- Search form -->\r\n    <!-- <div fxFlex fxHide.lt-sm=\"true\" class=\"search-bar\">\r\n      <form class=\"top-search-form\">\r\n        <mat-icon role=\"img\">search</mat-icon>\r\n        <input autofocus=\"true\" placeholder=\"Search\" type=\"text\" />\r\n      </form>\r\n    </div> -->\r\n\r\n    <span fxFlex></span>\r\n    <!-- Language Switcher -->\r\n   <!-- <button mat-button [matMenuTriggerFor]=\"menu\">\r\n        <span class=\"flag-icon {{currentLang.flag}} mr-05\"></span>\r\n        <span>{{currentLang.name}}</span>\r\n    </button>\r\n    <mat-menu #menu=\"matMenu\">\r\n        <button mat-menu-item *ngFor=\"let lang of availableLangs\" (click)=\"setLang(lang)\">\r\n            <span class=\"flag-icon mr-05 {{lang.flag}}\"></span>\r\n            <span>{{lang.name}}</span>\r\n        </button>\r\n    </mat-menu>\r\n    \\-->\r\n    <!-- Open \"views/search-view/result-page.component\" to understand how to subscribe to input field value -->\r\n    <egret-customizer [isCustomizerOpen]=\"openCustomizer\"\r\n                      (customizerClosed)=\"openCustomizer = false\">\r\n    </egret-customizer>\r\n    <!-- Notification toggle button -->\r\n    <button\r\n            mat-icon-button\r\n            matTooltip=\"Notifications\"\r\n            (click)=\"toggleNotific()\"\r\n            [style.overflow]=\"'visible'\"\r\n            class=\"topbar-button-right\"\r\n    >\r\n        <mat-icon>notifications</mat-icon>\r\n        <span class=\"notification-number mat-bg-warn\">3</span>\r\n    </button>\r\n    <!-- Top left user menu -->\r\n    <button\r\n            mat-icon-button\r\n            [matMenuTriggerFor]=\"accountMenu\"\r\n            class=\"topbar-button-right img-button\">\r\n        <img src=\"assets/images/face-7.jpg\" alt=\"\"/>\r\n    </button>\r\n\r\n    <mat-menu #accountMenu=\"matMenu\">\r\n        <button mat-menu-item [routerLink]=\"['/profile/overview']\">\r\n            <mat-icon>account_box</mat-icon>\r\n            <span>Profile</span>\r\n        </button>\r\n        <button mat-menu-item (click)=\"openCustomizer = true\">\r\n            <mat-icon>settings</mat-icon>\r\n            <span>Layout Settings</span>\r\n        </button>\r\n        <button mat-menu-item>\r\n            <mat-icon>notifications_off</mat-icon>\r\n            <span>Disable alerts</span>\r\n        </button>\r\n        <button mat-menu-item [routerLink]=\"['/sessions/signin']\">\r\n            <mat-icon>exit_to_app</mat-icon>\r\n            <span>Sign out</span>\r\n        </button>\r\n    </mat-menu>\r\n</mat-toolbar>\r\n"
            }),
            __metadata("design:paramtypes", [ThemeService,
                LayoutService,
                core$1.TranslateService,
                core.Renderer2])
        ], HeaderSideComponent);
        return HeaderSideComponent;
    }());

    var HeaderTopComponent = /** @class */ (function () {
        function HeaderTopComponent(layout, navService, themeService, translate, renderer, loginService, accountService) {
            this.layout = layout;
            this.navService = navService;
            this.themeService = themeService;
            this.translate = translate;
            this.renderer = renderer;
            this.loginService = loginService;
            this.accountService = accountService;
            this.egretThemes = [];
            this.openCustomizer = false;
            this.currentLang = 'en';
            this.availableLangs = [{
                    name: 'English',
                    code: 'en',
                }, {
                    name: 'Spanish',
                    code: 'es',
                }];
        }
        HeaderTopComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.layoutConf = this.layout.layoutConf;
            this.egretThemes = this.themeService.egretThemes;
            this.menuItemSub = this.navService.menuItems$
                .subscribe(function (res) {
                res = res.filter(function (item) { return item.type !== 'icon' && item.type !== 'separator'; });
                var limit = 4;
                var mainItems = res.slice(0, limit);
                if (res.length <= limit) {
                    return _this.menuItems = mainItems;
                }
                var subItems = res.slice(limit, res.length - 1);
                mainItems.push({
                    name: 'More',
                    type: 'dropDown',
                    tooltip: 'More',
                    icon: 'more_horiz',
                    sub: subItems
                });
                _this.menuItems = mainItems;
            });
        };
        HeaderTopComponent.prototype.ngOnDestroy = function () {
            this.menuItemSub.unsubscribe();
        };
        HeaderTopComponent.prototype.setLang = function () {
            this.translate.use(this.currentLang);
        };
        HeaderTopComponent.prototype.changeTheme = function (theme) {
            this.layout.publishLayoutChange({ matTheme: theme.name });
        };
        HeaderTopComponent.prototype.toggleNotific = function () {
            this.notificPanel.toggle();
        };
        HeaderTopComponent.prototype.toggleSidenav = function () {
            if (this.layoutConf.sidebarStyle === 'closed') {
                return this.layout.publishLayoutChange({
                    sidebarStyle: 'full'
                });
            }
            this.layout.publishLayoutChange({
                sidebarStyle: 'closed'
            });
        };
        HeaderTopComponent.prototype.logout = function () {
            this.loginService.logout();
        };
        HeaderTopComponent.ctorParameters = function () { return [
            { type: LayoutService },
            { type: NavigationService },
            { type: ThemeService },
            { type: core$1.TranslateService },
            { type: core.Renderer2 },
            { type: webCore.LoginService },
            { type: webCore.AccountService }
        ]; };
        __decorate([
            core.Input(),
            __metadata("design:type", Object)
        ], HeaderTopComponent.prototype, "notificPanel", void 0);
        HeaderTopComponent = __decorate([
            core.Component({
                selector: 'egret-header-top',
                template: "<div class=\"header-topnav mat-elevation-z2\">\r\n    <div class=\"container\">\r\n        <div class=\"topnav\">\r\n            <!-- App Logo -->\r\n            <div class=\"topbar-branding\">\r\n                <img src=\"assets/images/logo.png\" alt=\"\" class=\"app-logo\">\r\n            </div>\r\n\r\n            <ul class=\"menu\" *ngIf=\"!layoutConf.isMobile\">\r\n                <li *ngFor=\"let item of menuItems; let i = index;\">\r\n                    <ng-container *jhiHasAnyAuthority=\"item.authorities\">\r\n                        <div *ngIf=\"item.type !== 'separator'\" routerLinkActive=\"open\">\r\n                            <a matRipple routerLink=\"/{{item.state}}\" *ngIf=\"item.type === 'link'\">\r\n                                <mat-icon>{{item.icon}}</mat-icon>\r\n                                {{item.name | translate}}\r\n                            </a>\r\n                            <div *ngIf=\"item.type === 'dropDown'\">\r\n                                <label matRipple for=\"drop-{{i}}\" class=\"toggle\">\r\n                                    <mat-icon>{{item.icon}}</mat-icon>\r\n                                    {{item.name | translate}}</label>\r\n                                <a matRipple>\r\n                                    <mat-icon>{{item.icon}}</mat-icon>\r\n                                    {{item.name | translate}}</a>\r\n                                <input type=\"checkbox\" id=\"drop-{{i}}\"/>\r\n                                <ul>\r\n                                    <li *ngFor=\"let itemLvL2 of item.subs; let j = index;\" routerLinkActive=\"open\">\r\n                                        <a matRipple\r\n                                           routerLink=\"{{item.state ? '/'+item.state : ''}}/{{itemLvL2.state}}\"\r\n                                           *ngIf=\"itemLvL2.type !== 'dropDown'\">\r\n                                            <mat-icon *ngIf=\"itemLvL2.icon\">{{itemLvL2.icon}}</mat-icon>\r\n                                            {{itemLvL2.name | translate}}\r\n                                        </a>\r\n\r\n                                        <div *ngIf=\"itemLvL2.type === 'dropDown'\">\r\n                                            <label matRipple for=\"drop-{{i}}{{j}}\"\r\n                                                   class=\"toggle\">{{itemLvL2.name | translate}}</label>\r\n                                            <a matRipple>\r\n                                                <mat-icon *ngIf=\"itemLvL2.icon\">{{itemLvL2.icon}}</mat-icon>\r\n                                                {{itemLvL2.name | translate}}</a>\r\n                                            <input type=\"checkbox\" id=\"drop-{{i}}{{j}}\"/>\r\n                                            <!-- Level 3 -->\r\n                                            <ul>\r\n                                                <li *ngFor=\"let itemLvL3 of itemLvL2.subs\" routerLinkActive=\"open\">\r\n                                                    <a matRipple\r\n                                                       routerLink=\"{{item.state ? '/'+item.state : ''}}{{itemLvL2.state ? '/'+itemLvL2.state : ''}}/{{itemLvL3.state}}\">\r\n                                                        <mat-icon *ngIf=\"itemLvL3.icon\">{{itemLvL3.icon}}</mat-icon>\r\n                                                        {{itemLvL3.name | translate}}\r\n                                                    </a>\r\n                                                </li>\r\n                                            </ul>\r\n                                        </div>\r\n                                    </li>\r\n                                </ul>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </ng-container>\r\n                </li>\r\n            </ul>\r\n            <egret-customizer [isCustomizerOpen]=\"openCustomizer\"\r\n                              (customizerClosed)=\"openCustomizer = false\"></egret-customizer>\r\n            <span fxFlex></span>\r\n            <!-- End Navigation -->\r\n\r\n            <!-- Language Switcher -->\r\n            <mat-select\r\n                    *ngIf=\"!layoutConf.isMobile\"\r\n                    placeholder=\"\"\r\n                    id=\"langToggle\"\r\n                    [style.width]=\"'auto'\"\r\n                    name=\"currentLang\"\r\n                    [(ngModel)]=\"currentLang\"\r\n                    (selectionChange)=\"setLang()\"\r\n                    class=\"topbar-button-right\">\r\n                <mat-option\r\n                        *ngFor=\"let lang of availableLangs\"\r\n                        [value]=\"lang.code\" ngDefaultControl>{{ lang.name }}</mat-option>\r\n            </mat-select>\r\n            <!-- Theme Switcher -->\r\n            <button\r\n                    mat-icon-button\r\n                    id=\"schemeToggle\"\r\n                    [style.overflow]=\"'visible'\"\r\n                    matTooltip=\"Color Schemes\"\r\n                    [matMenuTriggerFor]=\"themeMenu\"\r\n                    class=\"topbar-button-right\">\r\n                <mat-icon>format_color_fill</mat-icon>\r\n            </button>\r\n            <mat-menu #themeMenu=\"matMenu\">\r\n                <mat-grid-list\r\n                        class=\"theme-list\"\r\n                        cols=\"2\"\r\n                        rowHeight=\"48px\">\r\n                    <mat-grid-tile\r\n                            *ngFor=\"let theme of egretThemes\"\r\n                            (click)=\"changeTheme(theme)\">\r\n                        <div mat-menu-item [title]=\"theme.name\">\r\n                            <div [style.background]=\"theme.baseColor\" class=\"egret-swatch\"></div>\r\n                            <mat-icon class=\"active-icon\" *ngIf=\"theme.isActive\">check</mat-icon>\r\n                        </div>\r\n                    </mat-grid-tile>\r\n                </mat-grid-list>\r\n            </mat-menu>\r\n            <!-- Notification toggle button -->\r\n            <button\r\n                    mat-icon-button\r\n                    matTooltip=\"Notifications\"\r\n                    (click)=\"toggleNotific()\"\r\n                    [style.overflow]=\"'visible'\"\r\n                    class=\"topbar-button-right\">\r\n                <mat-icon>notifications</mat-icon>\r\n                <span class=\"notification-number mat-bg-warn\">3</span>\r\n            </button>\r\n            <!-- Top left user menu -->\r\n            <button mat-icon-button [matMenuTriggerFor]=\"accountMenu\" class=\"topbar-button-right mr-1 img-button\">\r\n                <img src=\"assets/images/face-7.jpg\" alt=\"\">\r\n            </button>\r\n            <mat-menu #accountMenu=\"matMenu\">\r\n                <button mat-menu-item [routerLink]=\"['/profile/overview']\">\r\n                    <mat-icon>account_box</mat-icon>\r\n                    <span>Profile</span>\r\n                </button>\r\n                <button mat-menu-item (click)=\"openCustomizer = true\">\r\n                    <mat-icon>settings</mat-icon>\r\n                    <span>Layout Settings</span>\r\n                </button>\r\n                <button mat-menu-item>\r\n                    <mat-icon>notifications_off</mat-icon>\r\n                    <span>Disable alerts</span>\r\n                </button>\r\n                <button mat-menu-item\r\n                        (click)=\"logout()\">\r\n                    <mat-icon>exit_to_app</mat-icon>\r\n                    <span>Sign out</span>\r\n                </button>\r\n            </mat-menu>\r\n            <!-- Mobile screen menu toggle -->\r\n            <button\r\n                    mat-icon-button\r\n                    class=\"mr-1\"\r\n                    (click)=\"toggleSidenav()\"\r\n                    *ngIf=\"layoutConf.isMobile\">\r\n                <mat-icon>menu</mat-icon>\r\n            </button>\r\n        </div>\r\n    </div>\r\n</div>\r\n"
            }),
            __metadata("design:paramtypes", [LayoutService,
                NavigationService,
                ThemeService,
                core$1.TranslateService,
                core.Renderer2,
                webCore.LoginService,
                webCore.AccountService])
        ], HeaderTopComponent);
        return HeaderTopComponent;
    }());

    var NotificationsComponent = /** @class */ (function () {
        function NotificationsComponent(router) {
            this.router = router;
            // Dummy notifications
            this.notifications = [{
                    message: 'New contact added',
                    icon: 'assignment_ind',
                    time: '1 min ago',
                    route: '/inbox',
                    color: 'primary'
                }, {
                    message: 'New message',
                    icon: 'chat',
                    time: '4 min ago',
                    route: '/chat',
                    color: 'accent'
                }, {
                    message: 'Server rebooted',
                    icon: 'settings_backup_restore',
                    time: '12 min ago',
                    route: '/charts',
                    color: 'warn'
                }];
        }
        NotificationsComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.router.events.subscribe(function (routeChange) {
                if (routeChange instanceof router.NavigationEnd) {
                    _this.notificPanel.close();
                }
            });
        };
        NotificationsComponent.prototype.clearAll = function (e) {
            e.preventDefault();
            this.notifications = [];
        };
        NotificationsComponent.ctorParameters = function () { return [
            { type: router.Router }
        ]; };
        __decorate([
            core.Input(),
            __metadata("design:type", Object)
        ], NotificationsComponent.prototype, "notificPanel", void 0);
        NotificationsComponent = __decorate([
            core.Component({
                selector: 'egret-notifications',
                template: "<div class=\"text-center mat-bg-primary pt-1 pb-1\">\r\n    <h6 class=\"m-0\">Notifications</h6>\r\n</div>\r\n<mat-nav-list class=\"notification-list\" role=\"list\">\r\n    <!-- Notification item -->\r\n    <mat-list-item *ngFor=\"let n of notifications\" class=\"notific-item\" role=\"listitem\" routerLinkActive=\"open\">\r\n        <mat-icon [color]=\"n.color\" class=\"notific-icon mr-1\">{{n.icon}}</mat-icon>\r\n        <a [routerLink]=\"[n.route || '/dashboard']\">\r\n            <div class=\"mat-list-text\">\r\n                <h4 class=\"message\">{{n.message}}</h4>\r\n                <small class=\"time text-muted\">{{n.time}}</small>\r\n            </div>\r\n        </a>\r\n    </mat-list-item>\r\n</mat-nav-list>\r\n<div class=\"text-center mt-1\" *ngIf=\"notifications.length\">\r\n    <small><a href=\"#\" (click)=\"clearAll($event)\">Clear all notifications</a></small>\r\n</div>\r\n"
            }),
            __metadata("design:paramtypes", [router.Router])
        ], NotificationsComponent);
        return NotificationsComponent;
    }());

    var SidebarSideComponent = /** @class */ (function () {
        function SidebarSideComponent(navService, themeService, layout, loginService, accountService) {
            this.navService = navService;
            this.themeService = themeService;
            this.layout = layout;
            this.loginService = loginService;
            this.accountService = accountService;
        }
        SidebarSideComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.iconTypeMenuTitle = this.navService.iconTypeMenuTitle;
            this.menuItemsSub = this.navService.menuItems$.subscribe(function (menuItem) {
                _this.menuItems = menuItem;
                //Checks item list has any icon type.
                _this.hasIconTypeMenuItem = !!_this.menuItems.filter(function (item) { return item.type === "icon"; }).length;
            });
            this.layoutConf = this.layout.layoutConf;
            this.accountService.fetch().subscribe(function (res) { return _this.account = res.body; });
        };
        SidebarSideComponent.prototype.ngAfterViewInit = function () {
        };
        SidebarSideComponent.prototype.ngOnDestroy = function () {
            if (this.menuItemsSub) {
                this.menuItemsSub.unsubscribe();
            }
        };
        SidebarSideComponent.prototype.toggleCollapse = function () {
            if (this.layoutConf.sidebarCompactToggle) {
                this.layout.publishLayoutChange({
                    sidebarCompactToggle: false
                });
            }
            else {
                this.layout.publishLayoutChange({
                    // sidebarStyle: "compact",
                    sidebarCompactToggle: true
                });
            }
        };
        SidebarSideComponent.prototype.logout = function () {
            this.loginService.logout();
        };
        SidebarSideComponent.ctorParameters = function () { return [
            { type: NavigationService },
            { type: ThemeService },
            { type: LayoutService },
            { type: webCore.LoginService },
            { type: webCore.AccountService }
        ]; };
        SidebarSideComponent = __decorate([
            core.Component({
                selector: 'egret-sidebar-side',
                template: "<div class=\"sidebar-panel\">\r\n    <div id=\"scroll-area\" [perfectScrollbar] class=\"navigation-hold\" fxLayout=\"column\">\r\n        <div class=\"sidebar-hold\">\r\n\r\n            <!-- App Logo -->\r\n            <div class=\"branding\">\r\n                <img src=\"assets/images/logo.png\" alt=\"\" class=\"app-logo\">\r\n                <span class=\"app-logo-text\">LAMIS</span>\r\n\r\n                <span style=\"margin: auto\" *ngIf=\"layoutConf.sidebarStyle !== 'compact'\"></span>\r\n                <div\r\n                        class=\"sidebar-compact-switch\"\r\n                        [ngClass]=\"{active: layoutConf.sidebarCompactToggle}\"\r\n                        (click)=\"toggleCollapse()\"\r\n                        *ngIf=\"layoutConf.sidebarStyle !== 'compact'\"><span></span></div>\r\n            </div>\r\n\r\n            <!-- Sidebar user -->\r\n            <div class=\"app-user\">\r\n                <div class=\"app-user-photo\">\r\n                    <img src=\"assets/images/face-7.jpg\" class=\"mat-elevation-z1\" alt=\"\">\r\n                </div>\r\n                <span class=\"app-user-name mb-05\">\r\n                <mat-icon class=\"icon-xs text-muted\">lock</mat-icon>\r\n                {{account?.firstName}} {{account?.lastName}}\r\n                </span>\r\n                <!-- Small buttons -->\r\n                <div class=\"app-user-controls\">\r\n                    <button\r\n                            class=\"text-muted\"\r\n                            mat-icon-button\r\n                            mat-xs-button\r\n                            [matMenuTriggerFor]=\"appUserMenu\">\r\n                        <mat-icon>settings</mat-icon>\r\n                    </button>\r\n                    <button\r\n                            class=\"text-muted\"\r\n                            mat-icon-button\r\n                            mat-xs-button\r\n                            matTooltip=\"Inbox\"\r\n                            routerLink=\"/inbox\">\r\n                        <mat-icon>email</mat-icon>\r\n                    </button>\r\n                    <button\r\n                            class=\"text-muted\"\r\n                            mat-icon-button\r\n                            mat-xs-button\r\n                            (click)=\"logout()\"\r\n                            matTooltip=\"Sign Out\">\r\n                        <mat-icon>exit_to_app</mat-icon>\r\n                    </button>\r\n                    <mat-menu #appUserMenu=\"matMenu\">\r\n                        <button mat-menu-item routerLink=\"/profile/overview\">\r\n                            <mat-icon>account_box</mat-icon>\r\n                            <span>Profile</span>\r\n                        </button>\r\n                        <button mat-menu-item routerLink=\"/profile/settings\">\r\n                            <mat-icon>settings</mat-icon>\r\n                            <span>Account Settings</span>\r\n                        </button>\r\n                        <button mat-menu-item\r\n                                (click)=\"logout()\">\r\n                            <mat-icon>exit_to_app</mat-icon>\r\n                            <span>Sign out</span>\r\n                        </button>\r\n                    </mat-menu>\r\n                </div>\r\n            </div>\r\n            <!-- Navigation -->\r\n            <egret-sidenav [items]=\"menuItems\" [hasIconMenu]=\"hasIconTypeMenuItem\"\r\n                           [iconMenuTitle]=\"iconTypeMenuTitle\"></egret-sidenav>\r\n        </div>\r\n    </div>\r\n</div>\r\n"
            }),
            __metadata("design:paramtypes", [NavigationService,
                ThemeService,
                LayoutService,
                webCore.LoginService,
                webCore.AccountService])
        ], SidebarSideComponent);
        return SidebarSideComponent;
    }());

    var SidebarTopComponent = /** @class */ (function () {
        function SidebarTopComponent(navService) {
            this.navService = navService;
        }
        SidebarTopComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.menuItemsSub = this.navService.menuItems$.subscribe(function (menuItem) {
                _this.menuItems = menuItem.filter(function (item) { return item.type !== 'icon' && item.type !== 'separator'; });
            });
        };
        SidebarTopComponent.prototype.ngAfterViewInit = function () {
            // setTimeout(() => {
            //   this.sidebarPS = new PerfectScrollbar('#sidebar-top-scroll-area', {
            //     suppressScrollX: true
            //   })
            // })
        };
        SidebarTopComponent.prototype.ngOnDestroy = function () {
            // if(this.sidebarPS) {
            //   this.sidebarPS.destroy();
            // }
            if (this.menuItemsSub) {
                this.menuItemsSub.unsubscribe();
            }
        };
        SidebarTopComponent.ctorParameters = function () { return [
            { type: NavigationService }
        ]; };
        SidebarTopComponent = __decorate([
            core.Component({
                selector: 'egret-sidebar-top',
                template: "<div class=\"sidebar-panel\">\r\n    <div id=\"sidebar-top-scroll-area\" [perfectScrollbar] class=\"navigation-hold\" fxLayout=\"column\">\r\n        <egret-sidenav [items]=\"menuItems\"></egret-sidenav>\r\n    </div>\r\n</div>\r\n"
            }),
            __metadata("design:paramtypes", [NavigationService])
        ], SidebarTopComponent);
        return SidebarTopComponent;
    }());

    var SidenavComponent = /** @class */ (function () {
        function SidenavComponent() {
            this.menuItems = [];
        }
        SidenavComponent.prototype.ngOnInit = function () {
        };
        __decorate([
            core.Input('items'),
            __metadata("design:type", Array)
        ], SidenavComponent.prototype, "menuItems", void 0);
        __decorate([
            core.Input('hasIconMenu'),
            __metadata("design:type", Boolean)
        ], SidenavComponent.prototype, "hasIconTypeMenuItem", void 0);
        __decorate([
            core.Input('iconMenuTitle'),
            __metadata("design:type", String)
        ], SidenavComponent.prototype, "iconTypeMenuTitle", void 0);
        SidenavComponent = __decorate([
            core.Component({
                selector: 'egret-sidenav',
                template: "<div class=\"sidenav-hold\">\r\n    <div class=\"icon-menu mb-1\" *ngIf=\"hasIconTypeMenuItem\">\r\n        <!-- Icon menu separator -->\r\n        <div class=\"mb-1 nav-item-sep\">\r\n            <mat-divider [ngStyle]=\"{margin: '0 -24px'}\"></mat-divider>\r\n            <span class=\"text-muted icon-menu-title\">{{iconTypeMenuTitle}}</span>\r\n        </div>\r\n        <!-- Icon menu items -->\r\n        <div class=\"icon-menu-item\" *ngFor=\"let item of menuItems\">\r\n            <ng-container *jhiHasAnyAuthority=\"item.authorities\">\r\n                <button *ngIf=\"!item.disabled && item.type === 'icon'\" mat-raised-button [matTooltip]=\"item.tooltip\"\r\n                        routerLink=\"/{{item.state}}\"\r\n                        routerLinkActive=\"mat-bg-primary\">\r\n                    <mat-icon>{{item.icon}}</mat-icon>\r\n                </button>\r\n            </ng-container>\r\n        </div>\r\n    </div>\r\n\r\n    <ul appDropdown class=\"sidenav\">\r\n        <li *ngFor=\"let item of menuItems\" appDropdownLink routerLinkActive=\"open\">\r\n            <ng-container *jhiHasAnyAuthority=\"item.authorities\">\r\n                <div class=\"nav-item-sep\" *ngIf=\"item.type === 'separator'\">\r\n                    <mat-divider></mat-divider>\r\n                    <span class=\"text-muted\">{{item.name | translate}}</span>\r\n                </div>\r\n                <div *ngIf=\"!item.disabled && item.type !== 'separator' && item.type !== 'icon'\" class=\"lvl1\">\r\n                    <a routerLink=\"/{{item.state}}\" appDropdownToggle matRipple *ngIf=\"item.type === 'link'\">\r\n                        <mat-icon>{{item.icon}}</mat-icon>\r\n                        <span class=\"item-name lvl1\">{{item.name | translate}}</span>\r\n                        <span fxFlex></span>\r\n                        <span class=\"menuitem-badge mat-bg-{{ badge.color }}\" [ngStyle]=\"{background: badge.color}\"\r\n                              *ngFor=\"let badge of item.badges\">{{ badge.value }}</span>\r\n                    </a>\r\n                    <a [href]=\"item.state\" appDropdownToggle matRipple *ngIf=\"item.type === 'extLink'\" target=\"_blank\">\r\n                        <mat-icon>{{item.icon}}</mat-icon>\r\n                        <span class=\"item-name lvl1\">{{item.name | translate}}</span>\r\n                        <span fxFlex></span>\r\n                        <span class=\"menuitem-badge mat-bg-{{ badge.color }}\" [ngStyle]=\"{background: badge.color}\"\r\n                              *ngFor=\"let badge of item.badges\">{{ badge.value }}</span>\r\n                    </a>\r\n\r\n                    <!-- DropDown -->\r\n                    <a *ngIf=\"item.type === 'dropDown'\" appDropdownToggle matRipple>\r\n                        <mat-icon>{{item.icon}}</mat-icon>\r\n                        <span class=\"item-name lvl1\">{{item.name | translate}}</span>\r\n                        <span fxFlex></span>\r\n                        <span class=\"menuitem-badge mat-bg-{{ badge.color }}\" [ngStyle]=\"{background: badge.color}\"\r\n                              *ngFor=\"let badge of item.badges\">{{ badge.value }}</span>\r\n                        <mat-icon class=\"menu-caret\">keyboard_arrow_right</mat-icon>\r\n                    </a>\r\n                    <!-- LEVEL 2 -->\r\n                    <ul class=\"submenu lvl2\" appDropdown *ngIf=\"item.type === 'dropDown'\">\r\n                        <li *ngFor=\"let itemLvL2 of item.subs\" appDropdownLink routerLinkActive=\"open\">\r\n                            <ng-container *jhiHasAnyAuthority=\"itemLvL2.authorities\">\r\n                            <a routerLink=\"{{item.state ? '/'+item.state : ''}}/{{itemLvL2.state}}\" appDropdownToggle\r\n                               *ngIf=\"itemLvL2.type !== 'dropDown'\"\r\n                               matRipple>\r\n                                <span class=\"item-name lvl2\">{{itemLvL2.name | translate}}</span>\r\n                                <span fxFlex></span>\r\n                            </a>\r\n\r\n                            <a *ngIf=\"itemLvL2.type === 'dropDown'\" appDropdownToggle matRipple>\r\n                                <span class=\"item-name lvl2\">{{itemLvL2.name | translate}}</span>\r\n                                <span fxFlex></span>\r\n                                <mat-icon class=\"menu-caret\">keyboard_arrow_right</mat-icon>\r\n                            </a>\r\n\r\n                            <!-- LEVEL 3 -->\r\n                            <ul class=\"submenu lvl3\" appDropdown *ngIf=\"itemLvL2.type === 'dropDown'\">\r\n                                <li *ngFor=\"let itemLvL3 of itemLvL2.subs\" appDropdownLink routerLinkActive=\"open\">\r\n                                    <ng-container *jhiHasAnyAuthority=\"itemLvL3.authorities\">\r\n                                    <a routerLink=\"{{item.state ? '/'+item.state : ''}}{{itemLvL2.state ? '/'+itemLvL2.state : ''}}/{{itemLvL3.state}}\"\r\n                                       appDropdownToggle\r\n                                       matRipple>\r\n                                        <span class=\"item-name lvl3\">{{itemLvL3.name | translate}}</span>\r\n                                    </a>\r\n                                    </ng-container>\r\n                                </li>\r\n                            </ul>\r\n                            </ng-container>\r\n                        </li>\r\n                    </ul>\r\n                </div>\r\n            </ng-container>\r\n        </li>\r\n    </ul>\r\n</div>\r\n"
            }),
            __metadata("design:paramtypes", [])
        ], SidenavComponent);
        return SidenavComponent;
    }());

    var CustomizerService = /** @class */ (function () {
        function CustomizerService(router, layout) {
            this.router = router;
            this.layout = layout;
            this.colors = [
                {
                    class: "black",
                    active: false
                },
                {
                    class: "white",
                    active: false
                },
                {
                    class: "dark-blue",
                    active: false
                },
                {
                    class: "grey",
                    active: false
                },
                {
                    class: "brown",
                    active: false
                },
                {
                    class: "gray",
                    active: false
                },
                {
                    class: "purple",
                    active: false
                },
                {
                    class: "blue",
                    active: false
                },
                {
                    class: "indigo",
                    active: false
                },
                {
                    class: "yellow",
                    active: false
                },
                {
                    class: "green",
                    active: false
                },
                {
                    class: "pink",
                    active: false
                },
                {
                    class: "red",
                    active: false
                }
            ];
            this.topbarColors = this.getTopbarColors();
            this.sidebarColors = this.getSidebarColors();
        }
        CustomizerService.prototype.getSidebarColors = function () {
            var _this = this;
            var sidebarColors = ['black', 'white', 'grey', 'brown', 'purple', 'dark-blue',];
            return this.colors.filter(function (color) {
                return sidebarColors.includes(color.class);
            })
                .map(function (c) {
                c.active = c.class === _this.layout.layoutConf.sidebarColor;
                return __assign({}, c);
            });
        };
        CustomizerService.prototype.getTopbarColors = function () {
            var _this = this;
            var topbarColors = ['black', 'white', 'dark-gray', 'purple', 'dark-blue', 'indigo', 'pink', 'red', 'yellow', 'green'];
            return this.colors.filter(function (color) {
                return topbarColors.includes(color.class);
            })
                .map(function (c) {
                c.active = c.class === _this.layout.layoutConf.topbarColor;
                return __assign({}, c);
            });
        };
        CustomizerService.prototype.changeSidebarColor = function (color) {
            this.layout.publishLayoutChange({ sidebarColor: color.class });
            this.sidebarColors = this.getSidebarColors();
        };
        CustomizerService.prototype.changeTopbarColor = function (color) {
            this.layout.publishLayoutChange({ topbarColor: color.class });
            this.topbarColors = this.getTopbarColors();
        };
        CustomizerService.prototype.removeClass = function (el, className) {
            if (!el || el.length === 0)
                return;
            if (!el.length) {
                el.classList.remove(className);
            }
            else {
                for (var i = 0; i < el.length; i++) {
                    el[i].classList.remove(className);
                }
            }
        };
        CustomizerService.prototype.addClass = function (el, className) {
            if (!el)
                return;
            if (!el.length) {
                el.classList.add(className);
            }
            else {
                for (var i = 0; i < el.length; i++) {
                    el[i].classList.add(className);
                }
            }
        };
        CustomizerService.prototype.findClosest = function (el, className) {
            if (!el)
                return;
            while (el) {
                var parent = el.parentElement;
                if (parent && this.hasClass(parent, className)) {
                    return parent;
                }
                el = parent;
            }
        };
        CustomizerService.prototype.hasClass = function (el, className) {
            if (!el)
                return;
            return ((" " + el.className + " ").replace(/[\n\t]/g, " ").indexOf(" " + className + " ") > -1);
        };
        CustomizerService.prototype.toggleClass = function (el, className) {
            if (!el)
                return;
            if (this.hasClass(el, className)) {
                this.removeClass(el, className);
            }
            else {
                this.addClass(el, className);
            }
        };
        CustomizerService.ctorParameters = function () { return [
            { type: router.Router },
            { type: LayoutService }
        ]; };
        CustomizerService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function CustomizerService_Factory() { return new CustomizerService(core.ɵɵinject(router.Router), core.ɵɵinject(LayoutService)); }, token: CustomizerService, providedIn: "root" });
        CustomizerService = __decorate([
            core.Injectable({
                providedIn: "root"
            }),
            __metadata("design:paramtypes", [router.Router,
                LayoutService])
        ], CustomizerService);
        return CustomizerService;
    }());

    var CustomizerComponent = /** @class */ (function () {
        function CustomizerComponent(navService, layout, themeService, customizer, renderer) {
            this.navService = navService;
            this.layout = layout;
            this.themeService = themeService;
            this.customizer = customizer;
            this.renderer = renderer;
            this.isCustomizerOpen = false;
            this.customizerClosed = new core.EventEmitter();
            this.sidenavTypes = [
                {
                    name: 'Default Menu',
                    value: 'default-menu'
                },
                {
                    name: 'Separator Menu',
                    value: 'separator-menu'
                },
                {
                    name: 'Icon Menu',
                    value: 'icon-menu'
                }
            ];
            this.selectedMenu = 'icon-menu';
            this.isTopbarFixed = false;
            this.isRTL = false;
            this.perfectScrollbarEnabled = true;
        }
        CustomizerComponent.prototype.ngOnInit = function () {
            this.layoutConf = this.layout.layoutConf;
            this.selectedLayout = this.layoutConf.navigationPos;
            this.isTopbarFixed = this.layoutConf.topbarFixed;
            this.isRTL = this.layoutConf.dir === 'rtl';
            this.egretThemes = this.themeService.egretThemes;
        };
        CustomizerComponent.prototype.changeTheme = function (theme) {
            // this.themeService.changeTheme(theme);
            this.layout.publishLayoutChange({ matTheme: theme.name });
        };
        CustomizerComponent.prototype.changeLayoutStyle = function (data) {
            this.layout.publishLayoutChange({ navigationPos: this.selectedLayout });
        };
        CustomizerComponent.prototype.changeSidenav = function (data) {
            this.navService.publishNavigationChange(data.value);
        };
        CustomizerComponent.prototype.toggleBreadcrumb = function (data) {
            this.layout.publishLayoutChange({ useBreadcrumb: data.checked });
        };
        CustomizerComponent.prototype.sidebarCompactToggle = function (data) {
            this.layout.publishLayoutChange({ sidebarCompactToggle: data.checked });
        };
        CustomizerComponent.prototype.toggleTopbarFixed = function (data) {
            this.layout.publishLayoutChange({ topbarFixed: data.checked });
        };
        CustomizerComponent.prototype.toggleDir = function (data) {
            var dir = data.checked ? 'rtl' : 'ltr';
            this.layout.publishLayoutChange({ dir: dir });
        };
        CustomizerComponent.prototype.tooglePerfectScrollbar = function (data) {
            this.layout.publishLayoutChange({ perfectScrollbar: this.perfectScrollbarEnabled });
        };
        CustomizerComponent.prototype.close = function () {
            this.isCustomizerOpen = false;
            this.customizerClosed.emit(null);
        };
        CustomizerComponent.ctorParameters = function () { return [
            { type: NavigationService },
            { type: LayoutService },
            { type: ThemeService },
            { type: CustomizerService },
            { type: core.Renderer2 }
        ]; };
        __decorate([
            core.Input(),
            __metadata("design:type", Boolean)
        ], CustomizerComponent.prototype, "isCustomizerOpen", void 0);
        __decorate([
            core.Output(),
            __metadata("design:type", core.EventEmitter)
        ], CustomizerComponent.prototype, "customizerClosed", void 0);
        CustomizerComponent = __decorate([
            core.Component({
                selector: 'egret-customizer',
                template: "<div id=\"app-customizer\" *ngIf=\"isCustomizerOpen\">\r\n    <mat-card class=\"p-0\">\r\n        <mat-card-title class=\"m-0 light-gray\">\r\n            <div class=\"card-title-text\" fxLayout=\"row wrap\" fxLayoutAlign=\"center center\">\r\n                <span fxFlex></span>\r\n                <button\r\n                        class=\"card-control\"\r\n                        mat-icon-button\r\n                        (click)=\"close()\">\r\n                    <mat-icon>close</mat-icon>\r\n                </button>\r\n            </div>\r\n        </mat-card-title>\r\n\r\n        <mat-card-content [perfectScrollbar]>\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Layouts</h6>\r\n                <mat-radio-group fxLayout=\"column\" [(ngModel)]=\"selectedLayout\" (change)=\"changeLayoutStyle($event)\">\r\n                    <mat-radio-button [value]=\"'top'\"> Top Navigation</mat-radio-button>\r\n                    <mat-radio-button [value]=\"'side'\"> Side Navigation</mat-radio-button>\r\n                </mat-radio-group>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Header Colors</h6>\r\n                <div class=\"mb-1\">\r\n                    <mat-checkbox [(ngModel)]=\"isTopbarFixed\" (change)=\"toggleTopbarFixed($event)\"\r\n                                  [disabled]=\"selectedLayout === 'top'\" [value]=\"selectedLayout !== 'top'\">Fixed Header\r\n                    </mat-checkbox>\r\n                </div>\r\n                <div class=\"colors\">\r\n                    <div\r\n                            class=\"color {{c.class}}\"\r\n                            *ngFor=\"let c of customizer.topbarColors\"\r\n                            (click)=\"customizer.changeTopbarColor(c)\">\r\n                        <mat-icon class=\"active-icon\" *ngIf=\"c.active\">check</mat-icon>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Sidebar colors</h6>\r\n                <div class=\"colors\">\r\n                    <div\r\n                            class=\"color {{c.class}}\"\r\n                            *ngFor=\"let c of customizer.sidebarColors\"\r\n                            (click)=\"customizer.changeSidebarColor(c)\">\r\n                        <mat-icon class=\"active-icon\" *ngIf=\"c.active\">check</mat-icon>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Material Themes</h6>\r\n                <div class=\"colors\">\r\n                    <div class=\"color\" *ngFor=\"let theme of egretThemes\"\r\n                         (click)=\"changeTheme(theme)\" [style.background]=\"theme.baseColor\">\r\n                        <mat-icon class=\"active-icon\" *ngIf=\"theme.isActive\">check</mat-icon>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Sidebar Toggle</h6>\r\n                <div class=\"mb-1\">\r\n                    <mat-checkbox [(ngModel)]=\"layoutConf.sidebarCompactToggle\" (change)=\"sidebarCompactToggle($event)\">\r\n                        Toggle Sidebar\r\n                    </mat-checkbox>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"pb-1 mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Breadcrumb</h6>\r\n                <div class=\"mb-1\">\r\n                    <mat-checkbox [(ngModel)]=\"layoutConf.useBreadcrumb\" (change)=\"toggleBreadcrumb($event)\">Use\r\n                        breadcrumb\r\n                    </mat-checkbox>\r\n                </div>\r\n                <small class=\"text-muted\">Breadcrumb types</small>\r\n                <mat-radio-group fxLayout=\"column\" [(ngModel)]=\"layoutConf.breadcrumb\"\r\n                                 [disabled]=\"!layoutConf.useBreadcrumb\">\r\n                    <mat-radio-button [value]=\"'simple'\"> Simple</mat-radio-button>\r\n                    <mat-radio-button [value]=\"'title'\"> Simple with title</mat-radio-button>\r\n                </mat-radio-group>\r\n            </div>\r\n            <div class=\"pb-1 pos-rel mb-1 border-bottom\">\r\n                <h6 class=\"title text-muted\">Navigation</h6>\r\n                <mat-radio-group\r\n                        fxLayout=\"column\"\r\n                        [(ngModel)]=\"selectedMenu\"\r\n                        (change)=\"changeSidenav($event)\"\r\n                        [disabled]=\"selectedLayout === 'top'\">\r\n                    <mat-radio-button\r\n                            *ngFor=\"let type of sidenavTypes\"\r\n                            [value]=\"type.value\">\r\n                        {{type.name}}\r\n                    </mat-radio-button>\r\n                </mat-radio-group>\r\n            </div>\r\n\r\n            <div class=\"pb-1 \">\r\n                <mat-checkbox [(ngModel)]=\"isRTL\" (change)=\"toggleDir($event)\">RTL</mat-checkbox>\r\n            </div>\r\n        </mat-card-content>\r\n    </mat-card>\r\n</div>\r\n",
                styles: [".handle{position:fixed;bottom:30px;right:30px;z-index:99}#app-customizer{position:fixed;bottom:0;top:0;right:0;min-width:180px;max-width:280px;z-index:999}#app-customizer .title{text-transform:uppercase;font-size:12px;font-weight:700;margin:0 0 1rem}#app-customizer .mat-card{margin:0;border-radius:0}#app-customizer .mat-card-content{padding:1rem 1.5rem 2rem;max-height:calc(100vh - 80px)}.pos-rel{position:relative;z-index:99}.pos-rel .olay{position:absolute;width:100%;height:100%;background:rgba(0,0,0,.5);z-index:100}.colors{display:flex;flex-wrap:wrap}.colors .color{position:relative;width:36px;height:36px;display:inline-block;border-radius:50%;margin:8px;text-align:center;box-shadow:0 4px 20px 1px rgba(0,0,0,.06),0 1px 4px rgba(0,0,0,.03);cursor:pointer}.colors .color .active-icon{position:absolute;left:0;right:0;margin:auto;top:6px}"]
            }),
            __metadata("design:paramtypes", [NavigationService,
                LayoutService,
                ThemeService,
                CustomizerService,
                core.Renderer2])
        ], CustomizerComponent);
        return CustomizerComponent;
    }());

    var classesToInclude = [
        HeaderTopComponent,
        SidebarTopComponent,
        SidenavComponent,
        NotificationsComponent,
        SidebarSideComponent,
        EgretSidebarComponent,
        HeaderSideComponent,
        AdminLayoutComponent,
        AuthLayoutComponent,
        BreadcrumbComponent,
        RelativeTimePipe,
        ExcerptPipe,
        GetValueByKeyPipe,
        CustomizerComponent
    ];
    var EgretModule = /** @class */ (function () {
        function EgretModule() {
        }
        EgretModule = __decorate([
            core.NgModule({
                imports: [
                    common.CommonModule,
                    forms.FormsModule,
                    router.RouterModule,
                    webCore.LamisCoreModule,
                    flexLayout.FlexLayoutModule,
                    adfCore.CoreModule,
                    material.MatSidenavModule,
                    material.MatListModule,
                    material.MatTooltipModule,
                    material.MatOptionModule,
                    material.MatSelectModule,
                    material.MatMenuModule,
                    material.MatSnackBarModule,
                    material.MatGridListModule,
                    material.MatToolbarModule,
                    material.MatIconModule,
                    material.MatButtonModule,
                    material.MatRadioModule,
                    material.MatCheckboxModule,
                    material.MatCardModule,
                    material.MatProgressSpinnerModule,
                    material.MatRippleModule,
                    material.MatDialogModule,
                    SharedDirectivesModule,
                    ngxPerfectScrollbar.PerfectScrollbarModule,
                    core$1.TranslateModule
                ],
                entryComponents: [],
                providers: [
                    ThemeService,
                    LayoutService,
                    NavigationService,
                    RoutePartsService
                ],
                declarations: classesToInclude,
                exports: __spread(classesToInclude, [SharedDirectivesModule]),
                schemas: [core.CUSTOM_ELEMENTS_SCHEMA]
            })
        ], EgretModule);
        return EgretModule;
    }());

    exports.AdminLayoutComponent = AdminLayoutComponent;
    exports.AppDropdownDirective = AppDropdownDirective;
    exports.AuthLayoutComponent = AuthLayoutComponent;
    exports.DropdownAnchorDirective = DropdownAnchorDirective;
    exports.DropdownLinkDirective = DropdownLinkDirective;
    exports.EgretModule = EgretModule;
    exports.EgretSideNavToggleDirective = EgretSideNavToggleDirective;
    exports.EgretSidenavHelperDirective = EgretSidenavHelperDirective;
    exports.EgretSidenavHelperService = EgretSidenavHelperService;
    exports.EgretSidenavTogglerDirective = EgretSidenavTogglerDirective;
    exports.ExcerptPipe = ExcerptPipe;
    exports.FontSizeDirective = FontSizeDirective;
    exports.GetValueByKeyPipe = GetValueByKeyPipe;
    exports.LayoutService = LayoutService;
    exports.MatchMediaService = MatchMediaService;
    exports.NavigationService = NavigationService;
    exports.RelativeTimePipe = RelativeTimePipe;
    exports.RoutePartsService = RoutePartsService;
    exports.ScrollToDirective = ScrollToDirective;
    exports.SharedDirectivesModule = SharedDirectivesModule;
    exports.ThemeService = ThemeService;
    exports.egretAnimations = egretAnimations;
    exports.ɵa = EgretSidebarComponent;
    exports.ɵb = EgretSidebarTogglerDirective;
    exports.ɵc = EgretSidebarHelperService;
    exports.ɵd = HeaderTopComponent;
    exports.ɵe = SidebarTopComponent;
    exports.ɵf = SidenavComponent;
    exports.ɵg = NotificationsComponent;
    exports.ɵh = SidebarSideComponent;
    exports.ɵi = HeaderSideComponent;
    exports.ɵj = BreadcrumbComponent;
    exports.ɵk = CustomizerComponent;
    exports.ɵl = CustomizerService;

    Object.defineProperty(exports, '__esModule', { value: true });

}));
//# sourceMappingURL=lamis-egret.umd.js.map
