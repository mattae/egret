import { OnInit } from '@angular/core';
import { IMenuItem } from '@lamis/web-core';
export declare class SidenavComponent implements OnInit {
    menuItems: IMenuItem[];
    hasIconTypeMenuItem: boolean;
    iconTypeMenuTitle: string;
    constructor();
    ngOnInit(): void;
}
