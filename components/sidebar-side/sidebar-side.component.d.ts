import { AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import { ThemeService } from '../../services/theme.service';
import { Account, AccountService, IMenuItem, LoginService } from '@lamis/web-core';
import { NavigationService } from '../../services/navigation.service';
import { ILayoutConf, LayoutService } from '../../services/layout.service';
export declare class SidebarSideComponent implements OnInit, OnDestroy, AfterViewInit {
    private navService;
    themeService: ThemeService;
    private layout;
    private loginService;
    private accountService;
    menuItems: IMenuItem[];
    hasIconTypeMenuItem: boolean;
    iconTypeMenuTitle: string;
    private menuItemsSub;
    layoutConf: ILayoutConf;
    account: Account;
    constructor(navService: NavigationService, themeService: ThemeService, layout: LayoutService, loginService: LoginService, accountService: AccountService);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    ngOnDestroy(): void;
    toggleCollapse(): void;
    logout(): void;
}
