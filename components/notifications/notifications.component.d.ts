import { OnInit } from '@angular/core';
import { Router } from '@angular/router';
export declare class NotificationsComponent implements OnInit {
    private router;
    notificPanel: any;
    notifications: {
        message: string;
        icon: string;
        time: string;
        route: string;
        color: string;
    }[];
    constructor(router: Router);
    ngOnInit(): void;
    clearAll(e: any): void;
}
