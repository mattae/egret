import { AfterViewInit, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LayoutService } from '../../../services/layout.service';
import { ThemeService } from '../../../services/theme.service';
export declare class AdminLayoutComponent implements OnInit, AfterViewInit {
    private router;
    translate: TranslateService;
    themeService: ThemeService;
    private layout;
    isModuleLoading: Boolean;
    private moduleLoaderSub;
    private layoutConfSub;
    private routerEventSub;
    scrollConfig: {};
    layoutConf: any;
    constructor(router: Router, translate: TranslateService, themeService: ThemeService, layout: LayoutService);
    ngOnInit(): void;
    onResize(event: any): void;
    ngAfterViewInit(): void;
    scrollToTop(selector: string): void;
    ngOnDestroy(): void;
    closeSidebar(): void;
    sidebarMouseenter(e: any): void;
    sidebarMouseleave(e: any): void;
}
