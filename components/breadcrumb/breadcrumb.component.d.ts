import { OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from "rxjs";
import { RoutePartsService } from '../../services/route-parts.service';
import { LayoutService } from '../../services/layout.service';
export declare class BreadcrumbComponent implements OnInit, OnDestroy {
    private router;
    private routePartsService;
    private activeRoute;
    layout: LayoutService;
    routeParts: any[];
    routerEventSub: Subscription;
    constructor(router: Router, routePartsService: RoutePartsService, activeRoute: ActivatedRoute, layout: LayoutService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    parseText(part: any): any;
}
