import { OnInit, Renderer2 } from '@angular/core';
import { ThemeService } from '../../services/theme.service';
import { LayoutService } from '../../services/layout.service';
import { TranslateService } from '@ngx-translate/core';
export declare class HeaderSideComponent implements OnInit {
    private themeService;
    private layout;
    translate: TranslateService;
    private renderer;
    notificPanel: any;
    availableLangs: {
        name: string;
        code: string;
        flag: string;
    }[];
    currentLang: {
        name: string;
        code: string;
        flag: string;
    };
    openCustomizer: boolean;
    egretThemes: any;
    layoutConf: any;
    constructor(themeService: ThemeService, layout: LayoutService, translate: TranslateService, renderer: Renderer2);
    ngOnInit(): void;
    setLang(lng: any): void;
    changeTheme(theme: any): void;
    toggleNotific(): void;
    toggleSidenav(): void;
    toggleCollapse(): void;
}
