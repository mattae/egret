import { OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { LayoutService } from '../../services/layout.service';
import { NavigationService } from '../../services/navigation.service';
import { ThemeService } from '../../services/theme.service';
import { AccountService, IMenuItem, LoginService } from '@lamis/web-core';
export declare class HeaderTopComponent implements OnInit, OnDestroy {
    private layout;
    private navService;
    themeService: ThemeService;
    translate: TranslateService;
    private renderer;
    private loginService;
    private accountService;
    layoutConf: any;
    menuItems: IMenuItem[];
    menuItemSub: Subscription;
    egretThemes: any[];
    openCustomizer: boolean;
    currentLang: string;
    availableLangs: {
        name: string;
        code: string;
    }[];
    notificPanel: any;
    constructor(layout: LayoutService, navService: NavigationService, themeService: ThemeService, translate: TranslateService, renderer: Renderer2, loginService: LoginService, accountService: AccountService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    setLang(): void;
    changeTheme(theme: any): void;
    toggleNotific(): void;
    toggleSidenav(): void;
    logout(): void;
}
