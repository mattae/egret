import { AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import { NavigationService } from '../../services/navigation.service';
export declare class SidebarTopComponent implements OnInit, OnDestroy, AfterViewInit {
    private navService;
    menuItems: any[];
    private menuItemsSub;
    constructor(navService: NavigationService);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    ngOnDestroy(): void;
}
