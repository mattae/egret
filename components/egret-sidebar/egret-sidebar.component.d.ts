import { ChangeDetectorRef, ElementRef, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { EgretSidebarHelperService } from './egret-sidebar-helper.service';
import { MatchMediaService } from '../../services/match-media.service';
export declare class EgretSidebarComponent implements OnInit, OnDestroy {
    private matchMediaService;
    private mediaObserver;
    private sidebarHelperService;
    private _renderer;
    private _elementRef;
    private cdr;
    name: string;
    right: boolean;
    opened: boolean;
    sidebarLockedOpen: boolean;
    isOver: boolean;
    private backdrop;
    private lockedBreakpoint;
    private unsubscribeAll;
    constructor(matchMediaService: MatchMediaService, mediaObserver: MediaObserver, sidebarHelperService: EgretSidebarHelperService, _renderer: Renderer2, _elementRef: ElementRef, cdr: ChangeDetectorRef);
    ngOnInit(): void;
    open(): void;
    close(): void;
    toggle(): void;
    showBackdrop(): void;
    hideBackdrop(): void;
    ngOnDestroy(): void;
}
export declare class EgretSidebarTogglerDirective {
    private egretSidebarHelperService;
    id: any;
    constructor(egretSidebarHelperService: EgretSidebarHelperService);
    onClick(): void;
}
