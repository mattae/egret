import { EgretSidebarComponent } from "./egret-sidebar.component";
export declare class EgretSidebarHelperService {
    sidebarList: EgretSidebarComponent[];
    constructor();
    setSidebar(name: any, sidebar: any): void;
    getSidebar(name: any): any;
    removeSidebar(name: any): void;
}
