import { EventEmitter, OnInit, Renderer2 } from '@angular/core';
import { NavigationService } from '../../services/navigation.service';
import { LayoutService } from '../../services/layout.service';
import { CustomizerService } from '../../services/customizer.service';
import { ITheme, ThemeService } from '../../services/theme.service';
export declare class CustomizerComponent implements OnInit {
    private navService;
    private layout;
    private themeService;
    customizer: CustomizerService;
    private renderer;
    isCustomizerOpen: boolean;
    customizerClosed: EventEmitter<any>;
    sidenavTypes: {
        name: string;
        value: string;
    }[];
    sidebarColors: any[];
    topbarColors: any[];
    layoutConf: any;
    selectedMenu: string;
    selectedLayout: string;
    isTopbarFixed: boolean;
    isRTL: boolean;
    egretThemes: ITheme[];
    perfectScrollbarEnabled: boolean;
    constructor(navService: NavigationService, layout: LayoutService, themeService: ThemeService, customizer: CustomizerService, renderer: Renderer2);
    ngOnInit(): void;
    changeTheme(theme: any): void;
    changeLayoutStyle(data: any): void;
    changeSidenav(data: any): void;
    toggleBreadcrumb(data: any): void;
    sidebarCompactToggle(data: any): void;
    toggleTopbarFixed(data: any): void;
    toggleDir(data: any): void;
    tooglePerfectScrollbar(data: any): void;
    close(): void;
}
