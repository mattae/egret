import { MenuService } from '@lamis/web-core';
export declare class NavigationService {
    private menuService;
    iconTypeMenuTitle: string;
    menuItems$: any;
    constructor(menuService: MenuService);
    publishNavigationChange(menuType: string): void;
}
