import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs';
interface confirmData {
    title?: string;
    message?: string;
}
export declare class AppConfirmService {
    private dialog;
    constructor(dialog: MatDialog);
    confirm(data?: confirmData): Observable<boolean>;
}
export {};
