import { Router } from "@angular/router";
import { LayoutService } from "./layout.service";
export declare class CustomizerService {
    private router;
    private layout;
    colors: {
        class: string;
        active: boolean;
    }[];
    selectedSidebarColor: any;
    topbarColors: any[];
    sidebarColors: any[];
    constructor(router: Router, layout: LayoutService);
    getSidebarColors(): {
        class: string;
        active: boolean;
    }[];
    getTopbarColors(): {
        class: string;
        active: boolean;
    }[];
    changeSidebarColor(color: any): void;
    changeTopbarColor(color: any): void;
    removeClass(el: any, className: any): void;
    addClass(el: any, className: any): void;
    findClosest(el: any, className: any): any;
    hasClass(el: any, className: any): boolean;
    toggleClass(el: any, className: any): void;
}
