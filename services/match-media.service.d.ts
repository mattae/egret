import { MediaObserver } from '@angular/flex-layout';
import { BehaviorSubject } from 'rxjs';
export declare class MatchMediaService {
    private media;
    activeMediaQuery: string;
    onMediaChange: BehaviorSubject<string>;
    constructor(media: MediaObserver);
    private init;
}
