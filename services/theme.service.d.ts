import { Renderer2 } from '@angular/core';
export interface ITheme {
    name: string;
    baseColor?: string;
    isActive?: boolean;
}
export declare class ThemeService {
    private document;
    egretThemes: ITheme[];
    activatedTheme: ITheme;
    private renderer;
    constructor(document: any);
    applyMatTheme(r: Renderer2, themeName: string): void;
    changeTheme(prevTheme: any, themeName: string): void;
    flipActiveFlag(themeName: string): void;
    setThemeFromQuery(): void;
}
