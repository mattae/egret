import { Renderer2 } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ThemeService } from './theme.service';
import { HttpClient } from '@angular/common/http';
import { AccountService, ServerApiUrlConfig } from '@lamis/web-core';
export interface ILayoutConf {
    navigationPos?: string;
    sidebarStyle?: string;
    sidebarCompactToggle?: boolean;
    sidebarColor?: string;
    dir?: string;
    isMobile?: boolean;
    useBreadcrumb?: boolean;
    breadcrumb?: string;
    topbarFixed?: boolean;
    topbarColor?: string;
    matTheme?: string;
    perfectScrollbar?: boolean;
}
export interface ILayoutChangeOptions {
    duration?: number;
    transitionClass?: boolean;
}
interface IAdjustScreenOptions {
    browserEvent?: any;
    route?: string;
}
export declare class LayoutService {
    private themeService;
    private http;
    private apiUrlConfig;
    private accountService;
    layoutConf: ILayoutConf;
    layoutConfSubject: BehaviorSubject<ILayoutConf>;
    layoutConf$: import("rxjs").Observable<ILayoutConf>;
    isMobile: boolean;
    currentRoute: string;
    private username;
    fullWidthRoutes: string[];
    constructor(themeService: ThemeService, http: HttpClient, apiUrlConfig: ServerApiUrlConfig, accountService: AccountService);
    setAppLayout(): void;
    publishLayoutChange(lc: ILayoutConf, opt?: ILayoutChangeOptions): void;
    applyMatTheme(r: Renderer2): void;
    adjustLayout(options?: IAdjustScreenOptions): void;
    isSm(): boolean;
    private getLayoutConfig;
    private saveLayoutConfig;
}
export {};
