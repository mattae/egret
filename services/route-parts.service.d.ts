import { ActivatedRouteSnapshot, Params, Router } from "@angular/router";
interface IRoutePart {
    title: string;
    breadcrumb: string;
    params?: Params;
    url: string;
    urlSegments: any[];
}
export declare class RoutePartsService {
    private router;
    routeParts: IRoutePart[];
    constructor(router: Router);
    ngOnInit(): void;
    generateRouteParts(snapshot: ActivatedRouteSnapshot): IRoutePart[];
}
export {};
